/*
 * File name: map.cc
 * Date:      2013/10/07 09:40
 * Author:    Miroslav Kulich
 */

#include <sstream>
#include <fstream>
#include <iostream>
#include <cmath>

#include "imr-h/logging.h"

#include "map_grid.h"

//using imr::logger;
using namespace imr;

const double CMapGrid::FLOOR = 0;
const double CMapGrid::UNKNOWN = 0.5;
const double CMapGrid::WALL = 1;
const double CMapGrid::EMPTY = 0;
const double CMapGrid::OCCUPIED = 1;


imr::CConfig& CMapGrid::getConfig(imr::CConfig& config)
{
    config.add<int>("grid-width", "grid width (nbr cells)", 150);
    config.add<int>("grid-height", "grid height (nbr cells)", 150);
    config.add<double>("grid-cell-size", "Size of the squared cell in meters", 0.1);
    config.add<std::string>("file", "map description file name", "maps/autolab.txt");

    return config;
}

CMapGrid::CMapGrid(imr::CConfig& cfg):
    cfg(cfg),
    height(cfg.get<int>("grid-height")),
    width(cfg.get<int>("grid-width")),
    cellSize(cfg.get<double>("grid-cell-size"))
{
    data = new double[height*width];

#ifdef _EXPLORATION
    for(int i = 0; i < height*width; i++) {
        data[i] = UNKNOWN;
    }
#endif
};

CMapGrid::CMapGrid(imr::CConfig& cfg, std::string name): cfg(cfg)
//CMapGrid::CMapGrid(imr::CConfig& cfg, std::string name): cfg(cfg), cellSize(cfg.get<double>("grid-cell-size"))
{
    std::ifstream infile (name.c_str(), std::ios_base::in);
    std::string line;
    int i = 0;
    int ii = 0;
    INFO("LOADING MAP " << name);
    while (getline(infile, line, '\n')){
        i++;

        if (i==1) {
            std::stringstream strStream(line);
            strStream >> height;
            INFO("MAP HEIGHT " << height);
            continue;
        }

        if (i==2) {
            width = line.size();
            INFO("MAP WIDTH " << width);
            data = new double[height*width];
        }

        for(unsigned int k=0;k<line.size();k++) {
            data[ii++] = line[k] == '.' ? (double) FLOOR : (double) WALL;
        }
    }

    INFO("Map loaded. Width " << width << " height " << height);
}

// CUSTOM added
CMapGrid::CMapGrid(imr::CConfig &cfg, int height, int width, double cellSize) :
        cfg(cfg),
        height(height),
        width(width),
        cellSize(cellSize) {
    data = new double[height * width];

#ifdef _EXPLORATION
    for(int i = 0; i < height*width; i++) {
        data[i] = UNKNOWN;
    }
#endif
};

CMapGrid::~CMapGrid()
{
    delete data;
}


double CMapGrid::getCell(int x, int y)
{
    assert_argument(x >= 0 && x < width, "getCell: X out of range");
    assert_argument(y >= 0 && y < height, "getCell: Y out of range");
    return data[y*width + x];
};

void CMapGrid::setCell(int x, int y, double value)
{
    assert_argument(x >= 0 && x < width, "setCell: X out of range");
    assert_argument(y >= 0 && y < height, "setCell: Y out of range");
    data[y*width + x] = value;
};

void CMapGrid::updateCell(int x, int y, double value)
{
    setCell(x,y,value);
};

void CMapGrid::SWAP(int &x, int &y)
{
    int p;
    p = x;
    x = y;
    y = p;
}

void CMapGrid::updateCells(int r_x, int r_y, int c_x, int c_y, double dist)
{
    //cout << "upt cells" << endl;
    double p, d;
    int dx = c_x - r_x;
    int dy = c_y - r_y;
    int steep = (std::abs(dy) >= std::abs(dx));

	if (steep) {
		SWAP(r_x, r_y);
		SWAP(c_x, c_y);
		// recompute Dx, Dy after swap
		dx = c_x - r_x;
		dy = c_y - r_y;
	}
	int xstep = 1;
	if (dx < 0) {
		xstep = -1;
		dx = -dx;
	}
	int ystep = 1;
	if (dy < 0) {
		ystep = -1;
		dy = -dy;
	}
	int twoDy = 2 * dy;
	int twoDyTwoDx = twoDy - 2 * dx; // 2*Dy - 2*Dx
	int e = twoDy - dx; //2*Dy - Dx
	int y = r_y;
	int xDraw, yDraw;
	for (int x = r_x; x != c_x; x += xstep) {
		if (steep) {
			xDraw = y;
			yDraw = x;
		} else {
			xDraw = x;
			yDraw = y;
		}
		// do stuff with xDraw yDraw like setCell...
		if(steep) {
			d = (r_y - xDraw)*(r_y - xDraw)+(r_x - yDraw)*(r_x - yDraw);
		} else {
			d = (r_x - xDraw)*(r_x - xDraw)+(r_y - yDraw)*(r_y - yDraw);
		}
		d = sqrt(d)/50.0;    // distance of crrently updated cell from robot, converted to [m] (cell is 2x2cm)
		p = cellProb(dist, d, getCell(xDraw, yDraw));
		setCell(xDraw, yDraw, p);
		// next
		if (e > 0) {
			e += twoDyTwoDx; //E += 2*Dy - 2*Dx;
			y = y + ystep;
		} else {
			e += twoDy; //E += 2*Dy;
		}
	}

    /* Laser measurement is in robot navigation cut to at most 1m to make exploration
        more precise. Therefore if the distance is less than 1, it is obvious there is
        a wall */
    if(dist < 1.0){
        if(steep) {
            //p = cellProb(dist, d, getCell(c_y, c_x));
            setCell(c_y, c_x, WALL - 0.00001);
        } else {
            //p = cellProb(dist, d, getCell(c_x, c_y));
            setCell(c_x, c_y, WALL - 0.00001);
        }
    }
}

double CMapGrid::cellProb(double r, double d, double Po)
{
    // see PAR lecture 4 for further details
    double Pf = 1.0-Po;
    double p;
    double smf = sensModelF(r, d);
    double smo = sensModelO(r, d);
    double po = (1.0 + smo - smf)/2.0;
    //double pf = (1.0 + smf - smo)/2.0;
    double pf = 1.0 - po;

    p = (po*Po)/(po*Po+pf*Pf);    // equation for P(s(a)=o)

    return p;
}

double CMapGrid::sensModelF(double r, double d)
{
	double S;
	double eps, tmp;

    // eps - sensor precision
	eps = r < 1 ? 0.01 : r * 0.01;     // epsilon - 1cm if range is less than 1m, else 1% from measurement

	if(d >= 0.0 && d <= (r - eps)) {
		tmp = d / (r - eps);
		S = 1.0 - (tmp * tmp);
	} else {
		S = 0.0;
	}

	return S;
}

double CMapGrid::sensModelO(double r, double d)
{
	double S;
	double eps, tmp;

	eps = r < 1.0 ? 0.01 : r * 0.01;

	if(d >= (r - eps) && d <= (r + eps)) {
		tmp = (d - r) / eps;
		S = 1.0 - (tmp * tmp);
	} else if(d > (r + eps)) {
		S = 1.0;
	} else {
		S = 0.0;
	}

	return S;
}

int CMapGrid::getHeight()
{
  return height;
};

int CMapGrid::getWidth()
{
  return width;
};


int CMapGrid::getX(double x)
{
    int xx = x / cellSize;
    assert_argument(xx >= 0 && xx < width, "X out of range");
    return xx;
}

int CMapGrid::getY(double y)
{
    //int yy = y / cellSize;
    int yy = (height - 1 - ((int) (y / cellSize)));
    //DEBUG("CMapGrid::getY: YPos = " << y << ", cS = " << cellSize << ", Y = " << yy);
    assert_argument(yy >= 0 && yy < height, "Y out of range!");
    return yy;
}

double CMapGrid::getPosX(int x)
{
    return (double) x * cellSize;
}

double CMapGrid::getPosY(int y)
{
    //return (double) y * cellSize;
    return (double) (height - 1 - y) * cellSize;
}
