
define hokuyolaser laser
(
  # laser-specific properties
  # factory settings for LMS200	
  #range_min 0.0
  range_max 5.0
  fov 270.0
  samples 682

  # generic model properties
  color "blue"
  size [ 0.05 0.05 0.07 ]
  alwayson 1
)



