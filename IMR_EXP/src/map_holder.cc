//
// Created by Dagy Tran on 07.02.17.
//

#include "map_holder.h"

using namespace imr::robot;

MapHolder::MapHolder(imr::CMapGrid &gridMap):window(CCMorse::WindowProxy::GetInstance()) {
// Clearing gridMap
	for (int clearx = 0; clearx < gridMap.getWidth(); clearx++) {
		for (int cleary = 0; cleary < gridMap.getHeight(); cleary++) {
			gridMap.updateCell(clearx, cleary, 0.5);
		}
	}
	positionMap = new CMapGrid(gridMap, true);
	laserMap = new CMapGrid(gridMap, false);
	targetMap = new CMapGrid(gridMap, true);
	dilatatedMap = new CMapGrid(gridMap, false);
	pathMap = new CMapGrid(gridMap, true);
	frontierMap = new CMapGrid(gridMap, true);

/// MAP LAYERS
	CCMorse::WindowLayer l;
	l.mName = "position_map";
	l.mVisible = true;
	l.mPriority = -1;
	l.mAlpha = 255;
	l.mTransparent = CMapGrid::FLOOR;
	l.mColor = CCMorse::RGBA_t(204, 0, 0);
	l.mMap = positionMap;
	window.AddLayer(l);

	l.mName = "laser_map";
	l.mVisible = true;
	l.mPriority = -40;
	l.mAlpha = 152;
	l.mTransparent = CMapGrid::FLOOR;
	l.mColor = CCMorse::RGBA_t(0, 200, 0);
	l.mMap = laserMap;
	window.AddLayer(l);

	l.mName = "target_map";
	l.mVisible = true;
	l.mPriority = -11;
	l.mAlpha = 255;
	l.mTransparent = CMapGrid::FLOOR;
	l.mColor = CCMorse::RGBA_t(255, 255, 0);
	l.mMap = targetMap;
	window.AddLayer(l);

	l.mName = "dilatated_map";
	l.mVisible = true;
	l.mPriority = -12;
	l.mAlpha = 255;
	l.mTransparent = CMapGrid::FLOOR;
	l.mColor = CCMorse::RGBA_t(0, 0, 255);
	l.mMap = dilatatedMap;
	window.AddLayer(l);

	l.mName = "path_map";
	l.mVisible = true;
	l.mPriority = -2;
	l.mAlpha = 255;
	l.mTransparent = CMapGrid::FLOOR;
	l.mColor = CCMorse::RGBA_t(255, 255, 0);
	l.mMap = pathMap;
	window.AddLayer(l);

	l.mName = "frontier_map";
	l.mVisible = true;
	l.mPriority = -6;
	l.mAlpha = 125;
	l.mTransparent = CMapGrid::FLOOR;
	l.mColor = CCMorse::RGBA_t(255, 255, 0);
	l.mMap = frontierMap;
	window.AddLayer(l);

	window.Flush();
}

void MapHolder::flush() {
	window.Flush();
}
void MapHolder::drawMap() {
	DEBUG("MAPHOLDER: DrawMap Started");

//	window.DrawMap(*positionMap);
	window.DrawMap(*laserMap);
//	window.DrawMap(*targetMap);
//	window.DrawMap(*dilatatedMap);
//	window.DrawMap(*pathMap);
	window.DrawMap(*frontierMap);
	DEBUG("MAPHOLDER: DrawMap Finished");
}

void MapHolder::drawDilatatedMap() {
	window.DrawMap(*dilatatedMap);
}

imr::CMapGrid *MapHolder::getPositionMap() {
	return positionMap;
}

imr::CMapGrid *MapHolder::getLaserMap() {
	return laserMap;
}

imr::CMapGrid *MapHolder::getTargetMap() {
	return targetMap;
}

imr::CMapGrid *MapHolder::getDilatatedMap() {
	return dilatatedMap;
}

imr::CMapGrid *MapHolder::getPathMap() {
	return pathMap;
}

imr::CMapGrid *MapHolder::getFrontierMap() {
	return frontierMap;
}

void MapHolder::updateCellPositionMap(int x, int y, double value) {
	positionMap->updateCell(x, y, value);
}

void MapHolder::updateCellLaserMap(int x, int y, double value) {
	laserMap->updateCell(x, y, value);
}

void MapHolder::updateCellTargetMap(int x, int y, double value) {
	targetMap->updateCell(x, y, value);
}

void MapHolder::updateCellDilatatedMap(int x, int y, double value) {
	dilatatedMap->updateCell(x, y, value);
}

void MapHolder::updateCellPathMap(int x, int y, double value) {
	pathMap->updateCell(x, y, value);
}

void MapHolder::updateCellFrontierMap(int x, int y, double value) {
	frontierMap->updateCell(x, y, value);
}

void MapHolder::cleanDilatatedMap() {
	for (int i = 0; i < dilatatedMap->getWidth(); ++i) {
		for (int j = 0; j < dilatatedMap->getHeight(); ++j) {
			dilatatedMap->updateCell(i, j, 0);
		}
	}
}