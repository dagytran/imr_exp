=============================================
Úloha: IMR_EXP
Manuál pro MORSE Client pro SyRoTek (rev. 105)
=============================================

Popis struktury adresáře:
-------------------------------------------
	
   ./ 						... Aktuální složka - projekt IMR_INTRO se simulací SyRoTeku v Morse.

   ./cfg/morse.json			... Konfigurační soubor simulace a klienta. Popsaný níže.

   ./src/ 					... Složka se zdrojovými kódy klienta.
   ./src/main.cc			... Hlavní soubor klienta.
   ./src/robot.cc 			... Soubor, kde je definován program robota.
   ./src/libCCMorse/ 		... Složka s hlavičkovými soubory knihovny.

   ./lib/libCCMorse.so		... Knihovnou klienta pro komunikaci s Morse v C++.

   .default.py 				... Builder scritpt (soubor, ve kterém je definován svět simulace).
   ./src/MorseSyrotek/ 		... Složka se zdrojovými kódy pro Morse simulator.
   ./data/MorseSyrotek/		... Složka s daty a 3D modely které jsou použity v simulaci.



Jak spustit simulaci v MORSE?
-------------------------------------------

	# Před prvním spuštěním:

		1) Překopírujeme zdrojové soubory úlohy do požadované složky.
	
		2) Přidáme simulační prostředí do Morse.
			Ve složce projektu, zadáme příkaz:	
			
			"$ morse import ./"


	# Simulaci spouštíme z libovolné složky příkazem:

			"$ morse run IMR_EXP"
			

		POZNÁMKA: Doporučuji použít přepínač "-g" pro určení velikosti okna simulace:

			"$ morse run -g [šířka okna]x[výška okna]+[poloha okna x],[poloha okna y] [název simulace]"

			například: "morse run -g 683x480+0,768 IMR_EXP"



Kompilace programu exploračního programu
-------------------------------------------

	Program zkompilujeme pomocí příkazu:
   
			"$ make"



Spuštění simulace
-------------------------------------------

	1) V jednom terminálu spustíme simulaci v MORSE pomocí výše uvedeného příkazu:

			"$ morse run IMR_EXP"

	2) Ve druhém terminálu spustíme klienta ze složky projektu pomocí příkazu:

			"$ ./client"

	3) Simulujeme!



Konfigurace simulačního prostředí a klienta
-------------------------------------------

	Konfigurační soubor je ve formátu JSON (více na "http://www.json.org/json-cz.html"
	nebo "http://www.w3schools.com/json/default.asp").

	Jakákoliv položka z konfigurace robota nebo	řídícího algoritmu může chybět a
	bude automaticky doplněna výchozí hodnotou. Pkud konfigurační soubot bude
	prázdný nebo ho simulace nenajde, spustí se s jedním plně výchozím robotem,
	viz minimalistická konfigurace robota.
	
	POZOR: Automaticky doplněná hodnota konfigurace nezajišťuje správnou funkčnost!!!
		Zajišťuje pouze to, že simulace půjde spustit.

	POZOR: Aby konfigurační soubor fungovat správně, je nutné dodržet formátování JSONu!!!

	POZOR: Jména jednotlivých položek konfigurace nelze měnit!!!
		Jiná jména položek, než která jsou zde uvedena, budou simulací ignorována.
		
	POZOR: Komentovat přímo kód JSONu nelze!!!
		Proto je komentáře nutné odstranit při případném kopírování.

	# Následuje okomentovaná ukázka konfigurace se všemi dostupnými konfiguracemi a
	jejich výchozími hodnotami:
	
{								// Simulace má:
	"Robots": [						// Pole robotů "Robots".
		{								// Robot má:
			"name": "robot0", 				// Jméno robota. Může být libovolný string začínající písmenem.
			"color": "dark_blue", 			// Barva modelu robota. Může nabývat hodnot: ["blue", "yellow", "red", "dark_blue"]
			"laser-remission": false,		// Úroveň snímání laseru. Když je 'true' laser vrací: body, vzdálenosti a odrazivosti povrchu. Když je 'false' vrací pouze: body a vzdálenosti.
			"laser-arc": false,				// Zda má být vykreslováno zorné pole laseru.
			"default-frequency": 10,		// Výchozí frekvenci. Pokud není přístroj nastaven jinak má tuto frekvenci. [Hz]
			"laser-frequency": 10,			// Frekvenci snímání laseru. [Hz]
			"pose-frequency": 10,			// Frekvenci snímání globální polohy. [Hz]
			"odometry-frequency": 10,		// Frekvenci snímání odometrické polohy. [Hz]
			"pen-frequency": 10,			// Frekvenci překreslování kreslícího plátna. [Hz]
			"waypoint-frequency": 10,		// Frekvenci obnovování waypoint driveru. [Hz]
			"motionvw-frequency": 10,		// Frekvenci obnovování nastavení rychlosti motorů. [Hz]
			"init-position": [0.41, 3.58],	// Pole koordinátů (x[m], y[m]), které určují, počáteční pozici robota.
			"init-rotation": 4.712391,		// Počáteční úhel natočení robota. [rad]
			"Provides": {					// Proxy, které budou v simulaci poskytována tímto robotem.
				"position2d": {					// Position2dProxy,  může mít dva druhy proxy:
					"global": [0],					// Pole indexů "global", určuje proxy, které budou mít k dispozici globální (přesnou) polohu.
					"local": [] 					// Pole indexů "local", určuje proxy, které budou určovat polohu z odometrie.
				},								// Konec určení indexů pro Position2dProxy.
				"laser": [0]					// Pole LaserProxy, které budou v simulaci k dispozici pro tohoto robota.
			},								// Konec nastavení proxy, které poskytuje robot.										
			"Driver": {						// Definici řídícího algoritmu, který má:
				"name": "snd", 					// Jméno řídícího algoritmu. Může nabývat hodnot: ["snd", "waypoint"]
				"robot_radius": 0.25, 			// Poloměr nejmenšího kruhu, který obklopuje robota. [m]
				"min_gap_width": 0.5, 			// Šířka nejmenší cesty, kterou řídící algoritmus může zkusit projet. [m]
				"obstacle_avoid_dist": 1.0,		// Největší vzdálenost od překážky. [m]
				"max_speed": 0.5, 				// Nejvýšší dovolená dopředná rychlost robota. [m/s]
				"max_turn_rate": 1.0, 			// Nejvyšší dovolená rychlost otáčení robota. [rad/s]
				"goal_pos_tol": 0.125,			// Tolerance cílové pozice robota. [m]
				"goal_angl_tol": 0.5335,		// Tolerance cílového natočení robota. [m]
				"Provides": {					// Proxy, která budou v simulaci poskytována tímto řídícím algoritmem.
					"position2d": [9],				// Pole indexů Position2dProxy.
					"laser": []						// Pole indexů LaserProxy.
				},								// Konec nastavení proxy, které poskytuje řídící algoritmus.
				"Requires":{					// Proxy, která POŽADUJE řídící algoritmus.
					"position2d": {					// Nastavení Position2dProxy pro řídící algoritmus má dva parametry:
						"input": [0],					// Pole indexů "input", určuje proxy, ze kterých bude řídící algoritmus čerpat data.
						"output": [0]					// Pole indexů "output", určuje proxy, do kterých bude řídící algoritmus zapisovat data.
					},								// Konec nastavení Position2dProxy.
					"laser": {						// Nastavení LaserProxy pro řídící algoritmus  má dva parametry:
						"input": [0],					// Pole indexů "input", určuje proxy, ze kterých bude řídící algoritmus čerpat data.
						"output": []					// Pole indexů "output", určuje proxy, do kterých bude řídící algoritmus zapisovat data.
					}								// Konec nastavení LaserProxy.
				},								// Konec nastavení proxy, které požaduje řídící algoritmus.
			}								// Konec objektu řídícího algoritmu.
		},								// Konec objektu robota a naznačení konfigurace dalšího robota.
		{...}							// Naznačení posledního robota v simulaci.
	],								// Konec pole robotů "Robots".
	
	"MapWindow": {					// Nastavení okna pro kreslení mapy má:
		"enabled": false,				// Pole zapínající okno mapy v simulaci když je 'true'.
		"fullscreen": false,			// Pole zapínající režim celoobrazovkového zobrazení mapy.
		"width": 130,					// Šířka okna mapy v pixelech. POZNÁMKA: Může být přepsána hodnotou ze souboru "client.cfg", pokud existuje.
		"height": 130,					// Výška okna mapy v pixelech. POZNÁMKA: Může být přepsána hodnotou ze souboru "client.cfg", pokud existuje.
		"ground-canvas-enabled": false,	// Pole povolující režim "mapa na zemi", pokud je "true", lze pomocí kláves "LCTRL+DOWN_ARROW" mapu projektovat na zem a pomocí kláves "LCTRL+UP_ARROW" mapu obnovit do klasického režimu.
		"init-position-on-ground": true,// Pole určující počáteční polohu okna mapy, pokud je "true", bude mapa zobrazena na začítku simulace na zemi.
		"transparency-alpha": 1.0		// Hodnota určující viditelnost okna mapy, pokud není v režimu fullscreen. (0.0 => plně průhledná - neviditelná, 1.0 => neprůhledná)
	},								// Konec nastavení okna pro kreslení mapy.
	
	"Simulation": {							// Nastavení simualčního prostředí má:
		"hide-obstacles-enabled": false,		// Pokud je "true", povolí skrývat části arény pomocí kláves "LCTRL+RIGHT_ARROW" a zase je zobrazovat pomocí kláves "LCTRL+LEFT_ARROW". Překážky stále existují jen nejsou vidět.
		"debug-mode": false,					// Odlaďovací mód, pokud je 'true', robota lze ovládat klávesnicí.
		"show-physics": false,					// Zobrazení kolizních mřížek objektů v simulaci, když je 'true'.
		"show-framerate": false,				// Zobrazení tabulky s časy výpočtů snímků simulace, když je 'true'.
		"fastmode": false,						// Zapne rychlý mód simulace - černobílá a vysoký kontrast, když je 'true'.
		"camera-location": [-1.0, 3.0, 1.85],	// Nastavení počáteční pozice kamery v simulaci.
		"camera-rotation": [0.85, 0, -2.0]		// Nastavení počáteční rotace kamery v simualci.
	}										// Konec nastavení simulačního prostředí.
	
}								// Konec konfiguračního souboru.


	# Následuje okomentovaná minimalistická možná konfigurace:
	
{					// Simulace má:
	"Robots": [			// Pole robotů "Robots" obsahující:
		{}					// Plně výchozí konfiguraci robota.
	]					// Konec pole robotů.
}					// Konec konfiguračního souboru.

