/*
 * File name: planner.cc
 * Date:      2011/09/03
 * Author:    Miroslav Kulich
 */


#include "plan.h"

using namespace imr;

CPlan& CPlan::operator=(const CPlan & src) {
	if (&src != this) {
		path.clear();
		path = src.path;
	}
	return * this;
}

CPlan::CPlan(const CPlan & src) {
	path = src.path;
}

CPlan::CPlan() {
}

void CPlan::setPath(std::vector<int> newPath) {
	concurrent::ScopedLock lk(mtx);
	path = newPath;
}

int CPlan::getNextPoint() {
	concurrent::ScopedLock lk(mtx);
	if (!path.empty()) {
		return path[0];
	} else {
		return -1;
	}
}


