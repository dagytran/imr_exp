#ifndef __PLAN_H__
#define __PLAN_H__
#include "imr-h/thread.h"

#include <vector>

namespace imr {
  class CPlan {
  private:
      std::vector<int> path;          // map over which planner makes the plans
      concurrent::Mutex mtx;                // mutex for quit flag

  public:
      CPlan();
      CPlan(const CPlan &);
      CPlan& operator=(const CPlan &);
      void setPath(std::vector<int> newPath);

      int getNextPoint();

  };

}
#endif