/*
 * File name: planner.h
 * Date:      2011/09/03
 * Author:    Miroslav Kulich
 */

#ifndef IMR_PLANNER
#define IMR_PLANNER

#include <string>

#include "imr-h/imr_config.h"
#include "map_grid.h"
#include "libCCMorse/WindowProxy.h"
#include "map_holder.h"

/**
    Planner class is essential class for users. Here the Dijkstra's algorithm
    shall be implemented with all the necessary features to achieve final path
    plan. By default it does not contain many methods, because is shall be done
    by user.
*/

namespace imr {
	class CPlanner {
	private:
		imr::CConfig &cfg;      // configuration of planner (contains starting and goal position)
		CMapGrid *map;          // gridMap over which planner makes the plans
		CMapGrid *dilatatedMapGrid;
		robot::MapHolder* mapHolder;

	public:
		/**
			Contructor. Initializes the internal configuration cfg with values
			from cfg given as parameter.
		*/
		CPlanner(imr::CConfig &cfg);

		/**
			Get default configuration of the planner.

			@param config   Configuration to be modified
			@return Modified configurations
		*/
		static imr::CConfig &getConfig(imr::CConfig &config);

		/**
			Map setter. Assign new map to private map.
		*/
		void setMap(CMapGrid &map);

		/**
			Method to be done by users. This method makes the plan with starting
			and goal position stored in the configuration cfg over the map.
			It should contain calls for map inflation, Dijkstra's algorithm and
			path smoothing.

			Implementation of this method and structure of the final plan is up to
			user himself. Hint: generaly good solution is to create a new class
			representing the plan. This class can contain many useful functions like
			get goal position, get next position etc.

			@return true if path was found, false otherwise (goal position is not
					reachable)
		*/
		bool plan();

		std::vector<int> getPath(int startX, int startY, std::vector<std::vector<bool> > frontierMap);

//    void nafoukniMapu(CCMorse::WindowProxy &window, CMapGrid &newMap);

		void nafoukniMapu(CMapGrid &newMap);


//	  std::vector<int> pathFinder(int startX, int startY, int goalX, int goalY, std::vector<int> previous, CCMorse::WindowProxy &window, CMapGrid &newMap);
		std::vector<int>
		pathFinder(int startX, int startY, int goalX, int goalY, std::vector<int> previous, CMapGrid &newMap);

		/**
		  Bresenham algorithm for drawing a straight line. Output of the method is
		  not defined (it does not draw anywhere, just generates [X,Y] points of
		  the line). There is TODO line in the lower part of the method for user to
		  implement required functionality.

		  @param x0   X coordinate of the first (starting) point
		  @param y0   Y coordinate of the first (starting) point
		  @param x1   X coordinate of the second (goal) point
		  @param y1   Y coordinate of the second (goal) point
	  */
		void bresenham(int x0, int y0, int x1, int y1);


		bool isClear(int x0, int y0, int x1, int y1, CMapGrid &map);

		void setMapHolder(robot::MapHolder &mapHolder);
	};
}
#endif
