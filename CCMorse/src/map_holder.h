//
// Created by Dagy Tran on 07.02.17.
//

#ifndef IMR_EXP_MAP_HOLDER_H
#define IMR_EXP_MAP_HOLDER_H

#include "map_grid.h"
#include "libCCMorse/MorseUtility.h"
#include "libCCMorse/WindowProxy.h"
#include "logging.h"

namespace imr {
	namespace robot {
		class MapHolder {

		private:
			CCMorse::WindowProxy &window;

		public:
			MapHolder(imr::CMapGrid &gridMap);

			imr::CMapGrid *positionMap;
			imr::CMapGrid *laserMap;
			imr::CMapGrid *targetMap;
			imr::CMapGrid *dilatatedMap;
			imr::CMapGrid *pathMap;
			imr::CMapGrid *frontierMap;

			void drawMap();
			void flush();

			void drawDilatatedMap();

			void updateCellPositionMap(int x, int y, double value);

			void updateCellLaserMap(int x, int y, double value);

			void updateCellTargetMap(int x, int y, double value);

			void updateCellDilatatedMap(int x, int y, double value);

			void updateCellPathMap(int x, int y, double value);

			void updateCellFrontierMap(int x, int y, double value);

			void cleanDilatatedMap();

			imr::CMapGrid *getPositionMap();

			imr::CMapGrid *getLaserMap();

			imr::CMapGrid *getTargetMap();

			imr::CMapGrid *getDilatatedMap();

			imr::CMapGrid *getPathMap();

			imr::CMapGrid *getFrontierMap();

		};
	}
}

#endif //IMR_EXP_MAP_HOLDER_H
