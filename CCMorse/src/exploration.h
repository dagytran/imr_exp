/*
 * File name: exploration.h
 * Date:      2009/07/08 08:42
 * Author:    Miroslav Kulich, Jan Faigl
 */

#ifndef __EXPLORATION_H__
#define __EXPLORATION_H__

#ifdef _MORSE
#include <WindowProxy.h>
#endif // _MORSE


#include "imr-h/imr_config.h"
#include "imr-h/thread.h"

#include "robot_types.h"
#include "map_grid.h"
#include "robot.h"
#include "plan.h"

/**
    Class of exploration represents another thread, which is executed
    simultaneously with robot control and data acquisition. It is
    supposed to seek frontiers, plan new path for robot in dynamicaly
    changing map and give new objectives to the robot.
*/

namespace imr {
	namespace robot {


		class CExploration : public imr::concurrent::CThread {
			typedef imr::concurrent::CThread ThreadBase;

		public:
			/**
				Get default configuration for exploration. Defines period of
				control loop.

				@param config   object of configuration to be modified
				@return configuration for exploration
			*/
			static imr::CConfig &getConfig(imr::CConfig &config);

			/**
			  Constructor fo exploration class. The class need a robot, which it
			  will control, a map to which it will write new data (explored space)
			  and basic configuration. Method assigns parameters to private variables

			  @param cfg  exploration configuration
			  @param gridMap  a clear map
			  @param robot    instance of controlled robot
			*/
			CExploration(imr::CConfig &cfg, CMapGrid &gridMap, CRobot *robot);

			~CExploration();

			/**
			  Stop the exploration. This method only locks the mutex (ScopedLock)
			  and sets quit variable to true
			*/
			void stop(void);


			void setPlan(imr::CPlan &);
			// void stopAll();    // not implemented at all

			void setMapHolder(imr::robot::MapHolder &mapHolder);

		protected:

			/**
			  Main method of this class for users. Here the controll loop is implemented.
			  User shall seek the frontiers, make new plan and pass the new plan to
			  the robot and many other useful stuff.
			*/
			void threadBody(void);

			std::vector<std::vector<bool> > getFrontierMap();

			std::vector<int> getPathQueue();

		private:
			imr::CConfig &cfg;        // configuration of the exploration instance
			CRobot *robot;            // robot controlled by the instance (allows acces to the robot to change the plan, read data etc.)
			CMapGrid &gridMap;        // gridMap of the environment, explored world
			bool quit;                // quit flag - if set to true, threadBody is stopped
			Mutex mtx;                // mutex for quit flag
			CPlan plan;                // plan
			MapHolder* mapHolder;
		};
	}
} // end namespace imr::robot

#endif

/* end of exploration.h */
