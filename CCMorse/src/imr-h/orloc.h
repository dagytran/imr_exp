/*
 * File name: orloc.cc
 * Date:      2012/5/10 10:43
 * Author:    Miroslav Kulich
 */


#ifndef _orloc_h_
#define _orloc_h_

#include <cmath>

#ifdef _MORSE
	#include "../libCCMorse/CCMorse.h"
#else
	#include "../libPlayer/Player.h"
#endif

#include "../robot_types.h"


const double       DIST_THRESHOLD   = 0.1;
const double       DIST_THRESHOLD_2 = DIST_THRESHOLD*DIST_THRESHOLD;
const unsigned int NUMBER_THRESHOLD = 20;
const double       PRECISION        = 0.05;

typedef CCMorse::Point2D_t player_point_2d_t;

typedef std::vector<player_point_2d_t> Scan;

using namespace std;
using namespace CCMorse;
using namespace imr::robot;


/// modify yaw of pos so that laser data are aligned with axes of the coordinate system
void angleCorrection(SPosition &pos, const LaserProxy &laser);

/// transform distances measured with laser into points in Cartesian space
Scan getPoints(const SPosition pos, const LaserProxy &laser);


#endif // _orloc_h_
