/*
 * File name: planner.cc
 * Date:      2011/09/03
 * Author:    Miroslav Kulich
 */


#include <cstdlib>
#include <vector>
#include "planner.h"
#include "imr-h/logging.h"
#include "libCCMorse/WindowProxy.h"
#include "dijkstra_heap.h"

using namespace imr;


imr::CConfig &CPlanner::getConfig(imr::CConfig &config) {
	config.add<int>("start_x", "x coordinate of start", 75);
	config.add<int>("start_y", "y coordinate of start", 36);
	config.add<int>("goal_x", "x coordinate of goal", 155);
	config.add<int>("goal_y", "y coordinate of goal", 169);
	return config;
}


CPlanner::CPlanner(imr::CConfig &cfg) : cfg(cfg) {
}


/**
    Non-member method. Swaps two integer values.
*/
void SWAP(int &x, int &y) {
	int p;
	p = x;
	x = y;
	y = p;
}

void CPlanner::bresenham(int x0, int y0, int x1, int y1) {
	int dx = x1 - x0;
	int dy = y1 - y0;
	int steep = (abs(dy) >= abs(dx));
	if (steep) {
		SWAP(x0, y0);
		SWAP(x1, y1);
		// recompute Dx, Dy after swap
		dx = x1 - x0;
		dy = y1 - y0;
	}
	int xstep = 1;
	if (dx < 0) {
		xstep = -1;
		dx = -dx;
	}
	int ystep = 1;
	if (dy < 0) {
		ystep = -1;
		dy = -dy;
	}
	int twoDy = 2 * dy;
	int twoDyTwoDx = twoDy - 2 * dx; // 2*Dy - 2*Dx
	int e = twoDy - dx; //2*Dy - Dx
	int y = y0;
	int xDraw, yDraw;
	for (int x = x0; x != x1; x += xstep) {
		if (steep) {
			xDraw = y;
			yDraw = x;
		} else {
			xDraw = x;
			yDraw = y;
		}

//		putPixel(xDraw,yDraw); //TODO: use your own method for drawing pixel
		// next
		if (e > 0) {
			e += twoDyTwoDx; //E += 2*Dy - 2*Dx;
			y = y + ystep;
		} else {
			e += twoDy; //E += 2*Dy;
		}
	}
}

bool CPlanner::isClear(int x0, int y0, int x1, int y1, CMapGrid &map) {
	int dx = x1 - x0;
	int dy = y1 - y0;
	int steep = (abs(dy) >= abs(dx));
	if (steep) {
		SWAP(x0, y0);
		SWAP(x1, y1);
		// recompute Dx, Dy after swap
		dx = x1 - x0;
		dy = y1 - y0;
	}
	int xstep = 1;
	if (dx < 0) {
		xstep = -1;
		dx = -dx;
	}
	int ystep = 1;
	if (dy < 0) {
		ystep = -1;
		dy = -dy;
	}
	int twoDy = 2 * dy;
	int twoDyTwoDx = twoDy - 2 * dx; // 2*Dy - 2*Dx
	int e = twoDy - dx; //2*Dy - Dx
	int y = y0;
	int xDraw, yDraw;
	for (int x = x0; x != x1; x += xstep) {
		if (steep) {
			xDraw = y;
			yDraw = x;
		} else {
			xDraw = x;
			yDraw = y;
		}

//		putPixel(xDraw,yDraw); //TODO: use your own method for drawing pixel

		if( map.getCell(xDraw, yDraw) == 1) {
			return false;
		}
		// next
		if (e > 0) {
			e += twoDyTwoDx; //E += 2*Dy - 2*Dx;
			y = y + ystep;
		} else {
			e += twoDy; //E += 2*Dy;
		}
	}
	return true;
}


void CPlanner::setMap(CMapGrid &map) {
	this->map = &map;
}

void CPlanner::setMapHolder(imr::robot::MapHolder &mapHolder) {
	this->mapHolder = &mapHolder;
}

struct SPoint {
	int x;
	int y;

	SPoint(int x, int y) :
			x(x), y(y) {
	}

};

// TODO Zbavit kreslení nafouknutí mapy
//void CPlanner::nafoukniMapu(CCMorse::WindowProxy &window, CMapGrid &newMap ) {
void CPlanner::nafoukniMapu(CMapGrid &newMap) {
	DEBUG("Planner: nafoukniMapu");
	std::vector<SPoint> vec;
	int a = 4;
	for (int x = -a; x < a; x++) {
		for (int y = -a; y < a; y++) {
			if (x * x + y * y < a*a)
				vec.push_back(SPoint(x, y));
		}
	}
	mapHolder->cleanDilatatedMap();
	DEBUG(mapHolder->getDilatatedMap()->getWidth() << " " << mapHolder->getDilatatedMap()->getHeight());
	for (int x = a; x < map->getWidth() - a; x++) {
		for (int y = a; y < map->getHeight() - a; y++) {
			if (mapHolder->getLaserMap()->getCell(x, y) > 0.4) {
				for (SPoint p: vec) {
					DEBUG((x + p.x) << " " << ( y + p.y));
					mapHolder->updateCellDilatatedMap(x + p.x, y + p.y, 1);
				}
			}
		}
	}
	DEBUG("Planner:draw dilateated map 180");

	mapHolder->drawDilatatedMap();
	DEBUG("Planner:nafoukniMapu : konec");
}

//W : 114, H: 124
std::vector<int> CPlanner::getPath(int startX, int startY, std::vector<std::vector<bool> > frontierMap) {
	bool result = true;
//	DEBUG("window");
	DEBUG((map == NULL ? "is null" : " is not null"));
//	CCMorse::WindowProxy &window = CCMorse::WindowProxy::GetInstance();
//	DEBUG("height " << map->getHeight() << " width " << map->getWidth());
	CMapGrid newMap(cfg, map->getHeight(), map->getWidth(), 2);
	DEBUG("nafoukniMapu");
	nafoukniMapu(newMap);
	DEBUG("za nafoukniMapu");
	// dijkstra initialization
	dijkstra::CHeap<int> heap(map->getHeight() * map->getWidth());
	std::vector<bool> visited(map->getHeight() * map->getWidth(), false);
	std::vector<int> dist(map->getHeight() * map->getWidth(), INT_MAX);
	std::vector<int> previous(map->getHeight() * map->getWidth(), -1);
	int goalX, goalY;
	int it = 0;
	DEBUG("Filling dijsktra heap");
	// Filling dijsktra heap
	for (int x = 0; x < map->getWidth(); x++) {
		for (int y = 0; y < map->getHeight(); y++) {
			if (newMap.getCell(x, y) != 1) {
				heap.add(x + y * map->getWidth(), INT_MAX);
			}
		}
	}

	int startIndex = map->getWidth() * startY + startX;

	heap.update(startIndex, 0);
	dist[startIndex] = 0;
	DEBUG("dijkstra start");
	// dijkstra
	long currentNodeIndex = startIndex;
	int tempX, tempY, tempIndex;
	int tempDist;

	while ((currentNodeIndex = heap.getFirst()) != -1) {
		tempX = (int) (currentNodeIndex % map->getWidth());
		tempY = (int) (currentNodeIndex / map->getWidth());
		if (frontierMap[tempX][tempY]) {
			goalX = tempX;
			goalY = tempY;
			break;
		}
		visited[currentNodeIndex] = true;

		if (dist[currentNodeIndex] == INT_MAX) {
			std::cout << "all additional nodes are not accessible" << std::endl;
			break; // all additional nodes are not accessible
		}
		// Visiting neighbours
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				tempIndex = (tempX + x) + (tempY + y) * map->getWidth();
				if (!(x == 0 && y == 0)
					&& tempIndex >= 0 && tempIndex < map->getHeight() * map->getWidth()// not out of gridMap
					&& tempX + x >= 0
					&& tempY + y >= 0
					&& tempX + x < map->getWidth()
					&& tempY + y < map->getHeight()
					&& !visited.at(tempIndex) // unvisited
					&& newMap.getCell(tempX + x, tempY + y) != 1 // not a wall
						) {
					// ortogonal
					if (x == 0 || y == 0) {
						tempDist = dist[currentNodeIndex] + 5;

					} else {
						// diagonal
						tempDist = dist[currentNodeIndex] + 7;
					}
					if (tempDist < dist[tempIndex]) {
						heap.update(tempIndex, tempDist);
						dist[tempIndex] = tempDist;
						previous[tempIndex] = (int) currentNodeIndex;
					}
				}
			}
		}
	}

//	return pathFinder(startX, startY, goalX, goalY, previous, window, newMap);
	return pathFinder(startX, startY, goalX, goalY, previous, newMap);
}

bool CPlanner::plan() {
	bool result = true;

//DO WHAT YOU WANT HERE
	return result;
}

//std::vector<int> CPlanner::pathFinder(int startX, int startY, int goalX, int goalY, std::vector<int> previous, CCMorse::WindowProxy &window, CMapGrid &newMap ) {
std::vector<int> CPlanner::pathFinder(int startX, int startY, int goalX, int goalY, std::vector<int> previous, CMapGrid &newMap ) {
	int startIndex = map->getWidth() * startY + startX;

	// path
	std::vector<int> path;
	std::vector<int> aliasedPath;
	int goalIndex = goalX + goalY * map->getWidth();
	path.push_back(goalIndex);
	std::cout << "Generating path " << std::endl;

//	while (previous[goalIndex] != startIndex) {
//		goalIndex = previous[goalIndex];
//		path.push_back(goalIndex);
//		window.DrawPixel(goalIndex % gridMap->getWidth() , goalIndex / gridMap->getWidth(), 255, 215, 0);
//	}
//	window.Flush();


	std::cout << "Generating aliased path " << std::endl;
	int startXtemp, startYtemp, goalXtemp, goalYtemp;
	for (int i = 0; i < path.size() - 1; i++) {
		std::cout << "i " << i << std::endl;

		startXtemp = (path[i] % map->getWidth());
		startYtemp = (path[i] / map->getWidth());

		goalXtemp = (path[i + 1] % map->getWidth());
		goalYtemp = (path[i + 1] / map->getWidth());

		while (isClear(startXtemp, startYtemp, goalXtemp, goalYtemp, newMap) && i < path.size() - 1) {
			i++;
			goalXtemp = (path[i] % map->getWidth());
			goalYtemp = (path[i] / map->getWidth());
		}
		i--;
		goalXtemp = (path[i] % map->getWidth());
		goalYtemp = (path[i] / map->getWidth());

//		window.updateProbabilitiesMap(startXtemp, startYtemp, goalXtemp, goalYtemp, 0, 0, 255);
	}

//	window.updateProbabilitiesMap(goalXtemp, goalYtemp, startX, startY, 0, 0, 255);
//	window.Flush();

	std::cout << "Generating aliased path  done" << std::endl;

	std::cout << "Generating aliased path " << std::endl;
	startXtemp = 0, startYtemp = 0, goalXtemp = 0, goalYtemp= 0 ;

	for (int i = path.size() - 1 ; i > 0; i--) {
		std::cout << "i " << i << std::endl;
		startXtemp = (path[i] % map->getWidth());
		startYtemp = (path[i] / map->getWidth());

		goalXtemp = (path[i - 1] % map->getWidth());
		goalYtemp = (path[i - 1] / map->getWidth());

		while (isClear(startXtemp, startYtemp, goalXtemp, goalYtemp, newMap) && i > 0) {
			i--;
			goalXtemp = (path[i] % map->getWidth());
			goalYtemp = (path[i] / map->getWidth());
		}
		i++;
		goalXtemp = (path[i] % map->getWidth());
		goalYtemp = (path[i] / map->getWidth());

//		window.updateProbabilitiesMap(startXtemp, startYtemp, goalXtemp, goalYtemp, 255, 0, 0);
	}

//	window.updateProbabilitiesMap(startXtemp, startYtemp, goalXtemp, goalYtemp,  255, 0, 0);
//	window.Flush();
	return aliasedPath;
}
