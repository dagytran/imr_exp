#! /usr/bin/env morseexec

####################################
#   SyRoTek Simulation             #
####################################
#   "MorseSyrotek" BUILDER SCRIPT  #
####################################

import os, sys, inspect, json


# Prior to everything we just need to save few paths
# Getting path to this folder
currentFolder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe() )) [0] ))
if currentFolder not in sys.path:
	sys.path.insert(0, currentFolder)

# Setting path to environments blend file
environmentPath = os.path.join(currentFolder, 'data', 'MorseSyrotek', 'environments', 'syrotek_arena.blend')
if environmentPath not in sys.path:
	sys.path.insert(0, environmentPath)

# Setting path to Canvas component folder and blend
canvasFolder = os.path.join(currentFolder, 'data', 'MorseSyrotek', 'components')
if canvasFolder not in sys.path:
	sys.path.insert(0, canvasFolder)

canvasPath = os.path.join(canvasFolder, 'Canvas.blend')
if canvasPath not in sys.path:
	sys.path.insert(0, canvasPath)

# Setting path to Syrotek robot folder
robotFolder = os.path.join(currentFolder, 'src', 'MorseSyrotek', 'builder', 'robots')
if robotFolder not in sys.path:
	sys.path.insert(0, robotFolder)

# Setting path to configuration file
configPath = os.path.join(currentFolder, 'cfg', 'morse.json')
if configPath not in sys.path:
	sys.path.insert(0, configPath)

# Setting path to map configuration file
clientConfigPath = os.path.join(currentFolder, 'client.cfg')
if clientConfigPath not in sys.path:
	sys.path.insert(0, clientConfigPath)

# Try open JSON configuration file
try:
	with open(configPath) as config_file:
		config = json.load(config_file)

# If configuration file is not in JSON format, set empty JSON object as configuration.
except ValueError:
	config = {}

# If configuration file is not found, set empty JSON object as configutation.
except FileNotFoundError:
	print("ERROR: The configuration file was not found on [" + configPath + "]!!! Proceeding with default simulation configuration.")
	config = {}


# Now we certainly have all paths for importing
from morse.builder import *
from MorseSyrotek.builder.robots import Syrotek

# In this script we using this mechanism:
# 	If there is no configuration, we create empty one and then we fill it with default values.

# Loading "Simulation" configurations.
# If there is no parent object "Simulation", return empty object "{}"
config["Simulation"]                           = config.get("Simulation", {})
# If there is no "debug-mode" in "Simulation", return default value.
config["Simulation"]["debug-mode"]             = config["Simulation"].get("debug-mode", False)
# Same here as above. And so on...
config["Simulation"]["show-physics"]           = config["Simulation"].get("show-physics", False) 
config["Simulation"]["show-framerate"]         = config["Simulation"].get("show-framerate", False)
config["Simulation"]["fastmode"]               = config["Simulation"].get("fastmode", False)
config["Simulation"]["camera-location"]        = config["Simulation"].get("camera-location", [-1.0, 3.0, 1.85])
config["Simulation"]["camera-rotation"]        = config["Simulation"].get("camera-rotation", [0.85, 0, -2.0])
config["Simulation"]["hide-obstacles-enabled"] = config["Simulation"].get("hide-obstacles-enabled", False)

# Finding out if "MapWindow" is enabled.
config["MapWindow"]            = config.get("MapWindow", {})
config["MapWindow"]["enabled"] = config["MapWindow"].get("enabled", False)
config["MapWindow"]["hide-obstacles-enabled"] = config["Simulation"]["hide-obstacles-enabled"]

# If MapWindow is enabled, we import and create the Canvas component and then set MapWindow configurations.
if config["MapWindow"]["enabled"] == True:
	from MorseSyrotek.builder.components import Canvas
	
	config["MapWindow"]["canvas-folder"] = canvasFolder
	config["MapWindow"]["fullscreen"] = config["MapWindow"].get("fullscreen", False)
	config["MapWindow"]["width"]      = config["MapWindow"].get("width", 32)
	config["MapWindow"]["height"]     = config["MapWindow"].get("height", 32)
	config["MapWindow"]["cell-size"]  = config["MapWindow"].get("cell-size", 0.107)
	config["MapWindow"]["map-file"]   = config["MapWindow"].get("map-file", "") #os.path.join(currentFolder, 'maps/autolab.txt'))
	config["MapWindow"]["ground-canvas-enabled"]   = config["MapWindow"].get("ground-canvas-enabled", False)
	config["MapWindow"]["init-position-on-ground"] = config["MapWindow"].get("init-position-on-ground", True)
	
	if config["MapWindow"]["fullscreen"] == True:
		config["MapWindow"]["transparency-alpha"] = 1.0
	else:
		config["MapWindow"]["transparency-alpha"] = config["MapWindow"].get("transparency-alpha", 1.0)

	canvas = Canvas(filename = canvasPath, alpha = config["MapWindow"]["transparency-alpha"])

	# Try to open map configurations file
	try:
		with open(clientConfigPath) as client_config_file:
			map_cfg = [line.rstrip('\n') for line in client_config_file]

			for i in range(len(map_cfg)):
				if map_cfg[i].find("grid-width") != (-1):
					l = map_cfg[i].split('=')
					config["MapWindow"]["width"] = int(float(l[1]))
					
				if map_cfg[i].find("grid-height") != (-1):
					l = map_cfg[i].split('=')
					config["MapWindow"]["height"] = int(float(l[1]))
				
				if map_cfg[i].find("grid-cell-size") != (-1):
					l = map_cfg[i].split('=')
					config["MapWindow"]["cell-size"] = int(float(l[1]))
				
				if map_cfg[i].find("map-file") != (-1):
					l = map_cfg.split('=')
					config["MapWindow"]["map-file"] = os.path.join(currentFolder, l[1])
			
	except FileNotFoundError:
		print("ERROR: The configuration file was not found on [" + clientConfigPath + "]!!! Proceeding with default simulation configuration.")


# If there is no robot in simulation, we add one with full-default configuration
config["Robots"] = config.get("Robots", [{}])
if len(config["Robots"]) == 0:
	config["Robots"].append({})

# For every configured robot...
for i in range(len(config["Robots"])):

	# Loading "Driver" configurations
	config["Robots"][i]["Driver"]                        = config["Robots"][i].get("Driver", {})
	config["Robots"][i]["Driver"]["name"]			     = config["Robots"][i]["Driver"].get("name",                "waypoint")
	config["Robots"][i]["Driver"]["robot_radius"]        = config["Robots"][i]["Driver"].get("robot_radius",        None)
	config["Robots"][i]["Driver"]["min_gap_width"]	     = config["Robots"][i]["Driver"].get("min_gap_width",       None)
	config["Robots"][i]["Driver"]["obstacle_avoid_dist"] = config["Robots"][i]["Driver"].get("obstacle_avoid_dist", None)
	config["Robots"][i]["Driver"]["max_speed"]		     = config["Robots"][i]["Driver"].get("max_speed",           0.5)
	config["Robots"][i]["Driver"]["max_turn_rate"]	     = config["Robots"][i]["Driver"].get("max_turn_rate",       1.0)
	config["Robots"][i]["Driver"]["goal_pos_tol"]		 = config["Robots"][i]["Driver"].get("goal_pos_tol",        None)
	config["Robots"][i]["Driver"]["goal_angl_tol"]       = config["Robots"][i]["Driver"].get("goal_angl_tol",       0.5335)

	config["Robots"][i]["Driver"]["Provides"]          = config["Robots"][i]["Driver"].get("Provides", {})
	config["Robots"][i]["Driver"]["Provides"]["laser"] = config["Robots"][i]["Driver"]["Provides"].get("laser", [])
	
	if config["Robots"][i]["Driver"]["name"] == "snd":
		config["Robots"][i]["Driver"]["Provides"]["position2d"] = config["Robots"][i]["Driver"]["Provides"].get("position2d", [(i+1)*10-1])
	else:
		config["Robots"][i]["Driver"]["Provides"]["position2d"] = config["Robots"][i]["Driver"]["Provides"].get("position2d", [])

	config["Robots"][i]["Driver"]["Requires"]               = config["Robots"][i]["Driver"].get("Requires", {})
	config["Robots"][i]["Driver"]["Requires"]["position2d"] = config["Robots"][i]["Driver"]["Requires"].get("position2d", {})
	config["Robots"][i]["Driver"]["Requires"]["laser"]      = config["Robots"][i]["Driver"]["Requires"].get("laser", {})

	if config["Robots"][i]["Driver"]["name"] == "snd":
		config["Robots"][i]["Driver"]["Requires"]["position2d"]["input"]  = config["Robots"][i]["Driver"]["Requires"]["position2d"].get("input", [i])
		config["Robots"][i]["Driver"]["Requires"]["position2d"]["output"] = config["Robots"][i]["Driver"]["Requires"]["position2d"].get("output", [i])
		config["Robots"][i]["Driver"]["Requires"]["laser"]["input"]       = config["Robots"][i]["Driver"]["Requires"]["laser"].get("input", [i])
		config["Robots"][i]["Driver"]["Requires"]["laser"]["output"]      = config["Robots"][i]["Driver"]["Requires"]["laser"].get("output", [])
	else:
		config["Robots"][i]["Driver"]["Requires"]["position2d"]["input"]  = config["Robots"][i]["Driver"]["Requires"]["position2d"].get("input", [])
		config["Robots"][i]["Driver"]["Requires"]["position2d"]["output"] = config["Robots"][i]["Driver"]["Requires"]["position2d"].get("output", [])
		config["Robots"][i]["Driver"]["Requires"]["laser"]["input"]       = config["Robots"][i]["Driver"]["Requires"]["laser"].get("input", [])
		config["Robots"][i]["Driver"]["Requires"]["laser"]["output"]      = config["Robots"][i]["Driver"]["Requires"]["laser"].get("output", [])

	# If robot does not have a name, we give him one
	config["Robots"][i]["name"] = config["Robots"][i].get("name", ("robot" + str(i)))

	print("Configuring robot named: " + config["Robots"][i]["name"])

	# Loading robot specific configurations
	config["Robots"][i]["color"]              = config["Robots"][i].get("color", "dark_blue")
	config["Robots"][i]["default-frequency"]  = config["Robots"][i].get("default-frequency", 10)
	config["Robots"][i]["laser-remission"]    = config["Robots"][i].get("laser-remission", False)
	config["Robots"][i]["laser-arc"]          = config["Robots"][i].get("laser-arc", False)
	config["Robots"][i]["laser-frequency"]    = config["Robots"][i].get("laser-frequency", config["Robots"][i]["default-frequency"])
	config["Robots"][i]["pose-frequency"]     = config["Robots"][i].get("pose-frequency", config["Robots"][i]["default-frequency"])
	config["Robots"][i]["odometry-frequency"] = config["Robots"][i].get("odometry-frequency", config["Robots"][i]["default-frequency"])
	config["Robots"][i]["pen-frequency"]      = config["Robots"][i].get("pen-frequency", config["Robots"][i]["default-frequency"])
	config["Robots"][i]["waypoint-frequency"] = config["Robots"][i].get("waypoint-frequency", config["Robots"][i]["default-frequency"])
	config["Robots"][i]["motionvw-frequency"] = config["Robots"][i].get("motionvw-frequency", config["Robots"][i]["default-frequency"])

	# Loading robots proxy indexes
	config["Robots"][i]["Provides"]                         = config["Robots"][i].get("Provides", {})
	config["Robots"][i]["Provides"]["laser"]                = config["Robots"][i]["Provides"].get("laser", [i])
	config["Robots"][i]["Provides"]["position2d"]           = config["Robots"][i]["Provides"].get("position2d", {})
	config["Robots"][i]["Provides"]["position2d"]["global"] = config["Robots"][i]["Provides"]["position2d"].get("global", [i])
	config["Robots"][i]["Provides"]["position2d"]["local"]  = config["Robots"][i]["Provides"]["position2d"].get("local", [])

	if config["MapWindow"]["enabled"] == True:
		config["Robots"][i]["Provides"]["window"] = [0]
	else:
		config["Robots"][i]["Provides"]["window"] = []


	#############
	# Here we finally construct the robot and pass all configuration for store in RobotProperties "sensor".
	robot = Syrotek(robot_configuration = config["Robots"][i], map_configuration = config["MapWindow"], debug = config["Simulation"]["debug-mode"])


	# Setting initial robot position
	config["Robots"][i]["init-position"] = config["Robots"][i].get("init-position", [(i * 0.26 + 0.41), 3.58])
	robot.translate(config["Robots"][i]["init-position"][0], config["Robots"][i]["init-position"][1], 0.05)		

	# Setting initial robot rotation
	config["Robots"][i]["init-rotation"] = config["Robots"][i].get("init-rotation", 4.712391)
	robot.rotate(0.0, 0.0, config["Robots"][i]["init-rotation"])

	# Setting default communication interface for robot
	robot.add_default_interface('socket')		
# All robots have been constructed and configured now.

#####

# Setting up simulation environment
env = Environment(filename = environmentPath, fastmode = config["Simulation"]["fastmode"])

env.show_framerate     (config["Simulation"]["show-framerate"])  # If 'True' then the framerate information will be shown.
env.show_physics       (config["Simulation"]["show-physics"])	 # If 'True' then the physical wireframes of models will be visible.
env.set_camera_location(config["Simulation"]["camera-location"]) # Sets initial camera location
env.set_camera_rotation(config["Simulation"]["camera-rotation"]) # Sets initial camera rotation

env.set_horizon_color(color = (0.05, 0.22, 0.4))

env.create()
