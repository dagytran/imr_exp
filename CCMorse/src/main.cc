/*
 * File name: manic_miner.cc
 * Date:      2009/06/27 11:16
 * Author:    Miroslav Kulich
 * Derived from Jan Faigl's tmap code
 */

#include <fstream>

#include <boost/program_options.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include "imr-h/logging.h"
#include "imr-h/imr_config.h"
#include "imr-h/boost_args_config.h"
#include "imr-h/imr_exceptions.h"

#include "libCCMorse/MorseError.h"

#include "robot.h"
#include "planner_robot.h"
#include "map_grid.h"
#include "planner.h"
#include "exploration.h"
#include "plan.h"



namespace po = boost::program_options;
namespace fs = boost::filesystem;

const std::string PROGRAM_VERSION = "0.1";

/// ----------------------------------------------------------------------------
/// Program options variables
/// ----------------------------------------------------------------------------
imr::CConfig robotCfg;
imr::CConfig expCfg;
imr::CConfig winCfg;
imr::CConfig mapCfg;
imr::CConfig plannerCfg;

/// ----------------------------------------------------------------------------
bool parseArgs(int argc, char * argv[])
{
    bool ret = true;
    std::string configFile;
    //   std::string guiConfigFile;
    std::string loggerCfg = "";

    po::options_description desc("General options");
    desc.add_options()
        ("help,h", "produce help message")
        ("logger-config,l", po::value<std::string>(&loggerCfg)->default_value(loggerCfg), "logger configuration file")
        ("config,c", po::value<std::string>(&configFile)->default_value(std::string(argv[0]) + ".cfg"), "configuration file")
        ;

    po::options_description robot("Robot options");
    boost_args_add_options(imr::robot::CRobot::getConfig(robotCfg), "robot-", robot);
    po::options_description explor("Exploration options");
    boost_args_add_options(imr::robot::CExploration::getConfig(expCfg), "exploration-", explor);
    //po::options_description window("Window options");
    //boost_args_add_options(imr::CWindow::getConfig(winCfg), "window-", window);

    po::options_description map("Map options");
    boost_args_add_options(imr::CMapGrid::getConfig(mapCfg), "gridMap-", map);
    po::options_description planner("Planner options");
    boost_args_add_options(imr::CPlanner::getConfig(plannerCfg), "planner-", planner);


    try {
        po::options_description cmdline_options;
        //cmdline_options.add(desc).add(robot).add(explor).add(window).add(gridMap).add(planner);
        cmdline_options.add(desc).add(robot).add(explor).add(map).add(planner);

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, cmdline_options), vm);
        po::notify(vm);
        std::ifstream ifs(configFile.c_str());
        store(parse_config_file(ifs, cmdline_options), vm);
        po::notify(vm);
        ifs.close();
        store(parse_config_file(ifs, cmdline_options), vm);
        po::notify(vm);
        ifs.close();

        if (vm.count("help")) {
            std::cerr << std::endl;
            std::cerr << "Sample mine detector client ver. " << PROGRAM_VERSION << std::endl;
            std::cerr << cmdline_options << std::endl;
            ret = false;
        }

        if(
            ret
            && loggerCfg != ""
            && fs::exists(fs::path(loggerCfg))
        ){
            //imr::initLogger("manic_miner", loggerCfg.c_str());
        } else {
            //imr::initLogger("manic_miner");
        }
    } catch (std::exception & e) {
        std::cerr << std::endl;
        std::cerr << "Error in parsing arguments: " << e.what() << std::endl;
        ret = false;
    }

    return ret;
}


void exploration()
{

    imr::CMapGrid            gridMap     (robotCfg);
    imr::robot::CRobot       robot       (robotCfg, gridMap);
    imr::robot::CExploration exploration (expCfg, gridMap, &robot);
    imr::CPlan plan;

	#ifdef _MORSE
		CCMorse::WindowProxy::GetInstance(robot.GetClient(), gridMap.getWidth(), gridMap.getHeight());
	#endif // _MORSE


	imr::robot::MapHolder mapHolder(gridMap);

	robot.setMapHolder(mapHolder);
	exploration.setMapHolder(mapHolder);

    robot.setPlan(plan);
    exploration.setPlan(plan);



    robot.execute();
    exploration.execute();

    INFO("Press ENTER to finish");
    getchar();

    robot.stop();
    exploration.stop();

    robot.join();
    exploration.join();
}

void planning()
{
    imr::CMapGrid             gridMap (mapCfg, mapCfg.get<std::string>("file"));
    imr::robot::CPlannerRobot robot   (robotCfg, gridMap);
    imr::CPlanner             planner (plannerCfg);

#ifdef _MORSE
    CCMorse::WindowProxy& window = CCMorse::WindowProxy::GetInstance(robot.GetClient(), gridMap.getWidth(), gridMap.getHeight());
#endif // _MORSE

    int sx = plannerCfg.get<int>("start_x");
    int sy = plannerCfg.get<int>("start_y");
    int gx = plannerCfg.get<int>("goal_x");
    int gy = plannerCfg.get<int>("goal_y");
//    INFO("start point: [" << sx << "," << sy << "]");
//    INFO("goal point: [" << gx << "," << gy << "]");



    planner.setMap(gridMap);
    planner.plan(); // here is the main planning function

#ifdef _MORSE
    window.DrawPixel(sx, sy, 255, 0, 0);
    window.DrawPixel(gx, gy, 0, 255, 0);
//    window.Flush(); // flushing all the graphical output

    window.SavePNG("plan");
#endif // _MORSE
}

/// ----------------------------------------------------------------------------
/// Main PROGRAM
/// ----------------------------------------------------------------------------
int main(int argc, char **argv)
{
    int ret = -1;

    if (parseArgs(argc, argv)) {
        INFO("Start Logging");
        try {

        /**
            _EXPLORATION parameter is defined during compilation phase. It allows to
            switch between exploration and simple planning task.
            If you look in the Makefile at line 41, there is an expression
            CPPFLAGSS += -D _EXPLORATION. When '#' is at the beginning, the line is
            commented and _EXPLORATION is not defined. Therefore planning task is
            compiled. If the line is uncommented ('#' is removed), parameter is defined
            and exploration is compiled.
        */
#ifdef _EXPLORATION
            exploration();
#else
            planning();
#endif

            ret = EXIT_SUCCESS;

        } catch (CCMorse::MorseError& e) {
            ERROR("Morse error: " << e.GetErrorStr() << " function: " << e.GetErrorFun());

        } catch (imr::exception& e) {
            ERROR("Error: " << e.what());
        }

        INFO("End Logging");
    }
    return ret;
}

/* end of manic_miner.cc */
