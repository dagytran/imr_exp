/*
 * File name: planner_robot.h
 * Date:      2016-09-26
 * Author:    Lukáš Bertl
 * Derived from: robot.h (Jan Faigl, Miroslav Kulich)
 */

#ifndef __PLANNER_ROBOT_H__
#define __PLANNER_ROBOT_H__

#include "libCCMorse/CCMorse.h"

#include <vector>

#include "imr-h/imr_config.h"
#include "imr-h/thread.h"

#include "map_grid.h"
#include "robot_types.h"

/**
    Class CPlannerRobot provides communication with the robot in simulator.
    Only WindowProxy is available here.

    When executed, CPlannerRobot class runs as new thread.
*/

namespace imr { namespace robot {

class CPlannerRobot : public imr::concurrent::CThread {

    typedef imr::concurrent::CThread ThreadBase;

    public:
        /**
            Get default configuration of the robot. All connections to
            sensors and motion controller are defined here as well as
            connection to Syrotek server.

            @param config   Configuration to be modified
            @return Modified configurations
        */
        static imr::CConfig& getConfig(imr::CConfig& config);

        /**
            Constructor. Initialze all sensors and drivers. After initialization
            the variables are checked, if everything was successful.
        */
        CPlannerRobot(imr::CConfig& cfg, CMapGrid &map);

        /**
            Destructor.
        */
        ~CPlannerRobot();

        /**
            Request to stop the control loop. Flag quit is set to true. When this
            is recognized in navigation loop, it stops.
        */
        void stop(void);

        /**
            Set path plan for robot. This shall be plan from current position to
            the closest frontier.

            @param newPlan  vector containing IDs of nodes
        */
        void setPlan(std::vector<int> newPlan);

        /**
            Returns pointer to PlayerClient instance.
        */
        CCMorse::PlayerClient* GetClient() { return client; }

    protected:
        /**
            Thread execution method. Whenever the execution of the thread (robot)
            is called, this method is raised. In this case threadBody calls a
            navigation method where the robot is controlled.
        */
        void threadBody(void);

        /**
            Main control loop. Position and sensors are read here and robot motion
            is driven from here.

            Implement YOUR own control law here.
            The main idea what happens in this method is following:
                - Read and update robot's position
                - Read laser data and update your map (occupancy grid)
                - The first two steps provide new data for planner
                - Read current plan (output from the planner)
                - Drive robot to next node from the plan
        */
        void navigation(void);

    private:
        imr::CConfig& cfg;     // configuration of the robot instance

        CCMorse::PlayerClient* client; 	// pointer to Player client, allows reading from simulator environment

        bool quit;         // Flag of quit request
        bool alive;        // currently not used

        SPosition pos; // current robot position

        CMapGrid &map;     // gridMap used during exploration

        Mutex mtx;     // mutex to lock the quit variable
        std::vector<int> path_plan;    // plan of the robot's path
        int plan_index; // index of current goal node in plan vector
};
}} //end namespace imr::robot

#endif

/* end of robot.h */
