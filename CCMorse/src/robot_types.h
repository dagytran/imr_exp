/*
 * File name: robot_types.h
 * Date:      2009/06/27 17:34
 * Author:    Miroslav Kulich, Jan Faigl
 */

#ifndef __ROBOT_TYPES_H__
#define __ROBOT_TYPES_H__

#include <vector>
#include "imr_assert.h"

// STL container iterator macros
#define VAR(V,init) __typeof(init) V=(init)
#define FOR_EACH(I,C) for(VAR(I,(C).begin());I!=(C).end();I++)
#define FOR_EACH_REVERSE(I,C) for(VAR(I,(C).rbegin());I!=(C).rend();I++)

/**
    Types defined here may save users time and keep him from reinvetion of
    a wheel.

    SPosition - simple structure, where the position of the robot can be stored
                Contains x, y coordinates and rotation of the robot (yaw)
                Also useful function is available - squared Euclidian distance

    PositionVector - type defined as a vector of SPosition elements. This can
                     be used for example to store robot's position over the time
                     or a plan made by planner.
*/

namespace imr { namespace robot {

   struct SPosition {
      double x;
      double y;
      double yaw;

        /**
            Constructors of the structure.
        */
      SPosition() {}
      SPosition(double x, double y) : x(x), y(y) {}
      SPosition(double x, double y, double yaw) : x(x), y(y), yaw(yaw) {}

        /**
            Calculate squared Euclidian distance.

            @param p    another position, from which the distance is computed
            @return squared distance
        */
      double squared_distance(const SPosition& p) const {
         double dx = x - p.x;
         double dy = y - p.y;
         return dx*dx + dy*dy;
      }

        /**
            Assign operator. Assign coordinates from position pos to this
            instance

            @param pos  assigned position
        */
      SPosition& operator=(const SPosition& pos) {
         if (this != &pos) {
            x = pos.x;
            y = pos.y;
            yaw = pos.yaw;
         }
         return *this;
      }

   };

    /// Vector of SPosition points.
   typedef std::vector<SPosition> PositionVector;
} } // end namespace imr::robot


#endif

/* end of robot_types.h */
