#! /usr/bin/env morseexec

####################################
#   SyRoTek Simulation             #
####################################
#   "PathPlanner" BUILDER SCRIPT   #
####################################

import os, sys, inspect, json


# Prior to everything we just need to save few paths
# Getting path to this folder
currentFolder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe() )) [0] ))
if currentFolder not in sys.path:
	sys.path.insert(0, currentFolder)

# Setting path to environments blend file
environmentPath = os.path.join(currentFolder, 'data', 'MorseSyrotek', 'environments', 'dummy_world.blend')
if environmentPath not in sys.path:
	sys.path.insert(0, environmentPath)

# Setting path to Canvas component folder and blend
canvasFolder = os.path.join(currentFolder, 'data', 'MorseSyrotek', 'components')
if canvasFolder not in sys.path:
	sys.path.insert(0, canvasFolder)

canvasPath = os.path.join(canvasFolder, 'Canvas.blend')
if canvasPath not in sys.path:
	sys.path.insert(0, canvasPath)

# Setting path to dummy robot folder
robotFolder = os.path.join(currentFolder, 'src', 'MorseSyrotek', 'builder', 'robots')
if robotFolder not in sys.path:
	sys.path.insert(0, robotFolder)

# Setting path to map configuration file
clientConfigPath = os.path.join(currentFolder, 'client.cfg')
if clientConfigPath not in sys.path:
	sys.path.insert(0, clientConfigPath)

# Try to open map configurations file
try:
	with open(clientConfigPath) as client_config_file:
		map_cfg = [line.rstrip('\n') for line in client_config_file]
		
except FileNotFoundError:
	print("ERROR: The configuration file was not found on [" + clientConfigPath + "]!!! Proceeding with default simulation configuration.")

# Now we certainly have all paths for importing
from morse.builder import *
from MorseSyrotek.builder.robots import Dummy
from MorseSyrotek.builder.components import Canvas

from math import pi

# Creating canvas object for drawing plan.
canvas = Canvas(filename = canvasPath, alpha = 1.0)

# Loading configuration from "client.cfg" file.
config = {}

for i in range(len(map_cfg)):
	if map_cfg[i].find("grid-width") != (-1):
		l = map_cfg[i].split('=')
		config["width"] = int(float(l[1]))
		
	if map_cfg[i].find("grid-height") != (-1):
		l = map_cfg[i].split('=')
		config["height"] = int(float(l[1]))

	if map_cfg[i].find("map-grid-cell-size") != (-1):
		l = map_cfg[i].split('=')
		config["cell-size"] = int(float(l[1]))

	if map_cfg[i].find("map-file") != (-1):
		l = map_cfg[i].split('=')
		config["map-file"] = os.path.join(currentFolder, l[1])


# If some of the configurations is missing, we set the default value.
config["canvas-folder"] = canvasFolder
config["width"]         = config.get("width", 130)
config["height"]        = config.get("height", 130)
config["cell-size"]     = config.get("cell-size", 0.03)
config["map-file"]      = config.get("map-file", os.path.join(currentFolder, 'maps/autolab.txt'))

# Contructor for dummy robot that is just holding pen for planning.
robot = Dummy(configuration = config)

# Setting up simulation environment
robot.translate(0.0, 0.0, 0.01)
robot.rotate(0.0, 0.0, 0.0)
robot.add_default_interface('socket')

env = Environment(environmentPath, fastmode = False)
env.set_camera_location([0.0, 0.0, 10.0])
env.set_camera_rotation([0.0, 0.0, 0.0])

env.create()

