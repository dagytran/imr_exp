/*
 * File name: robot.cc
 * Date:      2009/06/27 12:33
 * Author:    Jan Faigl, Miroslav Kulich
 */


#include <unistd.h> //usleep
#include <fstream>

#include "robot.h"
#include "imr-h/logging.h"
#include "imr-h/imr_exceptions.h"

#include "libCCMorse/MorseError.h"

#include "planner.h"
#include "robot_types.h"
#include "orloc.h"

using namespace imr::robot;
using namespace CCMorse;

typedef unsigned char uchar;


/// ----------------------------------------------------------------------------
/// Class CRobot

//// - static method ------------------------------------------------------------
imr::CConfig &CRobot::getConfig(imr::CConfig &config) {
	//config.add<std::string>("host", "player server hostname", "localhost");
	//config.add<int>("port", "player server port", 6665);
#ifdef _MORSE
	config.add<std::string>("host", "morse server hostname", "localhost");
	config.add<int>("port", "morse server port", 4000);
#else
	config.add<std::string>("host", "syrotek server hostname", "syrotek.felk.cvut.cz");
	config.add<int>("port", "syrotek server port", 38000);
#endif // _MORSE
	config.add<int>("laser-index", "laser index", 0);
	config.add<int>("position2d-index", "position 2d index", 0);
	config.add<int>("global-position2d-index", "position 2d index", 2);
	config.add<int>("snd-index", "snd index", 1);
	CMapGrid::getConfig(config);
	return config;
}

/// ----------------------------------------------------------------------------
CRobot::CRobot(imr::CConfig &cfg, CMapGrid &map) : ThreadBase(), cfg(cfg), client(0), quit(false),
												   laser(0), position(0), global(0), snd(0), gridMap(map), plan() {
	client = new CCMorse::PlayerClient(cfg.get<std::string>("host"), cfg.get<int>("port"));
	laser = new LaserProxy(client, cfg.get<int>("laser-index"));
	position = new Position2dProxy(client, cfg.get<int>("position2d-index"));
	global = new Position2dProxy(client, cfg.get<int>("global-position2d-index"));
	snd = new Position2dProxy(client, cfg.get<int>("snd-index"));

	assert_argument(laser, "Can not create laser proxy");
	assert_argument(position, "Can not create position2d proxy");
	assert_argument(global, "Can not create proxy to global position");
	assert_argument(snd, "Can not create snd proxy");

	// check laser
	while (!laser->IsFresh()) {  // laser init retry
		client->Read();
		while (client->Peek(0)) {
			client->Read();
		}
	}
}

/// ----------------------------------------------------------------------------
CRobot::~CRobot() {
	stop();
	join();
	if (client) {
#ifdef _MORSE
		delete &CCMorse::WindowProxy::GetInstance();
#endif // _MORSE
		delete snd;
		delete global;
		delete position;
		delete laser;
		delete client;
	}
}


/// ----------------------------------------------------------------------------
void CRobot::stop(void) {
	ScopedLock lk(mtx);
	quit = true;
}

/// - protected method ---------------------------------------------------------
void CRobot::threadBody(void) {
	try {
		navigation();

	} catch (CCMorse::MorseError &e) {
		ERROR("Morse error: " << e.GetErrorStr() << " function: " << e.GetErrorFun());

	} catch (imr::exception &e) {
		ERROR("Imr error: " << e.what());
		exit(-1);
	}
}

void CRobot::setMapHolder(MapHolder &mapHolder) {
	this->mapHolder = &mapHolder;
}

/// - protected method ---------------------------------------------------------
void CRobot::navigation(void) {
	bool q = false;
	DEBUG("ROBOT: Navigation started.");


//	#ifdef _MORSE
	// Clearing gridMap
	CCMorse::WindowProxy &window = CCMorse::WindowProxy::GetInstance();

//	#endif

	position->SetMotorEnable(true);
	int i = 0;
	do {
		client->Read();
		while (client->Peek(0)) {
			client->Read();
		}

		/** README - Important note
			While using real Syrotek system, the position system time to time
			does not consider new position to be fresh even though it is.
			Therefore WHEN THIS ERROR OCCURS it is necessary to remove
			global->IsFresh() from following condition.
		*/
		if (global->IsFresh() && global->IsValid()) { // process position 2d first
			DEBUG("ROBOT: UPDATE POZICE");
			pos.x = global->GetXPos();
			pos.y = global->GetYPos();
			pos.yaw = global->GetYaw();
//        if (i % 20 == 0) {
//            DEBUG("POSITION  " << pos.x << " " << pos.y << " " << pos.yaw);
//            //      DEBUG("SPEED  " << global-> GetXSpeed()<< " " << global-> GetYSpeed() << " " << global-> GetYawSpeed());
//            DEBUG("YAW  " << global-> GetYaw());
//        }
//		i++;
			mapHolder->updateCellPositionMap(gridMap.getX(pos.x), gridMap.getY(pos.y), 1);
//			mapHolder->updateCellTargetMap(100, 100, 1);
//			mapHolder->updateCellPathMap(75, 75, 1);
//		DEBUG("DRAW MAP")
//		DEBUG("after DRAW MAP")
			global->NotFresh();
		}

		if (laser->IsFresh() && laser->IsValid()) {
			DEBUG("ROBOT: UPDATE LASERU");

			// use orloc angle correction
			angleCorrection(pos, *laser);
//		DEBUG("laser start  ");

			/*
			  - pro kazdy laser
			  - nakresli usecku z aktualniho bodu do bodu mereni laseru
			  - pro kazdy bod usecky spocti:
				- magicka_konstanta = laser_vidi_prekazku ? 0.75 : 0.25
				- nova_pravdepodobnost_bodu = (magicka_konstanta*stara_pravdepodobnost_bodu)/(magicka_konstanta*stara_pravdepodobnost_bodu + (1-magicka_konstanta)*(1-stara_pravdepodobnost_bodu))
			*/
			for (int laser_id = 0; laser_id < laser->GetCount(); laser_id++) {
				updateProbabilitiesMap(pos, laser->GetRange(laser_id), laser_id);
			}
			DEBUG("ROBOT: PO UPDATU LASERU");

			laser->NotFresh();
		}

		mapHolder->drawMap();

		DEBUG("1 ");
		{
			// TODO: command the robot
			DEBUG("ROBOT: Next point");
			int nextPoint = plan.getNextPoint();
			DEBUG("ROBOT: nextpoint " << nextPoint);
			if (nextPoint != -1) {
				int x = nextPoint % gridMap.getWidth();
				int y = (nextPoint - x) / gridMap.getWidth();
				DEBUG("ROBOT: x " << x << " y" << y);
				snd->GoTo(x, y, 0);
				DEBUG("ROBOT: za goto");
			}
		}
		ScopedLock lk(mtx); //check quit request
		q |= quit;
	} while (!q);

	INFO("ROBOT: Main loop has been left");
}

void CRobot::updateProbabilitiesMap(SPosition position, double distance, int laserIndex) {
	double pixelSize = 0.03; // 3cm
	double sensorOffset = 0.04; // 4cm;
	double alpha = position.yaw;
	double beta = laser->GetBearing(laserIndex) + alpha;

	double x1 = position.x;
	double y1 = position.y;

	double x0 = x1 + sensorOffset * cos(alpha);
	double y0 = y1 + sensorOffset * sin(alpha);

	double xWall = x0 + distance * cos(beta);
	double yWall = y0 + distance * sin(beta);

	if (xWall >= 3.42) {
		xWall = 3.41;
	} else if (xWall <= 0) {
		xWall = 0;
	}

	if (yWall <= 0) {
		yWall = 0;
	} else if (yWall >= 3.72) {
		yWall = 3.71;
	}

	if (x1 >= 3.42) {
		x1 = 3.41;
	}

	if (y1 >= 3.72) {
		y1 = 3.71;
	}

	// real to pixel
//	DEBUG("Raw Start POSITION  " << x1 << " " << y1 );
//	DEBUG("Raw Start POSITION  " << xWall << " " << yWall );


	int pixelX0 = gridMap.getX(x1);
	int pixelY0 = gridMap.getY(y1);
	int pixelXWall = gridMap.getX(xWall);
	int pixelYWall = gridMap.getY(yWall);

	if (pixelXWall >= gridMap.getWidth())
		pixelXWall = gridMap.getWidth() - 1;
	if (pixelYWall >= gridMap.getHeight())
		pixelYWall = gridMap.getHeight() - 1;
	if (pixelXWall <= 0)
		pixelXWall = 1;
	if (pixelYWall <= 0)
		pixelYWall = 1;

//	DEBUG("PIXEL Start POSITION  " << pixelX0 << " " << pixelY0 );

//	DEBUG("PIXEL wall POSITION  " << pixelXWall << " " << pixelYWall );

	//realXwall, realYWall = fcn(pos.x,pos.y)
	// pixX,pixY = real2pixel(realXwall,realYWall)
	int dx = pixelXWall - pixelX0;
	int dy = pixelYWall - pixelY0;
	int steep = (abs(dy) >= abs(dx));
	if (steep) {
		swap(pixelX0, pixelY0);
		swap(pixelXWall, pixelYWall);
		// recompute Dx, Dy after swap
		dx = pixelXWall - pixelX0;
		dy = pixelYWall - pixelY0;
	}
	int xstep = 1;
	if (dx < 0) {
		xstep = -1;
		dx = -dx;
	}
	int ystep = 1;
	if (dy < 0) {
		ystep = -1;
		dy = -dy;
	}
	int twoDy = 2 * dy;
	int twoDyTwoDx = twoDy - 2 * dx; // 2*Dy - 2*Dx
	int e = twoDy - dx; //2*Dy - Dx
	int y = pixelY0;
	int xDraw, yDraw;
	double cell;
	for (int x = pixelX0; x != pixelXWall; x += xstep) {
		if (steep) {
			xDraw = y;
			yDraw = x;
		} else {
			xDraw = x;
			yDraw = y;
		}

		/*- pro kazdy bod usecky spocti:
      	  - magicka_konstanta = laser_vidi_prekazku ? 0.75 : 0.25
      	  - nova_pravdepodobnost_bodu = (magicka_konstanta*stara_pravdepodobnost_bodu)/(magicka_konstanta*stara_pravdepodobnost_bodu + (1-magicka_konstanta)*(1-stara_pravdepodobnost_bodu))*/

		cell = mapHolder->getLaserMap()->getCell(xDraw, yDraw);
		mapHolder->updateCellLaserMap(xDraw, yDraw, (0.25 * cell) / (0.25 * cell + (0.75 * (1 - cell)))); // čára ke zdi

		// next
		if (e > 0) {
			e += twoDyTwoDx; //E += 2*Dy - 2*Dx;
			y = y + ystep;
		} else {
			e += twoDy; //E += 2*Dy;
		}
	}

	int px, py;

	if (steep) {
		px = pixelYWall;
		py = pixelXWall;
	} else {
		px = pixelXWall;
		py = pixelYWall;
	}

	if (px >= gridMap.getWidth())
		px = gridMap.getWidth() - 1;
	if (py >= gridMap.getHeight())
		py = gridMap.getHeight() - 1;
	if (px <= 0)
		px = 1;
	if (py <= 0)
		py = 1;

	cell = mapHolder->getLaserMap()->getCell(px, py);
	mapHolder->updateCellLaserMap(px, py, (0.75 * cell) / (0.75 * cell + (0.25 * (1 - cell)))); // zeď

}

int CRobot::getX() {
	gridMap.getX(global->GetXPos());
}

int CRobot::getY() {
	gridMap.getY(global->GetYPos());
}

void CRobot::setPlan(imr::CPlan &newPlan) {
	plan = newPlan;
}

/* end of robot.cc */

