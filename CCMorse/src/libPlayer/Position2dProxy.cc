#include "Position2dProxy.h"
#include "PlayerClient.h"
#include "logging.h"
#include "snd.h" 


using namespace CCMorse;

Position2dProxy::Position2dProxy(PlayerClient* aClient, int aIndex) : 
  Proxy(aClient, aIndex), 
  xSpeed(0),
  ySpeed(0) {
  client->addProxy(this);
  switch (aIndex) {
    case 0:
      type = ODOMETRY;
      break;
    case 1:
      type = GLOBAL_POSITION;
      break;
    case 2:
      type = SND_NAVIGATION;
      break;
  }
}



void Position2dProxy::Set() {
  bool isfr;
  if (type == SND_NAVIGATION) { 
      Fresh();
  } else {
    CSimpleJsonObject json = client->getMessage( type == ODOMETRY ? "odoposition" : "position",isfr);
    if (isfr) {
      x = stod(json.getValue("x"));
      y = stod(json.getValue("y"));
      phi = stod(json.getValue("heading"));
//     INFO("POSITION: " << x << " " << y << " " << phi);
      Fresh();
      Validate();
    }
  }    
}

Position2dProxy::~Position2dProxy() {
}


void Position2dProxy::SetupConfiguration() {
}

void Position2dProxy::Subscribe(uint32_t aIndex) {

}

void Position2dProxy::Unsubscribe() {

}


void Position2dProxy::SetSpeed(double aXSpeed, double aYSpeed) {
  CSimpleJsonObject json;
  std::string msgName = "velocity";
  json.set("message",msgName);
  json.set("forward",aXSpeed);
  json.set("angular",aYSpeed);
  std::string msg = json.exportJson();
  client->sendCommand(msg);  
  xSpeed = aXSpeed;
  ySpeed = aYSpeed;
}



void Position2dProxy::GoTo(Pose2D_t pos) {
}

void Position2dProxy::GoTo(double aX, double aY, double aYaw) {
  static SNDNavigation snd(client);
  snd.GoTo(aX,aY,aYaw,true);
}


void Position2dProxy::SetMotorEnable(bool enable) {
  SetSpeed(0,0);
}


