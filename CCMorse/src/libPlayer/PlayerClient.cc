
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <chrono>
#include <sys/socket.h>
#include <netdb.h>

#include "PlayerClient.h"
#include "logging.h"


using namespace CCMorse;

bool PlayerClient::connect() {
  bool ipv6 = false;
  bool connected = false;
  do {
    socketFd = ::socket(ipv6 ? AF_INET6 : AF_INET, SOCK_STREAM, 0);
    if (socketFd < 0) {
      ERROR("Failed to create socket: " <<  strerror(errno));
      break;
    }
    
    const struct sockaddr * addr;
    
    struct hostent *host_info;
    host_info =  gethostbyname(ipAddress.c_str());
    if (host_info == NULL) {
      ERROR("Failed to resolve host name '" << ipAddress.c_str() << "' : " << strerror(errno));
      break;
    }
    
    int addr_size;
    if (!ipv6) {
      struct sockaddr_in * addr4 = new struct sockaddr_in;
      addr4->sin_family = PF_INET;
      addr4->sin_port = htons(serverPort);
      memcpy(&addr4->sin_addr.s_addr, host_info->h_addr, host_info->h_length);
      addr = (const struct sockaddr*) addr4;
      addr_size = sizeof(struct sockaddr_in);
    } else {
      struct sockaddr_in6 * addr6 = new struct sockaddr_in6;
      addr6->sin6_family = PF_INET6;
      addr6->sin6_port = htons(serverPort);
      memcpy((char *) &(addr6->sin6_addr.s6_addr), host_info->h_addr_list[0], host_info->h_length);
      addr = (const struct sockaddr*) addr6;
      addr_size = sizeof(struct sockaddr_in);
    }
    INFO("connecting to robot " << ipAddress << ":" << serverPort <<" ...");
    if (::connect(socketFd, addr, addr_size) == 0) {
      INFO("connected to robot " << ipAddress << ":" <<  serverPort);
      connected = true;
    } else {
      ERROR("Failed to connect to robot at " << ipAddress <<  ":" << serverPort);
    }
    delete addr;
  } while (false);
  
  
  return connected;
}

void PlayerClient::addMessage(const char* message) {
//   INFO("addMessage: waiting");
  std::unique_lock<std::mutex> lock(stateMutex);
  CSimpleJsonObject json(message);
  std::string msgType = json.getValue("message");
//   INFO("Message: " << msgType);
  state[msgType] = std::make_pair(true,json);
  isFresh = true;
//   INFO("addMessage: waiting done");

}

CSimpleJsonObject PlayerClient::getMessage(std::string type, bool &isFresh) {
//    INFO("getMessage: waiting");
  std::unique_lock<std::mutex> lock(stateMutex);
  isFresh = state[type].first;
  state[type].first = false;
//    INFO("getMessage: waiting done");
  return state[type].second;
}


void PlayerClient::receive() {
//   INFO("receiving");
  CJsonStreamParser parser;
  bool q = false;
  while (!q) {
    char b;
    if (::recv(socketFd, &b, 1, 0) == 1) {
      const char * message = parser.parseChar(b);
      if (message) {
//         INFO("received message '" <<  message << "'");
        addMessage(message);
      }
    } else {
      fprintf(stderr, "receive failed: %s\n", strerror(errno));
      break;
    }
   std::unique_lock<std::mutex> lk(quitMutex);
   q |= quit;
  }
  INFO("receive thread interrupted");
}




PlayerClient::PlayerClient(const std::string aHostname, int aPort, int aTransport) : 
  ipAddress(aHostname),
  serverPort(aPort),
  socketFd(-1),
  isFresh(false),
  quit(false)
{
  connect();
  recThread =  std::thread(&PlayerClient::receive,this);
  CSimpleJsonObject json;
  std::string msgName = "velocity";
//   {“message”: “velocity”, “forward”:0.1, “angular”:0}
  json.set("message",msgName);
  json.set("forward",0.1);
  json.set("angular",0);
  std::string msg = json.exportJson();

  if (::send(socketFd, msg.c_str(), msg.length()+1, 0) < 0) {
     ERROR("send error: " << strerror(errno));
  }
}

PlayerClient::~PlayerClient() {
  {
    std::unique_lock<std::mutex> lk(quitMutex);
    quit = true;
  }
  DEBUG("Waiting for recThread.");
  recThread.join();
  DEBUG("recThread finished.");
  ::shutdown(socketFd,SHUT_RDWR);
}


Proxy* PlayerClient::getProxy(int type) {
  INFO("getProxy");
  for(Proxy *p: proxies) {
    INFO("   type " << p->getProxyType() << " " << type);
    if (p->getProxyType() == type) {
      return p;
    }
  }
  return NULL;
}


void PlayerClient::sendCommand(std::string msg) {   
  if (::send(socketFd, msg.c_str(), msg.length()+1, 0) < 0) {
    ERROR("send error: " << strerror(errno));
  }
}


bool PlayerClient::Peek(uint32_t timeout) {
  /*
  uint32_t to = timeout;
  do {
    INFO("Still waiting for " << to << " ms.");
    {
      std::unique_lock<std::mutex> lock(stateMutex);
      if (isFresh) { return true;} 
    }
    if (to < 50) {
      std::this_thread::sleep_for(std::chrono::milliseconds(to));
      to = 0;
    } else {
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
      to -= 50;
    }
  } while (to > 0);
  INFO("Peek: waiting");
  std::unique_lock<std::mutex> lock(stateMutex);
  INFO("Peek: waiting done");
  return isFresh;
  */
  return false;
}


void PlayerClient::Read() {
  while(true) {
    {
//       DEBUG("IN READ");
      bool ff;
      {
        std::unique_lock<std::mutex> lock(stateMutex);
        ff = isFresh;
      }
      if (ff) {
        for(Proxy *p: proxies) {
          p->Set();
        }
        {
          std::unique_lock<std::mutex> lock(stateMutex);
          isFresh = false;
        }
//          DEBUG("DATA READ");
        return; 
      }
//        DEBUG("... still not fresh");
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
    

  
  
}

