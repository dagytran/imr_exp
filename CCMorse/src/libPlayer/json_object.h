/*
 * File name: json_object.h
 * Date:      2015/03/09 08:30
 * Author:    Jan Chudoba
 */

#ifndef __JSON_OBJECT_H__
#define __JSON_OBJECT_H__

#include <stdarg.h>
#include <string>
#include <map>
#include <vector>

class CSimpleJsonObject
{
public:
   std::map<std::string, std::string> data;

public:
   CSimpleJsonObject();
   CSimpleJsonObject(const char * str);

   void clear() { data.clear(); }

   void set(std::string key, std::string & value) { data[key] = value; }
   void set(std::string key, int value);
   void set(std::string key, double value);

   bool isKey(std::string & key);
   bool isKey(const char * key) { std::string k(key); return isKey(k); }
   std::string getValue(std::string & key);
   std::string getValue(const char * key) { std::string k(key); return getValue(k); }
   bool getValueIfExists(const char * key, std::string & value);
   int getValueInt(std::string & key);
   int getValueInt(const char * key) { std::string k(key); return getValueInt(k); }
   double getValueDouble(std::string & key);
   double getValueDouble(const char * key) { std::string k(key); return getValueDouble(k); }
   bool getDoubleArrayValues(std::string & key, std::vector<double> & array);
   bool getDoubleArrayValues(const char * key, std::vector<double> & array) { std::string k(key); return getDoubleArrayValues(k, array); }

   bool parseJsonString(const char * str);
   std::string exportJson();
   bool exportJson(int fileDescriptor);

protected:
   void warning(const char * str, ...);
};

class CJsonStreamParser
{
public:
   CJsonStreamParser() { reset = false; jsonLevel = 0; }

   const char * parseChar(char c, int * length = NULL);

protected:
   std::string buffer;
   bool reset;
   int jsonLevel;
};

#endif

/* end of json_object.h */
