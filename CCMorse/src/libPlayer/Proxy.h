#ifndef JSON_PROXY_H
#define JSON_PROXY_H

namespace CCMorse {
class PlayerClient;


typedef struct {
    double px, py;          // Point in 2D (X[m], Y[m])
} Point2D_t;

typedef struct {
     double px, py, pa;     // Translation and rotation (X[m], Y[m], Yaw[rad])
} Pose2D_t;


enum ProxyType { UNKNOWN = -1, ODOMETRY, GLOBAL_POSITION, SND_NAVIGATION, LASER };


class Proxy  {
private:
  bool isFresh;    
  bool isValid;
  
protected:
  PlayerClient* client;
  int index;
  int type;
public:
  /// constructor
  Proxy(PlayerClient* aClient, int aIndex = 0) : isFresh(false),   isValid(false), client(aClient), index(aIndex), type(UNKNOWN) {};
  /// destructor
  virtual ~Proxy() {};
  virtual void Set() = 0;
  bool IsValid() { return isValid; }
  void Validate() { isValid = true; }
  bool IsFresh() { return isFresh; } 
  void NotFresh() { isFresh = false; }
  void Fresh() { isFresh = true; }
  int getProxyType() { return type; }
};
}
#endif // JSON_PROXY_H
