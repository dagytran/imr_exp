#ifndef JSON_POSITION2DPROXY_H
#define JSON_POSITION2DPROXY_H
#include<cstdint>
#include "Proxy.h"


namespace CCMorse {
class Position2dProxy : public Proxy
{
    public:
        /// constructor
        Position2dProxy(PlayerClient* aClient, int aIndex = 0);
        /// destructor
        virtual ~Position2dProxy();

        void Set();
        
        /// Send a motor command for velocity control mode.
        /// Specify the forward, sideways, and angular speeds in m/sec, m/sec,
        /// and radians/sec, respectively.
        void SetSpeed(double aXSpeed, double aYawSpeed);

        
        void GoTo(Pose2D_t pos);

        void GoTo(double aX, double aY, double aYaw);




        /// Enable/disable the motors.
        /// Set @p state to 0 to disable or 1 to enable.
        /// Be VERY careful with this method!  Your robot is likely to run across the
        /// room with the charger still attached.
        void SetMotorEnable(bool enable);


        /// Accessor method
        double GetXPos() const { return x; };

        /// Accessor method
        double GetYPos() const { return y; };

        /// Accessor method
        double GetYaw() const { return phi; };

        /// Accessor method
        double GetXSpeed() const { return xSpeed; };

        /// Accessor method
        double GetYSpeed() const { return ySpeed; };

        /// Accessor method
        double GetYawSpeed() const { return 0; };
        int getProxyType() { return type; }

    private:
      double x;
      double y;
      double phi;
      double xSpeed;
      double ySpeed;
      void SetupConfiguration();
      void Subscribe(uint32_t aIndex);
      void Unsubscribe();
};
}
#endif // JSON_MORSEPOSITION2DPROXY_H
