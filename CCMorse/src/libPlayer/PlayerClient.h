#ifndef JSON_PLAYERCLIENT_H
#define JSON_PLAYERCLIENT_H

#include <string>
#include <thread>
#include <mutex>
#include <vector> 

#include "json_object.h"
#include "Proxy.h"

#define JSON_HOSTNAME      "syrotek.felk.cvut.cz"
#define JSON_PORTNUM       38000
#define JSON_TRANSPORT_TCP 0
#define JSON_TRANSPORT_UDP 1



namespace CCMorse {

class PlayerClient {
public:
  /// Make a client and connect it as indicated.
  PlayerClient(const std::string aHostname = JSON_HOSTNAME, int aPort = JSON_PORTNUM, int aTransport = JSON_TRANSPORT_TCP);
  /// destructor
  ~PlayerClient();
  
  
  /// @brief Check whether there is data waiting on the connection, blocking
  /// for up to @p timeout milliseconds (set to 0 to not block).
  ///
  /// @returns
  /// - false if there is no data waiting
  /// - true if there is data waiting
  bool Peek(uint32_t timeout = 0);
  
  
  /// @brief A blocking Read
  ///
  /// Use this method to read data from the server, blocking until at
  /// least one message is received.  Use @ref PlayerClient::Peek() to check
  /// whether any data is currently waiting.
  /// In pull mode, this will block until all data waiting on the server has
  /// been received, ensuring as up to date data as possible.
  void Read();
  
  
  /// @brief Set connection retry limit, which is the number of times
  /// that we'll try to reconnect to the server after a socket error.
  /// Set to -1 for inifinite retry.
  void SetRetryLimit(int limit) {};
  
  
  
  void addMessage(const char* message);
  CSimpleJsonObject getMessage(std::string type, bool &isFresh);
  void addProxy(Proxy *proxy) { proxies.push_back(proxy); }
  void sendCommand(std::string msg); 
  Proxy* getProxy(int type);
  
private:
  const std::string ipAddress;
  const int serverPort = 38000;
  int socketFd;
  std::thread recThread;
  std::map<std::string, std::pair<bool,CSimpleJsonObject> > state;
  std::mutex stateMutex;
  std::mutex quitMutex;
  bool isFresh;
  std::vector<Proxy*> proxies;
  bool quit;
  
  bool connect();
  void receive();
  
};
}
#endif // JSON_PLAYERCLIENT_H
