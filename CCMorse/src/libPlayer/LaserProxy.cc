#include "LaserProxy.h"
#include "PlayerClient.h"
#include "logging.h"

using namespace CCMorse;


LaserProxy::LaserProxy(PlayerClient* aClient, int aIndex) : 
  Proxy(aClient, aIndex), 
  id("unknown"),
  timestamp(0),
  count(0), 
  maxRange(5.0),
  scanRes(0),
  minAngle(0),
  maxAngle(0) {
  type = LASER;
  client->addProxy(this);
}

LaserProxy::~LaserProxy() {
}


// "{\"message\":\"ranger\", \"timestamp\":\"%u\", \"id\":\"%s\", \"size\":\"%u\", \"min_angle\":\"%.6f\", \"max_angle\":\"%.6f\", \"ranges\":[", timestamp, id, range_count, minAngle, maxAngle);

void LaserProxy::Set() {
  bool isfr;
  CSimpleJsonObject json = client->getMessage("ranger",isfr);
  if (isfr) {
    id = json.getValue("id");
    count = json.getValueInt("size"); //TODO: count is unsigned!
    timestamp = 0; //TODO: set timestamp properly (it is unsigned)
    minAngle = json.getValueDouble("min_angle");
    maxAngle = json.getValueDouble("max_angle");
    scanRes = (maxAngle - minAngle)/count;
    json.getDoubleArrayValues("ranges", range);
//     INFO("ID: " << id << " COUNT: " << count << " MIN_ANGLE: " << minAngle << " MAX_ANGLE: " << maxAngle << " SCAN_RES: " << scanRes);
//     INFO("VALUES: ");
//     for(double r:range) {
//       std::cout << r << " ";
//     }  
    Fresh();
    Validate();
  }    
}


//TODO: position needed!
Point2D_t LaserProxy::GetPoint(uint32_t aIndex) const
{
/*  
    Point2D_t point;

    Pose2D_t pose = GetVar(m_Laser->GetPoint(aIndex));
    point.px = pose.px;
    point.py = pose.py;

    return point;
*/
}
