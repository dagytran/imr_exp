/*
 * File name: json_object.cc
 * Date:      2015/03/09 08:38
 * Author:    Jan Chudoba
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>

#include "json_object.h"

CSimpleJsonObject::CSimpleJsonObject()
{
}

CSimpleJsonObject::CSimpleJsonObject(const char * str)
{
   parseJsonString(str);
}

void CSimpleJsonObject::set(std::string key, int value)
{
   char str[16];
   snprintf(str, sizeof(str), "%d", value);
   data[key] = std::string(str);
}

void CSimpleJsonObject::set(std::string key, double value)
{
   char str[32];
   snprintf(str, sizeof(str), "%f", value);
   data[key] = std::string(str);
}

bool CSimpleJsonObject::isKey(std::string & key)
{
   return (data.find(key) != data.end());
}

bool CSimpleJsonObject::getValueIfExists(const char * key, std::string & value)
{
   bool result;
   std::map<std::string, std::string>::iterator it = data.find(std::string(key));
   if (it != data.end()) {
      value = it->second;
      result = true;
   } else {
      result = false;
   }
   return result;
}

std::string CSimpleJsonObject::getValue(std::string & key)
{
   std::map<std::string, std::string>::iterator it = data.find(key);
   if (it == data.end()) {
      return std::string();
   } else {
      return it->second;
   }
}

int CSimpleJsonObject::getValueInt(std::string & key)
{
   std::string str = getValue(key);
   if (str.empty()) return 0;
   return atoi(str.c_str());
}

double CSimpleJsonObject::getValueDouble(std::string & key)
{
   std::string str = getValue(key);
   if (str.empty()) return 0;
   return atof(str.c_str());
}

bool CSimpleJsonObject::getDoubleArrayValues(std::string & key, std::vector<double> & array)
{
   std::string str = getValue(key);
   std::size_t start = 0, end = 0;
   array.clear();
   while ((end = str.find(',', start)) != std::string::npos) {
      array.push_back(atof(str.substr(start, end - start).c_str()));
      start = end + 1;
   }
   array.push_back(atof(str.substr(start).c_str()));
   return true;
}

static bool isspace(char c) {
   switch (c) {
   case ' ': return true;
   case '\t': return true;
   case '\r': return true;
   case '\n': return true;
   default: return false;
   }
}

typedef enum {
   JPS_NONE,
   JPS_KEY_QUOTES,
   JPS_KEY,
   JPS_COLON,
   JPS_VALUE,
   JPS_COMMA,
   JPS_NUMBER
} TJsonParserState;

bool CSimpleJsonObject::parseJsonString(const char * str)
{
   bool error = false;
   int level = 0;
   TJsonParserState state = JPS_NONE;
   char c;
   int pos = 0;
   std::string currentKey;
   std::string currentValue;
   bool inQuotes = false;
   bool inArray = false;
   bool esc = false;
   while ((c = *(str++)) != 0) {
      pos++;
      //fprintf(stderr, "DEBUG: json c='%c' %d\n", c, c);
      if (level == 0) {
         if (c == '{') {
            level = 1;
            state = JPS_KEY_QUOTES;
         } else {
            if (!isspace(c)) {
               warning("invalid character '%c' on position %d when expecting '{'\n", c, pos);
               error = true;
            }
         }
      } else {
         // level > 0
         if (level == 1) {
            switch(state) {
            case JPS_NONE: break;
            case JPS_KEY_QUOTES:
               if (isspace(c)) break;
               if (c == '}') {
                  warning("}");
                  level--;
                  break;
               }
               if (c == '"') {
                  state = JPS_KEY;
                  currentKey.clear();
                  currentValue.clear();
               }
               else {
                  warning("invalid character '%c' on position %d when expecting '\"'\n", c, pos);
                  error = true;
               }
               break;
            case JPS_KEY:
               if (c != '"') currentKey += c;
               else {
                  state = JPS_COLON;
               }
               break;
            case JPS_COLON:
               if (isspace(c)) break;
               if (c == ':') state = JPS_VALUE;
               else {
                  warning("invalid character '%c' on position %d when expecting ':'\n", c, pos);
                  error = true;
               }
               break;
            case JPS_VALUE:
               if (!inQuotes) {
                  if (!inArray) {
                     if (isspace(c)) break;
                     if (c == '"') inQuotes = true;
                     else if (c == '[') inArray = true;
                     else if (c == '{') { level++; currentValue+=c; }
                     else if (c == ',' || c == '}') {
                        set(currentKey, currentValue);
                        state = JPS_KEY_QUOTES;
                        if (c == '}') level--;
                        break;
                     }
                     else currentValue += c;
                  } else { // in array:
                     if (c != ']') {
                        currentValue += c;
                     } else {
                        inArray = false;
                        set(currentKey, currentValue);
                        state = JPS_COMMA;
                     }
                  }
               } else { // in quotes:
                  if (!esc) {
                     if (c == '\\') {
                        esc = true;
                     } else {
                        if (c != '\"') {
                           currentValue += c;
                        } else {
                           inQuotes = false;
                           set(currentKey, currentValue);
                           state = JPS_COMMA;
                        }
                     }
                  } else {
                     switch (c) {
                     case '"': currentValue+=c; break;
                     case '\\': currentValue+=c; break;
                     case '/': currentValue+=c; break;
                     case 'b': currentValue+='\b'; break;
                     case 'f': currentValue+='\f'; break;
                     case 'n': currentValue+='\n'; break;
                     case 'r': currentValue+='\r'; break;
                     case 't': currentValue+='\t'; break;
                     case 'u': warning("4 hex digit escape sequence not supported"); break;
                     }
                     esc = false;
                  }
               } // in quotes
               break;
            case JPS_COMMA:
               if (isspace(c)) break;
               if (c == ',') state = JPS_KEY_QUOTES;
               else if (c == '}') {
                  level--;
                  break;
               }
               else {
                  warning("invalid character '%c' on position %d when expecting ','\n", c, pos);
                  error = true;
               }
               break;
            case JPS_NUMBER: break;
            }
         } else {
            // level > 1
            currentValue += c;
            if (!inQuotes) {
               if (c == '{') {
                  level++;
               } else
               if (c == '}') {
                  level--;
               } else
               if (c == '\"') {
                  inQuotes = true;
               }
            } else {
               if (!esc) {
                  if (c == '\\') esc = true;
                  else if (c == '\"') inQuotes = false;
               } else {
                  esc = false;
               }
            }
         }
      }
   }
   //warning("level=%d", level);
   return (!error && level == 0);
}

static bool isNumber(std::string & str)
{
   const char * s = str.c_str();
   char c;
   bool num = false;
   while ((c = *s++) != 0) {
      if (c >= '0' && c<='9') {
         num = true;
         continue;
      }
      if (num && (c=='e' || c=='E')) continue;
      if (c == '-') continue;
      if (c == '.') continue;
      if (c == '+') continue;
      return false;
   }
   return num;
}

static std::string escapeString(std::string str)
{
   std::string result;
   for (unsigned int i=0; i<str.length(); i++) {
      char c = str[i];
      switch (c) {
      case '\"': result += "\\\""; break;
      case '\\': result += "\\\\"; break;
      case '\n': result += "\\n"; break;
      case '\r': result += "\\r"; break;
      case '\t': result += "\\t"; break;
      case '\f': result += "\\f"; break;
      case '\b': result += "\\b"; break;
      default: result += c;
      }
   }
   return result;
}

std::string CSimpleJsonObject::exportJson()
{
   std::string result = "{";
   bool first = true;
   for (std::map<std::string, std::string>::iterator it = data.begin(); it != data.end(); it++) {
      if (!first) result+=","; else first = false;
      result += "\"" + escapeString(it->first) + "\":";
      bool isnumber = isNumber(it->second);
      if (!isnumber) result += "\"";
      result += escapeString(it->second);
      if (!isnumber) result += "\"";
   }
   result += "}";
   return result;
}

bool CSimpleJsonObject::exportJson(int fileDescriptor)
{
   ::write(fileDescriptor, "{", 1);
   bool first = true;
   for (std::map<std::string, std::string>::iterator it = data.begin(); it != data.end(); it++) {
      if (!first) write(fileDescriptor, ",", 1); else first = false;
      write(fileDescriptor, "\"", 1);
      std::string escaped = escapeString(it->first);
      write(fileDescriptor, escaped.c_str(), escaped.length());
      write(fileDescriptor, "\":", 2);
      bool isnumber = isNumber(it->second);
      if (!isnumber) write(fileDescriptor, "\"", 1);
      escaped = escapeString(it->second);
      write(fileDescriptor, escaped.c_str(), escaped.length());
      if (!isnumber) write(fileDescriptor, "\"", 1);
   }
   write(fileDescriptor, "}", 1);
   return true;
}


void CSimpleJsonObject::warning(const char * str, ...)
{
	va_list arg;
	va_start(arg, str);
   fprintf(stderr, "CSimpleJsonObject WARNING: ");
   vfprintf(stderr, str, arg);
   fprintf(stderr, "\n");
	va_end(arg);
}

// =============================================================================

const char * CJsonStreamParser::parseChar(char c, int * length)
{
   if (reset) {
      reset = false;
      buffer.clear();
   }
   if (buffer.empty()) {
      if (c == '{') {
         buffer += c;
         jsonLevel = 1;
      }
   } else {
      buffer += c;
      if (c == '}') {
         jsonLevel--;
         if (jsonLevel == 0) {
            // have whole message
            reset = true;
            if (length) *length = buffer.length();
         }
      }
   }
   return (reset) ? buffer.c_str() : NULL;
}

/* end of json_object.cc */
