#ifndef JSON_LASERPROXY_H
#define JSON_LASERPROXY_H

#include<cstdint>
#include<vector>
#include <string>

#include "Proxy.h"

namespace CCMorse {
class LaserProxy : public Proxy
{   
    public:
        /// constructor
        LaserProxy(PlayerClient* aClient, int aIndex = 0);
        /// destructor
        virtual ~LaserProxy();

        void Set();

        
        /// Number of points in scan
        uint32_t GetCount() const { return count; };

        /// Max range for the latest set of data (meters)
        double GetMaxRange() const { return maxRange; };

        /// Angular resolution of scan (radians)
        double GetScanRes() const { return scanRes; };

        /// Range resolution of scan (m)
        double GetRangeRes() const { return rangeRes; };

        /// Scanning Frequency (Hz)
        double GetScanningFrequency() const { return -1; };

        /// Scan range for the latest set of data (radians)
        double GetMinAngle() const { return minAngle; };
        
        /// Scan range for the latest set of data (radians)
        double GetMaxAngle() const { return maxAngle; };

        /// Scan range from the laser config (call RequestConfigure first) (radians)
        ///double GetConfMinAngle() const { return m_MinAngle; };
        
        
        /// Scan range from the laser config (call RequestConfigure first) (radians)
        ///double GetConfMaxAngle() const { return m_MaxAngle; };

        /// Whether or not reflectance (i.e., intensity) values are being returned.
        ///bool IntensityOn() const { return GetVar(m_Laser->IsIntensityOn()) != 0 ? true : false; };



        /// Scan data (Cartesian): x,y (m)
        Point2D_t GetPoint(uint32_t aIndex) const;

        /// Get the range
        double GetRange(uint32_t aIndex) const { return range[aIndex]; };

        /// Get the bearing
        double GetBearing(uint32_t aIndex) const { return minAngle+aIndex*scanRes; };


        /// Get the intensity
        ///int GetIntensity(uint32_t aIndex) const { return GetVar(m_Laser->GetIntensity(aIndex)); };

        /// Get the laser ID, call RequestId first
        ///int GetID() const { return GetVar(m_Laser->GetID()); };


        /// Configure the laser scan pattern.  Angles @p min_angle and
        /// @p max_angle are measured in radians.
        /// @p scan_res is measured in units of 0.01 degrees;
        /// valid values are: 25 (0.25 deg), 50 (0.5 deg) and
        /// 100 (1 deg).  @p range_res is measured in mm; valid values
        /// are: 1, 10, 100.  Set @p intensity to @p true to
        /// enable intensity measurements, or @p false to disable.
        /// @p scanning_frequency is measured in Hz
        ///void Configure(double aMinAngle, double aMaxAngle, uint32_t aScanRes, uint32_t aRangeRes, bool aIntensity, double aScanningFrequency) {};

        /// Request the current laser configuration; it is read into the
        /// relevant class attributes.
        ///void RequestConfigure();

        /// Request the ID of the laser; read it with GetID()
        ///void RequestID() {}

        /// Get the laser's geometry; it is read into the
        /// relevant class attributes.
        /// void RequestGeom() {}

        /// Accessor for the pose of the laser with respect to its parent
        /// object (e.g., a robot).  Fill it in by calling RequestGeom.
        ///Pose3D_t GetPose() { return GetVar(m_Laser->GetGeoPose()); }

        /// Accessor for the pose of the laser's parent object (e.g., a robot).
        /// Filled in by some (but not all) laser data messages.
        ///Pose3D_t GetRobotPose() { return GetVar(m_Laser->GetRobotPose()); }

        /// Accessor for the size (fill it in by calling RequestGeom)
        ////BBox3D_t GetSize() { return GetVar(m_Laser->GetSizeExtent()); }

        /// Minimum range reading on the left side
        ///double GetMinLeft() const { return m_Laser->GetMinLeft(); }

        /// Minimum range reading on the right side
        ////double GetMinRight() const { return m_Laser->GetMinRight(); }

        /// @deprecated Minimum range reading on the left side
        ///double MinLeft () const { return GetMinLeft(); }

        /// @deprecated Minimum range reading on the right side
        /// double MinRight () const { return GetMinRight(); }

        /// Range access operator.  This operator provides an alternate
        /// way of access the range data.  For example, given an @p
        /// LaserProxy named @p lp, the following expressions are
        /// equivalent: @p lp.GetRange(0) and @p lp[0].
        double operator [] (uint32_t index) const { return range[index]; }
        
        int getProxyType() { return LASER; }
        Proxy * getProxy(int type);

private:
      // local storage of config
      std::string id;
      uint32_t timestamp;
      int count;
      double maxRange;
      double scanRes;      
      double rangeRes;     
      double minAngle;     
      double maxAngle;
      std::vector<double> range;

  
};
}
#endif // JSON_LASERPROXY_H
