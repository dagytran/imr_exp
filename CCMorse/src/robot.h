/*
 * File name: robot.h
 * Date:      2009/06/27 12:31
 * Author:    Jan Faigl, Miroslav Kulich
 */

#ifndef __ROBOT_H__
#define __ROBOT_H__

#include <vector>

#ifdef _MORSE
  #include "libCCMorse/CCMorse.h"
#else
  #include "libPlayer/Position2dProxy.h"
  #include "libPlayer/LaserProxy.h"
  #include "libPlayer/PlayerClient.h"
  #define PI 3.14159265359
#endif

#include "imr-h/imr_config.h"
#include "imr-h/thread.h"

#include "robot_types.h"
#include "map_grid.h"
#include "plan.h"
#include "map_holder.h"

/**
    Class CRobot provides communication with the robot (in simulator or the
    real one). Position and other sensor data are available here. Also the
    commands for SND driver (robot motion controller) are send from here.

    When executed, CRobot class runs as new thread.
*/

namespace imr { namespace robot {
   class CRobot : public imr::concurrent::CThread {
      typedef imr::concurrent::CThread ThreadBase;
      public:

        /**
            Get default configuration of the robot. All connections to
            sensors and motion controller are defined here as well as
            connection to Syrotek server.

            @param config   Configuration to be modified
            @return Modified configurations
        */
         static imr::CConfig& getConfig(imr::CConfig& config);

         /**
            Constructor. Initialze all sensors and drivers. After initialization
            the variables are checked, if everything was successful.
         */
         CRobot(imr::CConfig& cfg, CMapGrid &map);

         /**
            Destructor.
         */
         ~CRobot();

        /**
            Request to stop the control loop. Flag quit is set to true. When this
            is recognized in navigation loop, it stops.
        */
         void stop(void);
         
        /**
            Returns pointer to PlayerClient instance.
        */
        CCMorse::PlayerClient* GetClient() { return client; }

	   int getX ();

	   int getY ();

	   void setPlan(imr::CPlan&);

	   void setMapHolder(MapHolder &mapHolder);
         
     protected:

        /**
            Thread execution method. Whenever the execution of the thread (robot)
            is called, this method is raised. In this case threadBody calls a
            navigation method where the robot is controlled.
        */
         void threadBody(void);

         /**
            Main control loop. Position and sensors are read here and robot motion
            is driven from here.

            Implement YOUR own control law here.
            The main idea what happens in this method is following:
                - Read and update robot's position
                - Read laser data and update your map (occupancy grid)
                - The first two steps provide new data for planner
                - Read current plan (output from the planner)
                - Drive robot to next node from the plan
         */
         void navigation(void);

	   void updateProbabilitiesMap(SPosition position, double distance, int laserIndex);

     private:
         imr::CConfig& cfg;     // configuration of the robot instance
         CCMorse::PlayerClient* client;    // pointer to Player client, allows reading from simulator environment
         bool quit;         // Flag of quit request
         bool alive;        // currently not used
         SPosition pos; // current robot position
         CCMorse::LaserProxy* laser;   // laser range sensor instance
         CCMorse::Position2dProxy* position;   // direct motor control
         CCMorse::Position2dProxy* global;     // robot global position
         CCMorse::Position2dProxy* snd;        // snd driver - allows user to move robot to given position
         CMapGrid &gridMap;     // gridMap used during exploration
         Mutex mtx;     // mutex to lock the quit variable
	  	 CPlan plan;                // plan
		   CMapGrid* positionMap;
		   CMapGrid* laserMap;
		   CMapGrid* targetMap;
	   	MapHolder* mapHolder;
   };
} } //end namespace imr::robot

#endif

/* end of robot.h */
