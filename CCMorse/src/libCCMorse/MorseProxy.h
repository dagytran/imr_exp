/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-16
 *
 * File contains definition of MorseProxy class.
 */

#ifndef CCMORSE_MORSEPROXY_H
#define CCMORSE_MORSEPROXY_H

#include "MorseDevice.h"
#include "PlayerClient.h"


namespace CCMorse {

/**
 * MorseProxy class have same API as ClientProxy from PlayerCc, but it is used to
 * communicate with Morse simulatior instead of Player/Stage.
 * It is a base class for other proxies, like @ref CCMorse::LaserProxy and @ref CCMorse::Position2dProxy.
 *
 * @see http://playerstage.sourceforge.net/doc/Player-3.0.2/player/classPlayerCc_1_1ClientProxy.html
 */
class MorseProxy {

    friend class MorseClient;
    friend class MorseRobot;
    friend class LaserProxy;
    friend class Position2dProxy;

    public:

#ifdef HAVE_BOOST_SIGNALS
        /**
         * A connection type. This is usefull when attaching signals to the
         * ClientProxy because it allows for detatching the signals.
         */
        typedef boost::signals::connection connection_t;

        /**
         * A function pointer type for read signals signal.
         */
        typedef boost::signal<void (void)> read_signal_t;
#else
        typedef int connection_t;       // if we're not using boost, just define them.
        typedef int read_signal_t;      // if we're not using boost, just define them.
#endif // HAVE_BOOST_SIGNALS

#ifdef HAVE_BOOST_THREAD
        /// A scoped lock
        typedef boost::mutex::scoped_lock ScopedLock_t;
#else
        typedef boost::mutex::scoped_lock ScopedLock_t;
#endif // HAVE_BOOST_THREAD

        /**
         *  Returns @c true if we have received any new data from the device.
         */
        bool IsValid() const { return GetVar(m_Valid); }

        /**
         * Fresh is set to @c true on each new read.  It is up to the user to
         * set it to @c false if the data has already been read.
         */
        bool IsFresh() const { return GetVar(m_Fresh); }

        /**
         * This states that the data in a client is currently not Fresh.
         */
        void NotFresh();

        /**
         *  Returns the driver name.
         */
        std::string GetDriverName() const;

        /**
         * Returns received data timestamp. [s]
         */
        double GetDataTime() const { return GetVar(m_Device->GetDataTime()); }

        /**
         * Returns difference between new and previous received data timestamps. [s]
         */
        double GetElapsedTime() const { return GetVar(m_Device->GetDataTime() - m_Device->GetLastTime()); }

        /**
         * Returns a pointer to the Morse aka Player Client.
         */
        PlayerClient* GetPlayerClient() const { return m_Client; }

        /**
         * Connect a signal to this proxy.
         */
        template<typename T>
        connection_t ConnectReadSignal(T aSubscriber)
        {
            #ifdef HAVE_BOOST_SIGNALS
                ScopedLock_t lock( GetMutex() );
                return m_ReadSignal.connect(aSubscriber);
            #else
                return -1;
            #endif
        }

        /**
         * Disconnect a signal to this proxy.
         */
        void DisconnectReadSignal(connection_t aSubscriber)
        {
            #ifdef HAVE_BOOST_SIGNALS
                ScopedLock_t lock( GetMutex() );
                aSubscriber.disconnect();
            #else
                // This line is here to prevent compiler warnings of "unused varaibles"
                aSubscriber = aSubscriber;
            #endif
        }

        /**
         * Returns string representation of some proxy information.
         * Output example: "ID: 1, Type: LaserProxy, Driver: snd"
         */
        std::string const ToString() { return ( "ID: " + NumberToString(GetID()) + ", Type: " + GetName() + ", Driver: " + GetDriverName()); }

        /**
         * Returns name of the proxy.
         */
        std::string const GetName() const { return GetVar(m_Name); }

        /**
         * Returns generated identification number of the proxy.
         */
        int const GetID() const { return GetVar(m_ID); }

        /**
         * Returns proxy index, configured by user.
         */
        int const GetIndex() const { return GetVar(m_Index); }

        /**
         * Returns ID of the robot to which is proxy subscribed.
         */
        int const GetSubscribedToRobotID() const { return GetVar(m_SubscribedToRobotID); }

        /**
         * Checks if the new data is valid and result is saved to MorseProxy::m_Valid member.
         * @ref MorseProxy::IsValid()
         */
        void CheckDataValidity();

    protected:
        /**
         * The MorseProxy standard constructor.
         * @attention Potected, so it can only be instantiated by other clients.
         */
        MorseProxy(CCMorse::PlayerClient* aClient, int aIndex);

        /**
         * Copy constructor.
         */
        MorseProxy(MorseProxy const &aOther);

        /**
         * Default constructor.
         */
        MorseProxy();

        /**
         * Destructor will try to close access to the device.
         */
        virtual ~MorseProxy();

        /**
         * Returns mutex for synchronization.
         */
        Mutex_t &GetMutex() const { return m_Mutex; }

        /**
         * Subscribe to the proxy. This needs to be defined for every proxy.
         *
         * @param   aIndex  The ID number of robot we want to connect to.
         *
         * I wish these could be pure virtual, but they're used in the constructor/destructor.
         */
        virtual void Subscribe(uint32_t aIndex) {};

        /**
         * Unsubscribe from the robot. This needs to be defined for every proxy.
         */
        virtual void Unsubscribe() {};

        /**
         * Sets proxy name.
         */
        void SetName(const std::string &aName);


        int m_SubscribedToRobotID;  ///< ID of the robot owning devices for this proxy.

        PlayerClient* m_Client; ///< The controlling client object.

        MorseDevice* m_Device;  ///< The main device of the proxy.

        bool m_Fresh;   ///< If set to true, the current data is considered "fresh" by user.
        bool m_Valid;   ///< If set to true, the current data is valid.

        /**
         * Get a variable from the client. All Get functions need to use this when
         * accessing data to make sure the data access is thread safe.
         */
        template<typename T>
        T GetVar(const T &aV) const { return CCMorse::GetVar(aV, m_Mutex); }

    private:
        std::string m_Name;     ///< Name of the proxy. Used to identify it's type.

        int m_ID;               ///< Generated ID of the proxy.
        int m_Index;            ///< Index given to the proxy via configuration.

        static int s_IDCounter; ///< Counter for proxy IDs.

        double m_LastTime;      ///< The last time that data was read by this client in [s].

        mutable Mutex_t m_Mutex;    ///< Mutex for synchronization.

//        /**
//         * A boost::signal which is used for our callbacks.
//         * The signal will normally be of a type such as:
//         * - boost::signal<void ()>
//         * - boost::signal<void (T)>
//         * where T can be any type.
//         *
//         * @attention we currently only use signals that return void because we
//         * don't have checks to make sure a signal is registered.  If an empty
//         * signal is called:
//         *
//         * @attention "Calling the function call operator may invoke undefined
//         * behavior if no slots are connected to the signal, depending on the
//         * combiner used. The default combiner is well-defined for zero slots when
//         * the return type is void but is undefined when the return type is any
//         * other type (because there is no way to synthesize a return value)."
//         */
//        read_signal_t m_ReadSignal;
//
//        /**
//         * Outputs the signal if there is new data.
//         */
//        void ReadSignal();

};
} // namespace CCMorse
#endif // CCMORSE_MORSEPROXY_H
