/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-18
 *
 * File contains definition of SndDriver class.
 */

#ifndef CCMORSE_SNDDRIVER_H
#define CCMORSE_SNDDRIVER_H

#include "MorseDriver.h"
#include "Position2dProxy.h"
#include "LaserProxy.h"
#include "snd/snd.h"


namespace CCMorse {

/**
 * SndDriver class is used to navigate robot to its goal position.
 * Based on @ref CCMorse::MorseDriver class and
 * using @ref CCMorse::SNDNavigation as driving algorithm.
 */
class SndDriver : public MorseDriver
{
    public:
        /**
         * Constructor.
         * @param   aRobot  Pointer to robot that this driver will control.
         */
        SndDriver(MorseRobot* aRobot) : MorseDriver(aRobot) { SetupProxies(); }

        /**
         * Destructor.
         */
        virtual ~SndDriver() { delete m_SND; };

        /**
         * Navigates robot on Goal defined in MorseRobot::SetGoal() function.
         */
        void GoTo();

        /**
         * Static function, checks if robot have all proxies that are required by
         * this driver.
         *
         * @param   aRobot  Pointer to robot that this driver will check.
         * @return  [@c true: Robot have all needed proxies, @c false: Otherwise]
         */
        static bool HasRobotEveryProxy(MorseRobot* aRobot);

    private:
        /**
         * Saves pointers to proxies in their member variables for storage.
         */
        void SetupProxies();

        SNDNavigation*      m_SND;      ///< Pointer to instance of snd navigation class.

        Position2dProxy*    m_PoseIn;   ///< Pointer to input Position2dProxy.
        Position2dProxy*    m_PoseOut;  ///< Pointer to output Position2dProxy.
        LaserProxy*         m_Laser;    ///< Pointer to input LaserProxy.

};
}   // namespace CCMorse;

#endif // CCMORSE_SNDDRIVER_H
