#include "MorseMotionVW.h"

using namespace CCMorse;


std::string MorseMotionVW::idMVW[] = {"motionvw", "motion", "motionvwdiff"};
std::vector<std::string> MorseMotionVW::identifiers (idMVW, idMVW + sizeof(idMVW)/sizeof(std::string) );

void MorseMotionVW::SetupConfiguration()
{
    m_vx = 0;
    m_vy = 0;
    m_va = 0;
    m_Stall = 0;
    m_Enabled = false;

    Dictionary_t config = GetConfigurations();
    Dictionary_t objectToRobot = MapData(config["object_to_robot"]);

    SetGeoPose    ( AsVectorOfDoubles (objectToRobot["translation"]) );
    SetRotation   ( AsVectorOfPoses   (objectToRobot["rotation"]) );
    SetSizeExtent (0, 0, 0);

    SetFreshConfig(true);
    SetFreshGeom(true);
}

bool const MorseMotionVW::IsItMe(const std::string &aName, const std::string &aRobotName)
{
    for(std::size_t i = 0; i < identifiers.size(); i++){
        if(aName.compare(aRobotName + "." + identifiers.at(i)) == 0)
            return true;
    }
    return false;
}

void MorseMotionVW::ChangeSpeed() const
{
    std::string status = CommunicateWithSim("set_speed [" + NumberToString(GetVX()) + ", " + NumberToString(GetW()) + "]");

    if(status.compare(COM_KO) == 0) {
        std::cout << "MorseMotionVW::changeSpeed(): Speed change was NOT successful!!!" << std::endl;

    } else if (status.compare(COM_OK) == 0) {
        return;//std::cout << "MorseMotionVW::changeSpeed(): Speed change was successful." << std::endl;

    } else {
        throw MorseError("MorseMotionVW::changeSpeed()", "Something somewhere went terribly wrong.");
    }
}

void MorseMotionVW::SetEnable(bool const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Enabled = aValue;

    if(m_Enabled == true){
        ChangeSpeed();

    } else {
        SetSpeed(0, 0, 0);
        ChangeSpeed();
    }
}

void MorseMotionVW::SetSpeed(double const &vx, double const &vy, double const &w)
{
    SetVX(vx);
    SetVY(vy);
    SetW(w);
}

void MorseMotionVW::Stop()
{
    CommunicateWithSim("stop");
    SetEnable(false);
}

std::string const MorseMotionVW::ToString() const
{
    std::string output ("{\"v\": " + NumberToString(GetVX()) + ", \"w\": " + NumberToString(GetW()) + "}");
    return output;
}

void MorseMotionVW::RefreshData()
{
    if(IsEnable()){
        ChangeSpeed();
    }
    UpdateTime();
    SetFresh(true);
}

void MorseMotionVW::SetVX(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_vx = aValue;
}

void MorseMotionVW::SetVY(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_vy = aValue;
}

void MorseMotionVW::SetW(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_va = aValue;
}

void MorseMotionVW::SetStall(int const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Stall = aValue;
}
