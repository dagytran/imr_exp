/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of MorseMotionVW class.
 */

#ifndef CCMORSE_MORSEMOTIONVW_H
#define CCMORSE_MORSEMOTIONVW_H

#include "MorseActuator.h"


namespace CCMorse{

/**
 * MotionVW class can be used for controlling robots movement via forward velocity V
 * and angular velocity W commands.
 * Can control:
 *  - "Linear and angular speed (V, W) actuator"
 *  - "Differential Driver Actuator: Linear and angular speed (V, W) actuator"
 *  - "Linear and angular speed (Vx, Vy, W) actuator"
 * in Morse simulation.
 *
 * @see https://www.openrobots.org/morse/doc/stable/user/actuators/v_omega.html
 * @see https://www.openrobots.org/morse/doc/stable/user/actuators/v_omega_diff_drive.html
 * @see https://www.openrobots.org/morse/doc/stable/user/actuators/xy_omega.html
 */
class MorseMotionVW : public MorseActuator
{
    friend class MorseClient;
    friend class Position2dProxy;

    public:

        /**
         * Default constructor.
         */
        MorseMotionVW() : MorseActuator() { SetupConfiguration(); }

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorseMotionVW(int aPort, std::string aName) : MorseActuator(aPort, aName) { SetupConfiguration(); }

        /**
         * Destructor.
         */
        ~MorseMotionVW() { }

        /**
         * Sends command to Morse simulation to change speed to set values.
         * Speed can be set in @ref MorseMotionVW::SetSpeed or in appropriate setters.
         */
        void ChangeSpeed() const;

        /**
         * Sends command to Morse simulation to stop the motion, sets speed to (0, 0, 0) and
         * disable motor.
         */
        void Stop();

        /**
         * Returns the value of set forward speed.
         * @return Linear velocity in X direction (forward movement) [m/s].
         */
        double const GetVX() const { return GetVar(m_vx); }

        /**
         * Returns the value of set sideways speed.
         * @return Linear velocity in Y direction (sidewards movement) [m/s].
         */
        double const GetVY() const { return GetVar(m_vy); }

        /**
         * Returns the value of set angular speed.
         * @return Angular velocity [rad/s].
         */
        double const GetW() const { return GetVar(m_va); }

        /**
         * Returns whether the motors are turned on.
         * @return  [@c true: motors running, @c false: motors are stopped]
         */
        bool const IsEnable() const { return GetVar(m_Enabled); }

        /**
         * Returns stall flag.
         */
        int const IsStall() const { return GetVar(m_Stall); }

        /**
         * Sets all speed values.
         *
         * @param   aVX Linear velocity in X direction (forward movement) [m/s].
         * @param   aVY Linear velocity in Y direction (sidewards movement) [m/s].
         * @param   aW  Angular velocity [rad/s].
         */
        void SetSpeed(double const &aVX, double const &aVY, double const &aW);

        /**
         * Sets V and W speed values. Use for non-holomic robots.
         *
         * @param   aV  Linear velocity in X direction (forward movement) [m/s].
         * @param   aW  Angular velocity [rad/s].
         */
        void SetSpeed(double const &aV, double const &aW) { SetSpeed(aV, 0.0, aW); }

        /**
         * Sets forward speed.
         * @param   aValue  Linear velocity in X direction (forward movement) [m/s].
         */
        void SetVX(double const &aValue);

        /**
         * Sets sidewards speed.
         * @param   aValue  Linear velocity in Y direction (sidewards movement) [m/s].
         */
        void SetVY(double const &aValue);

        /**
         * Sets angular speed.
         * @param   aValue  Angular velocity [rad/s].
         */
        void SetW(double const &aValue);

        /**
         * Sets motors enabled.
         * @param   aValue  @c true for turning on motors, @c false for stop
         */
        void SetEnable(bool const &aValue);

        /**
         * Sets stall flag.
         * @param   aValue  @c true for stall, @c false otherwise
         */
        void SetStall(int const &aValue);

        /**
         * Returns string representation of set speeds.
         */
        std::string const ToString() const;

        /**
         * Static function that can be used to assure that given device is MotionVW.
         *
         * @warning This function relies on that device name is same as name of device type.
         * For example device named "motion" is considered as MotionVW device but device
         * named "position" not.
         *
         * @param   aName       Name of the device.
         * @param   aRobotName  Name of the robot that is suppoused to own device.
         * @return  [@c true: is MotionVW device, @c false: not MotionVW device]
         */
        static bool const IsItMe(const std::string &aName, const std::string &aRobotName);

        /**
         * If motors are enabled sends command to Morse simulation.
         */
        virtual void RefreshData();

    private:
        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        double  m_vx,   ///< Linear velocity in X direction (forward movement) [m/s].
                m_vy,   ///< Linear velocity in Y direction (sidewards movement) [m/s].
                m_va;   ///< Angular velocity [rad/s].

        int m_Stall;    ///< Stall flag [0, 1].

        bool m_Enabled; ///< If motor is enabled.

        static std::string idMVW[];                 ///< Array of MotionVW identificators. Used in @ref MorseMotionVW::IsItMe.
        static std::vector<std::string> identifiers;///< Vector of MotionVW identificators. Used in @ref MorseMotionVW::IsItMe.
};
} // namespace CCMorse;

#endif // CCMORSE_MORSEMOTIONVW_H
