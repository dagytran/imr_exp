#include "PlayerClient.h"
#include "MorseClient.h"

#define GET_CLIENT (reinterpret_cast<MorseClient*>(GetMorseClient()))

using namespace CCMorse;


PlayerClient::PlayerClient(const std::string aHostname, int aPort, int aTransport)
{
    MorseClient* client = MorseClient::GetInstance(aHostname, aPort, aTransport);
    m_MorseClient = reinterpret_cast<_MorseClient*>(client);
}

PlayerClient::~PlayerClient()
{
    MorseClient* client = GET_CLIENT;
    delete client;
}

bool PlayerClient::Connected()
{
    return GET_CLIENT->Connected();
}

void PlayerClient::StartThread()
{
    GET_CLIENT->StartThread();
}

void PlayerClient::StopThread()
{
    GET_CLIENT->StopThread();
}

void PlayerClient::Run(int Timeout)
{
    GET_CLIENT->Run(Timeout);
}

void PlayerClient::Stop()
{
    GET_CLIENT->Stop();
}

bool PlayerClient::Peek(uint32_t timeout)
{
    return GET_CLIENT->Peek(timeout);
}

void PlayerClient::SetRequestTimeout(uint32_t seconds)
{
    GET_CLIENT->SetRequestTimeout(seconds);
}

void PlayerClient::SetRetryLimit(int limit)
{
    GET_CLIENT->SetRetryLimit(limit);
}

int PlayerClient::GetRetryLimit()
{
    return GET_CLIENT->GetRetryLimit();
}

void PlayerClient::SetRetryTime(double seconds)
{
    GET_CLIENT->SetRetryTime(seconds);
}

double PlayerClient::GetRetryTime()
{
    return GET_CLIENT->GetRetryTime();
}

void PlayerClient::Read()
{
    GET_CLIENT->Read();
}

void PlayerClient::ReadIfWaiting()
{
    GET_CLIENT->ReadIfWaiting();
}

//void PlayerClient::SetDataMode(uint32_t aMode)
//{
//    GET_CLIENT->SetDataMode(aMode);
//}

//void PlayerClient::SetReplaceRule(bool aReplace, int aType, int aSubtype, int aInterf)
//{
//    GET_CLIENT->SetReplaceRule(aReplace, aType, aSubtype, aInterf);
//}

int PlayerClient::LookupCode(std::string aName) const
{
    return GET_CLIENT->LookupCode(aName);
}

std::string PlayerClient::LookupName(int aCode) const
{
    return GET_CLIENT->LookupName(aCode);
}

//uint32_t PlayerClient::GetOverflowCount()
//{
//    return GET_CLIENT->GetOverflowCount();
//}
