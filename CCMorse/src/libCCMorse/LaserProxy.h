/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of LaserProxy class.
 */

#ifndef CCMORSE_MORSELASERPROXY_H
#define CCMORSE_MORSELASERPROXY_H

#include "MorseProxy.h"
#include "MorseLaserScanner.h"


namespace CCMorse{

/**
 * LaserProxy class.
 * Based on @ref CCMorse::MorseProxy class.
 * Implementing the same interface as PlayerCc::LaserProxy to
 * use it with Morse simulator.
 *
 * @see http://playerstage.sourceforge.net/doc/Player-3.0.2/player/classPlayerCc_1_1LaserProxy.html
 */
class LaserProxy : public MorseProxy
{
    public:
        /**
         * Constructor.
         *
         * @param   aClient Pointer to PlayerClient.
         * @param   aIndex  Index of the proxy.
         */
        LaserProxy(PlayerClient* aClient, int aIndex = 0) : MorseProxy(aClient, aIndex) { SetupConfiguration(); }

        /**
         * Destructor.
         */
        virtual ~LaserProxy() { Unsubscribe(); }

        /**
         * Returns number of points in scan.
         */
        uint32_t GetCount() const { return GetVar(m_Laser->GetScanCount()); }

        /**
         * Returns max range for the latest set of data. [m]
         */
        double GetMaxRange() const { return GetVar(m_Laser->GetMaxRange()); }

        /**
         * Returns angular resolution of scan. [rad]
         */
        double GetScanRes() const { return GetVar(m_Laser->GetScanRes()); }

        /**
         * Returns range resolution of scan. [m]
         */
        double GetRangeRes() const { return GetVar(m_Laser->GetRangeRes()); }

        /**
         * Returns scanning frequency, [Hz]
         */
        double GetScanningFrequency() const { return GetVar(m_Laser->GetScanningFreq()); }

        /**
         * Returns scan range for the latest set of data. [rad]
         */
        double GetMinAngle() const { return GetVar(m_Laser->GetScanStart()); }

        /**
         * Returns scan range for the latest set of data, [rad]
         */
        double GetMaxAngle() const { return (GetMinAngle() + GetCount() * GetScanRes()); }

        /**
         * Returns scan range from the laser config. [rad]
         */
        double GetConfMinAngle() const { return GetVar(m_MinAngle); }

        /**
         * Returns scan range from the laser config. [rad]
         */
        double GetConfMaxAngle() const { return GetVar(m_MaxAngle); }

        /**
         * Returns whether or not intensity (i.e., reflectance) values are being measured.
         */
        bool IntensityOn() const { return (GetVar(m_Laser->IsIntensityOn()) != 0 ? true : false); }

        /**
         * Returns scan data (Cartesian): (X [m], Y [m])
         */
        Point2D_t GetPoint(uint32_t aIndex) const;

        /**
         * Get the range. [m]
         */
        double GetRange(uint32_t aIndex) const { return GetVar(m_Laser->GetRange(aIndex)); }

        /**
         * Get the bearing. [rad]
         */
        double GetBearing(uint32_t aIndex) const { return (GetMinAngle() + aIndex * GetScanRes()); }

        /**
         * Get the intensity. [RSSI]
         */
        int GetIntensity(uint32_t aIndex) const { return (IntensityOn() ? (int) GetVar(m_Laser->GetIntensity(aIndex)) : 0); }

        /**
         * Get the laser ID.
         */
        int GetID() const { return GetVar(m_Laser->GetID()); }

        /**
         * Configure the laser scan pattern.
         *
         * @warning Since Morse simualtion connot be reconfigured after start, this function does nothing.
         *
         * @param   aMinAngle, aMaxAngle, aScanRes    Angles, measured in radians. [rad]
         * @param   aRangeRes   Lenght, measured in meters. [m]
         * @param   aIntensity  If ste to @c true, enables intensity measurements, or @c false to disable.
         * @param   aScanningFrequency  Frquency, measured in Hertz. [Hz]
         */
        void Configure(double aMinAngle, double aMaxAngle, uint32_t aScanRes, uint32_t aRangeRes, bool aIntensity, double aScanningFrequency) {};

        /**
         * Request the current laser configuration; it is read into the
         * relevant class attributes.
         */
        void RequestConfigure();
        void RequestID() {}
        void RequestGeom() {}

        /**
         * Accessor for the pose of the laser with respect to its parent
         * object (e.g., a robot).
         */
        Pose3D_t GetPose() { return GetVar(m_Laser->GetGeoPose()); }

        /**
         * Accessor for the pose of the laser's parent object (e.g., a robot).
         * Filled in by some (but not all) laser data messages.
         */
        Pose3D_t GetRobotPose() { return GetVar(m_Laser->GetRobotPose()); }

        /**
         * Accessor for the size.
         */
        BBox3D_t GetSize() { return GetVar(m_Laser->GetSizeExtent()); }

        /**
         * Minimum range reading on the left side.
         */
        double GetMinLeft() const { return GetVar(m_Laser->GetMinLeft()); }

        /**
         * Minimum range reading on the right side.
         */
        double GetMinRight() const { return GetVar(m_Laser->GetMinRight()); }

        /**
         * @deprecated Minimum range reading on the left side.
         */
        double MinLeft () const { return GetMinLeft(); }

        /**
         * @deprecated Minimum range reading on the right side.
         */
        double MinRight () const { return GetMinRight(); }

        /**
         * Range access operator.  This operator provides an alternate
         * way of access the range data.  For example, given an @p
         * LaserProxy named @p lp, the following expressions are
         * equivalent: @p lp.GetRange(0) and @p lp[0].
         */
        double operator [] (uint32_t index) const { return GetRange(index); }

    private:
        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        /**
         * Subscribe proxy to the robot.
         *
         * @param   aIndex  Index of the robot we want to connect to.
         */
        void Subscribe(uint32_t aIndex);

        /**
         * Unsubscribe from the robot.
         */
        void Unsubscribe();

        MorseLaserScanner *m_Laser; ///< Pointer to MorseLaserScanner sensor.

        // local storage of config
        double  m_MinAngle,     ///< First bearing of the scan. [rad]
                m_MaxAngle,     ///< Last bearing of the scan. [rad]
                m_ScanRes,      ///< Angular resolution of the scan. [rad]
                m_RangeRes,     ///< Range resolution. [m]
                m_ScanFreq;     ///< Scanning frequency. [Hz]

        bool    m_Intensity;    ///< Is intesity data measured.

};
} // namespace CCMorse;

#endif // CCMORSE_MORSELASERPROXY_H
