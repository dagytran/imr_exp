/*
*  SocketException class
*
*  From: http://www.tldp.org/LDP/LG/issue74/tougher.html#4
*  Copyright © 2002, Rob Tougher.
*  Published in Issue 74 of Linux Gazette, January 2002
*/

#ifndef SocketException_class
#define SocketException_class

#include <string>


class SocketException
{
  public:
    SocketException ( std::string s ) : m_s ( s ) {};
    ~SocketException (){};

    std::string description() { return m_s; }

  private:
    std::string m_s;
};

#endif
