#include "MorseClient.h"
#include "MorseDevice.h"
#include "MorsePose.h"
#include "MorseOdometry.h"
#include "MorseLaserScanner.h"
#include "MorseMotionVW.h"
#include "MorseWaypoint.h"
#include "MorseProxy.h"
#include "SndDriver.h"
#include "WaypointDriver.h"
#include "MorsePen.h"

#include <string>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <functional>
#include <time.h>

using namespace CCMorse;


MorseTime* MorseClient::m_Time = NULL;
MorseClient* MorseClient::m_Me = NULL;

MorseClient* MorseClient::GetInstance()
{
    return m_Me;
}

MorseClient* MorseClient::GetInstance(const std::string aHostname, int aPort, int aTransport)
{
    if (!m_Me) {
        m_Me = new MorseClient(aHostname, aPort, aTransport);
        m_Me->SetupTime();
        m_Me->SetupDeviceList();
    }

    return m_Me;
}

MorseClient::MorseClient(const std::string aHostname, int aPort, int aTransport)
{
    m_Hostname  = aHostname;
    m_Port      = aPort;
    m_Transport = aTransport;
    m_Mode      = MORSE_DATAMODE_PUSH;

    m_ConnectionStatus = false;

    #ifdef HAVE_BOOST_THREAD
        m_IsStop = true;
        m_Thread = NULL;
    #endif

    Connect(m_Hostname, m_Port);

    if( !Connected() )
        throw MorseError("MorseClient::MorseClient", "Connections to simulation was not established.");

    std::string reply = SendToSocket("id0 simulation list_robots");
    std::string replySuccess = reply.substr(0, 12);

    if(replySuccess.compare("id0 SUCCESS ") == 0){
        reply = reply.substr(12, (reply.length() - 12));

        std::vector<std::string> robotNames = AsVectorOfStrings(reply);

        for(std::vector<std::string>::iterator name = robotNames.begin(); name != robotNames.end(); ++name){

            std::cout << "MorseClient: Found robot named: " << (*name) << std::endl;
            MorseRobot* robot = new MorseRobot(*name);
            m_Robots.push_back( robot );
            m_RDMap[robot] = NULL;

        }

    } else {
        throw MorseError("MorseClient::MorseClient", "Query for list of all robots was not successful.");
    }
}

MorseClient::~MorseClient()
{
    #ifdef HAVE_BOOST_THREAD
        m_IsStop = true;
        m_Thread = NULL;
    #endif

    Disconnect();

    for(std::vector<MorseRobot*>::iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot)
        delete (*robot);

    for(std::vector<MorseDriver*>::iterator driver = m_Drivers.begin(); driver != m_Drivers.end(); ++driver)
        delete (*driver);

    delete m_Time;
}

void MorseClient::SetupDeviceList()
{
    std::string reply = SendToSocket("id1 simulation get_all_stream_ports");
    std::string replySuccess = reply.substr(0, 12);

    if(replySuccess.compare("id1 SUCCESS ") == 0){

        std::cout << "MorseClient::SetupDeviceList(): Query for all streams was successful." << std::endl;

        Dictionary_t devmap = MapData( reply.substr(12, (reply.length() - 12)) );

        for(Dictionary_t::iterator dev = devmap.begin(); dev != devmap.end(); ++dev) {

            std::string  name = dev->first;
            int          port = AsInteger(dev->second);
            MorseDevice* md   = NULL;

            for(std::vector<MorseRobot*>::iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot){

                bool typeCheck = false, configCheck = false;

                if(!typeCheck && MorsePose::IsItMe(name, (*robot)->GetName())){
                    MorsePose* device = new MorsePose(port, name);
                    device->InitTime();
                    md = device;
                    typeCheck = true;

                    std::cout << "MorseClient::SetupDeviceList(): Setting \"" << name << "\" on Port: " << port << " as MorsePose on \"" << (*robot)->GetName() << "\"." << std::endl;
                }

                if(!typeCheck && MorseOdometry::IsItMe(name, (*robot)->GetName())){
                    MorseOdometry* device = new MorseOdometry(port, name);
                    device->InitTime();
                    md = device;
                    typeCheck = true;

                    std::cout << "MorseClient::SetupDeviceList(): Setting \"" << name << "\" on Port: " << port << " as MorseOdometry on \"" << (*robot)->GetName() << "\"." << std::endl;
                }

                if(!typeCheck && MorseMotionVW::IsItMe(name, (*robot)->GetName())){
                    MorseMotionVW* device = new MorseMotionVW(port, name);
                    device->InitTime();
                    md = device;
                    typeCheck = true;

                    std::cout << "MorseClient::SetupDeviceList(): Setting \"" << name << "\" on Port: " << port << " as MorseMotionVW on \"" << (*robot)->GetName() << "\"." << std::endl;
                }

                if(!typeCheck && MorsePen::IsItMe(name, (*robot)->GetName())){
                    MorsePen* device = new MorsePen(port, name);
                    device->InitTime();
                    md = device;
                    typeCheck = true;

                    std::cout << "MorseClient::SetupDeviceList(): Setting \"" << name << "\" on Port: " << port << " as MorsePen on \"" << (*robot)->GetName() << "\"." << std::endl;
                }

                if(!typeCheck && MorseLaserScanner::IsItMe(name, (*robot)->GetName())){
                    MorseLaserScanner* device = new MorseLaserScanner(port, name);
                    device->InitTime();
                    device->SetIntensityOn((*robot)->IsLaserRssi());
                    md = device;
                    typeCheck = true;

                    std::cout << "MorseClient::SetupDeviceList(): Setting \"" << name << "\" on Port: " << port << " as MorseLaserScanner on \"" << (*robot)->GetName() << "\"." << std::endl;
                }

                if(!typeCheck && MorseWaypoint::IsItMe(name, (*robot)->GetName())){
                    MorseWaypoint* device = new MorseWaypoint(port, name);
                    device->InitTime();
                    md = device;
                    typeCheck = true;

                    std::cout << "MorseClient::SetupDeviceList(): Setting \"" << name << "\" on Port: " << port << " as MorseWaypoint on \"" << (*robot)->GetName() << "\"." << std::endl;
                }

                if(!typeCheck && MorseProperties::IsItMe(name, (*robot)->GetName())){
                    MorseProperties* config = new MorseProperties(port, name);
                    (*robot)->SetConfiguration(config);
                    typeCheck = true;
                    configCheck = true;

                    std::cout << "MorseClient::SetupDeviceList(): Setting \"" << name << "\" on Port: " << port << " as MorseProperties on \"" << (*robot)->GetName() << "\"." << std::endl;
                }

                if(!typeCheck){
                    std::size_t p = name.find_first_of('.');
                    std::string r = name.substr(0, p);

                    if((*robot)->GetName().compare(r) == 0){
                        MorseDevice *device = new MorseDevice(port, name);
                        device->InitTime();
                        md = device;
                        typeCheck = true;

                        std::cout << "MorseClient::SetupDeviceList(): Setting unknown device \"" << name << "\" on Port: " << port << " as MorseDevice on \"" << (*robot)->GetName() << "\"." << std::endl;
                    }
                }

                if(typeCheck && !configCheck)
                    (*robot)->AddDevice(md);
            }
        }

    } else {
        throw MorseError("MorseClient::SetupDeviceList", "Error occured while asking for available devices.");
    }

    for(std::vector<MorseRobot*>::iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot) {
        if( (*robot)->GetDevices().size() > 1) {
            (*robot)->PrintRobotSummary();
        }
    }

}

void MorseClient::SetupTime()
{
    m_Me = this;
    m_Time = new MorseTime(m_Port);

    std::cout.unsetf(std::ios_base::floatfield);
    std::cout << std::setprecision(16);
    std::cout << "MorseClient::SetupTime(): Time = " << NumberToString(GetTimeNow()) << ", Mode = " << m_Time->GetMode();
}

MorseDevice* const MorseClient::GetDevice(std::string const &aName) const
{
    for(std::vector<MorseRobot*>::const_iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot) {
        MorseDevice* device = (*robot)->GetDevice(aName);
        if (device->GetID() != -1)
            return device;
    }

    //throw MorseError("MorseClient::GetDevice(" + aName + ")", "Device with this name was not found.");
    std::cout << "MorseClient::GetDevice(" + aName + "): Device with this name was not found. Returning default device." << std::endl;
    return ( new MorseDevice() );
}

MorseDevice* const MorseClient::GetDevice(int aID) const
{
    for(std::vector<MorseRobot*>::const_iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot) {
        MorseDevice* device = (*robot)->GetDevice(aID);
        if (device->GetID() != -1)
            return device;
    }

    //throw MorseError("MorseClient::GetDevice(" + NumberToString(aID) + ")", "Device with this ID was not found.");
    std::cout << "MorseClient::GetDevice(" + NumberToString(aID) + "): Device with this ID was not found. Returning default device." << std::endl;
    return ( new MorseDevice() );
}

std::string const MorseClient::SendToSocket(const std::string &aMessage) const
{
    ScopedLock_t lock( GetMutex() );
    return ( CCMorse::SendToSocket( (*m_ClientSocket), aMessage ) );
}

int const MorseClient::GetDevicePort(std::string const &aDeviceName)
{
    std::string reply = SendToSocket("id simulation get_stream_port [\"" + aDeviceName + "\"]");
    std::string replySucces = reply.substr(0, 11);

    if(replySucces.compare("id SUCCESS ") == 0)
        return StringToNumber(reply.substr(11, reply.length() - 11), 0);

    //throw MorseError("MorseClient::GetDevicePort", "Error has occured while asking for device stream port.");
    std::cout << "MorseClient::GetDevicePort(" + aDeviceName + "): Error has occured while asking for device stream port. Returning \"-1\"." << std::endl;
    return -1;
}

void MorseClient::EraseDevice(std::string const &aName)
{
    if(aName.compare("time") == 0)
        return;

    for(std::vector<MorseRobot*>::iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot)
        (*robot)->EraseDevice(aName);
}

void MorseClient::EraseDevice(MorseDevice* aDevice)
{
    if(aDevice == m_Time)
        return;

    for(std::vector<MorseRobot*>::iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot)
        (*robot)->EraseDevice(aDevice);
}

MorseRobot* const MorseClient::GetRobotOwningDevice(MorseDevice* const aDevice) const
{
    //ScopedLock_t lock( GetMutex() );
    for(std::vector<MorseRobot*>::const_iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot){
        MorseDevice* device = (*robot)->GetDevice( aDevice->GetID() );
        if(device->GetID() != (-1))
            return (*robot);
    }

    //throw MorseError("MorseClient::GetRobotOwningDevice", "Robot not found.");
    std::cout << "MorseClient::GetRobotOwningDevice(): Robot not found. Returning NULL." << std::endl;
    return NULL;
}

MorseRobot* const MorseClient::GetRobotProvidingPosition2d(int aIndex) const
{
    //ScopedLock_t lock( GetMutex() );
    std::vector<int> indexes;

    for(std::vector<MorseRobot*>::const_iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot){

        indexes = (*robot)->GetProvidesPosition2dIndexList();
        indexes.insert(indexes.begin(), (*robot)->GetDriverProvidesPosition2dIndexList().begin(), (*robot)->GetDriverProvidesPosition2dIndexList().end());

        for(std::vector<int>::iterator index = indexes.begin(); index != indexes.end(); ++index)
            if( (*index) == aIndex )
                return (*robot);

        indexes.clear();
    }

    //throw MorseError("MorseClient::GetRobotProvidingPosition2d", "Robot not found.");
    std::cout << "MorseClient::GetRobotProvidingPosition2d(): Robot not found. Returning NULL." << std::endl;
    return NULL;
}

MorseRobot* const MorseClient::GetRobotProvidingLaser(int aIndex) const
{
    //ScopedLock_t lock( GetMutex() );
    std::vector<int> indexes;

    for(std::vector<MorseRobot*>::const_iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot){

        indexes = (*robot)->GetProvidesLaserIndexList();
        indexes.insert(indexes.begin(), (*robot)->GetDriverProvidesLaserIndexList().begin(), (*robot)->GetDriverProvidesLaserIndexList().end());

         for(std::vector<int>::iterator index = indexes.begin(); index != indexes.end(); ++index)
            if( (*index) == aIndex )
                return (*robot);

        indexes.clear();
    }

    //throw MorseError("MorseClient::GetRobotProvidingLaser", "Robot not found.");
    std::cout << "MorseClient::GetRobotProvidingLaser(): Robot not found. Returning NULL." << std::endl;
    return NULL;
}

MorseRobot* const MorseClient::GetRobotWithID(int aRobotID) const
{
    //ScopedLock_t lock( GetMutex() );
    for(std::vector<MorseRobot*>::const_iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot)
        if( (*robot)->GetID() == (unsigned) aRobotID )
            return (*robot);

    //throw MorseError("MorseClient::GetRobotWithID", "Robot not found.");
    std::cout << "MorseClient::GetRobotWithID(" + NumberToString(aRobotID) + "): Robot with this ID was not found. Returning NULL." << std::endl;
    return NULL;
}

std::vector<MorseProxy*> const MorseClient::GetProxyList() const
{
    std::vector<MorseProxy*> output;

    for(std::vector<MorseRobot*>::const_iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot)
        output.insert(output.begin(), (*robot)->GetProxies().begin(), (*robot)->GetProxies().end());

    return output;
}

std::vector<MorseProxy*> const MorseClient::GetProxiesSubscribedToRobot(int aRobotID) const
{
    return (GetProxiesSubscribedToRobot(GetRobotWithID(aRobotID)));
}

std::vector<MorseProxy*> const MorseClient::GetProxiesSubscribedToRobot(MorseRobot* const aRobot) const
{
    return (aRobot->GetSubscribedProxies());
}

MorseDriver* const MorseClient::GetDriverForRobot(MorseRobot* const aRobot)
{
    //ScopedLock_t lock( GetMutex() );
    MorseDriver* output = m_RDMap[aRobot];

    if( output != NULL )
        return output;

    output = DriverFactory(aRobot);

    if( output != NULL ) {
        return output;

    } else {
        throw MorseError("MorseClient::GetDriverForRobot()", "Driver was not found or could not be created.");
    }
}

MorseDriver* const MorseClient::DriverFactory(MorseRobot* const aRobot)
{
    MorseDriver* driver = NULL;

    if( aRobot->GetDriverName().compare("snd") == 0 && SndDriver::HasRobotEveryProxy(aRobot) ){
        //std::cout << "MorseClient::DriverFactory(): Creating SndDriver for robot named " << aRobot->GetName() << std::endl;
        driver = new SndDriver(aRobot);
    }

    if( aRobot->GetDriverName().compare("waypoint") == 0 && WaypointDriver::HasRobotEveryProxy(aRobot) ){
        //std::cout << "MorseClient::DriverFactory(): Creating WaypointDriver for robot named " << aRobot->GetName() << std::endl;
        driver = new WaypointDriver(aRobot);
    }

    if( driver != NULL ){
        //ScopedLock_t lock( GetMutex() );
        m_Drivers.push_back( driver );
        m_RDMap[aRobot] = driver;

    } else {
        throw MorseError("MorseClient::DriverFactory()", "Driver was not created.");
    }

    return driver;
}

void MorseClient::Read()
{
    //ScopedLock_t lock( GetMutex() );
    assert(NULL != this);

    GetTime()->RefreshData();

    for(std::vector<MorseRobot*>::iterator robot = m_Robots.begin(); robot != m_Robots.end(); ++robot)
        (*robot)->RefreshDevices();
}

void MorseClient::ReadIfWaiting()
{
    if(Peek())
        Read();
}

void MorseClient::Connect(const std::string aHostname, uint32_t aPort)
{
    assert("" != aHostname);
    assert(0  != aPort);

    //std::cout << "LOG: Connecting " << *this << std::endl;

    try{    // Try connect with Morse simulation.
        m_ClientSocket = new ClientSocket( GetHostname(), GetPort() );

    } catch ( SocketException& e ){
        m_ConnectionStatus = false;
        std::cerr << "Exception was caught:" << e.description() << std::endl;
    }

    m_ConnectionStatus = true;
}

void MorseClient::Disconnect()
{
    //std::cout << "LOG: Disconnecting " << *this << std::endl;

    for(std::vector<MorseProxy*>::const_iterator proxy = GetProxyList().begin(); proxy != GetProxyList().end(); ++proxy)
        (*proxy)->Unsubscribe();

    delete m_ClientSocket;
}

void MorseClient::StartThread()
{
    #ifdef HAVE_BOOST_THREAD
        assert(NULL == m_Thread);
        m_Thread = new boost::thread(boost::bind(&MorseClient::RunThread, this));
        m_IsStop = false;
    #else
        throw MorseError("MorseClient::StartThread", "Thread support not included.");
    #endif
}

void MorseClient::StopThread()
{
    #ifdef HAVE_BOOST_THREAD
        Stop();
        assert(m_Thread);
        m_Thread->join();
        delete m_Thread;
        m_Thread = NULL;
        std::cout << "MorseClient::StopThread(): Joined." << std::endl;
    #else
        throw MorseError("MorseClient::StopThread", "Thread support not included.");
    #endif
}

void MorseClient::RunThread()
{
    #ifdef HAVE_BOOST_THREAD
        m_IsStop = false;
        std::cout << "MorseClient::RunThread(): Starting run..." << std::endl;

        while (!m_IsStop){
            if( m_Mode == MORSE_DATAMODE_PUSH ){
                if (Peek()){
                    Read();
                }
            } else {
                Read();
            }

            boost::xtime xt;
            boost::xtime_get(&xt, boost::TIME_UTC_);
            boost::thread::sleep(xt); // we sleep for 0 seconds
        }
    #else
        throw MorseError("MorseClient::RunThread", "Thread support not included.");
    #endif
}

void MorseClient::Run(int Timeout)
{
    timespec sleep = {0, Timeout*1000000};
    m_IsStop = false;
    std::cout << "PRINT: starting run" << std::endl;

    while (!m_IsStop) {
        if(m_Mode == MORSE_DATAMODE_PUSH)
            ReadIfWaiting();
        else
            Read();

        nanosleep(&sleep, NULL);
    }
}

void MorseClient::Stop()
{
    m_IsStop = true;
}

bool MorseClient::Peek(uint32_t Timeout)
{
    ////boost::mutex::scoped_lock lock(m_Mutex);
    // dont know how to implement this with Morse
/*
    bool newData = false;
    std::vector<MorseProxy*> proxies = GetRobotWithID(0)->GetSubscribedProxies();

    Read();
    for(std::vector<MorseProxy*>::iterator p = proxies.begin(); p != proxies.end(); ++p)
        newData = newData && !((*p)->IsValid());

    return newData;
*/
    return false;
}

int MorseClient::LookupCode(std::string aName) const
{
    const int index = GetDevice(aName)->GetID();

    if(index == -1)
        throw MorseError("MorseClient::LookupCode", "Device of this name isn't connected to client");

    return index;
}

std::string MorseClient::LookupName(int aCode) const
{
    const std::string name = GetDevice(aCode)->GetName();

    if(name.compare("") == 0)
        throw MorseError("MorseClient::LookupName", "Device with this ID/Port isn't connected to client");

    return name;
}

uint32_t MorseClient::GetOverflowCount()
{
    return 0;
}

void MorseClient::SetDataMode(uint32_t aMode) { }

void MorseClient::SetReplaceRule(bool aReplace, int aType, int aSubtype, int aInterf) { }
