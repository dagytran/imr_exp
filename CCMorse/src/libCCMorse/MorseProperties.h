/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of MorseProperties class.
 */

#ifndef CCMORSE_MORSEPROPERTIES_H
#define CCMORSE_MORSEPROPERTIES_H

#include "MorseSensor.h"


namespace CCMorse {

/**
 * Robot Properties class that holds configuration for robot and can
 * carry all sorts of data from Morse simulation.
 *
 * Is based on #MorseSensor class.
 */
class MorseProperties : public MorseSensor
{
    public:
        /**
         * Default constructor.
         */
        MorseProperties() : MorseSensor() { SetupConfiguration(); }

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorseProperties(int aPort, std::string aName) : MorseSensor(aPort, aName) { SetupConfiguration(); }

        /**
         * Destructor.
         */
        ~MorseProperties()
        {
            m_ProvidesGlobalPosition2dIndexList.clear();
            m_ProvidesLocalPosition2dIndexList.clear();
            m_ProvidesLaserIndexList.clear();
            m_ProvidesWindowIndexList.clear();
            m_DriverProvidesPosition2dIndexList.clear();
            m_DriverRequiresInputPosition2dIndexList.clear();
            m_DriverRequiresOutputPosition2dIndexList.clear();
            m_DriverProvidesLaserIndexList.clear();
            m_DriverRequiresInputLaserIndexList.clear();
            m_DriverRequiresOutputLaserIndexList.clear();
        }

        /**
         * Gets fresh data from sensor.
         * @warning Does nothing with this device. Must be implemented because it's virtual.
         */
        virtual void RefreshData() { }

        /**
         * Static function that can be used to assure that given device is Properties.
         *
         * @warning This function relies on that device name is same as name of device type.
         * For example device named "properties" or "config" is considered as Properties device
         * but device named "laser" not.
         *
         * @param   aName       Name of the device.
         * @param   aRobotName  Name of the robot that is suppoused to own device.
         * @return  [@c true: is Properties device, @c false: not Properties device]
         */
        static bool const IsItMe(const std::string &aName, const std::string &aRobotName);

        /**
         * Returns width of the robot from direction of movement.
         * @return  Robot width. [m]
         */
        double const GetRobotWidth() const { return GetVar(m_RobotWidth); }

        /**
         * Returns length of the robot.
         * @return  Robot lenght. [m]
         */
        double const GetRobotLength() const { return GetVar(m_RobotLength); }

        /**
         * Returns radius of the robot. The radius of the circle into which the entire robot physically fits.
         * @return  Robot radius. [m]
         */
        double const GetRobotRadius() const { return GetVar(m_RobotRadius); }

        /**
         * Returns mass of the robot.
         * @return  Robot mass. [kg]
         */
        double const GetRobotMass() const { return GetVar(m_RobotMass); }

        /**
         * Returns scanning frequency of robots laser scanner.
         * @return  Laser scanner frequency. [Hz]
         */
        double const GetLaserFreq() const { return GetVar(m_LaserFreq); }

        /**
         * Returns maximal forward speed of the robot.
         * @return  Max forward speed. [m/s]
         */
        double const GetMaxSpeed() const { return GetVar(m_MaxSpeed); }

        /**
         * Returns maximal angular speed of the robot.
         * @return  Max angular speed. [rad/s]
         */
        double const GetMaxTurnRate() const { return GetVar(m_MaxTurnRate); }

        /**
         * Returns distance of the main wheels.
         * @return Wheel distance. [m]
         */
        double const GetWheelsDist() const { return GetVar(m_WheelsDist); }

        /**
         * Returns minimal width of gap that robot can go thru.
         * @return Minimal width. [m]
         */
        double const GetMinGapWidth() const { return GetVar(m_MinGapWidth); }

        /**
         * Returns maximal avoiding distance from obstacle.
         * @return Maximal avoiding distance. [m]
         */
        double const GetSafetyDistMax() const { return GetVar(m_SafetyDistMax); }

        /**
         * Returns maximum distance allowed from the final goal for the driver algorithm to stop.
         * @return  Goal position tolerance. [m]
         */
        double const GetGoalPositionTol() const { return GetVar(m_GoalPositionTol); }

        /**
         * Returns maximum angular error from the final goal position for the driver algorithm to stop.
         * @return  Goal angular tolerance. [rad]
         */
        double const GetGoalAngleTol() const { return GetVar(m_GoalAngleTol); }

        /**
         * Returns whether Laser Scanner should measure intensities.
         */
        bool const IsLaserRssi() const { return GetVar(m_LaserRssi); }

        /**
         * Returns name of the color of the robot.
         * @return  Color name string.
         */
        std::string const GetRobotColor() const { return GetVar(m_RobotColor); }

        /**
         * Returns name of the driver algorithm that robot is configured to use.
         * @return  Driver name string. {"waypoint", "snd"}
         */
        std::string const GetDriver() const { return GetVar(m_Driver); }

        /**
         * Returns vector of Position2DProxy indexes (global and local) that this robot is configured to provide.
         */
        std::vector<int> const GetProvidesPosition2dIndexList() const;

        /**
         * Returns vector of Position2DProxy::global indexes that this robot is configured to provide.
         */
        std::vector<int> const &GetProvidesGlobalPosition2dIndexList() const { return (m_ProvidesGlobalPosition2dIndexList); }

        /**
         * Returns vector of Position2DProxy::local indexes that this robot is configured to provide.
         */
        std::vector<int> const &GetProvidesLocalPosition2dIndexList() const { return (m_ProvidesLocalPosition2dIndexList); }

        /**
         * Returns vector of LaserProxy indexes that this robot is configured to provide.
         */
        std::vector<int> const &GetProvidesLaserIndexList() const { return (m_ProvidesLaserIndexList); }

        /**
         * Returns vector of WindowProxy indexes that this robot is configured to provide.
         */
        std::vector<int> const &GetProvidesWindowIndexList() const { return (m_ProvidesWindowIndexList); }

        /**
         * Returns vector of Position2DProxy indexes that driver of this robot is configured to provide.
         */
        std::vector<int> const &GetDriverProvidesPosition2dIndexList() const { return (m_DriverProvidesPosition2dIndexList); }

        /**
         * Returns vector of Position2DProxy indexes that driver of this robot is require after robot as inputs.
         */
        std::vector<int> const &GetDriverRequiresInputPosition2dIndexList() const { return (m_DriverRequiresInputPosition2dIndexList); }

        /**
         * Returns vector of Position2DProxy indexes that driver of this robot is require after robot as outputs.
         */
        std::vector<int> const &GetDriverRequiresOutputPosition2dIndexList() const { return (m_DriverRequiresOutputPosition2dIndexList); }

        /**
         * Returns vector of LaserProxy indexes that driver of this robot is configured to provide.
         */
        std::vector<int> const &GetDriverProvidesLaserIndexList() const { return (m_DriverProvidesLaserIndexList); }

        /**
         * Returns vector of LaserProxy indexes that driver of this robot is require after robot as inputs.
         */
        std::vector<int> const &GetDriverRequiresInputLaserIndexList() const { return (m_DriverRequiresInputLaserIndexList); }

        /**
         * Returns vector of LaserProxy indexes that driver of this robot is require after robot as outputs.
         */
        std::vector<int> const &GetDriverRequiresOutputLaserIndexList() const { return (m_DriverRequiresOutputLaserIndexList); }

    private:
        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        // Provides proxy:
        std::vector<int> m_ProvidesGlobalPosition2dIndexList;       ///< Vector of Position2DProxy::global indexes that this robot is configured to provide.
        std::vector<int> m_ProvidesLocalPosition2dIndexList;        ///< Vector of Position2DProxy::local indexes that this robot is configured to provide.
        std::vector<int> m_ProvidesLaserIndexList;                  ///< Vector of LaserProxy indexes that this robot is configured to provide.
        std::vector<int> m_ProvidesWindowIndexList;                 ///< Vector of WindowProxy indexes that this robot is configured to provide.
        std::vector<int> m_DriverProvidesPosition2dIndexList;       ///< Vector of Position2DProxy indexes that driver of this robot is configured to provide.
        std::vector<int> m_DriverRequiresInputPosition2dIndexList;  ///< Vector of Position2DProxy indexes that driver of this robot is require after robot as inputs.
        std::vector<int> m_DriverRequiresOutputPosition2dIndexList; ///< Vector of Position2DProxy indexes that driver of this robot is require after robot as outputs.
        std::vector<int> m_DriverProvidesLaserIndexList;            ///< Vector of LaserProxy indexes that driver of this robot is configured to provide.
        std::vector<int> m_DriverRequiresInputLaserIndexList;       ///< Vector of LaserProxy indexes that driver of this robot is require after robot as inputs.
        std::vector<int> m_DriverRequiresOutputLaserIndexList;      ///< Vector of LaserProxy indexes that driver of this robot is require after robot as outputs.

        // Robot geometry and configuration:
        double  m_RobotWidth,       ///< Width of the robot from direction of movement. [m]
                m_RobotLength,      ///< Length of the robot. [m]
                m_RobotRadius,      ///< Radius of the robot. [m]
                m_RobotMass,        ///< Mass of the robot. [kg]
                m_LaserFreq,        ///< Scanning frequency of laser scanner. [Hz]
                m_MaxSpeed,         ///< Maximal speed of the robot. [m/s]
                m_MaxTurnRate,      ///< Maximal turning rate of the robot. [rad/s]
                m_WheelsDist;       ///< Distance of the main wheels. [m]

        bool    m_LaserRssi;        ///< If the laser device shoud measure intensity. [true/false]

        // Driver configuration:
        double  m_MinGapWidth,      ///< Minimal width of gap that robot can go thru. [m]
                m_SafetyDistMax,    ///< Maximal avoiding distance from obstacle. [m]
                m_GoalPositionTol,  ///< Maximum distance allowed from the final goal fot the algorithm to stop. [m]
                m_GoalAngleTol;     ///< Maximum angular error from the final goal position for the algorithm to stop. [rad]

        std::string m_RobotColor;   ///< Color of the robot.
        std::string m_Driver;       ///< Prefered type of driver for CCMorse::Position2dProxy::GoTo() function.

        static std::string idPR[];  ///< Array of Properties identificators. Used in @ref MorseProperties::IsItMe.
        static std::vector<std::string> identifiers;    ///< Vector of Properties identificators. Used in @ref MorseProperties::IsItMe.

};
} // namespace CCMorse

#endif // CCMORSE_MORSEPROPERTIES_H
