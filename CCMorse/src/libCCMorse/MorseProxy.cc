#include "MorseProxy.h"
#include "MorseClient.h"

#define GET_CLIENT ( reinterpret_cast<MorseClient*>(GetPlayerClient()->GetMorseClient()) )

using namespace CCMorse;


int MorseProxy::s_IDCounter = 0;

MorseProxy::MorseProxy(CCMorse::PlayerClient* aClient, int aIndex)
{
    if( aClient == NULL )
        throw MorseError("MorseProxy::MorseProxy", "Client is NULL.");

    m_Name   = "MorseProxy";
    m_Client = aClient;
    m_Index  = aIndex;
    m_ID     = s_IDCounter;

    s_IDCounter++;

    m_Fresh    = false;
    m_Valid    = false;
    m_LastTime = 0;

    m_SubscribedToRobotID = -1;
}

MorseProxy::MorseProxy(MorseProxy const &aOther)
{
    m_Client = aOther.m_Client;
    if( m_Client == NULL )
        throw MorseError("MorseProxy::MorseProxy", "Client is NULL.");

    m_Index  = aOther.m_Index;
    m_ID     = aOther.m_ID;
    m_Name   = aOther.m_Name;

    m_Fresh    = aOther.m_Fresh;
    m_Valid    = aOther.m_Valid;
    m_LastTime = aOther.m_LastTime;

    m_SubscribedToRobotID = aOther.m_SubscribedToRobotID;
}

MorseProxy::MorseProxy()
{
    m_Name   = "DefaultProxy";
    m_Client = NULL;
    m_Index  = -1;
    m_ID     = s_IDCounter;

    s_IDCounter++;

    m_Fresh    = false;
    m_Valid    = false;
    m_LastTime = 0;

    m_SubscribedToRobotID = -1;
}


MorseProxy::~MorseProxy()
{
    Unsubscribe();
    // each client needs to unsubscribe themselves,
    // but we will take care of removing them from the list
    GET_CLIENT->GetRobotWithID(m_SubscribedToRobotID)->EraseProxy( this );
}

void MorseProxy::SetName(const std::string &aName)
{
    ScopedLock_t lock( GetMutex() );
    m_Name = aName;
}

void MorseProxy::CheckDataValidity()
{
    ScopedLock_t lock( GetMutex() );

    m_LastTime      = m_Device->GetLastTime();
    double dataTime = m_Device->GetDataTime();

    //std::cout << "MorseProxy::CheckDataValidity(): Last = " << m_LastTime << ", Now = " << dataTime << std::endl;

    if (dataTime > m_LastTime){
        m_Fresh = true;
        m_Valid = true;
        m_LastTime = dataTime;

    } else {
        m_Valid = false;
    }
}

void MorseProxy::NotFresh()
{
    m_Device->SetFresh(false);
    ScopedLock_t lock( GetMutex() );
    m_Fresh = false;
}

std::string MorseProxy::GetDriverName() const
{
    return GET_CLIENT->GetRobotWithID(m_SubscribedToRobotID)->GetDriverName();
}



/*
void MorseProxy::ReadSignal()
{
    // only emit a signal when the interface has received data
    m_LastTime = m_Device->GetLastTime();
    double dataTime = m_Device->GetDataTime();

    //std::cout << "MorseProxy::ReadSignal(): Last = " << m_LastTime << ", Now = " << dataTime << std::endl;

    if (dataTime > m_LastTime){
        ScopedLock_t lock( GetMutex() );
        m_Fresh = true;
        m_Valid = true;
        m_LastTime = dataTime;

        //#ifdef HAVE_BOOST_SIGNALS
        //    m_ReadSignal();
        //#endif
    } else {
        m_Valid = false;
    }
}
*/
