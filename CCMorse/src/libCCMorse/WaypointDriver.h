/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-18
 *
 * File contains definition of WaypointDriver class.
 */

#ifndef CCMORSE_WAYPOINTDRIVER_H
#define CCMORSE_WAYPOINTDRIVER_H

#include "MorseDriver.h"
#include "MorseWaypoint.h"


namespace CCMorse {

/**
 * WaypointDriver class is used to navigate robot to its goal position.
 * Based on @ref CCMorse::MorseDriver class and
 * using @ref CCMorse::MorseWaypoint as driving device.
 */
class WaypointDriver : public MorseDriver
{
    public:
        /**
         * Constructor.
         * @param   aRobot  Pointer to robot that this driver will control.
         */
        WaypointDriver(MorseRobot* aRobot) : MorseDriver(aRobot) { SetupProxies(); }

        /**
         * Destructor.
         */
        virtual ~WaypointDriver() {};

        /**
         * Navigates robot on Goal defined in MorseRobot::SetGoal() function.
         */
        void GoTo();

        /**
         * Static function, checks if robot have all proxies that are requered by
         * this driver.
         *
         * @param   aRobot  Pointer to robot that this driver will check.
         * @return  [@c true: Robot have all needed proxies, @c false: Otherwise]
         */
        static bool HasRobotEveryProxy(MorseRobot* aRobot);

    private:
        /**
         * Saves pointers to proxies in their member variables for storage.
         */
        void SetupProxies();

        MorseWaypoint* m_Waypoint;  ///< Pointer to Waypoint device.

};
}   // namespace CCMorse;

#endif // CCMORSE_WAYPOINTDRIVER_H
