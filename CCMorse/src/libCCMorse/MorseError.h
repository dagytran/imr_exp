/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of MorseError class.
 */

#ifndef CCMORSE_MORSEERROR_H
#define CCMORSE_MORSEERROR_H

#include <string>
#include <iostream>

namespace CCMorse{


/**
 * The simple exception class.
 * When @em libCCMorse receives an error it throws a MorseError exception.
 */
class MorseError
{
    public:
        /**
         * Default constructor.
         *
         * @param aFunction Name of the function that whrows exception.
         * @param aStr      Message of the exception.
         * @param aCode     Error code.
         */
        MorseError(const std::string aFunction = "", const std::string aStr = "", const int aCode = -1);

        /**
         * Destructor.
         */
        ~MorseError() {}

        /**
         * Returns the error string.
         */
        std::string GetErrorStr() const { return m_Str; };

        /**
         * Returns the function that threw the error.
         */
        std::string GetErrorFun() const { return m_Function; };

        /**
         * Returns a numerical error code.
         */
        int GetErrorCode() const { return m_Code; };

    private:
        std::string m_Str;      ///< String describing the error.
        std::string m_Function; ///< String describing the location of the error in the source.

        int m_Code;             ///< Error code.

};
}   // namespace CCMorse;

namespace std
{
    std::ostream& operator << (std::ostream& os, const CCMorse::MorseError& e);
}

#endif // CCMORSE_MORSEERROR_H
