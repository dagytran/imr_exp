/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of MorsePose class.
 */

#ifndef CCMORSE_MORSEPOSE_H
#define CCMORSE_MORSEPOSE_H

#include "MorseSensor.h"


namespace CCMorse{

/**
 * Pose class can be used for obtaining data from pose
 * sensor in Morse simulation.
 *
 * @see https://www.openrobots.org/morse/doc/stable/user/sensors/pose.html
 */
class MorsePose : public MorseSensor
{
    friend class MorseClient;
    friend class Position2dProxy;

    public:
        /**
         * Default constructor.
         */
        MorsePose() : MorseSensor() { SetupConfiguration(); }

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorsePose(int aPort, std::string aName) : MorseSensor(aPort, aName) { SetupConfiguration(); }

        /**
         * Destructor.
         */
        ~MorsePose() { }

        /**
         * Returns value of X in cooridate system.
         * @return  X cooridate of position. [m]
         */
        double const GetX() const     { return GetVar(m_X); }

        /**
         * Returns value of Y in cooridate system.
         * @return  Y cooridate of position. [m]
         */
        double const GetY() const     { return GetVar(m_Y); }

        /**
         * Returns value of Y in cooridate system.
         * @return  Y cooridate of position. [m]
         */
        double const GetZ() const     { return GetVar(m_Z); }

        /**
         * Returns value of Yaw of the sensor.
         * @return  Yaw. Rotation angle with respect to the Z axis. [rad]
         */
        double const GetYaw() const   { return GetVar(m_Yaw); }

        /**
         * Returns value of Pitch of the sensor.
         * @return  Pitch. Rotation angle with respect to the Y axis. [rad]
         */
        double const GetPitch() const { return GetVar(m_Pitch); }

        /**
         * Returns value of Roll of the sensor.
         * @return  Roll. Rotation angle with respect to the X axis. [rad]
         */
        double const GetRoll() const  { return GetVar(m_Roll); }

        /**
         * Returns absolute postion of the sensor in 2D.
         * @return 2D Position (X [m], Y [m], Yaw [rad]).
         */
        Pose2D_t const GetPose2D() const;

        /**
         * Returns absolute postion of the sensor in 3D.
         * @return 3D Position (X [m], Y [m], Z[m], Yaw [rad], Pitch [rad], Roll [rad]).
         */
        Pose3D_t const GetPose3D() const;

        /**
         * Gets new position data from Morse simulation.
         */
        virtual void RefreshData();

        /**
         * Returns string representation of position data stored in sensor.
         */
        std::string const ToString() const;

        /**
         * Static function that can be used to assure that given device is Pose.
         *
         * @warning This function relies on that device name is same as name of device type.
         * For example device named "pose" is considered as Pose device but device
         * named "laser" not.
         *
         * @param   aName       Name of the device.
         * @param   aRobotName  Name of the robot that is suppoused to own device.
         * @return  [@c true: is Pose device, @c false: not Pose device]
         */
        static bool const IsItMe(const std::string &aName, const std::string &aRobotName);

    private:
        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        /**
         * Sets X value.
         */
        void SetX(double const &aValue);

        /**
         * Sets Y value.
         */
        void SetY(double const &aValue);

        /**
         * Sets Z value.
         */
        void SetZ(double const &aValue);

        /**
         * Sets Yaw value.
         */
        void SetYaw(double const &aValue);

        /**
         * Sets Pitch value.
         */
        void SetPitch(double const &aValue);

        /**
         * Sets Roll value.
         */
        void SetRoll(double const &aValue);

        double  m_X,        ///< X coordinate of the sensor, in world coordinate. [m]
                m_Y,        ///< Y coordinate of the sensor, in world coordinate. [m]
                m_Z,        ///< Z coordinate of the sensor, in world coordinate. [m]
                m_Yaw,      ///< Rotation around the Z axis of the sensor. [rad]
                m_Pitch,    ///< Rotation around the Y axis of the sensor. [rad]
                m_Roll;     ///< Rotation around the X axis of the sensor. [rad]

        static std::string idP[];   ///< Array of Pose identificators. Used in @ref MorsePose::IsItMe.
        static std::vector<std::string> identifiers;    ///< Vector of Pose identificators. Used in @ref MorsePose::IsItMe.
};
} // namespace CCMorse;

#endif // CCMORSE_MORSEPOSE_H
