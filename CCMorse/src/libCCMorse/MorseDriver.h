/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of MorseDriver class.
 */

#ifndef CCMORSE_MORSEDRIVER_H
#define CCMORSE_MORSEDRIVER_H

#include "MorseRobot.h"

namespace CCMorse {

/**
 * MorseDriver class is base class for driver algorithms that are called from
 * MorseRobot::GoTo() function.
 */
class MorseDriver
{
    public:
        /**
         * Constructor.
         * @param aRobot    Pointer to robot that this driver will control.
         */
        MorseDriver(MorseRobot* aRobot);

        /**
         * Destructor.
         */
        virtual ~MorseDriver();

        /**
         * Navigate robot on Goal defined in MorseRobot::SetGoal() function.
         */
        virtual void GoTo();

    protected:
        /**
         * Returns pointer to controlled robot.
         */
        MorseRobot* const GetRobot() const { return m_Robot; }

    private:
        MorseRobot* m_Robot;    ///< Pointer to controlled robot.
};
}   // namespace CCMorse;

#endif // CCMORSE_MORSEDRIVER_H
