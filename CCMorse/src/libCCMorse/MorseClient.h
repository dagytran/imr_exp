/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-10-17
 *
 * File contains definition of MorseClient class.
 */

#ifndef CCMORSE_MORSECLIENT_H
#define CCMORSE_MORSECLIENT_H

#ifndef CCMORSE_PLAYERCLIENT_H
    //#define HAVE_BOOST_SIGNALS
    #define HAVE_BOOST_THREAD

    #ifdef HAVE_BOOST_SIGNALS
        #include <boost/signal.hpp>
    #endif

    #ifdef HAVE_BOOST_THREAD
        #include <boost/thread/mutex.hpp>
        #include <boost/thread/thread.hpp>
        #include <boost/thread/xtime.hpp>
        #include <boost/bind.hpp>
    #else
        // we have to define this so we don't have to
        // comment out all the instances of scoped_lock
        // in all the proxies
        namespace boost {
            class thread {
                public: thread() {};
            };

            class mutex {
                public:
                    mutex() {};
                    class scoped_lock {
                        public: scoped_lock(mutex) {};
                    };
            };
        }
    #endif // HAVE_BOOST_THREAD
#endif // CCMORSE_PLAYERCLIENT_H

#include "MorseRobot.h"
#include "MorseDriver.h"
#include "MorseDevice.h"
#include "MorseTime.h"

#define MORSE_HOSTNAME      "localhost"
#define MORSE_PORTNUM       4000
#define MORSE_TRANSPORT_TCP 0
#define MORSE_TRANSPORT_UDP 1

#define MORSE_DATAMODE_PUSH 0
#define MORSE_DATAMODE_PULL 1


namespace CCMorse {


/**
 * MorseClient class description.
 */
class MorseClient
{
    friend class PlayerClient;

    typedef std::map<MorseRobot*, MorseDriver*> RobotDriverMap_t;

    public:
        typedef boost::mutex  Mutex_t;
        typedef boost::thread Thread_t;

        // - Static public CCMorse methods --------------------------------------------
        /**
         * Static method that returns pointer to this client object.
         * @return Pointer ot this client instance.
         */
        static MorseClient* GetInstance();

        /**
         * Static method that returns pointer to this client object or
         * create instance of a client with parameters if instance of the
         * client does not exist yet.
         *
         * @param  aHostname   The hostname of Morse simulation. Default: "localhost"
         * @param  aPort       The port number of simulation. Default: 4000
         * @param  aTransport  The transport protocol for connection to simulation. Default: TCP
         * @return Pointer ot this client instance.
         */
        static MorseClient* GetInstance(const std::string aHostname, int aPort, int aTransport);

        /**
         * Static method that get time utilities class (@ref MorseTime).
         * @return Pointer to clients time class. @ref m_Time
         */
        static MorseTime* const GetTime() { return m_Time; }

        /**
         * Static method that get "Now" simulation time. @ref MorseTime::GetNow
         * @return Simulator time, in seconds, since Epoch.
         */
        static double const GetTimeNow() { return m_Time->GetNow(); }

        /**
         * Static method that get simulation time statistics. @ref MorseTime::GetStatistics
         * @return Struct @ref TimeStatistics_t.
         */
        static const TimeStatistics_t GetTimeStats() { return m_Time->GetStatistics(); }

        /**
         * Contructor. Make a client and connect it as indicated. It's strongly recomended use GetInstance
         * function to obtain client object, but if you feel a need to construct new client object don't forget
         * to call SetupTime and SetupDevices functions after construction of client!
         *
         * @param  aHostname   The hostname of Morse simulation. Default: "localhost"
         * @param  aPort       The port number of simulation. Default: 4000
         * @param  aTransport  The transport protocol for connection to simulation. Default: TCP
         */
        MorseClient(const std::string aHostname = MORSE_HOSTNAME, int aPort = MORSE_PORTNUM, int aTransport = MORSE_TRANSPORT_TCP);


        /**
         *  Destructor. Disconnects and destructs client.
         */
        ~MorseClient();


// - Public PlayerCc::PlayerClient methods ------------------------------------
        /**
         * Returns mutex for synchronization.
         */
        Mutex_t &GetMutex() const { return m_Mutex; }

        /**
         * Returns connection status.
         *
         * @return [@c true: client is connected to simulation, @c false: otherwise]
         */
        bool Connected() { return GetVar(m_ConnectionStatus); }

        /**
         * Start the run thread.
         *
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse.
         */
        void StartThread();

        /**
         * Stop the run thread.
         *
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse..
         */
        void StopThread();

        /**
         * This starts a blocking loop on @ref Read().
         *
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse.e.
         * @param   Timeout     Timeout in milliseconds. Default: 10ms.
         */
        void Run(int Timeout = 10);

        /**
         * Stops the @ref Run() loop.
         *
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse.
         */
        void Stop();

        /**
         * Check whether there is data waiting on the connection, blocking
         * for up to @param timeout milliseconds (set to 0 to not block).
         *
         * @warning Method copying PlayerCc::PlayerClient method. Because Morse
         * cannot use peek in principle, method always returns @c false.
         *
         * @param   timeout     Timeout in millisecond for @ref MorseClient::Read(). Default: 0ms.
         * @return  [@c false if there is no data waiting, @c true if there is data waiting]
         */
        bool Peek(uint32_t timeout = 0);

        /**
         * Set the timeout for client requests.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @param   seconds     Request timeout in seconds.
         */
        void SetRequestTimeout(uint32_t seconds) { m_RequestTimeout = seconds; }

        /**
         * Set connection retry limit, which is the number of times,
         * that we'll try to reconnect to the server after a socket error.
         * Set to -1 for inifinite retry.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @param limit     Number of retries. @ref m_RetryLimit
         */
        void SetRetryLimit(int limit) { m_RetryLimit = limit; }

        /**
         * Get connection retry limit, which is the number of times
         * that we'll try to reconnect to the server after a socket error.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @return Connection retry limit. @ref m_RetryLimit
         */
        int GetRetryLimit() { return GetVar(m_RetryLimit); }

        /**
         * Set connection retry time, which is number of seconds to
         * wait between reconnection attempts.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @param seconds   Time between connection attempts in seconds. @ref m_RetryTime
         */
        void SetRetryTime(double seconds) { m_RetryTime = seconds; }

        /**
         * Get connection retry time, which is number of seconds to
         * wait between reconnection attempts.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @return Time between connectrion attemts in seconds. @ref m_RetryTime
         */
        double GetRetryTime() { return GetVar(m_RetryTime); }

        /**
         * A blocking Read. Use this method to read data from the server, blocking
         * until at least one message is received.  Use @ref MorseClient::Peek()
         * to check whether any data is currently waiting.
         * In pull mode, this will block until all data waiting on the server has
         * been received, ensuring as up to date data as possible.
         */
        void Read();

        /**
         * A nonblocking Read. Use this method if you want to read in a nonblocking
         * manner.  This is the equivalent of checking if @ref Peek is @c true and
         * then @ref Read().
         */
        void ReadIfWaiting();

        /**
         * Set whether the client operates in Push/Pull modes.
         * You can toggle the mode in which the server sends data to your
         * client with this method.  The @p mode should be one of
         *   - @ref MORSE_DATAMODE_PUSH (all data)
         *   - @ref MORSE_DATAMODE_PULL (data on demand)
         * When in pull mode, it is highly recommended that a replace rule is set
         * for data packets to prevent the server message queue becoming flooded.
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse.
         */
        void SetDataMode(uint32_t aMode);

        /**
         * Set a replace rule for the clients queue on the server.
         * If a rule with the same pattern already exists, it will be replaced
         * with the new rule (i.e., its setting to replace will be updated).
         *
         * @warning Method copying PlayerCc::PlayerClient method and currently is
         * not implemented.
         *
         * @param aReplace  Should we replace these messages? true/false
         * @param aType     type of message to set replace rule for (-1 for wildcard).
         * @param aSubtype  message subtype to set replace rule for (-1 for wildcard).
         * @param aInterf   Interface to set replace rule for (-1 for wildcard).
         *      This can be used to set the replace rule for all members of a
         *      certain interface type.
         */
        void SetReplaceRule(bool aReplace, int aType = -1, int aSubtype = -1, int aInterf = -1);

        /**
         * Returns the hostname.
         * @return Servers hostname. @ref m_Hostname
         */
        std::string GetHostname() const { return GetVar(m_Hostname); }

        /**
         * Returns the port.
         * @return Servers port number. @ref m_Port
         */
        int GetPort() const { return GetVar(m_Port); }

        /**
         * Get the interface/identification code for a given name.
         *
         * @param   aName   Name of device. @ref MorseDevice::m_Name
         * @return Identification number of device.
         */
        int LookupCode(std::string aName) const;

        /**
         * Get the name for a given interface/identification code.
         *
         * @param aCode Identification code of device. @ref MorseDevice::m_ID
         * @return Name of the device.
         */
        std::string LookupName(int aCode) const;

        /**
         * Get count of the number of discarded messages on the server since the last
         * call to this method.
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse.
         *
         * @return Count of discarded messages.
         */
        uint32_t GetOverflowCount();

// - Dynamic public CCMorse methods -------------------------------------------
        /**
         * Returns pointer to device with given name.
         * @param aName Name of the device.
         * @return Pointer to device. @ref MorseDevice
         */
        MorseDevice* const GetDevice(std::string const &aName) const;

        /**
         * Returns pointer to device with given ID.
         * @param aID Identification number of the device.
         * @return Pointer to device. @ref MorseDevice
         */
        MorseDevice* const GetDevice(int aID) const;

        /**
         * Remove device from robots device list.
         * Robot @ref MorseRobot have a list of all devices that
         * are refreshed when @ref Read() is called.
         *
         * @param aName Name of the device that will be erased.
         */
        void EraseDevice(std::string const &aName);

        /**
         * Remove device from robots device list.
         * Robot @ref MorseRobot have a list of all devices that
         * are refreshed when @ref Read() is called.
         *
         * @param aDevice Pointer to device that will be erased.
         */
        void EraseDevice(MorseDevice* aDevice);

        /**
         * Returns devices port number. You can connect right to
         * the device data stream thru socket with this port.
         *
         * @param aDeviceName   Name of the device.
         * @return Port number od the device data stream.
         */
        int const GetDevicePort(std::string const &aDeviceName);

        /**
         * Sends message with clients socket to simulator.
         *
         * @param aMessage  String message for simulator.
         * @return Answer to message from simulator.
         */
        std::string const SendToSocket(std::string const &aMessage) const;

        /**
         * Returns pointer to robot owning given device.
         *
         * @param aDevice   Pointer to device. @ref MorseDevice
         * @return Pointer to robot. @ref MorseRobot
         */
        MorseRobot* const GetRobotOwningDevice(MorseDevice* const aDevice) const;

        /**
         * Returns pointer to robot providing Position2dProxy with given index.
         *
         * @param aIndex    Configuration index number of the proxy.
         * @return Pointer to robot. @ref MorseRobot
         */
        MorseRobot* const GetRobotProvidingPosition2d(int aIndex) const;

        /**
         * Returns pointer to robot providing LaserProxy with given index.
         *
         * @param aIndex    Configuration index number of the proxy.
         * @return Pointer to robot. @ref MorseRobot
         */
        MorseRobot* const GetRobotProvidingLaser(int aIndex) const;

        /**
         * Returns pointer to robot with given ID.
         * @param aRobotID Robot identification number.
         * @return Pointer to robot. @ref MorseRobot
         */
        MorseRobot* const GetRobotWithID(int aRobotID) const;

        /**
         * Returns list of all proxies in simulation.
         * @return Vector of all proxies.
         */
        std::vector<MorseProxy*> const GetProxyList() const;

        /**
         * Returns list of proxies subscribed to robot with given ID.
         * @param aRobotID Robot identification number.
         * @return Vector of proxies subscribed to robot.
         */
        std::vector<MorseProxy*> const GetProxiesSubscribedToRobot(int aRobotID) const;

        /**
         * Returns list of proxies subscribed to given robot.
         * @param aRobot    Pointer to robot. @ref MorseRobot
         * @return Vector of proxies subscribed to robot.
         */
        std::vector<MorseProxy*> const GetProxiesSubscribedToRobot(MorseRobot* const aRobot) const;

        /**
         * Returns pointer to driver instance that belongs to given robot.
         * @param aRobot    Pointer to robot. @ref MorseRobot
         * @return Pointer to robots driver. @ref MorseDriver
         */
        MorseDriver* const GetDriverForRobot(MorseRobot* aRobot);

        /**
         * Returns pointer to newly created driver instance. If robot don't have
         * a driver but is configured, this method will create it.
         *
         * @param aRobot    Pointer to robot. @ref MorseRobot
         * @return Pointer to robots driver. @ref MorseDriver
         */
        MorseDriver* const DriverFactory(MorseRobot* const aRobot);

    private:

// - Dynamic private PlayerCc::PlayerClient methods ---------------------------
        /**
         * Connect to the indicated server (simulator) with host and port.
         *
         * @param aHostname Hostname of the server.
         * @param aPort     Port number of the server.
         * @exception throws SocketError if connection is unsuccessfull.
         */
        void Connect(const std::string aHostname, uint32_t aPort);

        /**
         * Disconnect from server.
         */
        void Disconnect();


// - Dynamic private CCMorse methods ------------------------------------------
        /**
         * Finds all available devices. All devices is added to robots device list.
         */
        void SetupDeviceList();

        /**
         * Setup time utilities.
         */
        void SetupTime();

        /**
         * Get a variable from the client. All Get functions need to use this when
         * accessing data to make sure the data access is thread safe.
         */
        template<typename T>
        T GetVar(const T &aV) const { return CCMorse::GetVar(aV, this->GetMutex()); }


// - Private atributes --------------------------------------------------------
        static MorseTime* m_Time;   ///< The time associated utilities.

        static MorseClient* m_Me;   ///< Pointer to this instance.

        void RunThread();           ///< A helper function for starting the thread.

        std::vector<MorseRobot*>    m_Robots;   ///< List of robots that this client controls.
        std::vector<MorseDriver*>   m_Drivers;  ///< List of drivers for robots.

        RobotDriverMap_t m_RDMap;   ///< Map that connecting robots with their drivers.

        ClientSocket* m_ClientSocket;   ///< Class for socket communication.

        Thread_t *m_Thread;         ///< This is the thread where we run @ref Run().

        mutable Mutex_t m_Mutex;    ///< A mutex for handling synchronization

        std::string m_Hostname;     ///< The hostname of the server, stored for convenience.

        int     m_Port,             ///< The port number of the server, stored for convenience.
                m_RequestTimeout,   ///< The request timeout in secs.
                m_RetryLimit,       ///< The max number of retries.
                m_Mode,             ///< Datamode flag [0 = PUSH, 1 = PULL]
                m_Transport;        ///< Which transport (TCP or UDP) we're using.

        double  m_RetryTime;        ///< The amount of time waiting betweet retries.

        bool    m_ConnectionStatus, ///< Are we connected to the simulation?
                m_IsStop;           ///< Is the thread currently stopped or stopping?



};
} // namespace CCMorse;

#endif // CCMORSE_MORSECLIENT_H
