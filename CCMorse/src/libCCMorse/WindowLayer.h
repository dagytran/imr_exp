/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-18
 *
 * File contains definition of WindowLayer class.
 */

#ifndef CCMORSE_WINDOWLAYER_H
#define CCMORSE_WINDOWLAYER_H

#include "MorseUtility.h"

#include "../map_grid.h"


namespace CCMorse{

/**
 * WindowLayer can be used in @ref CCMorse::WindowProxy to draw
 * several grid maps in layers.
 */
class WindowLayer
{
    public:
        std::string    mName;        ///< name of the layer (user identification)
        bool           mVisible;     ///< is layer drawn (true) or not (false)
        int            mPriority;    ///< priority of the layer - the higher priority (= THE LOWER NUMBER; Linux-like priority assignment!!!) the higher position while drawing
        int            mAlpha;       ///< transparency of the layer (details about alpha at drawMapTransparent())
        double         mTransparent; ///< value of the map grid considered to be "invisible"
        RGBA_t         mColor;       ///< colorof the layer
        imr::CMapGrid* mMap;         ///< map as the canvas of the layer

        /**
         * Constructor with all parameters.
         */
        WindowLayer(std::string const aName, bool const aVisible, int const aPriority, int const aAlpha, double const aTransparent, RGBA_t const aColor, imr::CMapGrid* const aMap);

        /**
         * Default constructor.
         */
        WindowLayer() : WindowLayer("myLayer", true, 0, 255, 0.0, RGBA_t(255, 0, 0), NULL) { }

        /**
         * Constructor with name parameter.
         */
        WindowLayer(std::string const aName) : WindowLayer(aName, true, 0, 255, 0.0, RGBA_t(255, 0, 0), NULL) { }

        /**
         * Destructor.
         */
        ~WindowLayer() {}

        /**
         * Map setters.
         */
        void SetMap(imr::CMapGrid* const aMap) { mMap = aMap; }
        void SetMap(imr::CMapGrid& aMap) { mMap = &aMap; }
};
} // namespace CCMorse;

#endif // CCMORSE_WINDOWLAYER_H
