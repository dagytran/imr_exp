/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-17
 *
 * File contains definition of MorseSensor class.
 */

#ifndef CCMORSE_MORSESENSOR_H
#define CCMORSE_MORSESENSOR_H

#include "MorseDevice.h"


namespace CCMorse{

/**
 * Sensor class is base class for sensors in simulation.
 *
 * Based on @ref CCMorse::MorseDevice class.
 */
class MorseSensor : public MorseDevice
{
    public:
        /**
         * Default constructor of device.
         */
        MorseSensor() : MorseDevice() {}

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorseSensor(int aPort, std::string aName) : MorseDevice(aPort, aName) {}

        /**
         * Copy Constructor.
         */
        MorseSensor(const MorseDevice& aDevice) : MorseDevice(aDevice) {}

        /**
         * Destructor.
         */
        virtual ~MorseSensor() {}

        /**
         * Returns dictionary representation of local data stored in sensor from Morse simulation.
         */
        Dictionary_t const GetLocalData() const;

        /**
         * Returns string representation of data in devices data stream.
         */
        std::string RecieveData();

        /**
         * Function that gets fresh data from simulation sensor when available.
         */
        virtual void RefreshData() {}

};
} // namespace CCMorse;

#endif // CCMORSE_MORSESENSOR_H
