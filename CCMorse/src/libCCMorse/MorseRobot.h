/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-16
 *
 * File contains definition of MorseRobot class.
 */

#ifndef CCMORSE_MORSEROBOT_H
#define CCMORSE_MORSEROBOT_H

#include "MorseDevice.h"
#include "MorseProperties.h"
#include "MorseProxy.h"


namespace CCMorse {

/**
 * Enum type ComDir_t is used to determine comunication direction
 * for configured proxies. @ref CCMorse::Proxy_t
 */
typedef enum {
    in, out, pass
} ComDir_t;

/**
 * Struct Proxy_t is used in MorseRobot class to save proxy information
 * in one place.
 */
typedef struct {
    MorseProxy* ptr;
    std::string type;
    int         id;
    int         index;
    ComDir_t    direction;
    bool        subscribed;
} Proxy_t;

/**
 * Robot class represents a robot in Morse simulation.
 * Robot has list of its devices, configuraition and list of its proxies.
 */
class MorseRobot
{
    friend class MorseClient;
    friend class SNDNavigation;

    public:
        /**
         * Constructor.
         * @param   aName   Name of the robot.
         */
        MorseRobot(std::string const aName);

        /**
         * Destructor.
         */
        virtual ~MorseRobot();

        /**
         * Returns pointer to robot configuration class.
         */
        MorseProperties* const GetConfiguration() const { return m_Config; }

    // Device methods:
        /**
         * Adds device pointer to robots device list.
         */
        void AddDevice(MorseDevice* aDevice) { m_Devices.push_back(aDevice); }

        /**
         * Returns pointer to one of robots devices with name.
         *
         * @throw   MorseError  If robot doesn't have device with given name.
         * @param   aName       Name of the device.
         * @return  Pointer to device.
         */
        MorseDevice* GetDevice(std::string const &aName) const;

        /**
         * Returns pointer to one of robots devices with ID.
         *
         * @throw   MorseError  If robot doesn't have device with given ID.
         * @param   aID         ID number of the device.
         * @return  Pointer to device.
         */
        MorseDevice* GetDevice(int aID) const;

        /**
         * Function that gets new data from Morse simulation for all robots devices
         * and checks if data is valid in all robots proxies.
         */
        void RefreshDevices();

        /**
         * Returns reference to vector of all robots devices.
         */
        std::vector<MorseDevice*> const &GetDevices() const { return m_Devices; }

        /**
         * Tries to erase device from robots device list, if device
         * with given name is not found, nothing happen.
         * @param   aName   Name of the device.
         */
        void EraseDevice(std::string const &aName);

        /**
         * Tries to erase device from robots device list, if device
         * with given pointer is not found, nothing happen.
         * @param   aDevice Pointer to the device.
         */
        void EraseDevice(MorseDevice* aDevice);

    // Proxy methods:
        /**
         * Initializes configured proxy.
         */
        void InitProxy(std::vector<int> const &aIndexes, std::string const aType, ComDir_t aDirection);

        /**
         * When proxy is subscribing to robot, this function saves
         * needed proxy information. Subscribed proxy must be initialized first.
         * @ref MorseRobot::InitProxy
         *
         * @throw   MorseError  If proxy wasn't initialized or configured.
         * @param   aProxy      Pointer to proxy class.
         */
        void SubscribeProxy(MorseProxy* const aProxy);

        /**
         * When proxy is subscribing to robot, this function saves
         * needed proxy information. Subscribed proxy must be initialized first.
         * @ref MorseRobot::InitProxy
         *
         * @throw   MorseError  If proxy wasn't initialized or configured.
         * @param   aProxy      Proxy information.
         */
        void SubscribeProxy(Proxy_t const aProxy);

        /**
         * Unsubscribing the proxy.
         * @param   aProxy  Pointer to proxy to be unsubscribed.
         */
        void UnsubscribeProxy(MorseProxy* const aProxy);

        /**
         * Returns whether Position2dProxy with given index measuring position
         * from global perspective.
         *
         * @param   aIndex  Position2dProxy index.
         * @return  [@c true: Proxy is measuring global position, @c false: Proxy is measuring odometry position]
         */
        bool const IsPosition2dProxyGlobal(int aIndex) const;

        /**
         * Returns pointer to robots proxy information entry by type and index.
         *
         * @param   aIndex  Configured index of the proxy.
         * @param   aType   String name of the proxy type.
         * @return  Pointer to proxy information if exists else return @c NULL.
         */
        Proxy_t* GetProxy(int const aIndex, std::string const &aType) const;

        /**
         * Returns pointer to robots proxy information entry by info structure.
         * Tested are only "index" and "type" members of the structure.
         *
         * @param   aInfo  Configured index of the proxy.
         * @return  Pointer to proxy information if exists else return @c NULL.
         */
        Proxy_t* GetProxy(Proxy_t const aInfo) const;

        /**
         * Returns pointer to robots proxy information entry by pointer to proxy.
         *
         * @param   aProxy  Pointer to existing proxy instance.
         * @return  Pointer to proxy information if exists else return @c NULL.
         */
        Proxy_t* GetProxy(MorseProxy* const aProxy) const;

        /**
         * Returns pointer to the subscribed proxy.
         *
         * @param   aIndex  Configured index of the proxy.
         * @param   aType   String name of the proxy type.
         * @param   aKey    Prefered comminication direction but can be replaced with CCMorse::ComDir_t::pass.
         * @return  Pointer to proxy if exists and is subscribed else return @c NULL.
         */
        MorseProxy* GetSubscribedProxy(int const aIndex, std::string const &aType, ComDir_t const aKey) const;

        /**
         * Returns vector of pointers to all subscribed and required by driver proxies.
         *
         * @throw   MorseError  If there is no such proxy found.
         * @param   aType   String name of the proxy type.
         * @param   aKey    Prefered comminication direction but can be replaced with CCMorse::ComDir_t::pass.
         * @return  Vector of pointers to proxies.
         */
        std::vector<MorseProxy*> const GetRequiredSubscribedProxy(std::string const &aType, ComDir_t const aKey) const;

        /**
         * Returns vector of pointers to all robots proxies.
         */
        std::vector<MorseProxy*> const GetProxies() const;

        /**
         * Returns vector of pointers to all subscribed robots proxies.
         */
        std::vector<MorseProxy*> const GetSubscribedProxies() const;

        /**
         * Returns vector of pointers to all robots proxy information.
         */
        std::vector<Proxy_t*> const &GetProxiesInfo() const { return m_Proxies; }

        /**
         * Returns vector of pointers to all subscribed robots proxy information.
         */
        std::vector<Proxy_t*> const GetSubscribedProxiesInfo() const;

        /**
         * Returns whether are all by the driver requiered proxies subscribed.
         * @return [@c true: All proxies needed by driver are subscribed, @c false: Otherwise]
         */
        bool const IsAllRequiredProxiesSubscribed() const;

        /**
         * Erases proxy information from robots list of proxies.
         *
         * @param   aProxy  Pointer to proxy which we want to erase.
         */
        void EraseProxy(MorseProxy* aProxy);

    // Goal destination methods:
        /**
         * Send a motor command for position control mode.
         * If motion speed is higher that maximal configured for the robot,
         * the desired motion speed is ignored and robot will use maximal configured speed.
         *
         * @param   aPos    Desired goal position of the robot as a Pose2D_t. (X[m], Y[m], Yaw[rad])
         * @param   aVel    Desired motion speed as a Pose2D_t. (VX[m/s], VY[m/s], W[rad/s])
         */
        void GoTo(Pose2D_t aPos, Pose2D_t aVel);

        /**
         * Send a motor command for position control mode using maximal configured speed.
         * @param   aPos    Desired goal position of the robot as a Pose2D_t. (X[m], Y[m], Yaw[rad])
         */
        void GoTo(Pose2D_t aPos);

        /**
         * Send a motor command for position control mode using maximal configured speed.
         *
         * @param   aX      X coordinate of desired goal position of the robot. [m]
         * @param   aY      Y coordinate of desired goal position od the robot. [m]
         * @param   aYaw    Angular position of the robot on goal position. [rad]
         */
        void GoTo(double aX, double aY, double aYaw);

        /**
         * Send a motor command for position control mode.
         * If motion speed is higher that maximal configured for the robot,
         * the desired motion speed is ignored and robot will use maximal configured speed.
         *
         * @param   aX      X coordinate of desired goal position of the robot. [m]
         * @param   aY      Y coordinate of desired goal position od the robot. [m]
         * @param   aYaw    Angular position of the robot on goal position. [rad]
         * @param   vX      Desired forward motion speed of the robot. [m/s]
         * @param   vY      Desired sideward motion speed of the robot. [m/s]
         * @param   vYaw    Desired angular motion speed of the robot. [rad/s]
         */
        void GoTo(double aX, double aY, double aYaw, double vX, double vY, double vYaw);

        /**
         * Send a motor command for position control mode using maximal configured speed
         * and previously set goal.
         */
        void GoTo();

        /**
         * Returns robots goal information.
         * @ref CCMorse::Goal_t
         */
        Goal_t const GetGoal() const { return GetVar(m_Goal); }

        /**
         * Returns set robot goal position. (X[m], Y[m], Yaw[rad])
         */
        Pose2D_t const GetGoalPos() const { Pose2D_t pos = {GetGoal().px, GetGoal().py, GetGoal().pa}; return pos; }

        /**
         * Sets new robot goal position.
         *
         * @param   aX      X coordinate of desired goal position of the robot. [m]
         * @param   aY      Y coordinate of desired goal position od the robot. [m]
         * @param   aYaw    Angular position of the robot on goal position. [rad]
         */
        void SetGoalPos(double const aX, double const aY, double const aYaw);

        /**
         * Sets new robot goal position.
         * @param   aPos    Desired goal position of the robot as a Pose2D_t. (X[m], Y[m], Yaw[rad])
         */
        void SetGoalPos(Pose2D_t const aPos) { SetGoalPos(aPos.px, aPos.py, aPos.pa); }

        /**
         * Returns @c true if robot is on his way to goal.
         */
        bool const IsTransitingToGoal() const { return ((GetGoal().status == transit) ? true : false); }

        /**
         * Returns @c true if robot just reached goal position.
         */
        bool const IsGoalReached() const { return ((GetGoal().status == arrived) ? true : false); }

        /**
         * Returns @c true if robot is waiting on next goal.
         */
        bool const IsGoalIdle() const { return ((GetGoal().status == idle) ? true : false); }

        /**
         * Sets maximal motion speed for GoTo function.
         * If motion speed is higher that maximal configured for the robot,
         * the desired motion speed is ignored and robot will use maximal configured speed.
         *
         * @param   aVel    Desired motion speed as a Pose2D_t. (VX[m/s], VY[m/s], W[rad/s])
         */
        void SetMaxVelocity(Pose2D_t const aVel);

        /**
         * Sets maximal motion speed for GoTo function.
         * If motion speed is higher that maximal configured for the robot,
         * the desired motion speed is ignored and robot will use maximal configured speed.
         *
         * @param   vX      Desired forward motion speed of the robot. [m/s]
         * @param   vY      Desired sideward motion speed of the robot. [m/s]
         * @param   vYaw    Desired angular motion speed of the robot. [rad/s]
         */
        void SetMaxVelocity(double vX, double vY, double vYaw) { Pose2D_t vel = {vX, vY, vYaw}; SetMaxVelocity(vel); }

        /**
         * Returns maximal motion speed currently set for GoTo function.
         */
        Pose2D_t const GetMaxVelocity() const { return GetVar(m_MaxVelocity); }

    // Properties getters:
        /**
         * Returns robots name. Name is declared in simulation configruration (builder script).
         */
        std::string const GetName()       const { return m_Name; }

        /**
         * Returns string name of color of the robot.
         */
        std::string const GetRobotColor() const { return (m_Config->GetRobotColor()); }

        /**
         * Returns string name of configured driver used for robot.
         */
        std::string const GetDriverName() const { return (m_Config->GetDriver()); }

        /**
         * Returns generated identification number of the robot.
         */
        unsigned const GetID() const { return m_ID; }

        /**
         * Returns information whether laser scanner on robot is measuring intensities (reflections).
         */
        bool const IsLaserRssi() const { return (m_Config->IsLaserRssi()); }

        /**
         * Returns maximal forward motion speed currently set for GoTo function.
         * @return  Maximal forward speed. [m/s]
         */
        double const GetMaxSpeed() const { return GetVar(m_MaxVelocity.px); }

        /**
         * Returns maximal angular motion speed currently set for GoTo function.
         * @return  Maximal angular speed. [rad/s]
         */
        double const GetMaxTurnRate() const { return GetVar(m_MaxVelocity.pa); }

        /**
         * Returns width of the robot. From forward motion axis.
         * @return  Robot width. [m]
         */
        double const GetRobotWidth() const { return (m_Config->GetRobotWidth()); }

        /**
         * Returns lenght of the robot. From forward motion axis.
         * @return  Robot lenght. [m]
         */
        double const GetRobotLength() const { return (m_Config->GetRobotLength()); }

        /**
         * Returns radius of the robot. The radius of the circle into which the entire robot physically fits.
         * @return  Robot radius. [m]
         */
        double const GetRobotRadius() const { return (m_Config->GetRobotRadius()); }

        /**
         * Returns mass of the robot.
         * @return  Robot mass. [kg]
         */
        double const GetRobotMass() const { return (m_Config->GetRobotMass()); }

        /**
         * Returns distance of the main wheels.
         * @return Wheel distance. [m]
         */
        double const GetWheelsDist() const { return (m_Config->GetWheelsDist()); }

        /**
         * Returns minimal width of gap that robot can go thru.
         * @return Minimal width. [m]
         */
        double const GetMinGapWidth() const { return (m_Config->GetMinGapWidth()); }

        /**
         * Returns maximal avoiding distance from obstacle.
         * @return Maximal avoiding distance. [m]
         */
        double const GetSafetyDistMax() const { return (m_Config->GetSafetyDistMax()); }

        /**
         * Returns maximum distance allowed from the final goal for the driver algorithm to stop.
         * @return  Goal position tolerance. [m]
         */
        double const GetGoalPositionTol() const { return (m_Config->GetGoalPositionTol()); }

        /**
         * Returns maximum angular error from the final goal position for the driver algorithm to stop.
         * @return  Goal angular tolerance. [rad]
         */
        double const GetGoalAngleTol() const { return (m_Config->GetGoalAngleTol()); }

        /**
         * Returns vector of Position2DProxy indexes (global and local) that this robot is configured to provide.
         */
        std::vector<int> const GetProvidesPosition2dIndexList() const { return (m_Config->GetProvidesPosition2dIndexList()); }

        /**
         * Returns vector of Position2DProxy::global indexes that this robot is configured to provide.
         */
        std::vector<int> const &GetProvidesGlobalPosition2dIndexList() const { return (m_Config->GetProvidesGlobalPosition2dIndexList()); }

        /**
         * Returns vector of Position2DProxy::local indexes that this robot is configured to provide.
         */
        std::vector<int> const &GetProvidesLocalPosition2dIndexList() const { return (m_Config->GetProvidesLocalPosition2dIndexList()); }

        /**
         * Returns vector of LaserProxy indexes that this robot is configured to provide.
         */
        std::vector<int> const &GetProvidesLaserIndexList() const { return (m_Config->GetProvidesLaserIndexList()); }

        /**
         * Returns vector of WindowProxy indexes that this robot is configured to provide.
         */
        std::vector<int> const &GetProvidesWindowIndexList() const { return (m_Config->GetProvidesWindowIndexList()); }

        /**
         * Returns vector of Position2DProxy indexes that driver of this robot is configured to provide.
         */
        std::vector<int> const &GetDriverProvidesPosition2dIndexList() const { return (m_Config->GetDriverProvidesPosition2dIndexList()); }

        /**
         * Returns vector of Position2DProxy indexes that driver of this robot is require after robot as inputs.
         */
        std::vector<int> const &GetDriverRequiresInputPosition2dIndexList() const { return (m_Config->GetDriverRequiresInputPosition2dIndexList()); }

        /**
         * Returns vector of Position2DProxy indexes that driver of this robot is require after robot as outputs.
         */
        std::vector<int> const &GetDriverRequiresOutputPosition2dIndexList() const { return (m_Config->GetDriverRequiresOutputPosition2dIndexList()); }

        /**
         * Returns vector of LaserProxy indexes that driver of this robot is configured to provide.
         */
        std::vector<int> const &GetDriverProvidesLaserIndexList() const { return (m_Config->GetDriverProvidesLaserIndexList()); }

        /**
         * Returns vector of LaserProxy indexes that driver of this robot is require after robot as inputs.
         */
        std::vector<int> const &GetDriverRequiresInputLaserIndexList() const { return (m_Config->GetDriverRequiresInputLaserIndexList()); }

        /**
         * Returns vector of LaserProxy indexes that driver of this robot is require after robot as outputs.
         */
        std::vector<int> const &GetDriverRequiresOutputLaserIndexList() const { return (m_Config->GetDriverRequiresOutputLaserIndexList()); }

        /**
         * Prints general info about the robot to standard output.
         */
        void PrintRobotSummary() const;

        /**
         * Returns string representation of proxy info.
         */
        std::string const GetProxySummary(Proxy_t const info) const;

        /**
         * Prints general status of all proxies of this robot to standard output.
         */
        void PrintProxiesStatus() const;

    private:
        /**
         * Sets status of the robot to "transiting to goal".
         */
        void SetTransitToGoal();

        /**
         * Sets status of the robot to "goal reached".
         */
        void SetGoalReached();

        /**
         * Sets status of the robot to idle - waiting for next goal.
         */
        void SetGoalIdle();

        /**
         * Sets configurations to the robot.
         */
        void SetConfiguration(MorseProperties* const aConfig);

        /**
         * Get a variable with use of scoped lock. All Get functions need to use this when
         * accessing data to make sure the data access is thread safe.
         */
        template<typename T>
        T GetVar(const T &aV) const { return CCMorse::GetVar(aV, this->GetMutex()); }

        /**
         * Returns mutex for sychronization.
         */
        Mutex_t &GetMutex() const { return m_Mutex; }

        std::vector<MorseDevice*> m_Devices;    ///< List of devices available to this robot.
        std::vector<Proxy_t*>     m_Proxies;    ///< List of proxies available to this robot.

        MorseProperties* m_Config;  ///< Configuration class storing all properties of the robot.

        Thread_t*   m_DriverThread; ///< Thread for driver algorythm.

        mutable Mutex_t m_Mutex;    ///< Mutex for synchronization.

        Goal_t      m_Goal;         ///< Goal position and status.
        Pose2D_t    m_MaxVelocity;  ///< Maximal motion speed of the robot.

        std::string m_Name;         ///< Name of the robot.

        unsigned m_ID;              ///< Identification number of the robot.
        static unsigned s_IDCounter;    ///< Robot identification number counter.

};
} // namespace CCMorse

#endif // CCMORSE_MORSEROBOT_H
