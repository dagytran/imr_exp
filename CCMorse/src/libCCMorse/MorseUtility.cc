#include "MorseUtility.h"
#include "MorseError.h"
#include "SocketException.h"

using namespace CCMorse;


std::string const CCMorse::BoolToString (bool b)
{
    if (b)
        return "True";
    else
        return "False";
}

std::string const CCMorse::SendToSocket(const ClientSocket &aSocket, const std::string &aMessage)
{
    try {
        aSocket << (aMessage + '\n');

    } catch ( SocketException& ) {
        std::cout << "CCMorse::SendToSocket: Sending message: " << aMessage << " failed." << std::endl;
    }

    return RecieveFromSocket(aSocket);
}

std::string const CCMorse::RecieveFromSocket(const ClientSocket &aSocket)
{
    std::string reply, r;
    try{
        do {
            aSocket >> r;
            reply += r;

        } while(r.length() == MAXRECV);

    } catch ( SocketException& ) {
        std::cout << "CCMorse::RecieveFromSocket: Recieving reply from sever failed." << std::endl;
        return COM_KO;
    }

    return reply;
}

Dictionary_t const CCMorse::MapData (const std::string &aData)
{
    Dictionary_t output;
    std::string key, value;
    std::size_t pos1, pos2, found;

    std::string data = aData;

    //std::cout << "CCMorse::MapData(): Mapping data: " << data << std::endl;

    for(std::size_t i = 0; i < data.length(); i++)
        if(data[i] == '\'')
            data[i] = '"';

    found = data.find_first_of('"');
    while(found != std::string::npos){

    // Finding keyword
        pos1 = found + 1;
        pos2 = data.find_first_of('"', pos1);
        key  = data.substr(pos1, pos2-pos1);

        //std::cout << "CCMorse::MapData: Found key: " << key << std::endl;

    // Finding value
        pos1 = data.find_first_of(':', pos2) + 2;

        // This is object
        if( data[pos1] == '{'){
            //std::cout << "CCMorse::MapData(): This is OBJECT." << std::endl;

            int level = 1;
            pos2 = pos1 + 1;

            while(level != 0){

                if(pos2 >= data.length())
                    throw MorseError("CCMorse::MapData()", "Out of bounds. Maybe missing '}'.");

                if( data[pos2] == '{')
                    level++;

                if( data[pos2] == '}')
                    level--;

                pos2++;
            }
        }

        // This is array
        else if( data[pos1] == '['){
            //std::cout << "CCMorse::MapData(): This is ARRAY." << std::endl;

            int level = 1;
            pos2 = pos1 + 1;

            while(level != 0){

                if(pos2 >= data.length())
                    throw MorseError("CCMorse::MapData()", "Out of bounds. Maybe missing ']'.");

                if( data[pos2] == '[')
                    level++;

                if( data[pos2] == ']')
                    level--;

                pos2++;
            }
        }

        // This is boolean or null
        else if( data[pos1] == 't' || data[pos1] == 'f' || data[pos1] == 'n' ){
            //std::cout << "CCMorse::MapData(): This is LOGIC or NULL." << std::endl;
            pos2 = data.find_first_of(",}", pos1);
        }

        // This is string
        else if( data[pos1] == '"' ){
            //std::cout << "CCMorse::MapData(): This is STRING." << std::endl;
            pos1++;
            pos2 = data.find_first_of('"', pos1);
        }

        // This is number
        else {
            //std::cout << "CCMorse::MapData(): This is NUMBER." << std::endl;
            std::string numChars = "1234567890+-.";
            if( numChars.find(data[pos1]) != std::string::npos )
                pos2 = data.find_first_of(",}", pos1);
        }

        value = data.substr(pos1, pos2-pos1);

        //std::cout << "CCMorse::MapData: Found value: " << value << std::endl;

    // Setting value to the key and store it to gridMap
        output[key] = value;

        if(pos2 + 1 < data.length())
            found = data.find_first_of('"', pos2 + 1);
        else
            found = std::string::npos;
    }

    //for(Dictionary_t::iterator it = output.begin(); it != output.end(); ++it)
    //    std::cout << it->first << " => " << it->second << std::endl;

    return output;
}

bool const CCMorse::IsNullOrEmpty(const std::string &aStr)
{
    if( aStr.compare("null") == 0 || aStr.compare("") == 0 )
        return true;
    else
        return false;
}

double const CCMorse::AsDouble(const std::string &aDbl)
{
    if(IsNullOrEmpty(aDbl)){
        //std::cout << "CCMorse::AsDouble(): Value was NULL or empty. Returning '0.0'." << std::endl;
        return 0.0;
    }

    return (StringToNumber(aDbl, 0.0));
}

int const CCMorse::AsInteger(const std::string &aInt)
{
    if(IsNullOrEmpty(aInt)){
        //std::cout << "CCMorse::AsInteger(): Value was NULL or empty. Returning '0'." << std::endl;
        return 0;
    }

    return (StringToNumber(aInt, 0));
}

bool const CCMorse::AsBool(const std::string &aBool)
{
    if(IsNullOrEmpty(aBool)){
        //std::cout << "CCMorse::AsBool(): Value was NULL or empty. Returning 'false'." << std::endl;
        return false;
    }

    if( aBool.compare("true") == 0 )
        return true;
    else
        return false;

}

std::vector<double> const CCMorse::AsVectorOfDoubles(const std::string &aVector)
{
    std::vector<double> output;

    if( IsNullOrEmpty(aVector) ){
        //std::cout << "CCMorse::AsVectorOfDoubles(): Value was NULL or empty. Returning an empty vector." << std::endl;
        return output;
    }

    std::size_t pos1, pos2, found;

    std::string test = aVector.substr(1, (aVector.length() - 2));

    found = test.find_first_not_of("1234567890+-eE., ");    // If found something thats not in number writing
    if(found != std::string::npos){                         // assuming its another []
        std::cout << "Found \"" << test.at(found) << "\" on: " << found << " in: " << test << std::endl;
        throw MorseError("MorseUtility::FindVector", "This is probably vector of vectors, this method parse only vectors of doubles.");

    } else {                                                // else, we parse it like double
        found = aVector.find_first_of("1234567890-.");
        while(found != std::string::npos){
            pos1 = found;
            pos2 = aVector.find_first_of(",]", pos1 + 1);

            output.push_back(StringToNumber(aVector.substr(pos1, pos2-pos1), 0.0));

            if(pos2 + 1 < aVector.length())
                found = aVector.find_first_of("1234567890-.", pos2 + 1);
            else
                found = std::string::npos;
        }
    }

    return output;
}

std::vector<int> const CCMorse::AsVectorOfIntegers(const std::string &aVector)
{
    std::vector<int> output;

    if( IsNullOrEmpty(aVector) ){
        //std::cout << "CCMorse::AsVectorOfDoubles(): Value was NULL or empty. Returning an empty vector." << std::endl;
        return output;
    }

    std::size_t pos1, pos2, found;

    std::string test = aVector.substr(1, (aVector.length() - 2));

    found = test.find_first_not_of("1234567890-, ");    // If found something thats not in number writing
    if(found != std::string::npos){                     // assuming its another []
        std::cout << "Found \"" << test.at(found) << "\" on: " << found << " in: " << test << std::endl;
        throw MorseError("MorseUtility::FindVector", "This is probably vector of vectors, this method parse only vectors of integers.");

    } else {                                            // else, we parse it like int
        found = aVector.find_first_of("1234567890-");
        while(found != std::string::npos){
            pos1 = found;
            pos2 = aVector.find_first_of(",]", pos1 + 1);

            output.push_back(StringToNumber(aVector.substr(pos1, pos2-pos1), 0));

            if(pos2 + 1 < aVector.length())
                found = aVector.find_first_of("1234567890-", pos2 + 1);
            else
                found = std::string::npos;
        }
    }

    return output;
}

std::vector<std::string> const CCMorse::AsVectorOfStrings(const std::string &aVector)
{
    std::vector<std::string> output;

    if( IsNullOrEmpty(aVector) ){
        //std::cout << "CCMorse::AsVectorOfDoubles(): Value was NULL or empty. Returning an empty vector." << std::endl;
        return output;
    }

    std::size_t pos1, pos2, found;

    found = aVector.find_first_of('"');
    while(found != std::string::npos){
        pos1 = found + 1;
        pos2 = aVector.find_first_of('"', pos1);

        if(aVector[pos2 + 1] == ',' || aVector[pos2 + 1] == ']')
            output.push_back(aVector.substr(pos1, pos2-pos1));

        if(pos2 + 1 < aVector.length())
            found = aVector.find_first_of('"', pos2 + 1);
        else
            found = std::string::npos;
    }

    return output;
}

std::vector<Pose2D_t> const CCMorse::AsVectorOfPoses(const std::string &aVector)
{
    std::vector<Pose2D_t> output;

    if( IsNullOrEmpty(aVector) ){
        //std::cout << "CCMorse::AsVectorOfPoses(): Value was NULL or empty. Returning an empty vector." << std::endl;
        return output;
    }

    std::size_t pos1, pos2, found;
    Pose2D_t p;
    std::vector<double> point;

    std::string poses = aVector.substr(1, (aVector.length() - 2));

    found = poses.find_first_of('[');
    while(found != std::string::npos){

        pos1 = found;
        pos2 = poses.find_first_of(']', pos1) + 1;

        point = AsVectorOfDoubles(poses.substr(pos1, pos2-pos1));

        p.px = point.at(0);
        p.py = point.at(1);
        p.pa = point.at(2);
        output.push_back(p);

        if(pos2 + 1 < poses.length())
            found = poses.find_first_of('[', pos2 + 1);
        else
            found = std::string::npos;
    }

    return output;
}

std::vector<std::string> const CCMorse::AsVectorOfObjects(const std::string &aVector)
{
    std::vector<std::string> output;

    if( IsNullOrEmpty(aVector) ){
        //std::cout << "CCMorse::AsVectorOfPoses(): Value was NULL or empty. Returning an empty vector." << std::endl;
        return output;
    }

    std::size_t pos1, pos2, found;

    pos1 = aVector.find_first_of('[');
    if( pos1 != std::string::npos ){
        int level = 1;
        pos2 = pos1 + 1;

        while(level != 0){

            if(pos2 >= aVector.length())
                throw MorseError("CCMorse::AsVectorOfObjects()", "The provided string is not represantation of vector. Maybe missing ']'.");

            if( aVector[pos2] == '[')
                level++;

            if( aVector[pos2] == ']')
                level--;

            pos2++;
        }
    } else
        return output;

    found = aVector.find_first_of('{');
    while(found != std::string::npos){

        pos1 = found;
        pos2 = aVector.find_first_of('}', pos1 + 1);

        output.push_back(aVector.substr(pos1, pos2-pos1));

        if(pos2 + 1 < aVector.length())
            found = aVector.find_first_of('{', pos2 + 1);
        else
            found = std::string::npos;
    }

    return output;
}

bool const CCMorse::CheckIntValue(int const aValue, int const aMin, int const aMax, std::string const aFun)
{
    if(aValue >= aMin && aValue < aMax)
        return true;
    else
        throw MorseError(aFun, "Value = " + NumberToString(aValue) + " is out of range. Value must be in <" + NumberToString(aMin) + "; " + NumberToString(aMax) + ") interval.");

    return false;
}

bool const CCMorse::CheckDoubleValue(double const aValue, double const aMin, double const aMax, std::string const aFun)
{
    if(aValue >= aMin && aValue <= aMax)
        return true;
    else
        throw MorseError(aFun, "Value = " + NumberToString(aValue) + " is out of range. Value must be in <" + NumberToString(aMin) + "; " + NumberToString(aMax) + "> interval.");

    return false;
}


