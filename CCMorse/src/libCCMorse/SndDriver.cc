#include "SndDriver.h"

using namespace CCMorse;


/// ----------------------------------------------------------------------------
/// - Class CCMorse::SndDriver
void SndDriver::SetupProxies()
{
    m_Laser   = reinterpret_cast<LaserProxy*>(GetRobot()->GetRequiredSubscribedProxy("LaserProxy", in).at(0));
    m_PoseIn  = reinterpret_cast<Position2dProxy*>(GetRobot()->GetRequiredSubscribedProxy("Position2dProxy", in).at(0));
    m_PoseOut = reinterpret_cast<Position2dProxy*>(GetRobot()->GetRequiredSubscribedProxy("Position2dProxy", out).at(0));

    m_SND = new SNDNavigation(m_Laser, m_PoseIn, m_PoseOut, GetRobot());
}

/// ----------------------------------------------------------------------------

bool SndDriver::HasRobotEveryProxy(MorseRobot* aRobot)
{
    return (aRobot->IsAllRequiredProxiesSubscribed());
}

/// ----------------------------------------------------------------------------

void SndDriver::GoTo()
{
    m_SND->GoTo(true);
}

/// - end of file --------------------------------------------------------------
