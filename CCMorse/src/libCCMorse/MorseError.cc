#include "MorseError.h"

using namespace CCMorse;


MorseError::MorseError(const std::string aFunction, const std::string aStr, const int aCode)
{
    m_Function = aFunction;
    m_Str = aStr;
    m_Code = aCode;
}

std::ostream& std::operator<<(std::ostream& os, const CCMorse::MorseError& e)
{
    return os   << e.GetErrorFun()
                << "(" << e.GetErrorCode() << ")"
                << " : "
                << e.GetErrorStr();
}
