/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-17
 *
 * File contains definition of MorseTime class.
 */

#ifndef CCMORSE_MORSETIME_H
#define CCMORSE_MORSETIME_H

#include "MorseDevice.h"


namespace CCMorse {

/**
 * Time class that have acces to time of the Morse simulation.
 * Based on @ref CCMorse::MorseDevice class.
 */
class MorseTime : public MorseDevice
{
    public:
        /**
         * Default constructor of device.
         */
        MorseTime(int aPort) : MorseDevice(aPort, "time") {}

        /**
         * Destructor.
         */
        ~MorseTime() {}

        /**
         * Returns time in the simulation.
         * @return  Simualtion time. [s]
         */
        double const GetNow() { SetNow(); return GetVar(m_Now); }

        /**
         * Returns string representation of time mode (strategy) of the simulation.
         */
        std::string const GetMode() const;

        /**
         * Returns statistics about simulation time.
         */
        TimeStatistics_t const GetStatistics() const;

        /**
         * Sends sleep command to simulation.
         */
        void Sleep(double aSeconds) { CommunicateWithSim("sleep [" + NumberToString(aSeconds) + "]" ); }

        /**
         * Gets current simulation time.
         */
        virtual void RefreshData();

    private:
        /**
         * Gets and save current simulation time to member variable.
         */
        void SetNow();

        double m_Now;   ///< Stores the last received time simualtion.

};
} // namespace CCMorse

#endif // CCMORSE_MORSETIME_H
