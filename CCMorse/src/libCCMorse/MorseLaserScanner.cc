#include "MorseLaserScanner.h"

using namespace CCMorse;


std::string MorseLaserScanner::idLS[] = {"laserscanner", "hokuyo", "sick", "sick_ld_mrs", "laser"};
std::vector<std::string> MorseLaserScanner::identifiers (idLS, idLS + sizeof(idLS)/sizeof(std::string) );

void MorseLaserScanner::SetupConfiguration()
{
    Dictionary_t config = GetConfigurations();

    m_ScanStart    = ((-AsDouble(config["scan_window"]) * PI) / 360);
    m_ScanRes      = ((AsDouble(config["resolution"]) * PI) / 180);
    m_ScanningFreq = 10; // The right value is set after robot configuration is loaded. Default is 10Hz.
    m_ScanCount    = (AsDouble(config["scan_window"])) / (AsDouble(config["resolution"]));
    m_RangeRes     = 0.000001;
    m_MaxRange     = AsDouble(config["laser_range"]);

    m_ScanID = 0;
    for(std::size_t i = 0; i < identifiers.size(); i++)
        if(GetName().find(identifiers.at(i)) != std::string::npos)
            m_LaserID = i;

    Dictionary_t objectToRobot = MapData(config["object_to_robot"]);

    SetGeoPose    ( AsVectorOfDoubles (objectToRobot["translation"]) );
    SetRotation   ( AsVectorOfPoses   (objectToRobot["rotation"]) );
    SetSizeExtent (0.08, 0.08, 0.16);

    SetFreshConfig(true);
    SetFreshGeom(true);
}

int const MorseLaserScanner::GetMinLeftIndex()
{
    //ScopedLock_t lock( GetMutex() );
    int minID = 0;
    double minRange = 10.0;

    for(unsigned i = 0; i <= (m_RangeList.size()/2); i++){
        if(GetRange(i) < minRange) {
            minID = i;
            minRange = GetRange(i);
        }
    }

    m_MinLeft = minRange;
    return minID;
}

int const MorseLaserScanner::GetMinRightIndex()
{
    //ScopedLock_t lock( GetMutex() );
    int minID = m_RangeList.size();
    double minRange = 10.0;

    for(unsigned i = ((m_RangeList.size()/2) + 1); i < m_RangeList.size(); i++){
        if(GetRange(i) < minRange) {
            minID = i;
            minRange = GetRange(i);
        }
    }

    m_MinRight = minRange;
    return minID;
}

std::string const MorseLaserScanner::ToString() const
{
    std::string output;

    output += "\"timestamp\": " + NumberToString(GetDataTime()) + ", \n";
    output += "\"point_list\": [\n";
    for(std::size_t i = 0; i < GetPointList().size(); i++)
        output += "(" + NumberToString(i) + ") [\"x\": " + NumberToString(GetPoint(i).px) + ", \"y\": " + NumberToString(GetPoint(i).py) + ", \"phi\": " + NumberToString(GetPoint(i).pa) + "]\n";

    output += "]\n";

    output += "\"range_list\": [\n";
    for(std::size_t i = 0; i < GetPointList().size(); i++)
        output += "(" + NumberToString(i) + "): " + NumberToString(GetRange(i)) + "\n";

    output += "]";

    if( IsIntensityOn() ){
        output += "\"remission_list\": [\n";
        for(unsigned i = 0; i < GetIntensityList().size(); i++){
            output += "(" + NumberToString(i) + "): " + NumberToString(GetIntensity(i)) + "\n";
        }
        output += "]";
    }


    return output;
}

void MorseLaserScanner::RefreshData()
{
    Dictionary_t data = GetLocalData();

    UpdateTime ( AsDouble(data["timestamp"]) );
    AddScanID();

    SetPointList( AsVectorOfPoses (data["point_list"]) );

    SetRangeList( AsVectorOfDoubles (data["range_list"]) );

    if( IsIntensityOn() )
        SetIntensityList( AsVectorOfDoubles(data["remission_list"]) );

    if( m_PointList.size() != m_RangeList.size() )
        throw MorseError("MorseLaserScanner::RefreshData()", "Point list and Range list dont have same size.");

    SetFresh(true);
}

bool const MorseLaserScanner::IsItMe(const std::string &aName, const std::string &aRobotName)
{
    for(std::size_t i = 0; i < identifiers.size(); i++){
        if(aName.compare(aRobotName + "." + identifiers.at(i)) == 0)
            return true;
    }
    return false;
}

void MorseLaserScanner::SetRobotPose(Pose3D_t const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_RobotPose = aValue;
}

std::vector<Pose2D_t> const &MorseLaserScanner::GetPointList() const
{
    //ScopedLock_t lock( GetMutex() );
    return m_PointList;
}

std::vector<double> const &MorseLaserScanner::GetRangeList() const
{
    //ScopedLock_t lock( GetMutex() );
    return m_RangeList;
}

std::vector<double> const &MorseLaserScanner::GetIntensityList() const
{
    //ScopedLock_t lock( GetMutex() );
    return m_IntensityList;
}

void MorseLaserScanner::SetPointList(std::vector<Pose2D_t> const &aPoints)
{
    //ScopedLock_t lock( GetMutex() );
    m_PointList.assign(aPoints.begin(), aPoints.end());
    for(unsigned i = 0; i < m_PointList.size(); i++){
        m_PointList[i].pa = GetScanStart() + (GetScanRes() * i);
    }
}

void MorseLaserScanner::SetRangeList(std::vector<double> const &aRanges)
{
    //ScopedLock_t lock( GetMutex() );
    m_RangeList.assign(aRanges.begin(), aRanges.end());
}

void MorseLaserScanner::SetIntensityOn(bool const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    aValue ? m_IntensityOn = 1 : m_IntensityOn = 0;
}

void MorseLaserScanner::SetIntensityList(std::vector<double> const &aIntensities)
{
    //ScopedLock_t lock( GetMutex() );
    m_IntensityList.assign(aIntensities.begin(), aIntensities.end());
}

void MorseLaserScanner::SetScanningFreq(double const aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_ScanningFreq = aValue;
}

void MorseLaserScanner::AddScanID()
{
    //ScopedLock_t lock( GetMutex() );
    m_ScanID++;
}

