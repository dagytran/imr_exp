#include "MorseTime.h"

using namespace CCMorse;


void MorseTime::SetNow()
{
    //ScopedLock_t lock( GetMutex() );
    m_Now = AsDouble(CommunicateWithSim("now"));
}

void MorseTime::RefreshData()
{
    SetNow();
}

TimeStatistics_t const MorseTime::GetStatistics() const
{
    std::string statsStr = CommunicateWithSim("statistics");
    TimeStatistics_t output = {0, 0, 0, 0};

    if(statsStr.compare("KO.") == 0){
        throw MorseError("MorseTime::GetStatistics()", "Query for simulation time statistics was not succesfull.");
        return output;
    }

    Dictionary_t stats = MapData(statsStr);

    output.mean_frame_by_sec     = AsDouble(stats["mean_frame_by_sec"]);
    output.mean_time             = AsDouble(stats["mean_time"]);
    output.variance_frame_by_sec = AsDouble(stats["variance_frame_by_sec"]);
    output.variance_time         = AsDouble(stats["variance_time"]);

    return output;
}

std::string const MorseTime::GetMode() const
{
    return (CommunicateWithSim("mode"));
}
