/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-18
 *
 * File contains definition of WindowLayer class.
 */

#ifndef CCMORSE_WINDOWPROXY_H
#define CCMORSE_WINDOWPROXY_H

#include "MorseProxy.h"
#include "MorsePen.h"
#include "WindowLayer.h"

#include "../map_grid.h"


namespace CCMorse{

/**
 * WindowProxy class is primarily used for drawing grid maps
 * on canvas in Morse simulation.
 * Based on @ref CCMorse::MorseProxy class.
 */
class WindowProxy : public MorseProxy
{
    public:
        static const double FLOOR    ;// = 0.0;
        static const double UNKNOWN  ;// = 0.5;
        static const double WALL     ;// = 1.0;
        static const double EMPTY    ;// = 0.0;
        static const double OCCUPIED ;// = 1.0;

        /**
         * Get instance of CWindow (reference to object, which can be used to
         * call drawing methods)
         *
         * Example: anywhere in the program in case you want to draw something
         * just include this header, get instance (after it is initialized which is
         * in planning() or exploration() methods in main.cc) and enjoy visualization.
         *
         * @note The reference when calling GetInstance().
         *
         * @code {.cpp}
         * #include "CMapGrid.h"
         * #include "WindowProxy.h"
         * ...
         * WindowProxy& win = GetInstance();
         * win.DrawMap(myMap);
         * ...
         * @endcode
         *
         * @return NULL if instance was not initialize yet, else instance of
         *        window
         */
        static WindowProxy& GetInstance();

        /**
         * Initialization method for window instance. Output is the same as in
         * previous method except the case window is not initialized yet.
         * In that case window is created with input parameters and already ini-
         * tialized instance is returned.
         *
         * To avoid misunderstanding - window dimensions are related to window
         * displayed on screen, image dimensions determine size of output image
         * saved to disk. (Displayed image in window is in fact zoomed)
         *
         * @param   aClient     Pointer to player client.
         * @param   width       Image width (see difference window and image dimensions)
         * @param   height      Image height
         */
        static WindowProxy& GetInstance(PlayerClient* aClient, int width, int height);

        /**
         * Destructor - free memory occupied by data array
         */
        ~WindowProxy() { Unsubscribe(); }

        /**
         * Get height of the grid (number of cells)
         * @return height of the grid in cells
         */
        int const GetHeight() const { return GetVar(GetPen()->GetHeight()); }

        /**
         * Get width of the grid (number of cells)
         * @return width of the grid in cells
         */
        int const GetWidth() const { return GetVar(GetPen()->GetWidth()); }

        /**
         * Get cell position X coordinate from X-direction distance
         * from arena origin
         * @param x    real distance [m]
         * @return position of the cell in the grid
         */
        int GetX(double x) { return GetVar(GetPen()->GetX(x)); }

        /**
         * Get cell position Y coordinate from Y-direction distance
         * from arena origin
         * @param y    real distance [m]
         * @return position of the cell in the grid
         */
        int GetY(double y) { return GetVar(GetPen()->GetY(y)); }

        /**
         * Get real world (arena) position X coordinate from X cell
         * coordinate
         * @param x    position of the cell in the grid
         * @return real distance [m]
         */
        double GetPosX(int x) { return GetVar(GetPen()->GetPosX(x)); }

        /**
         * Get real world (arena) position Y coordinate from Y cell
         * coordinate
         * @param y    position of the cell in the grid
         * @return real distance [m]
         */
        double GetPosY(int y) { return GetVar(GetPen()->GetPosY(y)); }

        /**
         * Sets one pixel with transparency to be drawn
         * next refresh on basic window surface.
         *
         * @param x    X coordinate of pixel
         * @param y    Y coordinate of pixel
         * @param r    Red component of required color
         * @param g    Green component of required color
         * @param b    Blue component of required color
         * @param a    Alpha component of required color
         */
        void SetPixel(int x, int y, int r, int g, int b, int a) { GetPen()->SetPixel(x, y, r, g, b, a); }

        /**
         * Sets one pixel without transparency to be drawn
         * next refresh on basic window surface.
         *
         * @param x    X coordinate of pixel
         * @param y    Y coordinate of pixel
         * @param r    Red component of required color
         * @param g    Green component of required color
         * @param b    Blue component of required color
         */
        void SetPixel(int x, int y, int r, int g, int b) { GetPen()->SetPixel(x, y, r, g, b); }

        /**
         * Draws one pixel with transparency immediately
         * on basic window surface.
         *
         * @param x    X coordinate of pixel
         * @param y    Y coordinate of pixel
         * @param r    Red component of required color
         * @param g    Green component of required color
         * @param b    Blue component of required color
         * @param a    Alpha component of required color
         */
        void DrawPixel(int x, int y, int r, int g, int b, int a) { GetPen()->SetPixel(x, y, r, g, b, a); }

        /**
         * Draws one pixel without transparency immediately
         * on basic window surface.
         *
         * @param x    X coordinate of pixel
         * @param y    Y coordinate of pixel
         * @param r    Red component of required color
         * @param g    Green component of required color
         * @param b    Blue component of required color
         */
        void DrawPixel(int x, int y, int r, int g, int b) { GetPen()->SetPixel(x, y, r, g, b); }

        /**
         * Draw given map.
         * With input type CMap the method distinguishes the WALL and the FLOOR
         * and assigns them black or white color (in the order).
         * Input type CMapGrid contains float values from 0 to 1. They are scaled to
         * 0-255 and this value is assigned to all RGB components (output image is
         * grayscale).
         *
         * These methods refresh basic surface.
         * @param   aMap    Map to be drawn (e.g. map of environment, inflated map or
         *                  map in general use as canvas)
         */
        void DrawMap(imr::CMapGrid &aMap);

        /**
         * Method created specially for user convinience. You can draw whatever you
         * want - map is here created as general purpose canvas. It can be useful
         * for example to draw inflated map over real map to see how it fits.
         * Parameter transparent determines which value in map is said to be invisible
         * (e.g. you want to draw only inflated walls, floor is invisible ->
         *  transparent = CMapGrid::FLOOR).
         * Alpha is parameter specifing value of alpha (transparency) channel. Its
         * value can be integer from TRANSPARENT (=0) to OPAQUE (=255).
         *
         * This method allows user to draw multiple layers over each other whitout
         * loss of visibility of the previous layers (e.g. you can still see original
         * walls under inflated map)
         *
         * @param aMap         Map used as canvas
         * @param color        required RGB color of drawn object
         * @param transparent  value in map supposed to be invisible
         * @param alpha        Alpha channel (transparency) of drawn object
         */
        void DrawMapTransparent(imr::CMapGrid &aMap, RGBA_t color, double transparent, int alpha);

        /**
         * Flush contents of Pen device (variable, see private part of the class)
         * to window (another variable; displayed image in the window).
         *
         * Because surface is freed, user must call first DrawMap() to create basic
         * surface and then it is possible to draw over it another transparent layers
         * with DrawMapTransparent().
         */
        void Flush();

        void SetImageName(std::string const aName) { GetPen()->SetImageName(aName); }

        /**
         * Save actual map (ONLY basic surface so far created using DrawMap()) to PNG
         * image with specified name
         *
         * @param   aName   Name of the file.
         */
        void SavePNG(std::string const aName) { GetPen()->SavePNG(aName); }

        /**
         * Sets straight line to be drawn between points [x0,y0] and [x1,y1] with [r,g,b] color.
         *
         * @param x0   X coordinate of the beginning
         * @param y0   Y coordinate of the beginning
         * @param x1   X coordinate of the end
         * @param y1   Y coordinate of the end
         * @param r    Red component of required color
         * @param g    Green component of required color
         * @param b    Blue component of required color
         */
        void bresenham(int x0, int y0, int x1, int y1, int r, int g, int b) { GetPen()->SetLine(x0, y0, x1, y1, r, g, b); }

        /**
         * Sets straight line to be drawn between points [x0,y0] and [x1,y1] with [r,g,b] color.
         *
         * @param x0   X coordinate of the beginning
         * @param y0   Y coordinate of the beginning
         * @param x1   X coordinate of the end
         * @param y1   Y coordinate of the end
         * @param r    Red component of required color
         * @param g    Green component of required color
         * @param b    Blue component of required color
         */
        void SetLine(int x0, int y0, int x1, int y1, int r, int g, int b) { GetPen()->SetLine(x0, y0, x1, y1, r, g, b); }

        /**
         * Sets straight line to be drawn between points [x0,y0] and [x1,y1] with [r,g,b,a] color.
         *
         * @param x0   X coordinate of the beginning
         * @param y0   Y coordinate of the beginning
         * @param x1   X coordinate of the end
         * @param y1   Y coordinate of the end
         * @param r    Red component of required color
         * @param g    Green component of required color
         * @param b    Blue component of required color
         * @param a    Alpha component of required color
         */
        void SetLine(int x0, int y0, int x1, int y1, int r, int g, int b, int a) { GetPen()->SetLine(x0, y0, x1, y1, r, g, b, a); }

        /**
         * Draws line immediately between points [x0,y0] and [x1,y1] with [r,g,b] color.
         *
         * @param x0   X coordinate of the beginning
         * @param y0   Y coordinate of the beginning
         * @param x1   X coordinate of the end
         * @param y1   Y coordinate of the end
         * @param r    Red component of required color
         * @param g    Green component of required color
         * @param b    Blue component of required color
         */
        void DrawLine(int x0, int y0, int x1, int y1, int r, int g, int b) { GetPen()->DrawLine(x0, y0, x1, y1, r, g, b); }

        /**
         * Draws line immediately between points [x0,y0] and [x1,y1] with [r,g,b,a] color.
         *
         * @param x0   X coordinate of the beginning
         * @param y0   Y coordinate of the beginning
         * @param x1   X coordinate of the end
         * @param y1   Y coordinate of the end
         * @param r    Red component of required color
         * @param g    Green component of required color
         * @param b    Blue component of required color
         * @param a    Alpha component of required color
         */
        void DrawLine(int x0, int y0, int x1, int y1, int r, int g, int b, int a) { GetPen()->DrawLine(x0, y0, x1, y1, r, g, b, a); }

        /**
         * Returns pointer to Pen device.
         */
        MorsePen* GetPen() const { return m_Pen; }

        /**
         * Adds single layer to the vector of layers just by name and then
         * set all parameters separately.
         *
         * @param   lname   Name of the layer.
         * @return -1 if layer could not be added (e.g. there is already present layer
         *         with the same name), 0 otherwise.
         */
        int AddLayer(std::string lname);

        /**
         * Adds single layer to the vector of layers by passing ready-to-use structure.
         *
         * @param   l       Initialize layer structure.
         * @return -1 if layer could not be added (e.g. there is already present layer
         *         with the same name), 0 otherwise.
         */
        int AddLayer(WindowLayer l);

        /**
         * Remove layer specified by name from the vector. If layer with specified name
         * is not found no deletion is performed.

         * @param   lname   Name of the layer to be deleted.
         */
        void RemoveLayer(std::string lname);

        /**
         * Setter that will find the layer by name from the vector.
         * If no such a layer exists, return, else set given property.
         *
         * @param   lname   Name of the modified layer.
         * @param   visible @ref WindowLayer::mVisible
         */
        void SetLayerVisibility(std::string lname, bool visible);

        /**
         * Setter that will find the layer by name from the vector.
         * If no such a layer exists, return, else set given property.
         *
         * @param   lname       Name of the modified layer.
         * @param   priority    @ref WindowLayer::mPriority
         */
        void SetLayerPriority(std::string lname, int priority);

        /**
         * Setter that will find the layer by name from the vector.
         * If no such a layer exists, return, else set given property.
         *
         * @param   lname   Name of the modified layer.
         * @param   alpha   @ref WindowLayer::mAlpha
         */
        void SetLayerAlpha(std::string lname, int alpha);

        /**
         * Setter that will find the layer by name from the vector.
         * If no such a layer exists, return, else set given property.
         *
         * @param   lname       Name of the modified layer.
         * @param   transparent @ref WindowLayer::mTransparent
         */
        void SetLayerTransparent(std::string lname, double transparent);

        /**
         * Setter that will find the layer by name from the vector.
         * If no such a layer exists, return, else set given property.
         *
         * @param   lname   Name of the modified layer.
         * @param   color   @ref WindowLayer::mColor
         */
        void SetLayerColor(std::string lname, RGBA_t color);

        /**
         * Setter that will find the layer by name from the vector.
         * If no such a layer exists, return, else set given property.
         *
         * @param   lname   Name of the modified layer.
         * @param   aMap    @ref WindowLayer::mMap
         */
        void SetLayerData(std::string lname, imr::CMapGrid &aMap);

    private:
        /**
         *  Simple constructor.
         */
        WindowProxy(PlayerClient* aClient) : MorseProxy(aClient, 0) { SetupConfiguration(); }

        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        /**
         * Subscribe proxy to the robot.
         *
         * @param   aIndex  Index of the robot we want to connect to.
         */
        void Subscribe(uint32_t aIndex);

        /**
         * Unsubscribe from the robot.
         */
        void Unsubscribe();

        MorsePen* m_Pen;        ///< Morse Pen actuator.

        std::string m_Name,     ///< Name of the image.
                    m_Drawing;  ///< Name of the image files when flushing without Morse.

        std::vector<WindowLayer> m_Layers;  ///< Vector of displayable layers.

        static WindowProxy* m_Me;   ///< Pointer to one instance of this class.

        /**
         * Check if provided pixel coordinates are on canvas.
         *
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   aFun Caller function name. For exception call.
         * @return  [@c true: if pixel is on canvas; @c false: otherwise]
         */
        bool const CheckCoordinates(int const aX, int const aY, std::string const aFun) const { return MorsePen::CheckCoordinates(aX, GetWidth(), aY, GetHeight(), aFun); }

        /**
         * Swap value in x with value in y.
         *
         * @param   x   First swapped value.
         * @param   y   Second swapped value.
         */
        void SWAP(int &x, int &y);

        /**
         * Sort layers in descendant way. Prioritized layer have to be drawn later.
         * Before layers are drawn they are sorted by the priority to simplify
         * drawing itself. This method compares two layers by the priority.
         * If priorities are the same, layers are ordered by alpha (more transparent
         * layer is over the more opaque one)
         * It is not intended to be called from anywhere else than sort algorithm.
         *
         * @param   l1  First layer.
         * @param   l2  Second layer.
         * @return  [@c true: when the element passed as first argument is considered
         *         to go before the second, @c false: otherwise]
         */
        static bool SortLayers(struct WindowLayer l1, struct WindowLayer l2);

        /**
         * Determine layer's index in the vector layers from its name.
         * Used when modifying layer's properties identified by the name.
         *
         * @param   lname   Name of the layer.
         * @return  Index in the vector.
         */
        int GetLayerIndex(std::string lname);

};
} // namespace CCMorse;

#endif // CCMORSE_WINDOWPROXY_H
