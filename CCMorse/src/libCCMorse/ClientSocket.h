/*
*  Definition of the ClientSocket class
*
*  From: http://www.tldp.org/LDP/LG/issue74/tougher.html#4
*  Copyright © 2002, Rob Tougher.
*  Published in Issue 74 of Linux Gazette, January 2002
*/

#ifndef CLIENTSOCKET_H
#define CLIENTSOCKET_H

#include "Socket.h"
#include "SocketException.h"


class ClientSocket : private Socket
{
    public:
        ClientSocket ( std::string host, int port );
        virtual ~ClientSocket(){};

        const ClientSocket& operator << ( const std::string& ) const;
        const ClientSocket& operator >> ( std::string& ) const;
};

#endif
