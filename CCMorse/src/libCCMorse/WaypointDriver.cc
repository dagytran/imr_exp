#include "WaypointDriver.h"

using namespace CCMorse;


/// ----------------------------------------------------------------------------
/// - Class CCMorse::WaypointDriver
void WaypointDriver::SetupProxies()
{
    MorseDevice* wp = GetRobot()->GetDevice( GetRobot()->GetName() + ".waypoint" );

    if( wp->GetID() == (-1)){
        delete wp;
        throw MorseError("WaypointDriver::SetupProxies()", "Unable to find Waypoint device.");
        m_Waypoint = NULL;

    } else {
        m_Waypoint = reinterpret_cast<MorseWaypoint*>(wp);
    }
}

/// ----------------------------------------------------------------------------

bool WaypointDriver::HasRobotEveryProxy(MorseRobot* aRobot)
{
    MorseDevice* wp = aRobot->GetDevice( aRobot->GetName() + ".waypoint" );

    if( wp->GetID() == (-1) ){
        delete wp;
        return false;
    }

    return true;
}

/// ----------------------------------------------------------------------------

void WaypointDriver::GoTo()
{
    if( m_Waypoint != NULL ){
        Pose2D_t pos = GetRobot()->GetGoalPos();
        m_Waypoint->GoTo(pos.px, pos.py, pos.pa, GetRobot()->GetGoalPositionTol(), GetRobot()->GetMaxSpeed());

    } else {
        throw MorseError("WaypointDriver::GoTo()", "Driver do not has Waypoint device.");
    }
}

/// - end of file --------------------------------------------------------------
