/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-17
 *
 * File contains definition of MorseWaypoint class.
 */


#ifndef CCMORSE_MORSEWAYPOINT_H
#define CCMORSE_MORSEWAYPOINT_H

#include "MorseActuator.h"


namespace CCMorse{

/**
 * MorseWaypoint class is representing Waypoint actuator in Morse simulaition.
 * Based on @ref CCMorse::MorseActuator class.
 *
 * @see http://www.openrobots.org/morse/doc/1.2/user/actuators/waypoint.html
 */
class MorseWaypoint : public MorseActuator
{
    public:
        /**
         * Default constructor.
         */
        MorseWaypoint() : MorseActuator() { SetupConfiguration(); }

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorseWaypoint(int aPort, std::string aName) : MorseActuator(aPort, aName) { SetupConfiguration(); }

        /**
         * Destructor.
         */
        virtual ~MorseWaypoint() {}

        /**
         * Sends command to Waypoint device to go on given postion.
         *
         * @param   aX      X coordinate of desired goal position of the robot. [m]
         * @param   aY      Y coordinate of desired goal position od the robot. [m]
         * @param   aZ      Z coordinate of desired goal position od the robot. [m]
         * @param   aTolerance  Positional tolerance of goal. [m]
         * @param   aSpeed  Maximal motion speed of robot. [m/s, rad/s]
         */
        void GoTo(double aX, double aY, double aZ, double aTolerance, double aSpeed);

        /**
         * Returns whether it arrived to given position.
         */
        bool IsArived();

        /**
         * Stops executing navigation to given position.
         */
        void Stop();

        /**
         * Returns string representation of status.
         */
        std::string const ToString();

        /**
         * Static function that can be used to assure that given device is Waypoint.
         *
         * @warning This function relies on that device name is same as name of device type.
         * For example device named "waypoint" or "wp" is considered as Waypoint device but device
         * named "position" not.
         *
         * @param   aName       Name of the device.
         * @param   aRobotName  Name of the robot that is suppoused to own device.
         * @return  [@c true: is Waypoint device, @c false: not Waypoint device]
         */
        static bool const IsItMe(const std::string &aName, const std::string &aRobotName);

        /**
         * If motors are enabled sends command to Morse simulation.
         */
        virtual void RefreshData();

    private:
        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        static std::string idWP[];  ///< Array of Waypoint identificators. Used in @ref MorseWaypoint::IsItMe.
        static std::vector<std::string> identifiers;    ///< Vector of Waypoint identificators. Used in @ref MorseWaypoint::IsItMe.

};
} // namespace CCMorse;

#endif // CCMORSE_MORSEWAYPOINT_H
