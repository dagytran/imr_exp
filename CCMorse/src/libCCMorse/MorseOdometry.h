/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of MorseOdometry class.
 */

#ifndef CCMORSE_MORSEODOMETRY_H
#define CCMORSE_MORSEODOMETRY_H

#include "MorseSensor.h"


namespace CCMorse{

/**
 * Odometry class can be used for obtaining data from odometry
 * sensor in Morse simulation.
 *
 * @see https://www.openrobots.org/morse/doc/stable/user/sensors/odometry.html
 */
class MorseOdometry : public MorseSensor
{
    friend class MorseClient;
    friend class Position2dProxy;

    public:

        /**
         * Default constructor.
         */
        MorseOdometry() : MorseSensor() { SetupConfiguration(); }

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorseOdometry(int aPort, std::string aName) : MorseSensor(aPort, aName) { SetupConfiguration(); }

        /**
         * Destructor.
         */
        ~MorseOdometry() { }

        /**
         * Returns value of X in cooridate system.
         * @return  X cooridate of position. [m]
         */
        double const GetX() const          { return GetVar(m_X); }

        /**
         * Returns value of Y in cooridate system.
         * @return  Y cooridate of position. [m]
         */
        double const GetY() const          { return GetVar(m_Y); }

        /**
         * Returns value of Y in cooridate system.
         * @return  Y cooridate of position. [m]
         */
        double const GetZ() const          { return GetVar(m_Z); }

        /**
         * Returns value of Yaw of the sensor.
         * @return  Yaw. Rotation angle with respect to the Z axis. [rad]
         */
        double const GetYaw() const        { return GetVar(m_Yaw); }

        /**
         * Returns value of Pitch of the sensor.
         * @return  Pitch. Rotation angle with respect to the Y axis. [rad]
         */
        double const GetPitch() const      { return GetVar(m_Pitch); }

        /**
         * Returns value of Roll of the sensor.
         * @return  Roll. Rotation angle with respect to the X axis. [rad]
         */
        double const GetRoll() const       { return GetVar(m_Roll); }

        /**
         * Returns value of delta X in cooridate system.
         * Delta is difference between new and previous data.
         * @return  Delta of X cooridate of position. [m]
         */
        double const GetDeltaX() const     { return GetVar(m_dX); }

        /**
         * Returns value of delta Y in cooridate system.
         * Delta is difference between new and previous data.
         * @return  Delta of Y cooridate of position. [m]
         */
        double const GetDeltaY() const     { return GetVar(m_dY); }

        /**
         * Returns value of delta Z in cooridate system.
         * Delta is difference between new and previous data.
         * @return  Delta of Z cooridate of position. [m]
         */
        double const GetDeltaZ() const     { return GetVar(m_dZ); }

        /**
         * Returns value of delta Yaw.
         * Delta is difference between new and previous data
         * @return  Delta Yaw. Difference of rotation angles with respect to the Z axis. [rad]
         */
        double const GetDeltaYaw() const   { return GetVar(m_dYaw); }

        /**
         * Returns value of delta Pitch.
         * Delta is difference between new and previous data
         * @return  Delta Pitch. Difference of rotation angles with respect to the Y axis. [rad]
         */
        double const GetDeltaPitch() const { return GetVar(m_dPitch); }

        /**
         * Returns value of delta Roll.
         * Delta is difference between new and previous data
         * @return  Delta Roll. Difference of rotation angles with respect to the X axis. [rad]
         */
        double const GetDeltaRoll() const  { return GetVar(m_dRoll); }

        /**
         * Returns absolute postion of the sensor in 2D.
         * @return 2D Position (X [m], Y [m], Yaw [rad]).
         */
        Pose2D_t const GetPose2D() const;

        /**
         * Returns absolute postion of the sensor in 3D.
         * @return 3D Position (X [m], Y [m], Z[m], Yaw [rad], Pitch [rad], Roll [rad]).
         */
        Pose3D_t const GetPose3D() const;

        /**
         * Gets new odometry position data from Morse simulation.
         */
        virtual void RefreshData();

        /**
         * Returns string representation of position data stored in sensor.
         */
        std::string const ToString() const;

        /**
         * Static function that can be used to assure that given device is Odometry.
         *
         * @warning This function relies on that device name is same as name of device type.
         * For example device named "odo" is considered as Odometry device but device
         * named "position" not.
         *
         * @param   aName       Name of the device.
         * @param   aRobotName  Name of the robot that is suppoused to own device.
         * @return  [@c true: is Odometry device, @c false: not Odometry device]
         */
        static bool const IsItMe(const std::string &aName, const std::string &aRobotName);

    private:

        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        /**
         * Sets X value.
         */
        void SetX(double const &aValue);

        /**
         * Sets Y value.
         */
        void SetY(double const &aValue);

        /**
         * Sets Z value.
         */
        void SetZ(double const &aValue);

        /**
         * Sets Yaw value.
         */
        void SetYaw(double const &aValue);

        /**
         * Sets Pitch value.
         */
        void SetPitch(double const &aValue);

        /**
         * Sets Roll value.
         */
        void SetRoll(double const &aValue);

        /**
         * Sets delta X value.
         */
        void SetDeltaX(double const &aValue);

        /**
         * Sets delta Y value.
         */
        void SetDeltaY(double const &aValue);

        /**
         * Sets delta Z value.
         */
        void SetDeltaZ(double const &aValue);

        /**
         * Sets delta Yaw value.
         */
        void SetDeltaYaw(double const &aValue);

        /**
         * Sets delta Pitch value.
         */
        void SetDeltaPitch(double const &aValue);

        /**
         * Sets delta Roll value.
         */
        void SetDeltaRoll(double const &aValue);

        double  m_X,        ///< X coordinate of the sensor. [m]
                m_Y,        ///< Y coordinate of the sensor. [m]
                m_Z,        ///< Z coordinate of the sensor. [m]

                m_Yaw,      ///< Rotation angle with respect to the Z axis. [rad]
                m_Pitch,    ///< Rotation angle with respect to the Y axis. [rad]
                m_Roll,     ///< Rotation angle with respect to the X axis. [rad]

                m_dX,       ///< Delta of X coordinate of the sensor. [m]
                m_dY,       ///< Delta of Y coordinate of the sensor. [m]
                m_dZ,       ///< Delta of Z coordinate of the sensor. [m]

                m_dYaw,     ///< Delta of rotation angle with respect to the Z axis. [rad]
                m_dPitch,   ///< Delta of rotation angle with respect to the Y axis. [rad]
                m_dRoll;    ///< Delta of rotation angle with respect to the X axis. [rad]

        static std::string idO[];   ///< Array of Odometry identificators. Used in @ref MorseOdometry::IsItMe.
        static std::vector<std::string> identifiers;    ///< Vector of Odometry identificators. Used in @ref MorseOdometry::IsItMe.

};
} // namespace CCMorse;

#endif // CCMORSE_MORSEODOMETRY_H
