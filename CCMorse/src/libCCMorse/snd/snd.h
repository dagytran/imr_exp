/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-18
 *
 * File contains definition of SNDNavigation class.
 */

#ifndef CCMORSE_SND_NAVIGATION_H
#define CCMORSE_SND_NAVIGATION_H

#include "../Position2dProxy.h"
#include "../LaserProxy.h"
#include "../MorseRobot.h"

#include "gap_and_valley.h"

namespace CCMorse {

class SNDNavigation {

    public:
        /**
         * Constructor.
         *
         * @param   aLaser      Pointer to subscribed laser proxy.
         * @param   aPosInput   Pointer to subscribed position 2d proxy that will be used as input.
         * @param   aPosOutput  Pointer to subscribed position 2d proxy that will be used as output.
         * @param   aRobot      Pointer to robot, mainly used to get robot and driver configuration.
         */
		SNDNavigation( LaserProxy* const aLaser, Position2dProxy* const aPosInput, Position2dProxy* const aPosOutput,
            MorseRobot* const aRobot );

        /**
         * Checks whether rising gap is safe.
         */
		bool isRisingGapSafe(Gap* pRisingGap, int iValleyDir, std::vector<double> fullLP, double fScanRes, double fMaxRange, double R);

		/**
		 * Checks whether filter is clear.
		 */
		bool isFilterClear(int iCenterSector, double width, double forwardLength, bool bDoRearCheck, std::vector<double> fullLP, double angRes, bool bPrint);

		/**
		 * Navigate robot to its goal position.
		 */
		void GoTo(bool slowDown);

	private:
        /**
         * Initialization method.
         */
        bool init();

        bool isInit;            ///< If navigation was initialized.

		MorseRobot& m_Robot;    ///< Reference to robot.

        LaserProxy& m_LP;       ///< Reference to LaserProxy providing lase data.

        Position2dProxy& m_PPI; ///< Reference to Position2dProxy providing position data.
		Position2dProxy& m_PPO; ///< Reference to Position2dProxy used to send commands to motors.

		double R; 				///< The radius of the minimum circle which contains the robot
		double minGapWidth; 	///< Minimum passage width the driver will try to exploit
		double safetyDistMax; 	///< Maximum distance allowed from an obstacle
		double maxSpeed; 		///< Maximum speed allowed
		double maxTurnRate; 	///< Maximum angular speed allowed
		double goalPositionTol; ///< Maximum distance allowed from the final goal for the algorithm to stop. (m)
		double goalAngleTol; 	///< Maximum angular error from the final goal position for the algorithm to stop (rad)
		double fMaxRange;       ///< Maximum range of sensor. [m]
		double fScanRes;        ///< Angular resolution of the scan. [rad]

		int iNumLPs;            ///< Number of points in the scan.
		int iNumSectors;        ///< Number of scan sectors.

};
}

#endif // CCMORSE_SND_NAVIGATION_H
