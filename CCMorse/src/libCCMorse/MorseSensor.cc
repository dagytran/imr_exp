#include "MorseSensor.h"
#include "ClientSocket.h"
#include "SocketException.h"
#include "MorseTime.h"
#include "MorseClient.h"

#include <stdlib.h>

using namespace CCMorse;


std::string MorseSensor::RecieveData()
{
    std::string data = "{}";
    try {

        ClientSocket socket ("localhost", GetPort());
        RecieveFromSocket(socket);

    } catch(SocketException& e) {
        std::cout << "Exception was caught:" << e.description() << "\n";
    }

    return data;
}

Dictionary_t const MorseSensor::GetLocalData() const
{
    return MapData( CommunicateWithSim("get_local_data") );
}
