#include "MorseActuator.h"
#include "ClientSocket.h"
#include "SocketException.h"

using namespace CCMorse;


void MorseActuator::SendCommand(std::string const &aJSON) const // JSON formated string
{
    try{
        ClientSocket client_socket ( "localhost", GetPort() );
        try{
            std::cout << "Sending command: " << aJSON << " to device: \"" << GetName() << "\" on port: " << NumberToString(GetPort()) << std::endl;
            client_socket << aJSON;
        }
        catch ( SocketException& ) {}
    }
    catch ( SocketException& e ){
        std::cerr << "Exception was caught:" << e.description() << std::endl;
    }
}
