/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of MorseActuator class.
 */

#ifndef CCMORSE_MORSEACTUATOR_H
#define CCMORSE_MORSEACTUATOR_H

#include "MorseDevice.h"


namespace CCMorse{

/**
 * Actuator class.
 * Base class for actuator classes.
 *
 * Based on @ref CCMorse::MorseDevice class.
 */
class MorseActuator : public MorseDevice
{
    public:
        /**
         * Default constructor of device.
         */
        MorseActuator() : MorseDevice() {}

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorseActuator(int aPort, std::string aName) : MorseDevice(aPort, aName) {}

        /**
         * Copy Constructor.
         */
        MorseActuator(const MorseActuator &aActuator) : MorseDevice( aActuator ) {}

        /**
         * Destructor.
         */
        virtual ~MorseActuator() {}

        /**
         * Sends command to devices data stream socket.
         */
        void SendCommand(std::string const &aJSON) const;
        void SendCommand() { SendCommand(ToString()); }

        /**
         * Function that sets fresh data to simulation actuator when available.
         */
        virtual void RefreshData() {}

};
} // namespace CCMorse;

#endif // CCMORSE_MORSEACTUATOR_H
