/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-18
 *
 * File contains definition of Position2dProxy class.
 */

#ifndef CCMORSE_MORSEPOSITION2DPROXY_H
#define CCMORSE_MORSEPOSITION2DPROXY_H

#include "MorseProxy.h"
#include "MorsePose.h"
#include "MorseMotionVW.h"
#include "MorseOdometry.h"


namespace CCMorse{

/**
 * Position2dProxy class.
 * Based on @ref CCMorse::MorseProxy class.
 * Implementing the same interface as PlayerCc::Position2dProxy to
 * use it with Morse simulator.
 *
 * @see http://playerstage.sourceforge.net/doc/Player-3.0.2/player/classPlayerCc_1_1Position2dProxy.html
 */
class Position2dProxy : public MorseProxy
{
    public:
        /**
         * Constructor.
         *
         * @param   aClient Pointer to PlayerClient.
         * @param   aIndex  Index of the proxy.
         */
        Position2dProxy(PlayerClient* aClient, int aIndex = 0) : MorseProxy(aClient, aIndex) { SetupConfiguration(); }

        /**
         * Copy constructor.
         */
        Position2dProxy(Position2dProxy const &aOther);

        /**
         * Destructor.
         */
        virtual ~Position2dProxy() { Unsubscribe(); }

        /**
         * Sends a motor command for velocity control mode.
         * Specify the forward, sideways, and angular speeds.
         *
         * @param   aXSpeed     Linear velocity in X direction (forward movement) [m/s].
         * @param   aYSpeed     Linear velocity in Y direction (sidewards movement) [m/s].
         * @param   aYawSpeed   Angular velocity [rad/s].
         */
        void SetSpeed(double aXSpeed, double aYSpeed, double aYawSpeed);

        /**
         * Sends a motor command for velocity control mode.
         * Specify the forward and angular speeds, but doesn't take the sideways speed.
         * (so use this one for non-holonomic robots).
         *
         * @param   aXSpeed     Linear velocity in X direction (forward movement) [m/s].
         * @param   aYawSpeed   Angular velocity [rad/s].
         */
        void SetSpeed(double aXSpeed, double aYawSpeed) { return SetSpeed(aXSpeed, 0, aYawSpeed); }

        /**
         * Sends a motor command for velocity control mode.
         * Specify the forward, sideways, and angular speeds.
         *
         * @param   vel (Forward speed [m/s], Sideways speed [m/s], Angular speed [rad/s]).
         */
        void SetSpeed(Pose2D_t vel) { return SetSpeed(vel.px, vel.py, vel.pa); }

        /**
         * Send a motor command for velocity/heading control mode.
         * Specify the forward and sideways velocity (m/sec), and angular
         * heading (rads).
         */
        void SetVelHead(double aXSpeed, double aYSpeed, double aYawHead);

        /**
         * Same as the previous SetVelHead(), but doesn't take the yspeed speed
         * (so use this one for non-holonomic robots).
         */
        void SetVelHead(double aXSpeed, double aYawHead) { return SetVelHead(aXSpeed, 0, aYawHead); }

        /**
         * Send a motor command for position control mode.
         * If motion speed is higher that maximal configured for the robot,
         * the desired motion speed is ignored and robot will use maximal configured speed.
         *
         * @param   aPos    Desired goal position of the robot as a Pose2D_t. (X[m], Y[m], Yaw[rad])
         * @param   aVel    Desired motion speed as a Pose2D_t. (VX[m/s], VY[m/s], W[rad/s])
         */
        void GoTo(Pose2D_t aPos, Pose2D_t aVel);

        /**
         * Send a motor command for position control mode using maximal configured speed.
         * @param   aPos    Desired goal position of the robot as a Pose2D_t. (X[m], Y[m], Yaw[rad])
         */
        void GoTo(Pose2D_t aPos);

        /**
         * Send a motor command for position control mode using maximal configured speed.
         *
         * @param   aX      X coordinate of desired goal position of the robot. [m]
         * @param   aY      Y coordinate of desired goal position od the robot. [m]
         * @param   aYaw    Angular position of the robot on goal position. [rad]
         */
        void GoTo(double aX, double aY, double aYaw);

        /**
         * Send a motor command for position control mode.
         * If motion speed is higher that maximal configured for the robot,
         * the desired motion speed is ignored and robot will use maximal configured speed.
         *
         * @param   aX      X coordinate of desired goal position of the robot. [m]
         * @param   aY      Y coordinate of desired goal position od the robot. [m]
         * @param   aYaw    Angular position of the robot on goal position. [rad]
         * @param   vX      Desired forward motion speed of the robot. [m/s]
         * @param   vY      Desired sideward motion speed of the robot. [m/s]
         * @param   vYaw    Desired angular motion speed of the robot. [rad/s]
         */
        void GoTo(double aX, double aY, double aYaw, double vX, double vY, double vYaw);

        /**
         * Sets command for carlike robot.
         */
        void SetCarlike(double aXSpeed, double aDriveAngle) {}

        /**
         * Request the current laser configuration; it is read into the
         * relevant class attributes.
         */
        void RequestGeom() {}

        /**
         * Returns the robots pose with respect to its body.
         */
        Pose3D_t GetOffset() { return GetVar(m_Pose->GetGeoPose()); }

        /**
         * Returns the size.
         */
        BBox3D_t GetSize() { return GetVar(m_Pose->GetSizeExtent()); }

        /**
         * Enables or disables the motors.
         *
         * Be VERY careful with this method!  Your robot is likely to run across the
         * room with the charger still attached.
         *
         * @param   enable  Set state to @c false to disable or @c true to enable motors.
         */
        void SetMotorEnable(bool enable);

//        /**
//         * Select velocity control mode.
//         *
//         * For the the p2os_position driver, set @p mode to 0 for direct wheel
//         * velocity control (default), or 1 for separate translational and
//         * rotational control.
//         *
//         * For the reb_position driver: 0 is direct velocity control, 1 is for
//         * velocity-based heading PD controller (uses DoDesiredHeading()).
//         */
//        void SelectVelocityControl(unsigned char mode);

        /**
         * Resets odometry to (0,0,0).
         */
        void ResetOdometry();

        /**
         * Sets the odometry to the pose @p (x, y, yaw).
         *
         * @param   aX      X coordinate of odometric position of the robot. [m]
         * @param   aY      Y coordinate of odometric position od the robot. [m]
         * @param   aYaw    Angular odometric position of the robot. [rad]
         */
        void SetOdometry(double aX, double aY, double aYaw);

/// @cond COMMENTED
//        /**
//         * Selects position mode.
//         * @param   mode    Set to 0 for velocity mode, 1 for position mode.
//         */
//        void SelectPositionMode(unsigned char mode);

//        /**
//         * Set PID terms.
//         */
//        void SetSpeedPID(double kp, double ki, double kd);

//        /**
//         * Set PID terms.
//         */
//        void SetPositionPID(double kp, double ki, double kd);

//        /**
//         * Set speed ramping profile.
//         *
//         * @param   spd Angular speed. [rad/s]
//         * @param   acc Acceleration. [rad/s*s]
//         */
//        void SetPositionSpeedProfile(double spd, double acc);

//        void DoStraightLine(double m);

//        void DoRotation(double yawspeed);

//        void DoDesiredHeading(double yaw, double xspeed, double yawspeed);
/// @endcond

        /**
         * Returns value of X in cooridate system.
         * @return  X cooridate of position. [m]
         */
        double GetXPos() const { return (GetVar(m_Global ? m_Pose->GetX() : m_Odometry->GetX())); }

        /**
         * Returns value of X in cooridate system.
         * @return  X cooridate of position. [m]
         */
        double GetYPos() const { return (GetVar(m_Global ? m_Pose->GetY() : m_Odometry->GetY())); }

        /**
         * Returns value of Yaw of the sensor.
         * @return  Yaw. Rotation angle with respect to the Z axis. [rad]
         */
        double GetYaw() const { return (GetVar(m_Global ? m_Pose->GetYaw() : m_Odometry->GetYaw())); }

        /**
         * Returns the value of set forward speed.
         * @return Linear velocity in X direction (forward movement) [m/s].
         */
        double GetXSpeed() const { return GetVar(m_Motion->GetVX()); }

        /**
         * Returns the value of set sideways speed.
         * @return Linear velocity in Y direction (sidewards movement) [m/s].
         */
        double GetYSpeed() const { return GetVar(m_Motion->GetVY()); }

        /**
         * Returns the value of set angular speed.
         * @return Angular velocity [rad/s].
         */
        double GetYawSpeed() const { return GetVar(m_Motion->GetW()); }

        /**
         * Returns stall flag.
         */
        bool GetStall() const { return (GetVar(m_Motion->IsStall()) != 0 ? true : false); }

    private:
        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        /**
         * Subscribe proxy to the robot.
         *
         * @param   aIndex  Index of the robot we want to connect to.
         */
        void Subscribe(uint32_t aIndex);

        /**
         * Unsubscribe from the robot.
         */
        void Unsubscribe();

        MorsePose*      m_Pose;     ///< Pointer to MorsePose sensor.
        MorseMotionVW*  m_Motion;   ///< Pointer to MorseMotionVW actuator.
        MorseOdometry*  m_Odometry; ///< Pointer to MorseOdometry sensor.

        bool m_Global;  ///< If proxy stores global or odometric position.

};
} // namespace CCMorse;

#endif // CCMORSE_MORSEPOSITION2DPROXY_H
