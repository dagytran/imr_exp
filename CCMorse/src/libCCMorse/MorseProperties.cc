#include "MorseProperties.h"

using namespace CCMorse;


std::string MorseProperties::idPR[] = {"properties", "configuration", "configurations", "config", "robot_properties"};
std::vector<std::string> MorseProperties::identifiers (idPR, idPR + sizeof(idPR)/sizeof(std::string) );

void MorseProperties::SetupConfiguration()
{
    Dictionary_t config = GetConfigurations();

    m_RobotWidth      = AsDouble(config["RobotWidth"]);
    m_RobotLength     = AsDouble(config["RobotLength"]);
    m_RobotRadius     = AsDouble(config["RobotRadius"]);
    m_RobotMass       = AsDouble(config["RobotMass"]);
    m_LaserFreq       = AsDouble(config["LaserFreq"]);
    m_MaxSpeed        = AsDouble(config["MaxSpeed"]);
    m_MaxTurnRate     = AsDouble(config["MaxTurnRate"]);
    m_WheelsDist      = AsDouble(config["WheelsDist"]);
    m_MinGapWidth     = AsDouble(config["MinGapWidth"]);
    m_SafetyDistMax   = AsDouble(config["SafetyDistMax"]);
    m_GoalPositionTol = AsDouble(config["GoalPositionTol"]);
    m_GoalAngleTol    = AsDouble(config["GoalAngleTol"]);

    m_LaserRssi = AsBool(config["LaserRssi"]);

    m_RobotColor = config["RobotColor"];
    m_Driver     = config["Driver"];

    m_ProvidesGlobalPosition2dIndexList       = AsVectorOfIntegers(config["RbtPrvGblPosition2d"]);
    m_ProvidesLocalPosition2dIndexList        = AsVectorOfIntegers(config["RbtPrvLclPosition2d"]);
    m_ProvidesLaserIndexList                  = AsVectorOfIntegers(config["RbtPrvLaser"]);
    m_ProvidesWindowIndexList                 = AsVectorOfIntegers(config["RbtPrvWindow"]);
    m_DriverProvidesPosition2dIndexList       = AsVectorOfIntegers(config["DrvPrvPosition2d"]);
    m_DriverRequiresInputPosition2dIndexList  = AsVectorOfIntegers(config["DrvReqInputPosition2d"]);
    m_DriverRequiresOutputPosition2dIndexList = AsVectorOfIntegers(config["DrvReqOutputPosition2d"]);
    m_DriverProvidesLaserIndexList            = AsVectorOfIntegers(config["DrvPrvLaser"]);
    m_DriverRequiresInputLaserIndexList       = AsVectorOfIntegers(config["DrvReqInputLaser"]);
    m_DriverRequiresOutputLaserIndexList      = AsVectorOfIntegers(config["DrvReqOutputLaser"]);

    Dictionary_t objectToRobot = MapData(config["object_to_robot"]);

    SetGeoPose    ( AsVectorOfDoubles (objectToRobot["translation"]) );
    SetRotation   ( AsVectorOfPoses   (objectToRobot["rotation"]) );
    SetSizeExtent (0.02, 0.02, 0.02);

    SetFreshConfig(true);
    SetFreshGeom(true);
    SetFresh(true);
}

std::vector<int> const MorseProperties::GetProvidesPosition2dIndexList() const
{
    std::vector<int> output = GetProvidesGlobalPosition2dIndexList();
    output.insert(output.begin(), GetProvidesLocalPosition2dIndexList().begin(), GetProvidesLocalPosition2dIndexList().end());
    return output;
}

bool const MorseProperties::IsItMe(const std::string &aName, const std::string &aRobotName)
{
    for(std::size_t i = 0; i < identifiers.size(); i++){
        if(aName.compare(aRobotName + "." + identifiers.at(i)) == 0)
            return true;
    }
    return false;
}
