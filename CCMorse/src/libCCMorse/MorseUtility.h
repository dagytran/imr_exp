/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-17
 *
 * File contains definition of types and functions common among CCMorse classes.
 */

#ifndef CCMORSE_MORSEUTILITY_H
#define CCMORSE_MORSEUTILITY_H

#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <unistd.h>


#ifndef CCMORSE_PLAYERCLIENT_H
    //#define HAVE_BOOST_SIGNALS
    #define HAVE_BOOST_THREAD

    #ifdef HAVE_BOOST_SIGNALS
        #include <boost/signal.hpp>
    #endif

    #ifdef HAVE_BOOST_THREAD
        #include <boost/thread/mutex.hpp>
        #include <boost/thread/recursive_mutex.hpp>
        #include <boost/thread/thread.hpp>
        #include <boost/thread/xtime.hpp>
        #include <boost/bind.hpp>
    #else
        // we have to define this so we don't have to
        // comment out all the instances of scoped_lock
        // in all the proxies
        namespace boost {
            class thread {
                public: thread() {};
            };

            class mutex {
                public:
                    mutex() {};
                    class scoped_lock {
                        public: scoped_lock(mutex) {};
                    };
            };
        }
    #endif // HAVE_BOOST_THREAD
#endif // CCMORSE_PLAYERCLIENT_H


#include "ClientSocket.h"

namespace CCMorse{


/**
 * Our scoped lock type.
 */
typedef boost::mutex::scoped_lock ScopedLock_t;

/**
 * Our mutex type.
 */
typedef boost::mutex Mutex_t;

/**
 * Out thread type.
 */
typedef boost::thread Thread_t;

/**
 * Teplate function, returns string representation of given type.
 *
 * @param   aNum    Number to be converted to string.
 * @return  String of given number.
 */
template <typename T>
std::string const NumberToString (T const &aNum);

/**
 * Teplate function, returns number in type to witch is assigned from string representation of the number.
 *
 * @param   aStr    String representing number to be converted to number.
 * @return  Number from given stirng.
 */
template <typename T>
T const StringToNumber(std::string const &aStr);

/**
 * Teplate function, returns minimum from two given values.
 *
 * @param   aX, aY  Given values.
 * @return  Minimal value.
 */
template <typename T>
T const Min(T aX, T aY);

/**
 * Teplate function, returns maximum from two given values.
 *
 * @param   aX, aY  Given values.
 * @return  Maximal value.
 */
template <typename T>
T const Max(T aX, T aY);

/**
 * Teplate function, clamps given number.
 * Function returns value that is higher or same as
 * low boundary and at the same time is lower or same as high boundary,
 * else returns lower boundary when given value is lower than low boundary
 * or returns high boundary when given value is higher than high boundary.
 *
 * @param   aX      Given values.
 * @param   aMin    Low boundary, minimal value.
 * @param   aMax    High boundary, maximal value.
 * @return  Clamped value.
 */
template <typename T>
T const Clamp(T aX, T aMin, T aMax);

/**
 * Get a variable using scoped lock. All Get functions need to use this when
 * accessing data to make sure the data access is thread safe.
 */
template<typename T>
T GetVar(const T &aV, Mutex_t &aMtx);

// Custom types
//--------------------------------------------------
/**
 * Structure used to define point in 2D cartesian space.
 */
typedef struct {
    double  px,     ///< X coordinate in 2D cartesian space. [m]
            py;     ///< Y coordinate in 2D cartesian space. [m]
} Point2D_t;

/**
 * Structure used to define position in 2D space.
 */
typedef struct {
    double  px,     ///< X coordinate in 2D cartesian space. [m]
            py,     ///< Y coordinate in 2D cartesian space. [m]
            pa;     ///< Alpha or Yaw of the object, that is rotation around the Z axis of the object. [rad]
} Pose2D_t;

/**
 * Structure used to define position in 3D space.
 */
typedef struct {
    double  px,     ///< X coordinate in 3D cartesian space. [m]
            py,     ///< Y coordinate in 3D cartesian space. [m]
            pz,     ///< Z coordinate in 3D cartesian space. [m]
            proll,  ///< Roll of the object, that is rotation around the X axis of the object. [rad]
            ppitch, ///< Pitch of the object, that is rotation around the Y axis of the object. [rad]
            pyaw;   ///< Yaw of the object, that is rotation around the Z axis of the object. [rad]
} Pose3D_t;

/**
 * Structure used to define physical dimensions of box that fully enclouses object.
 */
typedef struct {
    double 	sw,     ///< Width of the object. [m]
            sl,     ///< Length of the object. [m]
            sh;     ///< Height of the object. [m]
} BBox3D_t;

/**
 * Structure used to store simulation time statistics.
 */
typedef struct {
    double  mean_time,              ///< Mean time between two consecvent frames in simualtion.
            variance_frame_by_sec,
            variance_time,
            mean_frame_by_sec;
} TimeStatistics_t;

/**
 * Structure used to define RGBA color.
 * Used in @ref CCMorse::MorsePen.
 */
struct RGBA_t {
    unsigned char   r,  ///< Red component of color must be in <0; 255>.
                    g,  ///< Green component of color must be in <0; 255>.
                    b,  ///< Blue component of color must be in <0; 255>.
                    a;  ///< Alpha (transparency) component of color must be in <0; 255>.

    /**
     * Default constructor.
     */
    RGBA_t() { r = 255; g = 255; b = 255; a = 255; }

    /**
     * Constructor with alpha.
     *
     * @param   aR      Red component of color must be in <0; 255>.
     * @param   aG      Green component of color must be in <0; 255>.
     * @param   aB      Blue component of color must be in <0; 255>.
     * @param   aA      Alpha component of color must be in <0; 255>.
     */
    RGBA_t(int const aR, int const aG, int const aB, int const aA)
    {
        r = (unsigned char) Clamp(aR, 0, 255);
        g = (unsigned char) Clamp(aG, 0, 255);
        b = (unsigned char) Clamp(aB, 0, 255);
        a = (unsigned char) Clamp(aA, 0, 255);
    }

    /**
     * Constructor without alpha, color will be fully opaque.
     *
     * @param   aR      Red component of color must be in <0; 255>.
     * @param   aG      Green component of color must be in <0; 255>.
     * @param   aB      Blue component of color must be in <0; 255>.
     */
    RGBA_t(int const aR, int const aG, int const aB)
    {
        r = (unsigned char) Clamp(aR, 0, 255);
        g = (unsigned char) Clamp(aG, 0, 255);
        b = (unsigned char) Clamp(aB, 0, 255);
        a = 255;
    }

    /**
     * The equals operator overload.
     */
    bool operator==(RGBA_t const &p) const
    { return (r == p.r && g == p.g && b == p.b); }

    /**
     * The not-equals operator overload.
     */
    bool operator!=(RGBA_t const &p) const
    { return (r != p.r || g != p.g || b != p.b); }

    /**
     * The assign operator overload.
     */
    void operator=(RGBA_t const &p)
    { r = p.r; g = p.g; b = p.b; a = p.a; }

    /**
     * Returns string representation of color object.
     */
    std::string const ToStr() const
    { return (NumberToString((int) r) + "," + NumberToString((int) g) + "," + NumberToString((int) b) + "," + NumberToString((int) a)); }

};
typedef struct RGBA_t RGBA_t;

/**
 * Structure used to define point (pixel) in image.
 */
struct ImagePoint_t {
    unsigned int    x,  ///< X coordinate in image, column.
                    y;  ///< Y coordinate in image, row.

    /**
     * Default constructor.
     */
    ImagePoint_t() { x = 0; y = 0; }

    /**
     * Constructor.
     * @param   aX, aY  Pixel coorinates in image.
     */
    ImagePoint_t(int const aX, int const aY)
    { x = (unsigned int) aX; y = (unsigned int) aY; }

    /**
     * The equals operator overload.
     */
    bool operator==(ImagePoint_t const &p) const
    { return (x == p.x && y == p.y); }

    /**
     * The not-equals operator overload.
     */
    bool operator!=(ImagePoint_t const &p) const
    { return (x != p.x || y != p.y); }

    /**
     * The less operator overload.
     */
    bool operator<(ImagePoint_t const &p) const
    { return ((y*1000 + x) < (p.y*1000 + p.x)); }

    /**
     * The assign operator overload.
     */
    void operator=(ImagePoint_t const &p)
    { x = p.x; y = p.y; }

    /**
     * Returns string representation of the point coordinates.
     */
    std::string const ToStr() const
    { return (NumberToString(x) + "," + NumberToString(y)); }
};
typedef struct ImagePoint_t ImagePoint_t;

/**
 * Enum used to determine status of robots goal.
 */
typedef enum {
    arrived,    ///< Robot has reached its goal position.
    transit,    ///< Robot is on its way to goal position.
    idle        ///< Robot is ready to pursue goal position.
} GoalStatus_t;

/**
 * Structure used to store goal destination and status of the robot.
 */
typedef struct {
    double px;           ///< X coordinate of robots Goal position. [m]
    double py;           ///< Y coordinate of robots Goal position. [m]
    double pa;           ///< Yaw of the robot on Goal position. [rad]
    GoalStatus_t status; ///< Status of robot in relation to the Goal position.
} Goal_t;

/**
 * Type used to store various data in dictionary like style.
 * For set keyword there is value.
 */
typedef std::map<std::string, std::string> Dictionary_t;

/**
 * The value of Ludolphs number (pi).
 */
const double PI = 3.1415926535897932384626433832795028841971693993751058209749445923078164062;

/**
 * The common string value for good communication between client and simulation.
 */
const std::string COM_OK = "DONE.";

/**
 * The common string value for bad communication between client and simulation.
 */
const std::string COM_KO = "KO.";

/**
 * Returns string representation of bool value.
 *
 * @param   b   Bool value.
 * @return  "True" if b is @c true or "False" if b is @c false.
 */
std::string const BoolToString(bool b);

/**
 * Sends message to given socket a returns answer to the message.
 *
 * @param   aSocket     Reference to socket.
 * @param   aMessage    Message will be send thru socket.
 * @return  Answer to message from server.
 */
std::string const SendToSocket(const ClientSocket &aSocket, const std::string &aMessage);

/**
 * Returns message from socket.
 *
 * @param   aSocket     Reference to socket.
 * @return  Message from server.
 */
std::string const RecieveFromSocket(const ClientSocket &aSocket);

/**
 * Returns data in dictionary type from data in JSON serialized form.
 *
 * @param   aData   JSON string data.
 * @return  Data in dictionary.
 */
Dictionary_t const MapData(const std::string &aData);

/**
 * Function that checks if given string is empty or it's equal to "null".
 *
 * @param   aStr    String to be tested.
 * @return  [@c true: if string is "" or "null", @c false: otherwise]
 */
bool const IsNullOrEmpty(const std::string &aStr);

/**
 * Returns double precision float number from string representation of float number.
 *
 * @param   aDbl    String representation of float number.
 * @return  Float number from string.
 */
double const AsDouble(const std::string &aDbl);

/**
 * Returns integer number from string representation of integer number.
 *
 * @param   aInt    String representation of integer number.
 * @return  Integer number from string.
 */
int const AsInteger(const std::string &aInt);

/**
 * Returns bool value from string representation of boolean.
 *
 * @param   aBool   String representation of boolean.
 * @return  [@c true: if string is "true", @c false otherwise]
 */
bool const AsBool(const std::string &aBool);

/**
 * Returns vector of doubles from string representation of float number array.
 *
 * @param   aVector String representation of float number array.
 * @return  Vector of doubles.
 */
std::vector<double> const AsVectorOfDoubles(const std::string &aVector);

/**
 * Returns vector of integers from string representation of integer numbers array.
 *
 * @param   aVector String representation of integer number array.
 * @return  Vector of integers.
 */
std::vector<int> const AsVectorOfIntegers(const std::string &aVector);

/**
 * Returns vector of strings from string representation of strings array.
 *
 * @param   aVector String representation of strings array.
 * @return  Vector of strings.
 */
std::vector<std::string> const AsVectorOfStrings(const std::string &aVector);

/**
 * Returns vector of positions in 2D from string representation of postion array.
 *
 * @param   aVector String representation of positions array.
 * @return  Vector of 2D positions.
 */
std::vector<Pose2D_t> const AsVectorOfPoses(const std::string &aVector);

/**
 * Returns vector of string representations of objects from string representation of objects array.
 *
 * @param   aVector String representation of objects array.
 * @return  Vector of object.
 */
std::vector<std::string> const AsVectorOfObjects(const std::string &aVector);

/**
 * Checks whether given integer value is in <aMin; aMax) interval.
 *
 * @param   aValue  Integer value to be checked.
 * @param   aMin    Minimum that value can be. (Value must be same or higher that aMin)
 * @param   aMax    Maximum that value cannot equal. (Value must be lower than aMax)
* @param   aFun    Caller function name. For exception call.
 * @return  [@c true: value is in interval, @c false: otherwise]
 */
bool const CheckIntValue(int const aValue, int const aMin, int const aMax, std::string const aFun);

/**
 * Checks whether given double value is in <aMin; aMax> interval.
 *
 * @param   aValue  Double value to be checked.
 * @param   aMin    Minimum that value can be. (Value must be same or higher that aMin)
 * @param   aMax    Maximum that value can be. (Value must be same or lower than aMax)
 * @param   aFun    Caller function name. For exception call.
 * @return  [@c true: value is in interval, @c false: otherwise]
 */
bool const CheckDoubleValue(double const aValue, double const aMin, double const aMax, std::string const aFun);

// Implementation of template functions
//--------------------------------------------------
template <typename T>
std::string const NumberToString (T const &aNum)
{
    std::stringstream ss;
    ss << aNum;
    return ss.str();
}

template <typename T>
T const StringToNumber(const std::string &aStr, T aDefValue = T())
{
    std::stringstream ss;
    for( std::string::const_iterator i = aStr.begin(); i != aStr.end(); i++ )
        if(isdigit(*i) || *i == 'e' || *i == '-' || *i == '+' || *i == '.')
            ss << *i;

    T result;
    return (ss >> result) ? result : aDefValue;
}

template<typename T>
T GetVar(const T &aV, Mutex_t &aMtx)
{ // these have to be defined here since they're templates
    ScopedLock_t lock( aMtx );
    T v = aV;
    return v;
}

template <typename T>
T const StringToNumber(std::string const &aStr) { return StringToNumber(aStr, T()); }

template <typename T>
T const Min(T aX, T aY) { return ( aX <= aY ? aX : aY ); }

template <typename T>
T const Max(T aX, T aY) { return ( aX >= aY ? aX : aY ); }

template <typename T>
T const Clamp(T aX, T aMin, T aMax) { return Max(aMin, Min(aX, aMax)); }

} // namespace CCMorse;

#endif // CCMORSE_MORSEUTILITY_H

