/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-10-18
 *
 * File contains definition of PlayerClient class.
 */

#ifndef CCMORSE_PLAYERCLIENT_H
#define CCMORSE_PLAYERCLIENT_H

//#define HAVE_BOOST_SIGNALS
#define HAVE_BOOST_THREAD


#ifdef HAVE_BOOST_SIGNALS
    #include <boost/signal.hpp>
#endif // HAVE_BOOST_SIGNALS

#ifdef HAVE_BOOST_THREAD
    #include <boost/thread/mutex.hpp>
    #include <boost/thread/thread.hpp>
    #include <boost/thread/xtime.hpp>
    #include <boost/bind.hpp>
#else
    // we have to define this so we don't have to
    // comment out all the instances of scoped_lock
    // in all the proxies
    namespace boost {
        class thread {
            public: thread() {};
        };

        class mutex {
            public:
                mutex() {};
                class scoped_lock {
                    public: scoped_lock(mutex /*m*/) {};
                };
        };
    }
#endif // HAVE_BOOST_THREAD

#include <iostream>
#include <string>

#define MORSE_HOSTNAME      "localhost"
#define MORSE_PORTNUM       4000
#define MORSE_TRANSPORT_TCP 0
#define MORSE_TRANSPORT_UDP 1

class _MorseClient;


namespace CCMorse {

/**
 * PlayerClient class is used to mimic interface of a PlayerCc::PlayerClient class for
 * use with Morse simulator.
 *
 * @see http://playerstage.sourceforge.net/doc/Player-3.0.2/player/classPlayerCc_1_1PlayerClient.html
 */
class PlayerClient{

    friend class MorseDriver;
    friend class BasicDriver;
    friend class SndDriver;

    friend class MorseProxy;
    friend class LaserProxy;
    friend class Position2dProxy;
    friend class WindowProxy;

    public:
        typedef boost::thread Thread_t; ///< Our thread type.
        typedef boost::mutex  Mutex_t;  ///< Our mutex type.


        /**
         * Contructor. Make a client and connect it as indicated.
         *
         * @param  aHostname   The hostname of Morse simulation. Default: "localhost"
         * @param  aPort       The port number of simulation. Default: 4000
         * @param  aTransport  The transport protocol for connection to simulation. Default: TCP
         */
        PlayerClient(const std::string aHostname = MORSE_HOSTNAME, int aPort = MORSE_PORTNUM, int aTransport = MORSE_TRANSPORT_TCP);

        /**
         * Destructor.
         */
        ~PlayerClient();

        /**
         * Returns mutex for handling synchronization.
         */
        Mutex_t &GetMutex() const { return m_Mutex; }

        /**
         * Returns connection status.
         *
         * @return [@c true: client is connected to simulation, @c false: otherwise]
         */
        bool Connected();

        /**
         * Start the run thread.
         *
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse.
         */
        void StartThread();

        /**
         * Stop the run thread.
         *
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse.
         */
        void StopThread();

        /**
         * This starts a blocking loop on @ref Read().
         *
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse.
         * @param   Timeout     Timeout in milliseconds. Default: 10ms.
         */
        void Run(int Timeout = 10);

        /**
         * Stops the @ref Run() loop.
         *
         * @warning Method copying PlayerCc::PlayerClient method. Currently it is not
         * recommended to use it with Morse.
         */
        void Stop();

        /**
         * Check whether there is data waiting on the connection, blocking
         * for up to @param timeout milliseconds (set to 0 to not block).
         *
         * @warning Method copying PlayerCc::PlayerClient method. Because Morse
         * cannot use peek in principle, method always returns @c false.
         *
         * @param   timeout     Timeout in millisecond for @ref PlayerClient::Read(). Default: 0ms.
         * @return  [@c false if there is no data waiting, @c true if there is data waiting]
         */
        bool Peek(uint32_t timeout = 0);

        /**
         * Set the timeout for client requests.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @param   seconds     Request timeout in seconds.
         */
        void SetRequestTimeout(uint32_t seconds);

        /**
         * Set connection retry limit, which is the number of times,
         * that we'll try to reconnect to the server after a socket error.
         * Set to -1 for inifinite retry.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @param   limit   Number of retries.
         */
        void SetRetryLimit(int limit);

        /**
         * Get connection retry limit, which is the number of times
         * that we'll try to reconnect to the server after a socket error.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @return  Connection retry limit.
         */
        int GetRetryLimit();

        /**
         * Set connection retry time, which is number of seconds to
         * wait between reconnection attempts.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @param   seconds     Time between connection attempts in seconds.
         */
        void SetRetryTime(double seconds);

        /**
         * Get connection retry time, which is number of seconds to
         * wait between reconnection attempts.
         *
         * @warning Method copying PlayerCc::PlayerClient method, currently not used for Morse.
         * @return  Time between connectrion attemts in seconds.
         */
        double GetRetryTime();

        /**
         * A blocking Read. Use this method to read data from the server, blocking
         * until at least one message is received.  Use @ref PlayerClient::Peek()
         * to check whether any data is currently waiting.
         * In pull mode, this will block until all data waiting on the server has
         * been received, ensuring as up to date data as possible.
         */
        void Read();

        /**
         * A nonblocking Read. Use this method if you want to read in a nonblocking
         * manner.  This is the equivalent of checking if @ref Peek is @c true and
         * then @ref PlayerClient::Read().
         */
        void ReadIfWaiting();

        /**
         * Get the interface/identification code for a given name.
         *
         * @param   aName   Name of device.
         * @return  Identification number of device.
         */
        int LookupCode(std::string aName) const;

        /**
         * Get the name for a given interface/identification code.
         *
         * @param   aCode   Identification code of device.
         * @return  Name of the device.
         */
        std::string LookupName(int aCode) const ;


/// @cond COMMENTED
//        /**
//        *  @brief You can change the rate at which your client receives data from the
//        *  server with this method.  The value of @p freq is interpreted as Hz;
//        *  this will be the new rate at which your client receives data (when in
//        *  continuous mode).
//        *
//        *  @exception throws PlayerError if unsuccessfull
//        */
//        void SetFrequency(uint32_t aFreq);

//        /**
//         *  @brief Set whether the client operates in Push/Pull modes
//         *
//         *  You can toggle the mode in which the server sends data to your
//         *  client with this method.  The @p mode should be one of
//         *    - @ref PLAYER_DATAMODE_PUSH (all data)
//         *    - @ref PLAYER_DATAMODE_PULL (data on demand)
//         *  When in pull mode, it is highly recommended that a replace rule is set
//         *  for data packets to prevent the server message queue becoming flooded.
//         *  For a more detailed description of data modes, see @ref
//         *  libplayerc_datamodes.
//         *
//         *  @exception throws PlayerError if unsuccessfull
//         */
//        void SetDataMode(uint32_t aMode);

//        /**
//         *  @brief Set a replace rule for the clients queue on the server.
//         *
//         *  If a rule with the same pattern already exists, it will be replaced
//         *  with the new rule (i.e., its setting to replace will be updated).
//         *  @param aReplace Should we replace these messages? true/false
//         *  @param aType type of message to set replace rule for
//         *           (-1 for wildcard).  See @ref message_types.
//         *  @param aSubtype message subtype to set replace rule for (-1 for
//         *           wildcard).
//         *  @param aInterf Interface to set replace rule for (-1 for wildcard).
//         *           This can be used to set the replace rule for all members of a
//         *           certain interface type.  See @ref interfaces.
//         *
//         *  @exception throws PlayerError if unsuccessfull
//         *
//         *  @ref ClientProxy::SetReplaceRule, PlayerClient::SetDataMode
//         */
//        void SetReplaceRule(bool aReplace, int aType = -1, int aSubtype = -1, int aInterf = -1);

//        /**
//         *  Get count of the number of discarded messages on the server since the last call to this method
//         */
//        uint32_t GetOverflowCount();
/// @endcond

    private:
        /**
         * Returns pointer to MorseClient.
         */
        _MorseClient* GetMorseClient() const { return m_MorseClient; }

        _MorseClient* m_MorseClient;    ///< Pointer to MorseClient.

        mutable Mutex_t m_Mutex;        ///< Mutex for sychronization.

};
}   // namespace CCMorse

#endif // CCMORSE_PLAYERCLIENT_H
