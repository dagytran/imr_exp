#include "LaserProxy.h"
#include "MorseClient.h"
#include "MorseRobot.h"

#define GET_CLIENT  ( reinterpret_cast<MorseClient*>(GetPlayerClient()->GetMorseClient()) )
#define MY_ROBOT    ( GET_CLIENT->GetRobotWithID(m_SubscribedToRobotID) )

using namespace CCMorse;


void LaserProxy::SetupConfiguration()
{
    SetName("LaserProxy");

    MorseRobot* robot = GET_CLIENT->GetRobotProvidingLaser(GetIndex());

    if(robot == NULL) {
        throw MorseError("LaserProxy::SetupConfiguration", "No robot providing this proxy index.");

    } else {
        bool checkLaser = false;

        for(std::vector<MorseDevice*>::const_iterator device = robot->GetDevices().begin(); device != robot->GetDevices().end(); ++ device){
            if( !checkLaser && MorseLaserScanner::IsItMe((*device)->GetName(), robot->GetName()) ){
                m_Laser = reinterpret_cast<MorseLaserScanner*>( (*device) );
                m_Device = m_Laser;
                m_Laser->SetSubscribed(1);
                m_Laser->SetScanningFreq( robot->GetConfiguration()->GetLaserFreq() );
                checkLaser = true;
                break;
            }
        }

        if(!checkLaser)
            throw MorseError("LaserProxy::SetupConfiguration", "The laser device was not found.");

        do {
            m_Laser->RefreshData();
        } while( m_Laser->IsFreshConfig() != 1 );

        RequestConfigure();

        Subscribe(robot->GetID());
        std::cout << "DEBUG: LaserProxy: Done." << std::endl;
    }
}

void LaserProxy::RequestConfigure()
{
    ScopedLock_t lock( GetMutex() );
    m_MinAngle  = m_Laser->GetScanStart();
    m_MaxAngle  = m_Laser->GetScanStart() + (m_Laser->GetScanCount() - 1)*m_Laser->GetScanRes();
    m_ScanRes   = m_Laser->GetScanRes();
    m_RangeRes  = m_Laser->GetRangeRes();
    m_ScanFreq  = m_Laser->GetScanningFreq();
    m_Intensity = (m_Laser->IsIntensityOn() != 0) ? true : false;
}

void LaserProxy::Subscribe(uint32_t aIndex)
{
    MorseRobot* robot = GET_CLIENT->GetRobotWithID(aIndex);

    std::cout
        << "LaserProxy::Subscribe(" << aIndex << "): Proxy is subscribed to robot named \""
        << robot->GetName() << "\"." << std::endl;

    m_SubscribedToRobotID = aIndex;

    robot->SubscribeProxy(this);
}

void LaserProxy::Unsubscribe()
{
    ScopedLock_t lock( GetMutex() );
    MY_ROBOT->UnsubscribeProxy(this);
}

Point2D_t LaserProxy::GetPoint(uint32_t aIndex) const
{
    Pose2D_t pose   = GetVar( m_Laser->GetPoint(aIndex) );
    Point2D_t point = {pose.px, pose.py};
    return point;
}
