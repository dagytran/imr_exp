/**
 *  @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 *  @date   2016-11-14
 *
 *  File contains definition of MorsePen class.
 */

#ifndef CCMORSE_MORSEPEN_H
#define CCMORSE_MORSEPEN_H

#include "MorseActuator.h"

#include <set>


namespace CCMorse{

/**
 * Pen class that can control Pen actuator to draw on canvas. Mainly used for mapgrids visualization.
 *
 * Based on #MorseActuator class.
 */
class MorsePen : public MorseActuator
{
    friend class MorseClient;
    friend class WindowProxy;

    public:
        /**
         * Default constructor.
         */
        MorsePen() : MorseActuator() { SetupConfiguration(); }

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorsePen(int aPort, std::string aName) : MorseActuator(aPort, aName) { SetupConfiguration(); }

        /**
         * Copy constructor.
         */
        MorsePen(const MorsePen &aPen) : MorseActuator( aPen ) { SetupConfiguration(); m_Data = aPen.m_Data; }

        /**
         * Destructor.
         */
        ~MorsePen() { m_Data.clear(); m_LastData.clear(); m_DirtyPixels.clear(); }

        /**
         * Sends pixels which are in vector of pixel coordinates to canvas.
         * @param   aNewPixels  Vector of pixel coordinates.
         */
        void UpdateImage(std::vector<ImagePoint_t> aNewPixels);

        /**
         * Sends all pixels from local memory to canvas.
         */
        void SetImage();

        /**
         * Saves PNG image of the canvas in the @ref MorsePen::GetFolderPath() folder.
         * @param   aName   Name of the saved file.
         */
        void SavePNG(std::string const &aName) const;

        /**
         * Returns width of the canvas in pixels.
         * @return  Width of the canvas.
         */
        int const GetWidth() const    { return (m_Width); }

        /**
         * Returns height of the canvas in pixels.
         * @return  Height of the canvas.
         */
        int const GetHeight() const   { return (m_Height); }

        /**
         * Returns X coordinate of pixel from given real position X.
         *
         * @param   aPosX   Real position X coordinate. [m]
         * @return  X coordinate of pixel on canvas.
         */
        int const GetX(double const aPosX) const;

        /**
         * Returns Y coordinate of pixel from given real position Y.
         *
         * @param   aPosY   Real position Y coordinate. [m]
         * @return  Y coordinate of pixel on canvas.
         */
        int const GetY(double const aPosY) const;

        /**
         * Returns X position coordinate from given X pixel coorinate.
         *
         * @param   aPixX   Pixel X coordinate.
         * @return  X position coordinate. [m]
         */
        double const GetPosX(int const aPixX) const;

        /**
         * Returns Y position coordinate from given Y pixel coorinate.
         *
         * @param   aPixY   Pixel Y coordinate.
         * @return  Y position coordinate. [m]
         */
        double const GetPosY(int const aPixY) const;

        /**
         * Returns name of the image.
         * @return  Name string.
         */
        std::string const GetImageName() const;

        /**
         * Sets name of the image.
         * @param   aName   Name of the image.
         */
        void SetImageName(std::string const &aName);

        /**
         * Returns string representation of path to active project folder.
         */
        std::string const GetFolderPath() const { return GetVar(m_FolderPath); }

        /**
         * Returns color of the pixel.
         *
         * @param   aX  X pixel coordinate.
         * @param   aY  Y pixel coordinate.
         * @return  Color of the pixel. (R, G, B, Alpha) <0; 255>
         */
        RGBA_t const GetPixel(int const aX, int const aY) const;

        /**
         * Returns color of the pixel.
         *
         * @param   aP   Point in real 2D coordinates (X[m], Y[m]). @ref Point2D_t
         * @return  Color of the pixel. (R, G, B, Alpha) <0; 255>
         */
        RGBA_t const GetPixel(Point2D_t const aP) const { return GetPixel(GetX(aP.px), GetY(aP.py)); }

        /**
         * Returns color of the pixel.
         *
         * @param   aP   Pixel coordinates (x, y). @ref ImagePoint_t
         * @return  Color of the pixel. (R, G, B, Alpha) <0; 255>
         */
        RGBA_t const GetPixel(ImagePoint_t const aP) const { return GetPixel(aP.x, aP.y); }

        /**
         * Returns whole image data.
         * @return Vector of pixels in rows major representation of image matrix.
         */
        std::vector<RGBA_t> const GetImageData() const { return GetVar(m_Data); }

        /**
         * Sets dimensions (in pixels) of the canvas.
         * @param   aWidth  New width of the canvas.
         * @param   aHeight New height of the canvas.
         */
        void SetDimensions(int const aWidth, int const aHeight);

        /**
         * Simple method to set one pixel with transparency to be drawn
         * next refresh on canvas.
         *
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   pR  Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG  Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0; 255>
         * @param   pA  Alpha component of required color. Value is clamped to interval: <0; 255>
         */
        void SetPixel(int const aX, int const aY, int const pR, int const pG, int const pB, int const pA);

        /**
         * Simple method to set one pixel without transparency to be drawn
         * next refresh on canvas.
         *
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   pR  Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG  Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0; 255>
         */
        void SetPixel(int const aX, int const aY, int const pR, int const pG, int const pB) { SetPixel(aX, aY, pR, pG, pB, 255); }

        /**
         * @copybrief MorsePen::SetPixel(int,int,int,int,int)
         * @param   aP  Real position 2D coordinates.
         * @param   pR  Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG  Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0; 255>
         */
        void SetPixel(Point2D_t const aP, int const pR, int const pG, int const pB) { SetPixel(GetX(aP.px), GetY(aP.py), pR, pG, pB, 255); }

        /**
         * @copybrief MorsePen::SetPixel(int,int,int,int,int)
         * @param   aP  Pixel coordinates.
         * @param   pR  Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG  Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0; 255>
         */
        void SetPixel(ImagePoint_t const aP, int const pR, int const pG, int const pB) { SetPixel(aP.x, aP.y, pR, pG, pB, 255); }

        /**
         * @copybrief MorsePen::SetPixel(int,int,int,int,int,int)
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   pR  Red component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pG  Green component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pA  Alpha component of required color. Value is clamped to interval: <0.0; 1.0>
         */
        void SetPixel(int const aX, int const aY, double const pR, double const pG, double const pB, double const pA) { SetPixel(aX, aY, (int) (pR*255), (int) (pG*255), (int) (pB*255), (int) (pA*255)); }

        /**
         * @copybrief MorsePen::SetPixel(int,int,int,int,int)
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   pR  Red component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pG  Green component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0.0; 1.0>
         */
        void SetPixel(int const aX, int const aY, double const pR, double const pG, double const pB) { SetPixel(aX, aY, pR, pG, pB, 1.0); }

        /**
         * @copybrief MorsePen::SetPixel(int,int,int,int,int)
         * @param   aP  Real position 2D coordinates.
         * @param   pR  Red component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pG  Green component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0.0; 1.0>
         */
        void SetPixel(Point2D_t const aP, double const pR, double const pG, double const pB) { SetPixel(GetX(aP.px), GetY(aP.py), pR, pG, pB, 1.0); }

        /**
         * @copybrief MorsePen::SetPixel(int,int,int,int,int)
         * @param   aP  Pixel coordinates.
         * @param   pR  Red component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pG  Green component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0.0; 1.0>
         */
        void SetPixel(ImagePoint_t const aP, double const pR, double const pG, double const pB) { SetPixel(aP.x, aP.y, pR, pG, pB, 1.0); }

        /**
         * Simple method to set one pixel with or without transparency to be drawn
         * next refresh on canvas.
         *
         * @param   aX      X coordinate of pixel.
         * @param   aY      Y coordinate of pixel.
         * @param   aPix    Pixel color.
         */
        void SetPixel(int const aX, int const aY, RGBA_t const aPix) { SetPixel(aX, aY, aPix.r, aPix.g, aPix.b, aPix.a); }

        /**
         * @copybrief MorsePen::SetPixel(int,int,RGBA_t)
         * @param   aP      Real position 2D coordinates.
         * @param   aPix    Pixel color.
         */
        void SetPixel(Point2D_t const aP, RGBA_t const aPix) { SetPixel(GetX(aP.px), GetY(aP.py), aPix.r, aPix.g, aPix.b, aPix.a); }

        /**
         * @copybrief MorsePen::SetPixel(int,int,RGBA_t)
         * @param   aP      Pixel coordinates.
         * @param   aPix    Pixel color.
         */
        void SetPixel(ImagePoint_t const aP, RGBA_t const aPix) { SetPixel(aP.x, aP.y, aPix.r, aPix.g, aPix.b, aPix.a); }

        /**
         * Simple method to set one pixel without transparency to be drawn
         * immediately on canvas.
         *
         * @param   aX          X coordinate of pixel.
         * @param   aY          Y coordinate of pixel.
         * @param   aRGBCode    String representation of RGB code, for example: "#FF22AA".
         */
        void DrawPixel(int const aX, int const aY, std::string const aRGBCode);

        /**
         * Simple method to set one pixel with transparency to be drawn
         * immediately on canvas.
         *
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   pR  Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG  Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0; 255>
         * @param   pA  Alpha component of required color. Value is clamped to interval: <0; 255>
         */
        void DrawPixel(int const aX, int const aY, int const pR, int const pG, int const pB, int const pA);

        /**
         * @copybrief MorsePen::DrawPixel(int,int,std::string)
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   pR  Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG  Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0; 255>
         */
        void DrawPixel(int const aX, int const aY, int const pR, int const pG, int const pB) { DrawPixel(aX, aY, pR, pG, pB, 255); }

        /**
         * @copybrief MorsePen::DrawPixel(int,int,std::string)
         * @param   aP      Real position 2D coordinates.
         * @param   pR      Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG      Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB      Blue component of required color. Value is clamped to interval: <0; 255>
         */
        void DrawPixel(Point2D_t const aP, int const pR, int const pG, int const pB) { DrawPixel(GetX(aP.px), GetY(aP.py), pR, pG, pB, 255); }

        /**
         * @copybrief MorsePen::DrawPixel(int,int,std::string)
         * @param   aP      Pixel coordinates.
         * @param   pR      Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG      Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB      Blue component of required color. Value is clamped to interval: <0; 255>
         */
        void DrawPixel(ImagePoint_t const aP, int const pR, int const pG, int const pB) { DrawPixel(aP.x, aP.y, pR, pG, pB, 255); }

        /**
         * @copybrief MorsePen::DrawPixel(int,int,int,int,int,int)
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   pR  Red component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pG  Green component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pA  Alpha component of required color. Value is clamped to interval: <0.0; 1.0>
         */
        void DrawPixel(int const aX, int const aY, double const pR, double const pG, double const pB, double const pA);

        /**
         * @copybrief MorsePen::DrawPixel(int,int,std::string)
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   pR  Red component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pG  Green component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0.0; 1.0>
         */
        void DrawPixel(int const aX, int const aY, double const pR, double const pG, double const pB) { DrawPixel(aX, aY, pR, pG, pB, 1.0); }

        /**
         * @copybrief MorsePen::DrawPixel(int,int,std::string)
         * @param   aP  Real position 2D coordinates.
         * @param   pR  Red component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pG  Green component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0.0; 1.0>
         */
        void DrawPixel(Point2D_t const aP, double const pR, double const pG, double const pB) { DrawPixel(GetX(aP.px), GetY(aP.py), pR, pG, pB, 1.0); }

        /**
         * @copybrief MorsePen::DrawPixel(int,int,std::string)
         * @param   aP  Pixel coordinates.
         * @param   pR  Red component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pG  Green component of required color. Value is clamped to interval: <0.0; 1.0>
         * @param   pB  Blue component of required color. Value is clamped to interval: <0.0; 1.0>
         */
        void DrawPixel(ImagePoint_t const aP, double const pR, double const pG, double const pB) { DrawPixel(aP.x, aP.y, pR, pG, pB, 1.0); }

        /**
         * Simple method to set one pixel with or without transparency to be drawn
         * immediately on canvas.
         *
         * @param   aX      X coordinate of pixel.
         * @param   aY      Y coordinate of pixel.
         * @param   aPix    Pixel color.
         */
        void DrawPixel(int const aX, int const aY, RGBA_t const aPix) { DrawPixel(aX, aY, aPix.r, aPix.g, aPix.b, aPix.a); }

        /**
         * @copybrief MorsePen::DrawPixel(int,int,RGBA_t)
         * @param   aP      Real position 2D coordinates.
         * @param   aPix    Pixel color.
         */
        void DrawPixel(Point2D_t const aP, RGBA_t const aPix) { DrawPixel(GetX(aP.px), GetY(aP.py), aPix.r, aPix.g, aPix.b, aPix.a); }

        /**
         * @copybrief MorsePen::DrawPixel(int,int,RGBA_t)
         * @param   aP      Pixel coordinates.
         * @param   aPix    Pixel color.
         */
        void DrawPixel(ImagePoint_t const aP, RGBA_t const aPix) { DrawPixel(aP.x, aP.y, aPix.r, aPix.g, aPix.b, aPix.a); }

        /**
         * Sets straight line to be drawn next refresh between points [x0,y0] and [x1,y1] with RGBA color.
         *
         * @param   aX0     X coordinate of the beginning.
         * @param   aY0     Y coordinate of the beginning.
         * @param   aX1     X coordinate of the end.
         * @param   aY1     Y coordinate of the end.
         * @param   pR      Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG      Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB      Blue component of required color. Value is clamped to interval: <0; 255>
         * @param   pA      Alpha component of required color. Value is clamped to interval: <0; 255>
         */
        void SetLine (int aX0, int aY0, int aX1, int aY1, int const pR, int const pG, int const pB, int const pA);

        /**
         * Sets straight line to be drawn next refresh between points [x0,y0] and [x1,y1] with RGB color.
         *
         * @param   aX0     X coordinate of the beginning.
         * @param   aY0     Y coordinate of the beginning.
         * @param   aX1     X coordinate of the end.
         * @param   aY1     Y coordinate of the end.
         * @param   pR      Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG      Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB      Blue component of required color. Value is clamped to interval: <0; 255>
         */
        void SetLine (int aX0, int aY0, int aX1, int aY1, int const pR, int const pG, int const pB) { SetLine(aX0, aY0, aX1, aY1, pR, pG, pB, 255); }

        /**
         * Sets straight line to be drawn immediately between points [x0,y0] and [x1,y1] with RGBA color.
         *
         * @param   aX0     X coordinate of the beginning.
         * @param   aY0     Y coordinate of the beginning.
         * @param   aX1     X coordinate of the end.
         * @param   aY1     Y coordinate of the end.
         * @param   pR      Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG      Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB      Blue component of required color. Value is clamped to interval: <0; 255>
         * @param   pA      Alpha component of required color. Value is clamped to interval: <0; 255>
         */
        void DrawLine (int aX0, int aY0, int aX1, int aY1, int const pR, int const pG, int const pB, int const pA);

        /**
         * Sets straight line to be drawn immediately between points [x0,y0] and [x1,y1] with RGB color.
         *
         * @param   aX0     X coordinate of the beginning.
         * @param   aY0     Y coordinate of the beginning.
         * @param   aX1     X coordinate of the end.
         * @param   aY1     Y coordinate of the end.
         * @param   pR      Red component of required color. Value is clamped to interval: <0; 255>
         * @param   pG      Green component of required color. Value is clamped to interval: <0; 255>
         * @param   pB      Blue component of required color. Value is clamped to interval: <0; 255>
         */
        void DrawLine (int aX0, int aY0, int aX1, int aY1, int const pR, int const pG, int const pB) { DrawLine(aX0, aY0, aX1, aY1, pR, pG, pB, 255); }

        /**
         * Sends changed pixels to canvas.
         */
        virtual void RefreshData();

        /**
         * Check if provided pixel coordinates are on canvas.
         *
         * @param   aX  X coordinate of pixel.
         * @param   aY  Y coordinate of pixel.
         * @param   aFun Caller function name. For exception call.
         * @return  [@c true: if pixel is on canvas; @c false: otherwise]
         */
        bool const CheckCoordinates(int const aX, int const aY, std::string const aFun) const { return CheckCoordinates(aX, GetWidth(), aY, GetHeight(), aFun); }

        /**
         * Static function, swaps value in x with value in y
         * @param   x   First swapped value.
         * @param   y   Second swapped value.
         */
        static void SWAP(int &x, int &y);

        /**
         * Static function, returns hexadecimal representation of float value.
         * Converts value <0.0; 1.0> to <0x00; 0xFF>.
         * @param   aValue  Value to be converted to hex.
         * @return  Two chars long HEX string representation.
         */
        static std::string const StrHexFromFloat(double const aValue);

        /**
         * Static function, returns hexadecimal representation of integer value.
         * Converts value <0; 255> to <0x00; 0xFF>.
         * @param   aValue  Value to be converted to hex.
         * @return  Two chars long HEX string representation.
         */
        static std::string const StrHexFromInt(int const aValue);

        /**
         * Static function, returns string RGB code.
         * @param   aR  Red component of required color. <0; 255> or <0.0; 1.0>
         * @param   aG  Green component of required color. <0; 255> or <0.0; 1.0>
         * @param   aB  Blue component of required color. <0; 255> or <0.0; 1.0>
         * @return  String representation of RGB code. Example: "#FF22AA"
         */
        static std::string const GetRGBCode(int const aR, int const aG, int const aB);
        static std::string const GetRGBCode(double const aR, double const aG, double const aB);

        /**
         * Static function, checks if provided pixel coordinates are between "0" and given limit.
         *
         * @param   aX      X coordinate of pixel.
         * @param   aMaxX   X must be lower than this.
         * @param   aY      Y coordinate of pixel.
         * @param   aMaxY   Y must be lower than this.
         * @param   aFun    Caller function name. For exception call.
         * @return  [@c true: if pixel is in limits; @c false: otherwise]
         */
        static bool const CheckCoordinates(int const aX, int const aMaxX, int const aY, int const aMaxY, std::string const aFun);

        /**
         * Static function, checks if provided pixel color is valid.
         *
         * @param   aR      Red component of color must be in <0; 255>.
         * @param   aG      Green component of color must be in <0; 255>.
         * @param   aB      Blue component of color must be in <0; 255>.
         * @param   aA      Alpha component of color must be in <0; 255>.
         * @param   aFun    Caller function name. For exception call.
         * @return  [@c true: if color is valid; @c false: otherwise]
         */
        static bool const CheckRGB(int const aR, int const aG, int const aB, int const aA, std::string const aFun);

        /**
         * Static function, checks if provided color  RGB code is valid.
         *
         * @param   aCode   RGB code to be checked.
         * @param   aFun    Caller function name. For exception call.
         * @return  @c true if color is valid; @c false otherwise
         */
        static bool const CheckRGBCode(std::string const aCode, std::string const aFun);

        /**
         * Static function that can be used to assure that given device is Pen.
         *
         * @warning This function relies on that device name is same as name of device type.
         * For example device named "canvas" or "pen" is considered as Pen device but device
         * named "laser" not.
         *
         * @param   aName       Name of the device.
         * @param   aRobotName  Name of the robot that is suppoused to own device.
         * @return  [@c true: is Pen device, @c false: not Pen device]
         */
        static bool const IsItMe(const std::string &aName, const std::string &aRobotName);

    private:
        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        /**
         * Static function, returns pixel index in data vector from pixel coordinates.
         *
         * @param   aX      X coordinate of pixel.
         * @param   aY      Y coordinate of pixel.
         * @param   aWidth  Width of the row in canvas.
         * @return  Index of the pixel in data vector.
         */
        static unsigned const PixelIndex(int const aX, int const aY, int const aWidth);

        /**
         * Returns pixel index in data vector from pixel coordinates.
         *
         * @param   aX      X coordinate of pixel.
         * @param   aY      Y coordinate of pixel.
         * @return  Index of the pixel in data vector.
         */
        unsigned const PixelIndex(int const aX, int const aY) const { return PixelIndex(aX, aY, GetWidth()); }

        /**
         * Returns pixel coordinates from data vector index.
         *
         * @param   aIndex  Pixel index in data vector.
         * @return  Pixel coordinated.
         */
        ImagePoint_t const PixelCoords(unsigned const aIndex) const;

        std::vector<RGBA_t> m_Data;     ///< Vector of image data.
        std::vector<RGBA_t> m_LastData; ///< Vector of image data before last refresh for comparison.

        std::set<ImagePoint_t> m_DirtyPixels;   ///< Set of pixels that have been modified before refresh.

        std::string m_ImageName,    ///< Name of the image.
                    m_FolderPath;   ///< Path to active projects folder.

        double m_CellSize;  ///< Real size of one pixel. [m]

        int m_Width,        ///< Width of the canvas (number of pixels).
            m_Height;       ///< Height of the canvas (number of pixels).

        static int m_ScreenShotCounter; ///< Static member, screen shot ID counter.
        static int m_UpdateCounter;     ///< Static member, update ID counter.

        static std::string idPN[];  ///< Array of Pen identificators. Used in @ref MorsePen::IsItMe.
        static std::vector<std::string> identifiers;    ///< Vector of Pen identificators. Used in @ref MorsePen::IsItMe.

};
} // namespace CCMorse;

#endif // CCMORSE_MORSEPEN_H
