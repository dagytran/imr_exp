#include "MorseWaypoint.h"

using namespace CCMorse;


std::string MorseWaypoint::idWP[] = {"waypoint", "wp", "basic"};
std::vector<std::string> MorseWaypoint::identifiers (idWP, idWP + sizeof(idWP)/sizeof(std::string) );

void MorseWaypoint::SetupConfiguration()
{
    Dictionary_t config = GetConfigurations();
    Dictionary_t objectToRobot = MapData(config["object_to_robot"]);

    SetGeoPose      ( AsVectorOfDoubles (objectToRobot["translation"]) );
    SetRotation     ( AsVectorOfPoses   (objectToRobot["rotation"]) );
    SetSizeExtent   (0, 0, 0);

    SetFreshConfig(true);
    SetFreshGeom(true);
}

bool const MorseWaypoint::IsItMe(const std::string &aName, const std::string &aRobotName)
{
    for(std::size_t i = 0; i < identifiers.size(); i++){
        if( aName.compare(aRobotName + "." + identifiers.at(i) ) == 0)
            return true;
    }
    return false;
}

void MorseWaypoint::GoTo(double aX, double aY, double aZ, double aTolerance, double aSpeed)
{
    std::string status = CommunicateWithSim("goto [" +
        NumberToString(aX) + ", " +
        NumberToString(aY) + ", " +
        NumberToString(aZ) + ", " +
        NumberToString(aTolerance) + ", " +
        NumberToString(aSpeed) + "]"
    );
}

bool MorseWaypoint::IsArived()
{
    if(CommunicateWithSim("get_status").compare("Arrived"))
        return true;
    else
        return false;
}

void MorseWaypoint::Stop()
{
    CommunicateWithSim("stop");
}

std::string const MorseWaypoint::ToString()
{
    return CommunicateWithSim("get_status");
}

void MorseWaypoint::RefreshData()
{
    UpdateTime();
    SetFresh(true);
}
