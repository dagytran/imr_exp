#include "WindowProxy.h"
#include "MorseClient.h"
#include "MorseRobot.h"

#include <sstream>
#include <fstream>
#include <algorithm>

#define GET_CLIENT  ( reinterpret_cast<MorseClient*>(GetPlayerClient()->GetMorseClient()) )
#define MY_ROBOT    ( GET_CLIENT->GetRobotWithID(m_SubscribedToRobotID) )

using namespace CCMorse;


WindowProxy* WindowProxy::m_Me     = NULL;

const double WindowProxy::FLOOR    = 0.0;
const double WindowProxy::UNKNOWN  = 0.5;
const double WindowProxy::WALL     = 1.0;
const double WindowProxy::EMPTY    = 0.0;
const double WindowProxy::OCCUPIED = 1.0;


void WindowProxy::SetupConfiguration()
{
    SetName("WindowProxy");

    bool checkPen = false;
    int id = 0;
    MorseRobot* robot = GET_CLIENT->GetRobotWithID(id);

    while( checkPen == false && robot != NULL ){

        for(std::vector<MorseDevice*>::const_iterator device = robot->GetDevices().begin(); device != robot->GetDevices().end(); ++ device){
            if( !checkPen && MorsePen::IsItMe((*device)->GetName(), robot->GetName()) ){
                m_Pen = reinterpret_cast<MorsePen*>( (*device) );
                m_Device = m_Pen;
                m_Pen->SetSubscribed(1);
                checkPen = true;
                break;
            }
        }

        if(!checkPen)
            robot = GET_CLIENT->GetRobotWithID( (++id) );
    }

    if(!checkPen)
        throw MorseError("WindowProxy::WindowProxy", "The pen device was not found.");

    m_Pen->RefreshData();

    Subscribe(robot->GetID());
    std::cout << "DEBUG: WindowProxy: Done." << std::endl;
}

WindowProxy& WindowProxy::GetInstance()
{
    return *m_Me;
}

WindowProxy& WindowProxy::GetInstance(PlayerClient* aClient, int width, int height)
{
    if (!m_Me) {

        m_Me = new WindowProxy(aClient);

        m_Me->GetPen()->SetDimensions(width, height);
        m_Me->m_Name    = "image";
        m_Me->m_Drawing = "fig";

        std::cout << "DEBUG: WindowProxy::GetInstance(width = " << width << ", height = " << height << ")" << std::endl;
    }
    return *m_Me;
}

void WindowProxy::DrawMapTransparent(imr::CMapGrid& aMap, RGBA_t color, double transparent, int alpha)
{
    color.a = alpha;
    transparent = 1 - transparent;

    for (int y = 0; y < aMap.getHeight(); y++)
        for (int x = 0; x < aMap.getWidth(); x++)
            if(transparent >= 0 && aMap.getCell(x, y) == transparent)
                GetPen()->SetPixel(x, y, color);

}

void WindowProxy::DrawMap(imr::CMapGrid& aMap)
{
    for (int y = 0; y < aMap.getHeight(); y++) {
        for (int x = 0; x < aMap.getWidth(); x++) {
            // clamp and swap colors - floor is white and wall is black
            double i = 1.0 - Clamp(aMap.getCell(x, y), 0.0, 1.0);
            GetPen()->SetPixel(x, y, i, i, i, 1.0);
        }
    }

    // Sort layers by priority
    std::sort(m_Layers.begin(), m_Layers.end(), SortLayers);

    // draw all overlying m_Layers
    for(std::vector<WindowLayer>::iterator layer = m_Layers.begin(); layer != m_Layers.end(); ++layer)
        if(layer->mVisible)
            DrawMapTransparent(*(layer->mMap), layer->mColor, layer->mTransparent, layer->mAlpha);

    // refresh canvas with new data
    Flush();
}

void WindowProxy::Flush()
{
    GetPen()->RefreshData();
}

void WindowProxy::Subscribe(uint32_t aIndex)
{
    MorseRobot* robot = GET_CLIENT->GetRobotWithID(aIndex);
    m_SubscribedToRobotID = aIndex;
    robot->SubscribeProxy(this);

    std::cout
        << "WindowProxy::Subscribe("
        << aIndex
        << "): Proxy is subscribed to robot named \""
        << robot->GetName()
        << "\"."
        << std::endl;
}

void WindowProxy::Unsubscribe()
{
    ScopedLock_t lock( GetMutex() );
    MY_ROBOT->UnsubscribeProxy(this);
}


void WindowProxy::SWAP(int &x, int &y)
{
    int p;
    p = x;
    x = y;
    y = p;
}

/** --------------------------------------------------------------------
                            Layers handling
----------------------------------------------------------------------*/
int WindowProxy::AddLayer(std::string lname)
{
    //ScopedLock_t lock( GetMutex() );
    if(GetLayerIndex(lname) >= 0)        // if there is already such a layer present, do not add it
        return -1;

    WindowLayer l;
    l.mName = m_Name;

    m_Layers.push_back(l);

    return 0;
}

int WindowProxy::AddLayer(struct WindowLayer l)
{
    //ScopedLock_t lock( GetMutex() );
    if(GetLayerIndex(l.mName) >= 0)        // if there is already such a layer present, do not add it
        return -1;

    m_Layers.push_back(l);

    return 0;
}

void WindowProxy::RemoveLayer(std::string lname)
{
    //ScopedLock_t lock( GetMutex() );
    int i = GetLayerIndex(lname);

    if(i < 0)
        return;

    m_Layers.erase(m_Layers.begin() + i);
}

void WindowProxy::SetLayerVisibility(std::string lname, bool visible)
{
    //ScopedLock_t lock( GetMutex() );
    int i = GetLayerIndex(lname);

    if(i < 0)
        return;

    m_Layers.at(i).mVisible = visible;
}

void WindowProxy::SetLayerPriority(std::string lname, int priority)
{
    //ScopedLock_t lock( GetMutex() );
    int i = GetLayerIndex(lname);

    if(i < 0)
        return;

    m_Layers.at(i).mPriority = priority;
}

void WindowProxy::SetLayerAlpha(std::string lname, int alpha)
{
    //ScopedLock_t lock( GetMutex() );
    int i = GetLayerIndex(lname);

    if(i < 0)
        return;

    m_Layers.at(i).mAlpha = alpha;
}

void WindowProxy::SetLayerTransparent(std::string lname, double transparent)
{
    //ScopedLock_t lock( GetMutex() );
    int i = GetLayerIndex(lname);

    if(i < 0)
        return;

    m_Layers.at(i).mTransparent = transparent;
}

void WindowProxy::SetLayerColor(std::string lname, RGBA_t color)
{
    //ScopedLock_t lock( GetMutex() );
    int i = GetLayerIndex(lname);

    if(i < 0)
        return;

    m_Layers.at(i).mColor = color;
}

void WindowProxy::SetLayerData(std::string lname, imr::CMapGrid &aMap)
{
    //ScopedLock_t lock( GetMutex() );
    int i = GetLayerIndex(lname);

    if(i < 0)
        return;

    m_Layers.at(i).mMap = &aMap;
}

bool WindowProxy::SortLayers(WindowLayer l1, WindowLayer l2)
{
    if(l1.mPriority > l2.mPriority)
        return true;

    else if(l1.mPriority ==  l2.mPriority && l1.mAlpha > l2.mAlpha)
        return true;

    return false;
}

int WindowProxy::GetLayerIndex(std::string lname)
{
    //ScopedLock_t lock( GetMutex() );
    for(unsigned i = 0; i < m_Layers.size(); i++)
        if(m_Layers.at(i).mName.compare(lname) == 0)
            return i;

    return -1;
}
