/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of MorseLaserScannes class.
 */

#ifndef CCMORSE_MORSELASERSCANNER_H
#define CCMORSE_MORSELASERSCANNER_H

#include "MorseSensor.h"


namespace CCMorse{

/**
 * LaserScanner class can be used for obtaining data from laser scanners
 * sensors in Morse simulation.
 *
 * @see https://www.openrobots.org/morse/doc/latest/user/sensors/laserscanner.html
 */
class MorseLaserScanner : public MorseSensor
{
    friend class MorseClient;
    friend class LaserProxy;

    public:
        /**
         * Default constructor.
         */
        MorseLaserScanner() : MorseSensor() { SetupConfiguration(); }

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorseLaserScanner(int aPort, std::string aName) : MorseSensor(aPort, aName) { SetupConfiguration(); }

        /**
         * Destructor.
         */
        ~MorseLaserScanner() { m_RangeList.clear(); m_PointList.clear(); m_IntensityList.clear(); }

        /**
         * Returns position of point where beam from last scan was reflected.
         *
         * Bearing of the beam can be computed as: "bearing [rad] = ScanStart + aIndex * ScanRes"
         * @ref MorseLaserScanner::GetBearing and @ref MorseLaserScanner::GetIndex.
         *
         * @param aIndex    Index number of the laser beam. Index "0" is at @ref MorseLaserScanner::GetScanStart().
         * @return Point of reflection for given index (bearing).
         */
        Pose2D_t GetPoint(int const &aIndex) const { return GetVar(m_PointList.at(aIndex)); }

        /**
         * Returns distance from robot to the point where beam from last scan was reflected.
         *
         * Bearing of the beam can be computed as: "bearing [rad] = ScanStart + aIndex * ScanRes"
         * @ref MorseLaserScanner::GetBearing and @ref MorseLaserScanner::GetIndex.
         *
         * @param aIndex    Index number of the laser beam. Index "0" is at @ref MorseLaserScanner::GetScanStart().
         * @return Range measured for given index (bearing).
         */
        double GetRange(int const &aIndex) const { return GetVar(m_RangeList.at(aIndex)); }

        /**
         * Returns intensity of the reflection from the point where beam from last scan was reflected.
         *
         * Bearing of the beam can be computed as: "bearing [rad] = ScanStart + aIndex * ScanRes"
         * @ref MorseLaserScanner::GetBearing and @ref MorseLaserScanner::GetIndex.
         *
         * @param aIndex    Index number of the laser beam. Index "0" is at @ref MorseLaserScanner::GetScanStart().
         * @return Value of intensity for given index (bearing).
         */
        double const GetIntensity(int const &aIndex) const { return (IsIntensityOn() ? GetVar(m_IntensityList.at(aIndex)) : 0.0); }

        /**
         * Returns vector of all points in last scan.
         *
         * Index number of the laser beam. Index "0" is at bearing @ref MorseLaserScanner::GetScanStart()
         * and last index is @ref MorseLaserScanner::GetScanCount().
         *
         * @return Vector of points. @ref CCMorse::Pose2D_t
         */
        std::vector<Pose2D_t> const &GetPointList() const;

        /**
         * Returns vector of ranges in last scan.
         *
         * Index number of the laser beam. Index "0" is at bearing @ref MorseLaserScanner::GetScanStart()
         * and last index is @ref MorseLaserScanner::GetScanCount().
         *
         * @return Vector of ranges. [m]
         */
        std::vector<double> const &GetRangeList() const;

        /**
         * Returns vector of intensities in last scan if intensity scaning is on. @ref MorseLaserScanner::IsIntensityOn()
         *
         * Index number of the laser beam. Index "0" is at bearing @ref MorseLaserScanner::GetScanStart()
         * and last index is @ref MorseLaserScanner::GetScanCount().
         *
         * @return Vector of intensities.
         */
        std::vector<double> const &GetIntensityList() const;

        /**
         * Returns bearing for given index.
         * @param   aIndex  Index number of laser beam in scan.
         * @return  Brearing of the laser beam. [rad]
         */
        double const GetBearing(int const aIndex) const { return (GetScanStart() + aIndex * GetScanRes()); }

        /**
         * Returns index number for given vearing.
         * @param   aBearing    Bearing of the laser beam in scan.
         * @return  Index of the laser beam in scan vector.
         */
        int const GetIndex(double const aBearing) const { return ((aBearing - GetScanStart()) / GetScanRes()); }

        /**
         * Returns position of the robot.
         * @warning Currently is not working.
         */
        Pose3D_t const GetRobotPose() const { return GetVar(m_RobotPose); }

        /**
         * Returns @c true if the intensity data is available.
         * @return  [@c true: intensity is scanned, @c false intensity is not scanned]
         */
        int const IsIntensityOn() const { return GetVar(m_IntensityOn); }

        /**
         * Returns ID of the laser device.
         */
        int const GetLaserID() const { return GetVar(m_LaserID); }

        /**
         * Returns count of laser beams in one scan.
         * @return  Count of beams in one scan.
         */
        int const GetScanCount() const { return GetVar(m_ScanCount); }

        /**
         * Returns ID number of the last scan.
         */
        int const GetScanID() const { return GetVar(m_ScanID); }

        /**
         * Returns index to laser beam with minimal range measured in right half of scan.
         *
         * First beam considered in right half of scan have index = (MorseLaserScannes::GetScanCount() / 2),
         * and last beam have index = (MorseLaserScannes::GetScanCount() - 1)
         *
         * @return  Index of measurement with minimal distance in right half of scan.
         */
        int const GetMinRightIndex();

        /**
         * Returns index to laser beam with minimal range measured in left half of scan.
         *
         * First beam considered in left half of scan have index = 0,
         * and last beam have index = ((MorseLaserScannes::GetScanCount() / 2) - 1)
         *
         * @return  Index of measurement with minimal distance in left half of scan.
         */
        int const GetMinLeftIndex();

        /**
         * Returns minimal range in right half of scan.
         * @return  Minimal distance measured in right half of scan.
         */
        double const GetMinRight()           { GetMinRightIndex(); return GetVar(m_MinRight); }

        /**
         * Returns minimal range in left half of scan.
         * @return  Minimal distance measured in left half of scan.
         */
        double const GetMinLeft()            { GetMinLeftIndex();  return GetVar(m_MinLeft); }

        /**
         * Returns bearing of the first laser beam in scan. Bearing of the laser beam on index 0.
         * @return  Bearing of the first measurement. [rad]
         */
        double const GetScanStart() const    { return GetVar(m_ScanStart); }

        /**
         * Returns resolution of bearing. It is the same as angle between two consecvent laser beams in scan.
         * @return Bearing resolution. [rad]
         */
        double const GetScanRes() const      { return GetVar(m_ScanRes); }

        /**
         * Returns frequency of scaning.
         * @return Scan frequency. [Hz]
         */
        double const GetScanningFreq() const { return GetVar(m_ScanningFreq); }

        /**
         * Returns resolution of range.
         * @return Range resolution. [m]
         */
        double const GetRangeRes() const     { return GetVar(m_RangeRes); }

        /**
         * Returns maximum range that this device can measure.
         * @return Maximum range measurement. [m]
         */
        double const GetMaxRange() const     { return GetVar(m_MaxRange); }

        /**
         * Gets new laser data from Morse simulation.
         */
        virtual void RefreshData();

        /**
         * Returns string representation of the laser data.
         * @return  Data print.
         */
        std::string const ToString() const;

        /**
         * Static function that can be used to assure that given device is LaserScanner.
         *
         * @warning This function relies on that device name is same as name of device type.
         * For example device named "hokuyo" is considered as LaserScanner device but device
         * named "position" not.
         *
         * @param   aName       Name of the device.
         * @param   aRobotName  Name of the robot that is suppoused to own device.
         * @return  [@c true: is LaserScanner device, @c false: not LaserScanner device]
         */
        static bool const IsItMe(const std::string &aName, const std::string &aRobotName);

    private:
        /**
         * This is called after construction of the class, to set all configurations of the device.
         */
        void SetupConfiguration();

        /**
         * Sets measured points to internal storage.
         */
        void SetPointList(std::vector<Pose2D_t> const &aPoints);

        /**
         * Sets measured ranges to internal storage.
         */
        void SetRangeList(std::vector<double> const &aRanges);

        /**
         * Sets that intensity is measured.
         */
        void SetIntensityOn(bool const &aValue);

        /**
         * Sets measured intensities to internal storage.
         */
        void SetIntensityList(std::vector<double> const &aIntensities);

        /**
         * Sets robot position in time of last scan.
         */
        void SetRobotPose(Pose3D_t const &aValue);

        /**
         * Sets scanning frequency of the device.
         */
        void SetScanningFreq(double const aValue);

        /**
         * Add one to ScanID counter.
         */
        void AddScanID();

        std::vector<Pose2D_t>   m_PointList;     ///< Scan data; points (x[m], y[m], a[rad]).
        std::vector<double>     m_RangeList;     ///< Range data; range (r[m]).
        std::vector<double>     m_IntensityList; ///< Scan reflection intensity data; RSSI.

        Pose3D_t m_RobotPose;   ///< Robot pose (m,m,rad), filled in if the scan came with a pose attached.

        double  m_ScanStart,    ///< Start bearing of the scan. [rad]
                m_ScanRes,      ///< Angular resolution of the scan. [rad]
                m_ScanningFreq, ///< Scanning frequency. [Hz]

                m_RangeRes,     ///< Range resolution. [m]
                m_MaxRange,     ///< Maximum range of sensor. [m]

                m_MinRight,     ///< Minimum range, in meters, in the right half of the scan (those ranges from the first beam after the middle of the scan, clockwise, to the last beam).
                m_MinLeft;      ///< Minimum range, in meters, in the left half of the scan (those ranges from the first beam, clockwise, up to the middle of the scan, including the middle beam, if one exists).

        int     m_IntensityOn,  ///< Is intesity data measured.
                m_ScanCount,    ///< Number of points in the scan.
                m_ScanID,       ///< ID for this scan.
                m_LaserID;      ///< Laser ID information.

        static std::string idLS[];                   ///< Array of LaserScanner identificators. Used in @ref MorseLaserScanner::IsItMe.
        static std::vector<std::string> identifiers; ///< Vector of LaserScanner identificators. Used in @ref MorseLaserScanner::IsItMe.

};
} // namespace CCMorse;

#endif // CCMORSE_MORSELASERSCANNER_H
