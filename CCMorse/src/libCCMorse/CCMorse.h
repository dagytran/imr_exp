/** @mainpage
 *
 * Hello, Welcome to the libCCMorse reference.
 *
 * @section intro_sec Introduction
 *
 * libCCMorse is library that will allow you control robots in Morse simulation
 * via scripts written in C++. libCCMorse is developed under linux using Boost.
 * libCCMorse was created as bachelor project for SyRoTek system, that exist on
 * Faculty of Electrical Engineering on Czech Technical University in Prague.
 *
 * libCCMorse consist of two main layers:
 *
 * - In the first layer are classes whose name begins with "Morse". This layer
 * reflects structure of Morse simulator devices and it is used to communicate with
 * Morse simulation.
 *
 * - Second layer using Player simulator API ( @ref CCMorse::PlayerClient class and proxy classes).
 * The Player simulator was formerly used for teaching students the basics of robotics
 * on SyRoTek system.
 * So the second layer of libCCMorse enables use of older SyRoTek programs on Morse
 * simulator with only small changes in code (basically just be change the appropriate includes).
 * This second layer is implemented by the first layer.
 *
 * These two layers allow you to choose from two different APIs.
 *
 *
 * @subsection syrotek_subsec What is SyRoTek?
 *
 * The SyRoTek (“System for robotic e-learning”) allows you to remotely (via internet)
 * control a multi-robot platform in a dynamic environment. With SyRoTek you will be able
 * to develop own algorithms and monitor their behaviour on-line during real experiments.
 * You can train and test your robotic skills with a large set of pre-prepared multi-robot
 * exercises and with comprehensive robotic courses you can enroll for. Besides, you can
 * arrange your own multi-robot scenario simply by a remote control of dynamic obstacles
 * in the SyRoTek arena and by specifying required robots. SyRoTek provides a large set of
 * sensors placed onboard the robots, while others stand-alone to get global overview of
 * the playfield status on-line.
 *
 * @see http://www.fel.cvut.cz/en/
 * @see http://syrotek.felk.cvut.cz/
 * @see http://www.openrobots.org/morse/doc/stable/morse.html
 * @see http://playerstage.sourceforge.net/doc/Player-3.0.2/player/
 * @see http://theboostcpplibraries.com/
 *
 * @section install_sec Installation
 *
 * @subsection step1 Step 1: Opening the box
 *
 * etc...
 *
 * @section licence_sec Licence
 *
 */

/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-20
 *
 * This is header file for include whole library libCCMorse
 * into your aplication.
 *
 * The level of offset determines the inheritance of class.
 */
#ifndef CCMORSE_H
#define CCMORSE_H

#include "MorseUtility.h"   // Utility functions and type definitions.
#include "MorseError.h"     // Class for Morse exceptions.
#include "WindowLayer.h"    // Type class for WindowProxy.

#include "MorseClient.h"    // Class that communicate with Morse simulation.
#include "PlayerClient.h"   // Class that mimics and implement PlayerCc::PlayerClient class.

#include "MorseRobot.h"     // Class that hold informations a about controlled robot.

#include "MorseDevice.h"    // Class that represents a part of the simulation, with which MorseClient can communicate.
    #include "MorseSensor.h"    // Inherit class from MorseStream. Specifing that stream is linked to sensor.
        #include "MorsePose.h"          // Inherit class from MorseSensor. Defines functions of Pose sensor.
        #include "MorseOdometry.h"      // Inherit class from MorseSensor. Defines functions of Odometry sensor.
        #include "MorseLaserScanner.h"  // Inherit class from MorseSensor. Defined functions of LaserScanner sensor.
        #include "MorseProperties.h"    //

    #include "MorseActuator.h"  // Inherit class from MorseStream. Specifing that stream is linked to actuator.
        #include "MorseMotionVW.h"      // Inherit class from MorseActuator. Defines functions of MotionVW actuator.
        #include "MorseWaypoint.h"      // Inherit class from MorseActuator. Defines functions of Waypoint driver.
        #include "MorsePen.h"           // Inherit class from MorseActuator. Enables drawing on map canvas component in MorseSyrotek.

    #include "MorseTime.h"      // Inherit class from MorseStream. Defines functions around simulation time.

#include "MorseProxy.h"     // Parental class to classes imitating Player proxies, "implements" PlayerCc::ClientProxy.
    #include "Position2dProxy.h"// Imitating PlayerCc::Position2dProxy class.
    #include "LaserProxy.h"     // Imitating PlayerCc::LaserProxy class.
    #include "WindowProxy.h"    // Class with utilities for drawing on MorseSyrotek canvas component.

// Drivers for Position2dProxy.
#include "MorseDriver.h"
    #include "WaypointDriver.h"
    #include "SndDriver.h"

// Classes for socket communication.
#include "Socket.h"
    #include "ClientSocket.h"
#include "SocketException.h"


#endif // CCMORSE_H
