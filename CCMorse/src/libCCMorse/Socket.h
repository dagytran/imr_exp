/*
*  Definition of the Socket class
*
*  From: http://www.tldp.org/LDP/LG/issue74/tougher.html#4
*  Copyright © 2002, Rob Tougher.
*  Published in Issue 74 of Linux Gazette, January 2002
*/

#ifndef SOCKET_H
#define SOCKET_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string>
#include <arpa/inet.h>

#ifndef MSG_NOSIGNAL
# define MSG_NOSIGNAL 0
# ifdef SO_NOSIGPIPE
#  define CEPH_USE_SO_NOSIGPIPE
# else
#  error "Cannot block SIGPIPE!"
# endif
#endif


const int MAXHOSTNAME = 256;
const int MAXCONNECTIONS = 24;
const int MAXRECV = 4096;

class Socket
{
    public:
        Socket();
        virtual ~Socket();

        // Server initialization
        bool create();
        bool bind ( const int port );
        bool listen() const;
        bool accept ( Socket& ) const;

        // Client initialization
        bool connect ( const std::string host, const int port );

        // Data Transimission
        bool send ( const std::string ) const;
        int recv ( std::string& aStr, int aBuffSize ) const;
        int recv ( std::string& aStr ) const { return Socket::recv(aStr, MAXRECV); }

        void set_non_blocking ( const bool );

        bool is_valid() const { return m_sock != -1; }

    private:
        int m_sock;
        sockaddr_in m_addr;
};

#endif
