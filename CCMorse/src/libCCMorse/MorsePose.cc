#include "MorsePose.h"

using namespace CCMorse;


std::string MorsePose::idP[] = {"pose", "position"};
std::vector<std::string> MorsePose::identifiers (idP, idP + sizeof(idP)/sizeof(std::string) );

void MorsePose::SetupConfiguration()
{
    Dictionary_t config = GetConfigurations();
    Dictionary_t objectToRobot = MapData(config["object_to_robot"]);

    SetGeoPose      ( AsVectorOfDoubles (objectToRobot["translation"]) );
    SetRotation     ( AsVectorOfPoses   (objectToRobot["rotation"]) );
    SetSizeExtent   (0.08, 0.08, 0.04);

    SetFreshConfig(true);
    SetFreshGeom(true);
}

void MorsePose::RefreshData()
{
    Dictionary_t data = GetLocalData();

    UpdateTime ( AsDouble(data["timestamp"]) );

    SetX     ( AsDouble(data["x"]));
    SetY     ( AsDouble(data["y"]));
    SetZ     ( AsDouble(data["z"]));
    SetYaw   ( AsDouble(data["yaw"]));
    SetPitch ( AsDouble(data["pitch"]));
    SetRoll  ( AsDouble(data["roll"]));

    SetFresh(true);
}

bool const MorsePose::IsItMe(const std::string &aName, const std::string &aRobotName)
{
    for(std::size_t i = 0; i < identifiers.size(); i++){
        if(aName.compare(aRobotName + "." + identifiers.at(i)) == 0)
            return true;
    }
    return false;
}

std::string const MorsePose::ToString() const
{
    std::string output(
        "\"Timestamp\": "   + NumberToString(GetDataTime()) +
        ", \"X\": "         + NumberToString(GetX()) +
        ", \"Y\": "         + NumberToString(GetY()) +
        ", \"Z\": "         + NumberToString(GetZ()) +
        ", \"Yaw\": "       + NumberToString(GetYaw()) +
        ", \"Pitch\": "     + NumberToString(GetPitch()) +
        ", \"Roll\": "      + NumberToString(GetRoll())
    );
    return output;
}

Pose2D_t const MorsePose::GetPose2D() const
{
    Pose2D_t output = {GetX(), GetY(), GetYaw()};
    return output;
}

Pose3D_t const MorsePose::GetPose3D() const
{
    Pose3D_t output = {GetX(), GetY(), GetZ(), GetYaw(), GetPitch(), GetRoll()};
    return output;
}

void MorsePose::SetX(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_X = aValue;
}

void MorsePose::SetY(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Y = aValue;
}

void MorsePose::SetZ(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Z = aValue;
}

void MorsePose::SetYaw(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Yaw = aValue;
}

void MorsePose::SetPitch(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Pitch = aValue;
}

void MorsePose::SetRoll(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Roll = aValue;
}


