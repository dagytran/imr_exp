#include "Position2dProxy.h"
#include "MorseClient.h"

#define GET_CLIENT  ( reinterpret_cast<MorseClient*>(GetPlayerClient()->GetMorseClient()) )
#define MY_ROBOT    ( GET_CLIENT->GetRobotWithID(m_SubscribedToRobotID) )

using namespace CCMorse;


void Position2dProxy::SetupConfiguration()
{
    SetName("Position2dProxy");

    MorseRobot* robot = GET_CLIENT->GetRobotProvidingPosition2d(GetIndex());

    if(robot == NULL) {
        throw MorseError("Position2dProxy::SetupConfiguration", "No robot providing this proxy index.");

    } else {
        bool checkPose = false, checkMotion = false, checkOdometry = false;

        for(std::vector<MorseDevice*>::const_iterator device = robot->GetDevices().begin(); device != robot->GetDevices().end(); ++ device){

            if(!checkPose && MorsePose::IsItMe( (*device)->GetName(), robot->GetName() )){
                m_Pose = reinterpret_cast<MorsePose*>( (*device) );
                m_Pose->SetSubscribed(1);
                checkPose = true;
            }

            if(!checkMotion && MorseMotionVW::IsItMe( (*device)->GetName(), robot->GetName() )){
                m_Motion = reinterpret_cast<MorseMotionVW*>( (*device) );
                m_Motion->SetSubscribed(1);
                checkMotion = true;
            }

            if(!checkOdometry && MorseOdometry::IsItMe( (*device)->GetName(), robot->GetName() )){
                m_Odometry = reinterpret_cast<MorseOdometry*>( (*device) );
                m_Odometry->SetSubscribed(1);
                checkOdometry = true;
            }

            if(checkMotion && checkPose && checkOdometry)
                break;
        }

        if(!checkPose)
            throw MorseError("Position2dProxy::SetupConfiguration", "The pose device was not found.");
        if(!checkMotion)
            throw MorseError("Position2dProxy::SetupConfiguration", "The motionvw device was not found.");
        if(!checkOdometry)
            throw MorseError("Position2dProxy::SetupConfiguration", "The odometry device was not found.");

        do {
            m_Pose->RefreshData();
            m_Motion->RefreshData();
            m_Odometry->RefreshData();
        } while( (m_Pose->IsFreshConfig() != 1) && (m_Motion->IsFreshConfig() != 1) && (m_Odometry->IsFreshConfig() != 1) );

        m_Global = robot->IsPosition2dProxyGlobal(GetIndex());

        if(m_Global)
            m_Device = m_Pose;
        else
            m_Device = m_Odometry;

        SetOdometry(m_Pose->GetX(), m_Pose->GetY(), m_Pose->GetYaw());

        Subscribe(robot->GetID());
        std::cout << "DEBUG: Position2dProxy: Done." << std::endl;
    }
}

Position2dProxy::Position2dProxy(Position2dProxy const &aOther)
{
    SetName("Position2dProxy");

    m_Client = aOther.m_Client;
    m_Index  = aOther.m_Index;
    m_ID     = aOther.m_ID;

    m_Fresh    = aOther.m_Fresh;
    m_Valid    = aOther.m_Valid;
    m_LastTime = aOther.m_LastTime;

    m_SubscribedToRobotID = aOther.m_SubscribedToRobotID;

    m_Global   = aOther.m_Global;
    m_Pose     = aOther.m_Pose;
    m_Motion   = aOther.m_Motion;
    m_Odometry = aOther.m_Odometry;
}

void Position2dProxy::Subscribe(uint32_t aIndex)
{
    MorseRobot* robot = GET_CLIENT->GetRobotWithID(aIndex);

    std::cout
        << "Position2dProxy::Subscribe(): Proxy with index = " << GetIndex() << " is subscribed to robot named \""
        << robot->GetName() << "\"." << std::endl;

    m_SubscribedToRobotID = aIndex;

    robot->SubscribeProxy(this);
}

void Position2dProxy::Unsubscribe()
{
    ScopedLock_t lock( GetMutex() );
    MY_ROBOT->UnsubscribeProxy(this);
}

namespace std {
ostream& operator<< (ostream &os, const Position2dProxy &c)
{
    os << "#Position2D (" << c.GetID() << ":" << c.GetIndex() << ")" << endl;
    os << "#xpos\typos\ttheta\tspeed\tsidespeed\tturn\tstall" << endl;
    os << c.GetXPos() << " " << c.GetYPos() << " " << c.GetYaw() << " " ;
    os << c.GetXSpeed() << " " << c.GetYSpeed() << " " << c.GetYawSpeed() << " ";
    os << c.GetStall() << endl;

    return os;
}}

void Position2dProxy::SetSpeed(double aXSpeed, double aYSpeed, double aYawSpeed)
{
    ScopedLock_t lock( GetMutex() );
    m_Motion->SetSpeed(aXSpeed, aYSpeed, aYawSpeed);
    m_Motion->ChangeSpeed();
}

void Position2dProxy::SetVelHead(double aXSpeed, double aYSpeed, double aYawHead)
{
    m_Motion->SetSpeed(aXSpeed, aYSpeed, aYawHead);
}

void Position2dProxy::GoTo(Pose2D_t pos, Pose2D_t vel)
{
    MY_ROBOT->GoTo(pos, vel);
}

void Position2dProxy::GoTo(Pose2D_t pos)
{
    MY_ROBOT->GoTo(pos);
}

void Position2dProxy::GoTo(double aX, double aY, double aYaw)
{
    MY_ROBOT->GoTo(aX, aY, aYaw);
}

void Position2dProxy::GoTo(double aX, double aY, double aYaw, double vX, double vY, double vYaw)
{
    MY_ROBOT->GoTo(aX, aY, aYaw, vX, vY, vYaw);
}

void Position2dProxy::SetMotorEnable(bool enable)
{
    ScopedLock_t lock( GetMutex() );
    m_Motion->SetEnable(enable);
}


void Position2dProxy::SetOdometry(double aX, double aY, double aYaw)
{
    ScopedLock_t lock( GetMutex() );
    m_Odometry->SetX(aX);
    m_Odometry->SetY(aY);
    m_Odometry->SetYaw(aYaw);
}

void Position2dProxy::ResetOdometry()
{
    ScopedLock_t lock( GetMutex() );
    SetOdometry(0, 0, 0);
}
