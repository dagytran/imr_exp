#include "MorsePen.h"

#include <ios>
#include <iostream>
#include <fstream>

using namespace CCMorse;


std::string MorsePen::idPN[] = {"pen", "canvaspen", "mappen", "canvas", "gridMap"};
std::vector<std::string> MorsePen::identifiers (idPN, idPN + sizeof(idPN)/sizeof(std::string) );

int MorsePen::m_ScreenShotCounter = 0;
int MorsePen::m_UpdateCounter = 0;


void MorsePen::SetupConfiguration()
{
    Dictionary_t config = GetConfigurations();

    m_CellSize = AsDouble(CommunicateWithSim("get_cell_size"));

    m_Width  = AsInteger(config["CanvasWidth"]);
    m_Height = AsInteger(config["CanvasHeight"]);

    m_FolderPath = config["CanvasFolder"] + "/";
    m_ImageName = "";

    m_Data.resize( m_Width*m_Height, RGBA_t(255, 255, 255, 255) );
    m_LastData.resize( m_Width*m_Height, RGBA_t(255, 255, 255, 255) );

    m_DirtyPixels.clear();

    Dictionary_t objectToRobot = MapData(config["object_to_robot"]);

    SetGeoPose    ( AsVectorOfDoubles (objectToRobot["translation"]) );
    SetRotation   ( AsVectorOfPoses   (objectToRobot["rotation"]) );
    SetSizeExtent (0, 0, 0);

    SetFreshConfig(true);
    SetFreshGeom(true);
    RefreshData();
}

void MorsePen::SetDimensions(int const aWidth, int const aHeight)
{
    if( aWidth > 0 && aHeight > 0 && (aWidth != GetWidth() || aHeight != GetHeight())){

        //ScopedLock_t lock( GetMutex() );
        std::vector<RGBA_t> resizedData (aWidth*aHeight, RGBA_t());

        unsigned width  = Min(aWidth, m_Width);
        unsigned height = Min(aHeight, m_Height);

        for(unsigned y = 0; y < height; y++)
            for(unsigned x = 0; x < width; x++)
                resizedData[ PixelIndex(x, y, aWidth) ] = m_Data[ PixelIndex(x, y, m_Width) ];

        m_Data.swap(resizedData);

        m_LastData.clear();
        m_LastData = m_Data;

        m_Width  = aWidth;
        m_Height = aHeight;

        CommunicateWithSim("resize_canvas [" + NumberToString(m_Width) + ", " + NumberToString(m_Height) + "]");

        m_CellSize = AsDouble(CommunicateWithSim("get_cell_size"));
    }
}

void MorsePen::RefreshData()
{
    ScopedLock_t lock( GetMutex() );

    for(std::set<ImagePoint_t>::iterator pix = m_DirtyPixels.begin(); pix != m_DirtyPixels.end(); ++pix)
        if(m_LastData[ PixelIndex(pix->x, pix->y) ] == m_Data[ PixelIndex(pix->x, pix->y) ])
            m_DirtyPixels.erase( *pix );

    if(m_DirtyPixels.size() != 0){
        std::vector<ImagePoint_t> newPixels ( m_DirtyPixels.begin(), m_DirtyPixels.end() );
        UpdateImage( newPixels );
    }

    m_DirtyPixels.clear();
    m_LastData = m_Data;
    SetFresh(true);
}

void MorsePen::SetImage()
{
    std::stringstream uploadNum;
    uploadNum << std::setfill('0') << std::setw(3) << (m_UpdateCounter++);

    if(m_UpdateCounter > 999)
        m_UpdateCounter = 0;

    std::string imageName = "set_image_" + uploadNum.str() + ".txt";
    std::string imagePath = m_FolderPath + imageName;

    std::ofstream imageFile;
    imageFile.open(imagePath.c_str(), std::ios::out);
    imageFile << "[";
    for(int x = 0; x < m_Width; x++){

        imageFile << "[";
        for(int y = 0; y < m_Height; y++)
            imageFile << "[" << m_Data[ PixelIndex(x, y) ].ToStr() << ((y == (m_Height-1)) ? "]" : "], ");

        imageFile << ((x == (m_Width-1)) ? "]" : "], ");
    }
    imageFile << "]";
    imageFile.close();

    CommunicateWithSim("set_image_data [\"" + imageName + "\"]");
}

void MorsePen::UpdateImage(std::vector<ImagePoint_t> aNewPixels)
{
    std::string output = "[";

    for(unsigned i = 0; i < aNewPixels.size(); i++){
        ImagePoint_t p = aNewPixels.at(i);
        output += ("[" + p.ToStr() + "," + GetPixel(p).ToStr() + ((i == (aNewPixels.size()-1)) ? "]" : "],") );
    }

    output += "]";

    //std::cout << "MorsePen::UpdateImage(): Output LEN = " << output.length() << std::endl;
    /// TODO: Sending all changes thru socket...

    if(output.length() < (MAXRECV-24)) {
        CommunicateWithSim("update_image_data [\"" + output + "\"]");

    } else {

        std::stringstream uploadNum;
        uploadNum << std::setfill('0') << std::setw(3) << (m_UpdateCounter++);

        if(m_UpdateCounter > 999)
            m_UpdateCounter = 0;

        std::string imageName = "update_image_" + uploadNum.str() + ".txt";
        std::string imagePath = m_FolderPath + imageName;

        std::ofstream imageFile;
        imageFile.open(imagePath.c_str(), std::ios::out);
        imageFile << output;
        imageFile.close();

        CommunicateWithSim("update_image_data_via_file [\"" + imageName + "\"]");
    }
}

bool const MorsePen::CheckCoordinates(int const aX, int const aMaxX, int const aY, int const aMaxY, std::string const aFun)
{
    return (CCMorse::CheckIntValue(aX, 0, aMaxX, aFun) && CCMorse::CheckIntValue(aY, 0, aMaxY, aFun));
}

bool const MorsePen::CheckRGB(int const aR, int const aG, int const aB, int const aA, std::string const aFun)
{
    return (
        CCMorse::CheckIntValue(aR, 0, 256, aFun)
        && CCMorse::CheckIntValue(aG, 0, 256, aFun)
        && CCMorse::CheckIntValue(aB, 0, 256, aFun)
        && CCMorse::CheckIntValue(aA, 0, 256, aFun)
    );
}

bool const MorsePen::CheckRGBCode(std::string const aCode, std::string const aFun)
{
    if(aCode.find_first_not_of("0123456789ABCDEFabcdef#") == std::string::npos && aCode[0] == '#'){
        return true;

    } else {
        std::cerr << aFun << ": The RGB code is not valid." << std::endl;
        return false;
        //throw MorseError(aFun, "The RGB code is not valid.");
    }
}

RGBA_t const MorsePen::GetPixel(int const aX, int const aY) const
{
    if(CheckCoordinates(aX, aY, "MorsePen::GetPixel"))
        //ScopedLock_t lock( GetMutex() );
        return m_Data[PixelIndex(aX, aY)];

    throw MorseError("MorsePen::GetPixel", "Pixel coordinates was over bounds.");
//    else {
//        RGBA_t null;
//        return null;
//    }
}

std::string const MorsePen::StrHexFromFloat(double const aValue)
{
    if(CheckDoubleValue(aValue, 0.0, 1.0, "MorsePen::StrHexFromFloat"))
        return MorsePen::StrHexFromInt( (int) (aValue*255) );

    std::cerr << "MorsePen::StrHexFromFloat(): Value must be in interval <0.0; 1.0>. Returning empty string." << std::endl;
    return "";
}

std::string const MorsePen::StrHexFromInt(int const aValue)
{
    if(CheckIntValue(aValue, 0, 256, "MorsePen::StrHexFromInt()")){
        std::stringstream stream;
        stream << std::setfill('0') << std::setw(2) << std::hex << aValue;
        return stream.str();
    }

    std::cerr << "MorsePen::StrHexFromInt(): Value must be in interval <0; 255>. Returning empty string." << std::endl;
    return "";
}

std::string const MorsePen::GetRGBCode(int const aR, int const aG, int const aB)
{
    return ("#" + StrHexFromInt(aR) + StrHexFromInt(aG) + StrHexFromInt(aB));
}

std::string const MorsePen::GetRGBCode(double const aR, double const aG, double const aB)
{
    return ("#" + StrHexFromFloat(aR) + StrHexFromFloat(aG) + StrHexFromFloat(aB));
}

void MorsePen::SetImageName(std::string const &aName)
{
    //ScopedLock_t lock( GetMutex() );
    m_ImageName = aName;
}

void MorsePen::DrawPixel(int const aX, int const aY, std::string const aRGBCode)
{
    if(CheckCoordinates(aX, aY, "MorsePen::DrawPixel") && CheckRGBCode(aRGBCode, "MorsePen::SetPixel")){
        CommunicateWithSim(
            "set_pixel_code [" + NumberToString(aX) + ", "
            + NumberToString(aY) + ", \""
            + aRGBCode + "\"]"
        );
    }
}

void MorsePen::DrawPixel(int const aX, int const aY, int const pR, int const pG, int const pB, int const pA)
{
    if(CheckCoordinates(aX, aY, "MorsePen::DrawPixel")){
        RGBA_t c (pR, pG, pB, pA);
        SetPixel(aX, aY, c);
        m_DirtyPixels.erase( m_DirtyPixels.find( ImagePoint_t(aX, aY) ) );
        CommunicateWithSim(
            "set_pixel [" + NumberToString(aX) + ", " + NumberToString(aY) + ", " + c.ToStr() + "]"
        );
    }
}

void MorsePen::SetPixel(int const aX, int const aY, int const pR, int const pG, int const pB, int const pA)
{
    if(CheckCoordinates(aX, aY, "MorsePen::SetPixel")) {
        //ScopedLock_t lock( GetMutex() );

        RGBA_t c (pR, pG, pB, pA);
        unsigned i = PixelIndex(aX, aY);

        if (c.a == 0)       // If a color is transparent no action needed
            return;

        if (c.a == 255) {   // If a color is opaque just aply it to pixel
            m_Data[i] = c;

        } else {            // If a color is partialy transparent we need to calculate new color
            unsigned int alpha = c.a + 1;
            unsigned int inv_alpha = 256 - c.a;

            RGBA_t dst = GetPixel(aX, aY);
            RGBA_t out (
                ((alpha * c.r + inv_alpha * dst.r) >> 8),
                ((alpha * c.g + inv_alpha * dst.g) >> 8),
                ((alpha * c.b + inv_alpha * dst.b) >> 8),
                255
            );

            m_Data[i] = out;
        }

        m_DirtyPixels.insert( ImagePoint_t(aX, aY) );
    }
}


int const MorsePen::GetX(double const aPosX) const
{
    int x = aPosX / m_CellSize;
    if (CCMorse::CheckIntValue(x, 0, GetWidth(), "MorsePen::GetX()"))
        return x;
    else
        return -1;
}

int const MorsePen::GetY(double const aPosY) const
{
    int y = GetHeight() - 1 - (aPosY / m_CellSize);
    if (CCMorse::CheckIntValue(y, 0, GetHeight(), "MorsePen::GetY()"))
        return y;
    else
        return -1;
}

double const MorsePen::GetPosX(int const aPixX) const
{
    return ((double) (aPixX * m_CellSize));
}

double const MorsePen::GetPosY(int const aPixY) const
{
    return ((double) ((GetHeight() - 1 - aPixY) * m_CellSize));
}

bool const MorsePen::IsItMe(const std::string &aName, const std::string &aRobotName)
{
    for(std::size_t i = 0; i < identifiers.size(); i++){
        if(aName.compare(aRobotName + "." + identifiers.at(i)) == 0)
            return true;
    }
    return false;
}

void MorsePen::DrawLine(int x0, int y0, int x1, int y1, int const r, int const g, int const b, int const a)
{
    if( CheckCoordinates(x0, y0, "MorsePen::DrawLine") && CheckCoordinates(x1, y1, "MorsePen::DrawLine") ){

        RGBA_t c (r, g, b, a);

        CommunicateWithSim(
            "set_line ["
            + NumberToString(x0) + ", " + NumberToString(y0) + ", "
            + NumberToString(x1) + ", " + NumberToString(y0)+ ", "
            + c.ToStr() + "]"
        );

        int dx = x1 - x0;
        int dy = y1 - y0;
        int steep = (abs(dy) >= abs(dx));
        if (steep) {
            SWAP(x0, y0);
            SWAP(x1, y1);
            // recompute Dx, Dy after swap
            dx = x1 - x0;
            dy = y1 - y0;
        }
        int xstep = 1;
        if (dx < 0) {
            xstep = -1;
            dx = -dx;
        }
        int ystep = 1;
        if (dy < 0) {
            ystep = -1;
            dy = -dy;
        }
        int twoDy = 2 * dy;
        int twoDyTwoDx = twoDy - 2 * dx; // 2*Dy - 2*Dx
        int e = twoDy - dx; //2*Dy - Dx
        int y = y0;
        int xDraw, yDraw;
        for (int x = x0; x != x1; x += xstep) {
            if (steep) {
                xDraw = y;
                yDraw = x;
            } else {
                xDraw = x;
                yDraw = y;
            }
            DrawPixel(xDraw, yDraw, c);
            // next
            if (e > 0) {
                e += twoDyTwoDx; //E += 2*Dy - 2*Dx;
                y = y + ystep;
            } else {
                e += twoDy; //E += 2*Dy;
            }
        }
    }
}

void MorsePen::SetLine(int x0, int y0, int x1, int y1, int const r, int const g, int const b, int const a)
{
    if( CheckCoordinates(x0, y0, "MorsePen::SetLine") && CheckCoordinates(x1, y1, "MorsePen::SetLine") ){

        int     dx = x1 - x0,
                dy = y1 - y0,
                steep = (abs(dy) >= abs(dx));

        if (steep) {
            SWAP(x0, y0);
            SWAP(x1, y1);
            // recompute Dx, Dy after swap
            dx = x1 - x0;
            dy = y1 - y0;
        }
        int xstep = 1;
        if (dx < 0) {
            xstep = -1;
            dx = -dx;
        }
        int ystep = 1;
        if (dy < 0) {
            ystep = -1;
            dy = -dy;
        }
        int twoDy = 2 * dy;
        int twoDyTwoDx = twoDy - 2 * dx; // 2*Dy - 2*Dx
        int e = twoDy - dx; //2*Dy - Dx
        int y = y0;
        int xDraw, yDraw;
        for (int x = x0; x != x1; x += xstep) {
            if (steep) {
                xDraw = y;
                yDraw = x;
            } else {
                xDraw = x;
                yDraw = y;
            }

            SetPixel(xDraw, yDraw, r, g ,b, a);
            // next
            if (e > 0) {
                e += twoDyTwoDx; //E += 2*Dy - 2*Dx;
                y = y + ystep;
            } else {
                e += twoDy; //E += 2*Dy;
            }
        }
    }
}

void MorsePen::SWAP(int &x, int &y)
{
    int p;
    p = x;
    x = y;
    y = p;
}

std::string const MorsePen::GetImageName() const
{
    //ScopedLock_t lock( GetMutex() );
    if(m_ImageName.compare("") == 0)
        return ("screenshot_" + NumberToString(m_ScreenShotCounter++));
    else
        return m_ImageName;
}

void MorsePen::SavePNG(std::string const &aName) const
{
    CommunicateWithSim("save_image_copy [\"" + aName + ".png\"]");
}

unsigned const MorsePen::PixelIndex(int aX, int aY, int aWidth)
{
    return (aY * aWidth + aX);
}

ImagePoint_t const MorsePen::PixelCoords(unsigned const aIndex) const
{
    int x = aIndex % GetWidth();
    int y = (aIndex - x) / GetWidth();
    ImagePoint_t p (x, y);
    return p;
}
