#include "MorseDevice.h"
#include "MorseClient.h"

using namespace CCMorse;


int MorseDevice::s_IDCounter = 0;

MorseDevice::MorseDevice()
{
    m_Port = 0;
    m_Name = "";
    m_ID   = -1;

    m_Subscribed  = 0;
    m_Fresh       = 0;
    m_FreshGeom   = 0;
    m_FreshConfig = 0;

    m_DataTime = 0;
    m_LastTime = 0;
}

MorseDevice::MorseDevice(int aPort, std::string aName)
{
    m_Port = aPort;
    m_Name = aName;
    m_ID   = s_IDCounter;
    s_IDCounter++;

    m_Subscribed  = 0;
    m_Fresh       = 0;
    m_FreshGeom   = 0;
    m_FreshConfig = 0;

    m_DataTime = 0;
    m_LastTime = 0;
}

MorseDevice::MorseDevice(const MorseDevice& aDevice)
{
    m_Port = aDevice.GetPort();
    m_Name = aDevice.GetName();
    m_ID   = aDevice.GetID();

    m_Subscribed  = aDevice.IsSubscribed();
    m_Fresh       = aDevice.IsFresh();
    m_FreshGeom   = aDevice.IsFreshGeom();
    m_FreshConfig = aDevice.IsFreshConfig();

    m_DataTime = aDevice.GetDataTime();
    m_LastTime = aDevice.GetLastTime();
}

MorseDevice::~MorseDevice()
{
    MorseClient::GetInstance()->EraseDevice( this );
}

std::string const MorseDevice::CommunicateWithSim(std::string const &aMessage) const
{
    //std::cout << "MorseDevice::CommunicateWithSim: Sending: " << ("id " + GetName() + " " + aMessage) << std::endl;

    std::string reply = MorseClient::GetInstance()->SendToSocket("id " + GetName() + " " + aMessage);

//    std::cout
//        << "MorseDevice::CommunicateWithSim: Got reply: " << std::endl
//        << "__" << reply.substr(0, 50) << "__" << std::endl
//        << ", ReplyStatus: __" << status << "__" << std::endl;

    if(reply.find("id SUCCESS") != std::string::npos) {
        //std::cout << "MorseDevice::CommunicateWithSim: Communication OK." << std::endl;
        reply = reply.substr(11, (reply.length() - 11));

//        if (reply[0] == '{')
//            return (reply.substr(1, (reply.length() - 2)));

        if (reply.compare("") == 0)
            return COM_OK;
        else
            return reply;

    } else if(reply.find("id FAILED") != std::string::npos) {
        std::cout << "MorseDevice::CommunicateWithSim: Communication with simulation FAILED." << std::endl;
        return COM_KO;

    } else if(reply.find("id PREEMPTED") != std::string::npos) {
        std::cout << "MorseDevice::CommunicateWithSim: Communication with simulation PREEMPTED." << std::endl;
        return COM_KO;

    } else {
        throw MorseError("MorseDevice::CommunicateWithSim", "Communication with simulation was lost.");
    }
}

Dictionary_t const MorseDevice::GetConfigurations() const
{
    Dictionary_t dict = MapData( CommunicateWithSim("get_configurations") );
    return (MapData( dict["configurations"] ));
}

Dictionary_t const MorseDevice::GetProperties() const
{
    Dictionary_t dict = MapData( CommunicateWithSim("get_properties") );
    return (MapData( dict["properties"] ));
}

Pose3D_t const MorseDevice::GetGeoPose() const
{
    //ScopedLock_t lock( GetMutex() );
    Pose3D_t p = {m_GeoPose.at(0), m_GeoPose.at(1), m_GeoPose.at(2), 0.0, 0.0, 0.0};
    return p;
}

BBox3D_t const MorseDevice::GetSizeExtent() const
{
    //ScopedLock_t lock( GetMutex() );
    BBox3D_t b = {m_SizeExtent.at(0), m_SizeExtent.at(1), m_SizeExtent.at(2)};
    return b;
}

void MorseDevice::SetSizeExtent(BBox3D_t aSize)
{
    SetSizeExtent(aSize.sl, aSize.sw, aSize.sh);
}

void MorseDevice::SetSizeExtent(double aLength, double aWidth, double aHeight)
{
    //ScopedLock_t lock( GetMutex() );
    m_SizeExtent.clear();
    m_SizeExtent.push_back(aLength);
    m_SizeExtent.push_back(aWidth);
    m_SizeExtent.push_back(aHeight);
}

std::string const MorseDevice::ToString() const
{
    std::string output ("Name: " + GetName() + ", Port: " + NumberToString(GetPort()));
    return output;
}

void MorseDevice::InitTime()
{
    //ScopedLock_t lock( GetMutex() );
    m_LastTime = MorseClient::GetTimeNow();
    m_DataTime = MorseClient::GetTimeNow();
}

void MorseDevice::UpdateTime()
{
    //ScopedLock_t lock( GetMutex() );
    m_LastTime = m_DataTime;
    m_DataTime = MorseClient::GetTimeNow();
    //std::cout << "MorseDevice::UpdateTime(): Last = " << m_LastTime << ", Now = " << m_DataTime << std::endl;
}

void MorseDevice::UpdateTime(double aTimestamp)
{
    //ScopedLock_t lock( GetMutex() );
    m_LastTime = m_DataTime;
    m_DataTime = aTimestamp;
}

void MorseDevice::RefreshData() {
    //UpdateTime();
    //SetFresh(true);
}

void MorseDevice::SetSubscribed(int aSubscribed)
{
    //ScopedLock_t lock( GetMutex() );
    m_Subscribed = aSubscribed;
}

void MorseDevice::SetFresh(int val)
{
    //ScopedLock_t lock( GetMutex() );
    m_Fresh = val;
}

void MorseDevice::SetFreshGeom(int val)
{
    //ScopedLock_t lock( GetMutex() );
    m_FreshGeom = val;
}

void MorseDevice::SetFreshConfig(int val)
{
    //ScopedLock_t lock( GetMutex() );
    m_FreshConfig = val;
}

void MorseDevice::SetUserData(void *val)
{
    //ScopedLock_t lock( GetMutex() );
    m_UserData = val;
}

void MorseDevice::SetGeoPose(const std::vector<double> &aGeoPose)
{
    //ScopedLock_t lock( GetMutex() );
    m_GeoPose.assign(aGeoPose.begin(), aGeoPose.end());
}

void MorseDevice::SetRotation(const std::vector<Pose2D_t> &aRotation)
{
    //ScopedLock_t lock( GetMutex() );
    m_Rotation.assign(aRotation.begin(), aRotation.end());
}
