#include "MorseOdometry.h"

using namespace CCMorse;


std::string MorseOdometry::idO[] = {"odometry", "odom", "odo"};
std::vector<std::string> MorseOdometry::identifiers (idO, idO + sizeof(idO)/sizeof(std::string) );

void MorseOdometry::SetupConfiguration()
{
    Dictionary_t config = GetConfigurations();
    Dictionary_t objectToRobot = MapData(config["object_to_robot"]);

    SetGeoPose    ( AsVectorOfDoubles (objectToRobot["translation"]) );
    SetRotation   ( AsVectorOfPoses   (objectToRobot["rotation"]) );
    SetSizeExtent (0.04, 0.04, 0.04);

    SetFreshConfig(true);
    SetFreshGeom(true);
}

void MorseOdometry::RefreshData()
{
    Dictionary_t data = GetLocalData();

    UpdateTime    ( AsDouble(data["timestamp"]) );

    SetDeltaX     ( AsDouble(data["dx"]));
    SetDeltaY     ( AsDouble(data["dy"]));
    SetDeltaZ     ( AsDouble(data["dz"]));
    SetDeltaYaw   ( AsDouble(data["dyaw"]));
    SetDeltaPitch ( AsDouble(data["dpitch"]));
    SetDeltaRoll  ( AsDouble(data["droll"]));

    SetX          ( GetX()     + GetDeltaX());
    SetY          ( GetY()     + GetDeltaY());
    SetZ          ( GetZ()     + GetDeltaZ());
    SetYaw        ( GetYaw()   + GetDeltaYaw());
    SetPitch      ( GetPitch() + GetDeltaPitch());
    SetRoll       ( GetRoll()  + GetDeltaRoll());

    SetFresh(true);
}

bool const MorseOdometry::IsItMe(const std::string &aName, const std::string &aRobotName)
{
    for(std::size_t i = 0; i < identifiers.size(); i++){
        if(aName.compare(aRobotName + "." + identifiers.at(i)) == 0)
            return true;
    }
    return false;
}

std::string const MorseOdometry::ToString() const
{
    std::string output(
        "\"Timestamp\": "   + NumberToString(GetDataTime()) +
        ", \"X\": "         + NumberToString(GetX()) +
        ", \"Y\": "         + NumberToString(GetY()) +
        ", \"Z\": "         + NumberToString(GetZ()) +
        ", \"Yaw\": "       + NumberToString(GetYaw()) +
        ", \"Pitch\": "     + NumberToString(GetPitch()) +
        ", \"Roll\": "      + NumberToString(GetRoll())
    );
    return output;
}


Pose2D_t const MorseOdometry::GetPose2D() const
{
    //ScopedLock_t lock( GetMutex() );
    Pose2D_t output = {GetX(), GetY(), GetYaw()};
    return output;
}

Pose3D_t const MorseOdometry::GetPose3D() const
{
    //ScopedLock_t lock( GetMutex() );
    Pose3D_t output = {GetX(), GetY(), GetZ(), GetYaw(), GetPitch(), GetRoll()};
    return output;
}

void MorseOdometry::SetX(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_X = aValue;
}

void MorseOdometry::SetY(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Y = aValue;
}

void MorseOdometry::SetZ(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Z = aValue;
}

void MorseOdometry::SetYaw(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Yaw = aValue;
}

void MorseOdometry::SetPitch(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Pitch = aValue;
}

void MorseOdometry::SetRoll(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_Roll = aValue;
}

void MorseOdometry::SetDeltaX(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_dX = aValue;
}

void MorseOdometry::SetDeltaY(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_dY = aValue;
}

void MorseOdometry::SetDeltaZ(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_dZ = aValue;
}

void MorseOdometry::SetDeltaYaw(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_dYaw = aValue;
}

void MorseOdometry::SetDeltaPitch(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_dPitch = aValue;
}

void MorseOdometry::SetDeltaRoll(double const &aValue)
{
    //ScopedLock_t lock( GetMutex() );
    m_dRoll = aValue;
}
