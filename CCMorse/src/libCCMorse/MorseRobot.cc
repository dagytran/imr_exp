#include "MorseRobot.h"
#include "MorseClient.h"
#include "MorseProxy.h"

#include <sys/ioctl.h>


using namespace CCMorse;


unsigned MorseRobot::s_IDCounter = 0;

MorseRobot::MorseRobot(std::string const aName)
{
    m_Name = aName;
    m_ID = s_IDCounter;
    s_IDCounter++;

    m_Goal.px     = 0;
    m_Goal.py     = 0;
    m_Goal.pa     = 0;
    m_Goal.status = idle;
}

MorseRobot::~MorseRobot()
{
    ScopedLock_t lock( GetMutex() );

    if(m_Goal.status != idle)
        m_DriverThread->interrupt();

    while(!m_Proxies.empty()){
        Proxy_t* proxy = m_Proxies.back();
        if( proxy->ptr != NULL ){
            delete (proxy->ptr);
        }
        m_Proxies.pop_back();
    }

    while(!m_Devices.empty()){
        MorseDevice* device = m_Devices.back();
        if( device != NULL ){
            delete device;
        }
    }

    delete m_Config;
}

void MorseRobot::SetConfiguration(MorseProperties* aConfig)
{
    m_Config = aConfig;

    m_MaxVelocity.px = m_Config->GetMaxSpeed();
    m_MaxVelocity.py = m_Config->GetMaxSpeed();
    m_MaxVelocity.pa = m_Config->GetMaxTurnRate();

    // Initialization of m_Proxies
    InitProxy(GetProvidesWindowIndexList(), "WindowProxy", pass);

    InitProxy(GetProvidesLaserIndexList(),             "LaserProxy", pass);
    InitProxy(GetDriverProvidesLaserIndexList(),       "LaserProxy", pass);
    InitProxy(GetDriverRequiresInputLaserIndexList(),  "LaserProxy", in);
    InitProxy(GetDriverRequiresOutputLaserIndexList(), "LaserProxy", out);

    InitProxy(GetProvidesPosition2dIndexList(),             "Position2dProxy", pass);
    InitProxy(GetDriverProvidesPosition2dIndexList(),       "Position2dProxy", pass);
    InitProxy(GetDriverRequiresInputPosition2dIndexList(),  "Position2dProxy", in);
    InitProxy(GetDriverRequiresOutputPosition2dIndexList(), "Position2dProxy", out);
}

void MorseRobot::InitProxy(std::vector<int> const &aIndexes, std::string const aType, ComDir_t aDirection)
{
    for(std::vector<int>::const_iterator id = aIndexes.begin(); id != aIndexes.end(); ++id){

        Proxy_t* exist = GetProxy( *id, aType );

        if( exist == NULL ){

//            std::cout << "MorseRobor::InitProxyInfo(): New proxy: Type: " << aType
//                << ", Index: " << NumberToString(*id)
//                << ", Direction: " << ((aDirection == 0) ? "in" : ((aDirection == 1) ? "out" : "pass"))
//            << std::endl;

            Proxy_t* proxy = new Proxy_t();

            proxy->type       = aType;
            proxy->index      = (*id);
            proxy->subscribed = false;
            proxy->ptr        = NULL;
            proxy->id         = -1;
            proxy->direction  = aDirection;

            m_Proxies.push_back( proxy );

        } else {

//            std::cout << "MorseRobor::InitProxyInfo(): Changing proxy: Type: " << aType
//                << ", Index: " << NumberToString(*id);

            if( exist->direction == pass )
                exist->direction = aDirection;

            else if( (exist->direction == in && aDirection == out) || (exist->direction == out && aDirection == in) )
                exist->direction = pass;

//            std::cout << ", Direction: " << ((exist->direction == 0) ? "in" : ((exist->direction == 1) ? "out" : "pass")) << std::endl;
        }
    }
}

void MorseRobot::SubscribeProxy(MorseProxy* const aProxy)
{
    Proxy_t info;

    info.ptr   = aProxy;
    info.type  = aProxy->GetName();
    info.id    = aProxy->GetID();
    info.index = aProxy->GetIndex();

    SubscribeProxy(info);
}

void MorseRobot::SubscribeProxy(Proxy_t const aInfo)
{
    Proxy_t* proxy = GetProxy(aInfo);

    if( proxy != NULL ){

        proxy->ptr        = aInfo.ptr;
        proxy->id         = aInfo.id;
        proxy->subscribed = true;

        //PrintProxiesStatus();

    } else {
        throw MorseError("MorseRobot::SubscribeProxy()", "Proxy subscription aborted because proxy was not configured.");
    }
}

void MorseRobot::UnsubscribeProxy(MorseProxy* const aProxy)
{
    Proxy_t* proxy = GetProxy(aProxy);
    if(proxy != NULL)
        proxy->subscribed = false;
}

bool const MorseRobot::IsPosition2dProxyGlobal(int aIndex) const
{
    std::vector<int> globals = GetProvidesGlobalPosition2dIndexList();

    for(std::vector<int>::iterator i = globals.begin(); i != globals.end(); ++i)
        if( (*i) == aIndex )
            return true;

    return false;
}

Proxy_t* MorseRobot::GetProxy(int const aIndex, std::string const &aType) const
{
    ScopedLock_t lock( GetMutex() );
    for(std::vector<Proxy_t*>::const_iterator proxy = m_Proxies.begin(); proxy!= m_Proxies.end(); ++proxy)
        if( (aType.compare((*proxy)->type) == 0) && (aIndex == (*proxy)->index) )
            return (*proxy);

    return NULL;
}

Proxy_t* MorseRobot::GetProxy(MorseProxy* const aProxy) const
{
    ScopedLock_t lock( GetMutex() );
    for(std::vector<Proxy_t*>::const_iterator proxy = m_Proxies.begin(); proxy != m_Proxies.end(); ++proxy)
        if( aProxy == (*proxy)->ptr )
            return (*proxy);

    return NULL;
}

Proxy_t* MorseRobot::GetProxy(Proxy_t const aInfo) const
{
    return GetProxy(aInfo.index, aInfo.type);
}

MorseProxy* MorseRobot::GetSubscribedProxy(int const aIndex, std::string const &aType, ComDir_t const aKey) const
{
    Proxy_t* proxy = GetProxy(aIndex, aType);

    if(proxy != NULL && proxy->subscribed && proxy->direction == aKey)
        return (proxy->ptr);
    else
        return NULL;
}

std::vector<MorseProxy*> const MorseRobot::GetRequiredSubscribedProxy(std::string const &aType, ComDir_t const aKey) const
{
    std::vector<MorseProxy*> output;

    if( !IsAllRequiredProxiesSubscribed() )
        throw MorseError("MorseRobot::GetRequiredSubscribedProxy", "Not all driver required proxies are subscribed, so not evet trying finding them.");

    MorseProxy* proxy;
    std::vector<int>  indexes;

    if(aType.compare("LaserProxy") == 0){
        indexes.insert(indexes.begin(), GetDriverRequiresInputLaserIndexList().begin(), GetDriverRequiresInputLaserIndexList().end());
        indexes.insert(indexes.begin(), GetDriverRequiresOutputLaserIndexList().begin(), GetDriverRequiresOutputLaserIndexList().end());
    }

    if(aType.compare("Position2dProxy") == 0){
        indexes.insert(indexes.begin(), GetDriverRequiresInputPosition2dIndexList().begin(), GetDriverRequiresInputPosition2dIndexList().end());
        indexes.insert(indexes.begin(), GetDriverRequiresOutputPosition2dIndexList().begin(), GetDriverRequiresOutputPosition2dIndexList().end());
    }

    for(std::vector<int>::iterator i = indexes.begin(); i != indexes.end(); ++i){

        proxy = GetSubscribedProxy( (*i), aType, aKey );

        if( proxy == NULL ) // if proxy is NULL try passthrought proxy
            proxy = GetSubscribedProxy( (*i), aType, pass );

        if( proxy != NULL )
            output.push_back(proxy);
    }

    if(output.size() == 0)
        throw MorseError("MorseRobot::GetRequiresSubscribedProxy", "Proxy was not found.");

    return output;
}

std::vector<MorseProxy*> const MorseRobot::GetProxies() const
{
    //ScopedLock_t lock( GetMutex() );
    std::vector<MorseProxy*> output;

    for(std::vector<Proxy_t*>::const_iterator proxy = m_Proxies.begin(); proxy != m_Proxies.end(); ++proxy)
        if( (*proxy)->ptr != NULL )
            output.push_back( (*proxy)->ptr );

    return output;
}

std::vector<MorseProxy*> const MorseRobot::GetSubscribedProxies() const
{
    //ScopedLock_t lock( GetMutex() );
    std::vector<MorseProxy*> output;

    for(std::vector<Proxy_t*>::const_iterator proxy = m_Proxies.begin(); proxy != m_Proxies.end(); ++proxy)
        if( (*proxy)->ptr != NULL && (*proxy)->subscribed == true )
            output.push_back( (*proxy)->ptr );

    return output;
}

std::vector<Proxy_t*> const MorseRobot::GetSubscribedProxiesInfo() const
{
    //ScopedLock_t lock( GetMutex() );
    std::vector<Proxy_t*> output;

    for(std::vector<Proxy_t*>::const_iterator proxy = m_Proxies.begin(); proxy != m_Proxies.end(); ++proxy)
        if( (*proxy)->ptr != NULL && (*proxy)->subscribed == true )
            output.push_back( *proxy );

    return output;
}

bool const MorseRobot::IsAllRequiredProxiesSubscribed() const
{
    MorseProxy* test = NULL;
    std::vector<int> passList;

    for(std::vector<int>::const_iterator id = GetDriverRequiresInputLaserIndexList().begin(); id != GetDriverRequiresInputLaserIndexList().end(); ++id){
        test = GetSubscribedProxy((*id), "LaserProxy", in);
        if( test == NULL )
            passList.push_back(*id);
    }

    for(std::vector<int>::const_iterator id = GetDriverRequiresOutputLaserIndexList().begin(); id != GetDriverRequiresOutputLaserIndexList().end(); ++id){
        test = GetSubscribedProxy((*id), "LaserProxy", out);
        if( test == NULL )
            passList.push_back(*id);
    }

    for(std::vector<int>::iterator id = passList.begin(); id != passList.end(); ++id){
        test = GetSubscribedProxy((*id), "LaserProxy", pass);
        if( test == NULL )
            return false;
    }

    passList.clear();
    for(std::vector<int>::const_iterator id = GetDriverRequiresInputPosition2dIndexList().begin(); id != GetDriverRequiresInputPosition2dIndexList().end(); ++id){
        test = GetSubscribedProxy((*id), "Position2dProxy", in);
        if( test == NULL )
            passList.push_back(*id);
    }

    for(std::vector<int>::const_iterator id = GetDriverRequiresOutputPosition2dIndexList().begin(); id != GetDriverRequiresOutputPosition2dIndexList().end(); ++id){
        test = GetSubscribedProxy((*id), "Position2dProxy", out);
        if( test == NULL )
            passList.push_back(*id);
    }

    for(std::vector<int>::iterator id = passList.begin(); id != passList.end(); ++id){
        test = GetSubscribedProxy((*id), "Position2dProxy", pass);
        if( test == NULL )
            return false;
    }

    return true;
}

void MorseRobot::EraseProxy(MorseProxy* aProxy)
{
    //ScopedLock_t lock( GetMutex() );
    std::vector<Proxy_t*>::iterator p = m_Proxies.end();

    for(std::vector<Proxy_t*>::iterator proxy = m_Proxies.begin(); proxy != m_Proxies.end(); ++proxy)
        if( aProxy == (*proxy)->ptr )
            p = proxy;

    if(p != m_Proxies.end())
        m_Proxies.erase( p );
}

MorseDevice* MorseRobot::GetDevice(std::string const &aName) const
{
    //ScopedLock_t lock( GetMutex() );
    for(std::vector<MorseDevice*>::const_iterator device = m_Devices.begin(); device != m_Devices.end(); ++device)
        if( aName.compare( (*device)->GetName() ) == 0 )
            return (*device);

    throw MorseError("MorseRobot::GetDevice()", "Device with name" + aName + "was not found.");
    return ( new MorseDevice() );
}

MorseDevice* MorseRobot::GetDevice(int aID) const
{
    //ScopedLock_t lock( GetMutex() );
    for(std::vector<MorseDevice*>::const_iterator device = m_Devices.begin(); device != m_Devices.end(); ++device)
        if( aID == (*device)->GetID() )
            return (*device);

    throw MorseError("MorseRobot::GetDevice()", "Device with id" + NumberToString(aID) + "was not found.");
    return ( new MorseDevice() );
}

void MorseRobot::RefreshDevices()
{
    ScopedLock_t lock( GetMutex() );

    for(std::vector<MorseDevice*>::const_iterator device = m_Devices.begin(); device != m_Devices.end(); ++device)
        (*device)->RefreshData();

    for(std::vector<Proxy_t*>::iterator proxy = m_Proxies.begin(); proxy != m_Proxies.end(); ++proxy)
        if((*proxy)->subscribed)
            (*proxy)->ptr->CheckDataValidity();
        else
            (*proxy)->ptr = NULL;
}

void MorseRobot::EraseDevice(std::string const &aName)
{
    //ScopedLock_t lock( GetMutex() );
    std::vector<MorseDevice*>::iterator d = m_Devices.end();

    for(std::vector<MorseDevice*>::iterator device = m_Devices.begin(); device != m_Devices.end(); ++device)
        if( (*device)->GetName().compare(aName) == 0 )
            d = device;

    if(d != m_Devices.end())
        m_Devices.erase( d );
}

void MorseRobot::EraseDevice(MorseDevice* aDevice)
{
    //ScopedLock_t lock( GetMutex() );
    std::vector<MorseDevice*>::iterator d = m_Devices.end();

    for(std::vector<MorseDevice*>::iterator device = m_Devices.begin(); device != m_Devices.end(); ++device)
        if( (*device) == aDevice )
            d = device;

    if(d != m_Devices.end())
        m_Devices.erase( d );
}

void MorseRobot::GoTo(Pose2D_t aPos, Pose2D_t aVel)
{
    if( IsGoalReached() ){
        m_DriverThread->join();
        SetGoalIdle();

    } else if( IsGoalIdle() ){

        MorseDriver* driver = MorseClient::GetInstance()->GetDriverForRobot(this);

        if( driver != NULL ){
            SetGoalPos(aPos);
            SetMaxVelocity(aVel);

            m_DriverThread = new boost::thread( &MorseDriver::GoTo, driver );

        } else {
            throw MorseError("MorseRobot::GoTo()", "Client returned null pointer to driver.");
        }

    } else {
        SetGoalPos(aPos);
        SetMaxVelocity(aVel);
    }
}

void MorseRobot::GoTo(Pose2D_t aPos)
{
    GoTo(aPos, GetMaxVelocity());
}

void MorseRobot::GoTo(double aX, double aY, double aYaw)
{
    Pose2D_t pos = {aX, aY, aYaw};
    GoTo(pos, GetMaxVelocity());
}

void MorseRobot::GoTo(double aX, double aY, double aYaw, double vX, double vY, double vYaw)
{
    Pose2D_t pos = {aX, aY, aYaw};
    Pose2D_t vel = {vX, vY, vYaw};
    GoTo(pos, vel);
}

void MorseRobot::GoTo()
{
    GoTo(GetGoalPos(), GetMaxVelocity());
}


void MorseRobot::SetGoalPos(double aX, double aY, double aYaw)
{
    //ScopedLock_t lock( GetMutex() );
    m_Goal.px     = aX;
    m_Goal.py     = aY;
    m_Goal.pa     = aYaw;
    m_Goal.status = transit;
}

void MorseRobot::SetTransitToGoal()
{
    //ScopedLock_t lock( GetMutex() );
    m_Goal.status = transit;
}

void MorseRobot::SetGoalReached()
{
    //ScopedLock_t lock( GetMutex() );
    m_Goal.status = arrived;
}

void MorseRobot::SetGoalIdle()
{
    //ScopedLock_t lock( GetMutex() );
    m_Goal.status = idle;
}

void MorseRobot::SetMaxVelocity(Pose2D_t const aVel)
{
    //ScopedLock_t lock( GetMutex() );
    m_MaxVelocity.px = Clamp(std::fabs(aVel.px), 0.01, m_Config->GetMaxSpeed());
    m_MaxVelocity.py = 0;
    m_MaxVelocity.pa = Clamp(std::fabs(aVel.pa), 0.01, m_Config->GetMaxTurnRate());
}

void MorseRobot::PrintRobotSummary() const
{
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    int width = w.ws_col - 1;

    std::cout << "+" << std::setfill('-') << std::setw(width) << "-" << std::endl << std::setfill(' ');
    std::cout << "| ROBOT: " << GetName() << std::endl;
    std::cout << "+" << std::setfill('-') << std::setw(width) << "-" << std::endl << std::setfill(' ');
    std::cout << std::setprecision(6);
    std::cout << "| Dimensions [w]x[l]:  " << GetRobotWidth() << " x " <<  GetRobotLength() << std::endl;
    std::cout << "| Robot radius [R]:    " << GetRobotRadius() << std::endl;
    std::cout << "| Color:               " << GetRobotColor() << std::endl;
    std::cout << "| Laser level:         " << (IsLaserRssi() ? "RSSI" : "RAW") << std::endl;
    std::cout << "| Mass:                " << GetRobotMass() << std::endl;
    std::cout << "| Max speed [V]x[W]:   " << GetMaxSpeed() << " x " << GetMaxTurnRate() << std::endl;
    std::cout << "| Number of devices:   " << ((unsigned) GetDevices().size()) << std::endl;
    std::cout << "| Providing:           ";

    std::vector<int> idList = GetProvidesPosition2dIndexList();
    for(std::vector<int>::iterator it = idList.begin(); it != idList.end(); it++)
        std::cout << "position2d:" << (*it) << ", ";

    idList.clear();
    idList = GetProvidesLaserIndexList();
    for(std::vector<int>::iterator it = idList.begin(); it != idList.end(); it++)
        std::cout << "laser:" << (*it) << ", ";

    std::cout << std::endl;

    std::cout << "| Driver:              " << std::endl;
    std::cout << "| - Name:              " << GetDriverName() << std::endl;
    std::cout << "| - Providing:         ";

    idList.clear();
    idList = GetDriverProvidesPosition2dIndexList();
    for(std::vector<int>::iterator it = idList.begin(); it != idList.end(); it++)
        std::cout << "position2d:" << (*it) << ", ";

    idList.clear();
    idList = GetDriverProvidesLaserIndexList();
    for(std::vector<int>::iterator it = idList.begin(); it != idList.end(); it++)
        std::cout << "laser:" << (*it) << ", ";;

    std::cout << std::endl
              << "| - Requires inputs:   ";

    idList.clear();
    idList = GetDriverRequiresInputPosition2dIndexList();
    for(std::vector<int>::iterator it = idList.begin(); it != idList.end(); it++)
        std::cout << "position2d:" << (*it) << ", ";

    idList.clear();
    idList = GetDriverRequiresInputLaserIndexList();
    for(std::vector<int>::iterator it = idList.begin(); it != idList.end(); it++)
        std::cout << "laser:" << (*it) << ", ";

    std::cout << std::endl
              << "| - Requires outputs:  ";

    idList.clear();
    idList = GetDriverRequiresOutputPosition2dIndexList();
    for(std::vector<int>::iterator it = idList.begin(); it != idList.end(); it++)
        std::cout << "position2d:" << (*it) << ", ";

    idList.clear();
    idList = GetDriverRequiresOutputLaserIndexList();
    for(std::vector<int>::iterator it = idList.begin(); it != idList.end(); it++)
        std::cout << "laser:" << (*it) << ", ";

    std::cout << std::endl
              << "+" << std::setfill('-') << std::setw(width) << "-" << std::endl << std::setfill(' ');

    std::cout.unsetf(std::ios_base::floatfield);
    std::cout << std::setprecision(16);
}

std::string const MorseRobot::GetProxySummary(Proxy_t const aInfo) const
{
    return (
        (aInfo.direction == in ? "input:" : (aInfo.direction == out ? "output:" : "passthrough:"))
        + aInfo.type + ":" + NumberToString(aInfo.index)
        + (aInfo.subscribed ? "" : ":NOT_subscribed")
    );

}

void MorseRobot::PrintProxiesStatus() const
{
    for(std::vector<Proxy_t*>::const_iterator proxy = GetProxiesInfo().begin(); proxy != GetProxiesInfo().end(); ++proxy)
        std::cout << GetProxySummary( **proxy ) << std::endl;
}
