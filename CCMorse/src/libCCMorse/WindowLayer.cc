#include "WindowLayer.h"

using namespace CCMorse;

WindowLayer::WindowLayer(std::string const aName, bool const aVisible, int const aPriority, int const aAlpha, double const aTransparent, RGBA_t const aColor, imr::CMapGrid* const aMap)
{
    mName        = aName;
    mVisible     = aVisible;
    mPriority    = aPriority;
    mAlpha       = Clamp(aAlpha, 0, 255);
    mTransparent = Clamp(aTransparent, 0.0, 1.0);
    mColor       = aColor;
    mMap         = aMap;
}
