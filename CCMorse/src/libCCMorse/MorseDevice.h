/**
 * @author Lukáš Bertl <mailto:bertlluk@fel.cvut.cz>
 * @date   2016-11-14
 *
 * File contains definition of MorseDevice class.
 */

#ifndef CCMORSE_MORSEDEVICE_H
#define CCMORSE_MORSEDEVICE_H

#include "MorseUtility.h"
#include "MorseError.h"


namespace CCMorse {


/**
 * MorseDevice class description.
 * This is base class for every device class.
 */
class MorseDevice
{
    friend class MorseClient;
    friend class MorseProxy;

    public:

        /**
         * Default constructor of device.
         */
        MorseDevice();

        /**
         * Constructor.
         *
         * @param aPort Port number of the device data stream.
         * @param aName Name of the device.
         */
        MorseDevice(int const aPort, std::string const aName);

        /**
         * Copy Constructor.
         */
        MorseDevice(MorseDevice const &aDevice);

        /**
         * Destructor.
         */
        virtual ~MorseDevice();

        /**
         * Returns port number of the devices data stream.
         * @return Port number.
         */
        int const GetPort() const { return GetVar(m_Port); }

        /**
         * Returns name of the device.
         * @return Device name.
         */
        std::string const GetName() const { return GetVar(m_Name); }

        /**
         * Returns generated identification number of the device.
         * @return ID number.
         */
        int const GetID() const { return GetVar(m_ID); }

        /**
         * Returns @c "1" if the device is subscribed.
         * @return Subscribtion status. [0 = not subscribed, 1 = subscribed]
         */
        int const IsSubscribed() const { return GetVar(m_Subscribed); }

        /**
         * Sets subscribtion status of the device.
         * @param aSubscribed Value of subscribtion status. [0 = not subscribed, 1 = subscribed]
         */
        void SetSubscribed(int aSubscribed);

        /**
         * Device data freshness flag.
         * @return [0 = data is not fresh, 1 = data is fresh]
         */
        int const IsFresh() const { return GetVar(m_Fresh); }

        /**
         * Device geometry freshness flag.
         * @return [0 = geometry is not fresh, 1 = geometry is fresh]
         */
        int const IsFreshGeom() const { return GetVar(m_FreshGeom); }

        /**
         * Device configuration freshness flag.
         * @return [0 = configuration is not fresh, 1 = configuration is fresh]
         */
        int const IsFreshConfig() const { return GetVar(m_FreshConfig); }

        /**
         * Returns pointer to user data.
         */
        void *GetUserData() const { return GetVar(m_UserData); }

        /**
         * Sets pointer to user data.
         */
        void SetUserData(void *val);

        /**
         * Returns simulation time of the new data.
         * @return Data timestamp.
         */
        double const GetDataTime() const { return GetVar(m_DataTime); }

        /**
         * Returns simulation time of the data right before new data.
         * @return Last data timestamp.
         */
        double const GetLastTime() const { return GetVar(m_LastTime); }

        /**
         * Returns position of the device to the robot.
         */
        Pose3D_t const GetGeoPose() const;

        /**
         * Returns physical size of the device.
         */
        BBox3D_t const GetSizeExtent() const;

        /**
         * Returns configuration dictionary of the device.
         * Meanings of variables in configuration are described in data
         * which returns the @ref MorseDevice::GetProperties() function.
         */
        Dictionary_t const GetConfigurations() const;

        /**
         * Returns properties dictionary of the device. Properties consist
         * of configuration variable name, variable type, default variable value,
         * and configured variable value.
         */
        Dictionary_t const GetProperties() const;

        /**
         * Returns string representation of the device.
         */
        std::string const ToString() const;

        /**
         * Function that obtains of sends fresh data when available.
         */
        virtual void RefreshData();

    protected:

        /**
         * Function that sends message to the Morse simulation thru socket.
         *
         * @param aMessage  Command or query for Morse simulation.
         * @return  Answer to the message from Morse simulation.
         */
        std::string const CommunicateWithSim(std::string const &aMessage) const;

        /**
         * Function that initialize time variables of the device.
         */
        void InitTime();

        /**
         * Upadates data times of the device.
         */
        void UpdateTime();

        /**
         * Updates data time of the device with timestamp provided by sensor.
         * This function is faster than UpdateTime(), because it don't have to query
         * simulation for time.
         */
        void UpdateTime(double aTimestamp);

        /**
         * Sets data freshness flag.
         * @param val Data status. [0 = data is not fresh, 1 = data is fresh]
         */
        void SetFresh(int val);

        /**
         * Sets data freshness flag.
         * @param val Freshness status. [false = data is not fresh, true = data is fresh]
         */
        void SetFresh(bool val) { if(val == false) SetFresh(0); else SetFresh(1); }

        /**
         * Sets geometry freshness flag.
         * @param val Geometry status. [0 = geometry is not fresh, 1 = geometry is fresh]
         */
        void SetFreshGeom(int val);

        /**
         * Sets geometry freshness flag.
         * @param val Geometry status. [false = geometry is not fresh, true = geometry is fresh]
         */
        void SetFreshGeom(bool val) { if(val == false) SetFreshGeom(0); else SetFreshGeom(1); }

        /**
         * Sets configuration freshness flag.
         * @param val Configuration status. [0 = configuration is not fresh, 1 = configuration is fresh]
         */
        void SetFreshConfig(int val);

        /**
         * Sets configuration freshness flag.
         * @param val Configuration status. [false = configuration is not fresh, true = configuration is fresh]
         */
        void SetFreshConfig(bool val) { if(val == false) SetFreshConfig(0); else SetFreshConfig(1); }

        /**
         * Sets position of the device to the robot.
         * @param aGeoPose Position vector. [x, y, z]
         */
        void SetGeoPose(const std::vector<double> &aGeoPose);

        /**
         * Sets rotation of the device to the robot.
         * @param aRotation 3x3 matrix of rotation.
         */
        void SetRotation(const std::vector<Pose2D_t> &aRotation);

        /**
         * Sets physical size of the device.
         * @param aSize Device box size.
         */
        void SetSizeExtent(BBox3D_t aSize);

        /**
         * Sets physical size of the device.
         * @param aLength   Length of the device. [m]
         * @param aWidth    Width of the device. [m]
         * @param aHeight   Height of the device. [m]
         */
        void SetSizeExtent(double aLength, double aWidth, double aHeight);


        /**
         * Returns mutex of the device for synchronization.
         */
        Mutex_t &GetMutex() const { return m_Mutex; }

        /**
         * Template function for thread safe retrival of variable.
         * @warning Currently do NOTHING.
         */
        template<typename T>
        T GetVar(const T &aV) const { return aV; }//CCMorse::GetVar(aV, this->GetMutex()); }

    private:
        static int  s_IDCounter;    ///< Identification number counter.

        int m_Port;             ///< Devices data stream port number.

        std::string m_Name;     ///< Name of the device.

        std::vector<double> m_GeoPose;      ///< Device geometry in the robot cs: pose gives the position (x[m], y[m], z[m]).
        std::vector<double> m_SizeExtent;   ///< Size gives the extent (length[m], width[m], height[m]).
        std::vector<Pose2D_t> m_Rotation;   ///< Device rotation to robot (rx[rad], ry[rad], rz[rad]).

        void *m_UserData;       ///< Extra user data for this device.

        mutable Mutex_t m_Mutex;    ///< Mutex for synchronization.

        int m_Subscribed,       ///< The subscribe flag is non-zero if the device has been successfully subscribed (read-only).
            m_Fresh,            ///< Data freshness flag.
            m_FreshGeom,        ///< Geometry freshness flag.
            m_FreshConfig,      ///< Configuration freshness flag.
            m_ID;               ///< A ID of the device.

        double 	m_DataTime,     ///< Data timestamp, i.e., the time at which the data was generated (s).
                m_LastTime;     ///< Data timestamp from the previous data.

};
} // namespace CCMorse;

#endif // CCMORSE_MORSEDEVICE_H
