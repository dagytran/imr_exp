/*
 * File name: planner_robot.cc
 * Date:      2016-09-26
 * Author:    Lukáš Bertl
 * Derived from: robot.cc (Jan Faigl, Miroslav Kulich)
 */

#include "planner_robot.h"

#include "robot_types.h"

#include <unistd.h> //usleep
#include <fstream>
#include <cmath>

#include "libCCMorse/CCMorse.h"

#include "imr-h/logging.h"
#include "imr-h/imr_exceptions.h"
#include "imr-h/orloc.h"

using namespace imr::robot;
using namespace CCMorse;

typedef unsigned char uchar;


/// ----------------------------------------------------------------------------
/// Class CPannerRobot

/// - static method ------------------------------------------------------------
imr::CConfig& CPlannerRobot::getConfig(imr::CConfig& config) {
	config.add<std::string>("host", "morse server hostname", "localhost");
	config.add<int>("port", "morse server port", 4000);
	CMapGrid::getConfig(config);
	return config;
}

/// ----------------------------------------------------------------------------
CPlannerRobot::CPlannerRobot(imr::CConfig& cfg, CMapGrid &map) : ThreadBase(), cfg(cfg), client(0), quit(false), map(map)
{
    client = new PlayerClient(cfg.get<std::string>("host"), cfg.get<int>("port"));
}

/// ----------------------------------------------------------------------------
CPlannerRobot::~CPlannerRobot() {
   stop();
   join();
   if (client) {
      delete client;
      delete &CCMorse::WindowProxy::GetInstance();
   }
}


/// ----------------------------------------------------------------------------
void CPlannerRobot::stop(void) {
   ScopedLock lk(mtx);
   quit = true;
}

/// - protected method ---------------------------------------------------------
void CPlannerRobot::threadBody(void) {
    try {
        navigation();
    } catch (CCMorse::MorseError& e) {
        ERROR("Morse error: " << e.GetErrorStr() << " function: " << e.GetErrorFun());
    } catch (imr::exception& e) {
        ERROR("Imr error: " << e.what());
        exit(-1);
    }
}


void CPlannerRobot::setPlan(std::vector<int> newPlan) {
    path_plan = newPlan;
    plan_index = path_plan.size() - 2;  // set plan pointer to node before the last one (one after start)
}

/// - protected method ---------------------------------------------------------
void CPlannerRobot::navigation(void)  {
    bool q = false;

    //CWindow& window = CWindow::getInstance();
    WindowProxy& window = WindowProxy::GetInstance();
    DEBUG("Navigation started.");
    window.DrawMap(map);
    window.Flush();

    do {
        client->Read();
        while(client->Peek(0)) {
            client->Read();
        }

        ScopedLock lk(mtx); //check quit request
        q |= quit;
    } while (!q);

    INFO("Main loop has been left");
}

/* end of robot.cc */
