import logging; logger = logging.getLogger("morse." + __name__)
import morse.core.robot

class Dummy(morse.core.robot.Robot):

    _name = 'dummy robot'

    def __init__(self, obj, parent=None):
        logger.info('%s initialization' % obj.name)
        morse.core.robot.Robot.__init__(self, obj, parent)
        logger.info('Component initialized')

    def default_action(self):
        pass
