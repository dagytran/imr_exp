import logging; logger = logging.getLogger("morse." + __name__)
import morse.core.wheeled_robot
import morse.core.robot

#class Syrotek(morse.core.wheeled_robot.MorsePhysicsRobot):
class Syrotek(morse.core.robot.Robot):
    """ 
    Class definition for the syrotek robot.
    """

    _name = 'syrotek robot'

    def __init__(self, obj, parent=None):

        logger.info('%s initialization' % obj.name)
        #morse.core.wheeled_robot.MorsePhysicsRobot.__init__(self, obj, parent)
        morse.core.robot.Robot.__init__(self, obj, parent)
        logger.info('Component initialized')

    def default_action(self):
        pass
