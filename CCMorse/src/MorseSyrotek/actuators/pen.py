import logging; logger = logging.getLogger("morse." + __name__)

import morse.core.actuator

from morse.core import status
from morse.core.services import service, async_service, interruptible
from morse.core.exceptions import MorseRPCInvokationError
from morse.helpers.components import add_data, add_property

import morse.core.blenderapi as blender
from morse.core.blenderapi import bpy
from morse.core.blenderapi import persistantstorage as logic
from morse.core.blenderapi import render, texture, keyboard, input_active

from PIL import Image

from mathutils import Vector, Matrix

import math, os, json, array

####################################
# 	Pen actuator for MorseSyrotek  #
####################################

class Pen(morse.core.actuator.Actuator):

	_name = "Pen"
	_short_desc = "Pen is used to draw map on Canvas."

	add_property('CanvasFolder',         None,  'CanvasFolder',          "str",   "Path to folder where canvas is.")
	add_property('Width',                32,    'CanvasWidth',           "int",   "Resolution width of showed image on canvas.")
	add_property('Height',               32,    'CanvasHeight',          "int",   "Resolution height of showed image on canvas.")
	add_property('FullscreenMode',       False, 'FullscreenMode',        "bool",  "Sets fullscreen map mode.")
	add_property('CellSize',             0.107, 'CellSize',              "float", "Sets size of an pixel in real world.")
	add_property('GroundPosEnabled',     False, 'GroundPositionEnabled', "bool",  "Sets if map can be projected to arena ground.")
	add_property('HideObstaclesEnabled', False, 'HideObstaclesEnabled',  "bool",  "Sets if obstacles can be hidden.")
	add_property('InitPositionOnGround', True,  'InitPositionOnGround',  "bool",  "Sets if the initial position of canvas is on ground")
	
	def __init__(self, obj, parent=None):
		logger.info("%s initialization" % obj.name)
		morse.core.actuator.Actuator.__init__(self, obj, parent)	# Call the constructor of the parent class


		self.Size = (self.Width, self.Height)	# tuple holding size of the canvas
		self.ObstaclesState = 2					# dynamic obstacles are hidden in default
		self.KeyPressedLast = [0, 0]			# counters for key delay

		self.Zoom    = [self.Width, self.Height]
		self.ZoomPos = [0, 0]
		
		if self.InitPositionOnGround == False:	# initial state of the canvas
			self.CanvasPosState = 0				# "Minimap" in the corner
		else:									# or
			self.CanvasPosState = 1				# canvas is projected on arena floor

		self.Scene  = blender.scene()

		if self.CanvasFolder is not None:
			self.CanvasObj  = blender.objectdata("canvas")
			self.CanvasMat  = self.CanvasObj.data.materials["CanvasMat"]
			self.CanvasTex  = self.CanvasMat.texture_slots[0].texture
			self.CanvasInit = self.CanvasTex.image
	
			self.Image = initImageArray(self.Width, self.Height)
			#self.Image = generateImageArray(self.Width, self.Height) # Some colors just for fun

			self.Refresh = True
			
			self.Canvas = self.Scene.objects["canvas"]
			self.Cam    = self.Scene.active_camera
	
			self.wWidth  = render().getWindowWidth()
			self.wHeight = render().getWindowHeight()
	
			self.setCanvasPositionAndScale()
			self.showObstacles()
			self.createTexture()

		logger.info('Component initialized')

	###################################
	# Default action
	def default_action(self):

		if keyboard().events[blender.LEFTALTKEY] == input_active() and keyboard().events[blender.OKEY] == input_active():
			self.setCanvasPositionAndScale()

		if self.CanvasFolder is not None:
			self.refresh()

		if self.FullscreenMode == True:
			self.zoomConstraint()

		if self.GroundPosEnabled == True:
			if keyboard().events[blender.LEFTALTKEY] == input_active() and keyboard().events[blender.DOWNARROWKEY] == input_active():
				self.CanvasPosState = 1
				self.setCanvasPositionAndScale()
	
			if keyboard().events[blender.LEFTALTKEY] == input_active() and keyboard().events[blender.UPARROWKEY] == input_active():
				self.CanvasPosState = 0
				self.setCanvasPositionAndScale()


		if self.HideObstaclesEnabled == True:
			if keyboard().events[blender.LEFTALTKEY] == input_active() and keyboard().events[blender.LEFTARROWKEY] == input_active():
				if self.KeyPressedLast[0] < 3:
					self.KeyPressedLast = [(self.KeyPressedLast[0] + 1), 0]
				else:
					self.ObstaclesState = self.ObstaclesState - 1
					self.showObstacles()
					self.KeyPressedLast = [0, 0]
	
			if keyboard().events[blender.LEFTALTKEY] == input_active() and keyboard().events[blender.RIGHTARROWKEY] == input_active():
				if self.KeyPressedLast[1] < 3:
					self.KeyPressedLast = [0, (self.KeyPressedLast[1] + 1)]
				else:
					self.ObstaclesState = self.ObstaclesState + 1
					self.showObstacles()
					self.KeyPressedLast = [0, 0]

	###################################
	# Methods availible in 'socket' interface
	@service
	def set_pixel(self, x, y, r, g, b, a = 255):
		if (x < 0 or y < 0 or x >= self.Width or y >= self.Height):
			raise MorseRPCInvokationError("X or Y out of bounds! <" + str(self.Width) + ", " + str(self.Height) + ">")

		# If a color is transparent no action needed
		if a <= 0:
			return

		# Calculate index of pixel in our image array
		k = pixelIndex(x, y, self.Width, self.Height)
		c = (clamp(r, 0, 255), clamp(g, 0, 255), clamp(b, 0, 255), clamp(a, 0, 255))
		self.Refresh = True

		# If a color is opaque just aply it to pixel
		if a >= 255:
			for i in range(4):
				self.Image[k+i] = c[i]

		# If a color is partialy transparent we need to calculate new color
		else:
			src = ((c[0]/255), (c[1]/255), (c[2]/255), (c[3]/255))
			dst = self.get_pixel(x, y)
			dst = ((dst[0]/255), (dst[1]/255), (dst[2]/255), (dst[3]/255))

			out = [0.0, 0.0, 0.0, 0.0]
			out[3] = (src[3] + dst[3]*(1 - src[3]))
			for i in range(3):
				out[i] = ( (src[i]*src[3] + dst[i]*dst[3]*(1 - src[3])) / out[3] )
			
			for i in range(4):
				self.Image[k+i] = toByte(out[i])

	@service
	def set_pixel_code(self, x, y, rgbcode):
		try:
			c = Image.getrgb(rgbcode)
		except ValueError:
			raise MorseRPCInvokationError("Not valid RGB code!")
			
		self.set_pixel(x, y, c[0], c[1], c[2], 255)

	@service
	def get_pixel(self, x, y):
		if (x < 0 or y < 0 or x >= self.Width or y >= self.Height):
			raise MorseRPCInvokationError("X or Y out of bounds! <" + str(self.Width) + ", " + str(self.Height) + ">")

		pixel = []
		k = pixelIndex(x, y, self.Width, self.Height)

		for i in range(4):
			pixel.append(self.Image[k+i])
			
		return pixel

	@service
	def set_line(self, x0, y0, x1, y1, r, g, b, a = 255):
		dx = (x1 - x0)
		dy = (y1 - y0)
		steep = (abs(dy) >= abs(dx))

		if steep:
			swap(x0, y0)
			swap(x1, y1)
			# recompute Dx, Dy after swap
			dx = (x1 - x0)
			dy = (y1 - y0)

		xstep = 1
		if dx < 0:
			xstep = -1
			dx = -dx

		ystep = 1
		if dy < 0:
			ystep = -1
			dy = -dy

		twoDy = 2 * dy
		twoDyTwoDx = twoDy - 2 * dx	# 2*Dy - 2*Dx
		e = twoDy - dx 				# 2*Dy - Dx

		y = y0
		for x in range(x0, x1, xstep): #(int x = x0; x != x1; x += xstep) {
			if steep:
				xDraw = y
				yDraw = x
			else:
				xDraw = x
				yDraw = y

			self.set_pixel(xDraw, yDraw, r, g, b, a)

			# next
			if e > 0:
				e += twoDyTwoDx	# E += 2*Dy - 2*Dx;
				y = y + ystep
			else:
				e += twoDy 		# E += 2*Dy;


	@service
	def resize_canvas(self, width, height):
		if(width > 0) and (height > 0) and ( (width != self.Width) or (height != self.Height) ):
			oldWidth = self.Width
			oldHeight = self.Height
			
			self.Width  = width
			self.Height = height
			self.Size   = (self.Width, self.Height)
	
			resizedImage = initImageArray(self.Width, self.Height)
		
			for y in range( min(self.Height, oldHeight) ):
				for x in range( min(self.Width, oldWidth) ):
					for i in range(4):
						k = (pixelIndex(x, y, oldWidth, oldHeight) + i)
						j = (pixelIndex(x, y, self.Width, self.Height) + i)
						resizedImage[j] = self.Image[k]
		
			self.Image = resizedImage
			self.setCanvasPositionAndScale()
			self.resizeTexture()
			self.Refresh = True

	@service
	def get_cell_size(self):
		try:
			self.CellSize = (blender.objectdata("ground").dimensions.x / self.Width) 
		except KeyError:
			pass

		return self.CellSize

	@service
	def get_image_data(self):
		output = [[self.get_pixel(x, y) for x in range(self.Width)] for y in range(self.Height)]
		return output

	@service
	def set_image_data(self, image):
		with open(os.path.join(self.CanvasFolder, image)) as image_file:
			imageArray = json.load(image_file)

		for y in range(self.Height):
			for x in range(self.Width):
				for i in range(4):
					k = pixelIndex(x, y, self.Width, self.Height)
					self.Image[k+i] = imageArray[x][y][i]

		os.remove(os.path.join(self.CanvasFolder, image))
		self.Refresh = True

	@service
	def update_image_data(self, pixels):
		pixelArray = json.loads(pixels)
		for i in pixelArray:
			k = pixelIndex(i[0], i[1], self.Width, self.Height)
			for j in range(4):
				self.Image[k+j] = i[j+2]

		self.Refresh = True

	@service
	def update_image_data_via_file(self, image):
		with open(os.path.join(self.CanvasFolder, image)) as image_file:
			imageArray = json.load(image_file)

		for i in imageArray:
			k = pixelIndex(i[0], i[1], self.Width, self.Height)
			for j in range(4):
				self.Image[k+j] = i[j+2]

		os.remove(os.path.join(self.CanvasFolder, image))
		self.Refresh = True

	@service
	def save_image_copy(self, file_name):
		im = Image.frombuffer('RGBA', self.Size, self.flipImageArray(), 'raw', 'RGBA', 0, 1)
		im.save(os.path.join(self.CanvasFolder, os.pardir, os.pardir, os.pardir, file_name))

	@service
	def clear_canvas(self):
		self.Image = initImageArray(self.Width, self.Height)

	# Destruction squad
	@service
	def destructor(self):
		self.removeTexture()

	def finalize(self):
		self.removeTexture()


	###################################
	# Helping methods for Pen class

	def setCanvasPositionAndScale(self):
		
		if self.FullscreenMode == True:
			self.Cam['Speed']       = 0.25
			self.Cam['Sensitivity'] = 0.0
			if self.wWidth == min(self.wWidth, self.wHeight):
				scaleX = self.calculateWindowMinScale()
				scaleY = 0.394
			else:
				scaleX = 0.394
				scaleY = self.calculateWindowMinScale()
			self.Canvas.worldScale = [scaleX, scaleY, 1]
			self.PosVector = [0.0, 0.0, -0.5, 1]
			self.moveCanvasInFrontOfCamera()
			
		elif self.CanvasPosState == 0 or not self.GroundPosEnabled:
			self.Cam['Speed']       = 2.0
			self.Cam['Sensitivity'] = 0.001
			self.Canvas.setParent( self.Cam )
			w, h = self.Size
			scaleX = 0.15
			scaleY = scaleX * h/w
			self.PosVector = [self.calculateWindowXpos(), self.calculateWindowYpos(w/h), -0.5, 1]
			self.moveCanvasInFrontOfCamera()
			self.Canvas.worldScale = [scaleX, scaleY, 1]

		elif self.CanvasPosState == 1:
			self.Cam['Speed']       = 2.0
			self.Cam['Sensitivity'] = 0.001
			self.moveCanvasToGround()
			

	def moveCanvasInFrontOfCamera(self, pos = None):
		if pos == None:
			pos = self.PosVector

		point = (Matrix( self.Cam.getCameraToWorld() ) * Vector(pos))
		
		for i in range(3):
			self.Canvas.worldPosition[i] = point[i]
		self.Canvas.worldOrientation = self.Cam.worldOrientation

		try:
			self.Scene.objects["ground"].setVisible(True)
		except KeyError:
			self.GroundPosEnabled = False


	def moveCanvasToGround(self):
		try:
			groundObj = blender.objectdata("ground")
			ground    = self.Scene.objects["ground"]
		except KeyError:
			self.GroundPosEnabled = False

		self.Canvas.setParent(ground)
		self.Canvas.worldPosition    = [groundObj.dimensions.x/2, groundObj.dimensions.y/2, 0.0]
		self.Canvas.worldOrientation = ground.worldOrientation
		self.Canvas.worldScale       = [groundObj.dimensions.x/self.CanvasObj.dimensions.x, groundObj.dimensions.y/self.CanvasObj.dimensions.y, 1]
		ground.setVisible(False)

	def zoomConstraint(self):
		DZ_MIN = 0.1
		DZ_MAX = 0.5
		
		cx, cy, cz = self.Cam.worldPosition	 	# Cam position
		px, py, pz = self.Canvas.worldPosition 	# Canvas position
		dx, dy, dz = cx-px, cy-py, cz-pz		# Difference in positions

		if dz > DZ_MAX:
			self.moveCanvasInFrontOfCamera([0.0, 0.0, -DZ_MAX, 1])

		else:
			X = (0.0, 0.0, -DZ_MAX)	# Vertice position
			P = (px, py, pz)		# Canvas position
			n = (0, 0, 1)			# Canvas plane normal vector

			# Canvas dimensions
			CDim = [
				self.CanvasObj.dimensions[0]*self.Canvas.worldScale[0],
				self.CanvasObj.dimensions[1]*self.Canvas.worldScale[1]
			]
	
			# Corner-Points of Canvas plus some extension
			if self.Size[0] < self.Size[1]:
				i = 14
				j = i / 1.4142135
			else:
				j = 14
				i = j / 1.4142135
			A = ( P[0] + (CDim[0] + i/ 2),    P[1] + (CDim[1] + j/ 2),    P[2] )	# Top-Right point
			D = ( P[0] - (CDim[0] + i/ 2),    P[1] - (CDim[1] + j/ 2),    P[2] )	# Bottom-Left point
	
			# Line vectors
			ua = ( A[0]-X[0],    A[1]-X[1],    A[2]-X[2] )
			ud = ( D[0]-X[0],    D[1]-X[1],    D[2]-X[2] )
	
			# Line parameters for given z = dz
			ta = (-dz - n[0]*A[0] - n[1]*A[1] - n[2]*A[2]) / (n[0]*ua[0] + n[1]*ua[1] + n[2]*ua[2])
			td = (-dz - n[0]*D[0] - n[1]*D[1] - n[2]*D[2]) / (n[0]*ud[0] + n[1]*ud[1] + n[2]*ud[2])
	
			# Border points for given z:
			BA = ( (A[0] + ta * ua[0]),    (A[1] + ta * ua[1]) )
			BD = ( (D[0] + td * ud[0]),    (D[1] + td * ud[1]) )

			self.moveCanvasInFrontOfCamera([
				(-clamp(dx, BD[0], BA[0])),
				(-clamp(dy, BD[1], BA[1])),
				(-clamp(dz, DZ_MIN, DZ_MAX)),
				1
			])


	def refresh(self):
		if self.Living and self.Refresh:
			self.updateTexture()
			self.Refresh = False

	def calculateWindowXpos(self):
		r = self.wWidth / self.wHeight
		return ( 0.026114137034065*pow(r, 3) - 0.12129688550426*pow(r, 2) + 0.18687904105417*r + 0.14743840861709 )

	def calculateWindowYpos(self, roff):
		r = self.wWidth / self.wHeight
		pos    = ( -0.012753384591876*pow(r, 3) - 0.038611757932247*pow(r, 2) + 0.37923970492413*r - 0.55137667366681 )
		offset = self.calculateWindowYoffset(roff)
		return (pos + offset)

	def calculateWindowYoffset(self, r):
		return ( 0.03647599*pow(r, 3) - 0.056409418*pow(r, 2) - 0.126786538*r + 0.145416667 )

	def calculateWindowMinScale(self):
		r = self.wWidth / self.wHeight
		return ( 0.000534*pow(r, 6) - 0.013580*pow(r, 5) + 0.120853*pow(r, 4) - 0.533153*pow(r, 3) + 1.303820*pow(r, 2) - 1.800714*r  + 1.315912 )

	def flipImageArray(self):
		buffer = array.array('B')
    
		for y in range(self.Height):
			for x in range(self.Width):
				pixel = array.array('B', self.get_pixel(x, y))
				buffer.extend( pixel )
    
		return buffer

	def resizeTexture(self):
		logic().texture.source = texture().ImageBuff(self.Width, self.Height, 255, False)

	def createTexture(self):
		"""Create a new Dynamic Texture"""
		obj = self.Canvas
		ID = texture().materialID(obj, 'MACanvasMat') # get the reference pointer (ID) of the internal texture
		object_texture = texture().Texture(obj, ID)   # create a texture object
		logic().texture = object_texture              # the texture has to be stored in a permanent Python object
		logic().texture.source = texture().ImageBuff(self.Width, self.Height, 255, False) # create ImageBuff as source for texture
		self.Living = True                            # flag that we using dynamic texture
		self.updateTexture()                          # and load self.Image to canvas texture

	def updateTexture(self):
		"""Update the Dynamic Texture"""
		logic().texture.source.plot(self.Image, self.Width, self.Height, 0, 0) # loading image data
		logic().texture.refresh(True)	# and update/replace the texture

	def removeTexture(self):
		"""Delete the Dynamic Texture, reversing back the final to its original state."""
		try:
			del logic().texture
			self.Living = False
		except:
			pass

	def showObstacles(self):
		if self.ObstaclesState < 0:
			self.ObstaclesState = 0
			
		elif self.ObstaclesState == 0:
			self.showStaticObstacles(True)
			self.showDynamicObstacles(True)
			
		elif self.ObstaclesState == 1:
			self.showStaticObstacles(False)
			self.showDynamicObstacles(True)

		elif self.ObstaclesState == 2:
			self.showStaticObstacles(True)
			self.showDynamicObstacles(False)
			
		elif self.ObstaclesState == 3:
			self.showStaticObstacles(False)
			self.showDynamicObstacles(False)

		else: # self.ObstaclesState > 3:
			self.ObstaclesState = 3

			

	def showStaticObstacles(self, value):
		for i in range(len(self.Scene.objects)):
			obj = self.Scene.objects.get("static." + str(i).zfill(3), None)
			if obj is None:
				break
			else:
				obj.setVisible(value)

		for i in range(len(self.Scene.objects)):
			obj = self.Scene.objects.get("dock_separator." + str(i).zfill(3), None)
			if obj is None:
				break
			else:
				obj.setVisible(value)

		for i in range(len(self.Scene.objects)):
			obj = self.Scene.objects.get("border." + str(i).zfill(3), None)
			if obj is None:
				break
			else:
				obj.setVisible(value)

	def showDynamicObstacles(self, value):
		for i in range(len(self.Scene.objects)):
			obj = self.Scene.objects.get("dynamic." + str(i).zfill(3), None)
			if obj is None:
				break
			else:
				obj.setVisible(value)

		for i in range(len(self.Scene.objects)):
			obj = self.Scene.objects.get("puck." + str(i).zfill(3), None)
			if obj is None:
				break
			else:
				obj.setVisible(value)

###################################
# Non-Class methods
def initImageArray(width, height):
	buffer = array.array('B')
	for y in range(height):
		for x in range(width):
			buffer.extend([255, 255, 255, 255])
	return buffer

def generateImageArray(width, height):
	buffer = array.array('B')
	for y in range(height):
		for x in range(width):
			buffer.extend([toByte(x/width), toByte(y/height), toByte((x+y)/(width+height)), 255])
	return buffer

def toByte(value):
	return clamp(int(value * 255), 0, 255)
	
def clamp(n, smallest, largest):
	return max(smallest, min(n, largest))

def pixelIndex(x, y, width, height):
	y = height - 1 - y
	return (4 * (y * width + x))

def swap(i, j):
	tmp = i
	i = j
	j = tmp

#	def findEdgeOfScreen(self):
#		xmin, ymin, xtest, ytest = self.getMins()
#		
#		yinc = -0.0001
#			
#		if (ymin == (0, 0, 0)):
#			yinc = -yinc
#
#		y = (ytest == (0, 0, 0))
#
#		if not y:
#			self._posVector[1] = self._posVector[1] + yinc
#			self.moveCanvasInFrontOfCamera()
#
#		else:
#			os.remove(os.path.join(self._canvasFolder, "edge.jpg"))
#			self._edgeFound = True
#			print("Pen::findEdgeOfScreen(): Edge was found!")
#			print("width: " + str(render().getWindowWidth()) + ", height: " + str(render().getWindowHeight()) + ", yoff: " + str(self._posVector[1]))

#	def getMins(self):
#		path = os.path.join(self.CanvasFolder, "edge.jpg")
#
#		render().makeScreenshot(path)
#		im = Image.open(path)
#		
#		width, height = im.size
#		pix = im.load()
#		
#		xline = []
#		for i in range(0, height-1):
#			xline.append( im.getpixel((width-1, i)) )
#			
#		yline = []
#		for i in range(0, width-1):
#			yline.append( im.getpixel((i, height-1)) )
#		
#		return [min(xline), min(yline), pix[width-1, height-3], pix[width-3, height-1]]
