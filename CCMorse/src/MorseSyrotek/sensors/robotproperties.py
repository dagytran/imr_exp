import logging; logger = logging.getLogger("morse." + __name__)
import morse.core.sensor

from morse.core import status
from morse.helpers.components import add_property

#############################################
#  RobotProperties sensor for MorseSyrotek  #
#############################################

class Robotproperties(morse.core.sensor.Sensor):

	_name = "Robotproperties"
	_short_desc = "Additional robot properties."

	# Here we can add as many additional properties or configurations we want.
	# Syntax:   (Name of property, Default value, Name of property in stream, Type, Description of the property)
	# Robot geometry and configuration:
	add_property('RobotWidth',  0.161,  'RobotWidth',  "float", "Width of the robot from direction of movement. [m]")
	add_property('RobotLength', 0.174,  'RobotLength', "float", "Length of the robot. [m]")
	add_property('RobotRadius', 0.1,    'RobotRadius', "float", "Radius of the robot. [m]")
	add_property('RobotColor',  "blue", 'RobotColor',  "str",   "Color of the robot.")
	add_property('LaserRssi',	False,  'LaserRssi',   "bool",  "True if Laser Scanner have remission_list.")
	add_property('LaserFreq',   10.0,   'LaserFreq',   "float", "Scanning frequency of laser scanner. [Hz]")
	add_property('RobotMass',   2.0,    'RobotMass',   "float", "Mass of the robot. [kg]")
	add_property('MaxSpeed', 	0.5,    'MaxSpeed',    "float", "Maximal speed of the robot. [m/s]")
	add_property('MaxTurnRate', 1.0,    'MaxTurnRate', "float", "Maximal turning rate of the robot. [rad/s]")
	add_property('WheelsDist',  0.153,  'WheelsDist',  "float", "Distance of the main wheels. [m]")
	# Driver configuration:                  
	add_property('Driver', 		  	"waypoint", 'Driver',          "str",   "Prefered type of driver for CCMorse::Position2dProxy::GoTo() function.")
	add_property('MinGapWidth',     0.2,        'MinGapWidth',     "float", "Minimal width of gap that robot can go thru.")
	add_property('SafetyDistMax',   0.4,        'SafetyDistMax',   "float", "Maximal avoiding distance from obstacle.")
	add_property('GoalPositionTol', 0.05,       'GoalPositionTol', "float", "Maximum distance allowed from the final goal for the algorithm to stop.")
	add_property('GoalAngleTol',    0.5335,     'GoalAngleTol',    "float", "Maximum angular error from the final goal position for the algorithm to stop.")
	# Robot provides proxies:                        
	add_property('RbtPrvWindow',         "",    'RbtPrvWindow',        "str", "Array of WindowProxy indexes that this robot provides.")
	add_property('RbtPrvLaser',          "[0]", 'RbtPrvLaser',         "str", "Array of LaserProxy indexes that this robot provides. : [0], [0, 1, 2]")
	add_property('RbtPrvGblPosition2d',  "[0]", 'RbtPrvGblPosition2d', "str", "Array of global Position2dProxy indexes that this robot provides. : [0], [0, 1, 2]")
	add_property('RbtPrvLclPosition2d',  "",    'RbtPrvLclPosition2d', "str", "Array of local Position2dProxy indexes that this robot provides. : [0], [0, 1, 2]")
	# Driver provides and requires proxies:
	add_property('DrvPrvPosition2d',       "[1]", 'DrvPrvPosition2d',       "str", "Array of Position2dProxy indexes that this driver provides.")
	add_property('DrvReqInputPosition2d',  "[0]", 'DrvReqInputPosition2d',  "str", "Array of Position2dProxy indexes that this driver requires as inputs." )
	add_property('DrvReqOutputPosition2d', "[0]", 'DrvReqOutputPosition2d', "str", "Array of Position2dProxy indexes that this driver requires as outputs." )
	add_property('DrvPrvLaser',            "",    'DrvPrvLaser',            "str", "Array of LaserProxy indexes that this driver provides.")
	add_property('DrvReqInputLaser',       "[0]", 'DrvReqInputLaser',       "str", "Array of LaserProxy indexes that this driver requires as inputs.")
	add_property('DrvReqOutputLaser',      "",    'DrvReqOutputLaser',      "str", "Array of LaserProxy indexes that this driver requires as outputs.")
	
	def __init__(self, obj, parent = None):						# Constructor of this "sensor".
		logger.info("%s initialization" % obj.name)
		morse.core.sensor.Sensor.__init__(self, obj, parent)	# Call the constructor of the parent class...
		logger.info('Component initialized')

	def default_action(self):  # Created RobotProperties as sensor, so this method must be implemented.
		pass	



