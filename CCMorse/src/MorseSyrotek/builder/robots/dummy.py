from morse.builder import *

from MorseSyrotek.builder.actuators import Pen
from MorseSyrotek.builder.sensors import Robotproperties

class Dummy(GroundRobot):

	def __init__(self, configuration, name = None, debug = True):
		
		GroundRobot.__init__(self, 'MorseSyrotek/robots/dummy.blend', name)
		self.properties(classpath = "MorseSyrotek.robots.dummy.Dummy")

		self.set_rigid_body()
		self.set_collision_bounds()
	
		self.config = Robotproperties()
		self.config.add_interface('socket')
		self.config.translate(0.0, 0.0, 0.0)
		self.config.frequency(0.001)
		self.config.properties(
			RbtPrvGblPosition2d    = "",    # Array of global Position2dProxy indexes that this robot provides. Syntax: [0], [0, 1, 2]
			RbtPrvLclPosition2d    = "",    # Array of local Position2dProxy indexes that this robot provides. Syntax: [0], [0, 1, 2]
			RbtPrvLaser 		   = "",    # Array of LaserProxy indexes that this robot provides. Syntax: [0], [0, 1, 2]
			RbtPrvWindow           = "[0]", # Array of WindowProxy indexes that this robot provides
			DrvPrvPosition2d 	   = "",    # Array of Position2dProxy indexes that this driver provides.
			DrvReqInputPosition2d  = "",    # Array of Position2dProxy indexes that this driver requires as inputs.
			DrvReqOutputPosition2d = "",    # Array of Position2dProxy indexes that this driver requires as outputs.
			DrvPrvLaser            = "",    # Array of LaserProxy indexes that this driver provides.
			DrvReqInputLaser       = "",    # Array of LaserProxy indexes that this driver requires as inputs.
			DrvReqOutputLaser      = ""	    # Array of LaserProxy indexes that this driver requires as outputs.
		)
		self.append(self.config)

		self.pen = Pen()
		self.pen.add_interface('socket')
		self.pen.properties(
			CanvasFolder   = configuration["canvas-folder"],
			FullscreenMode = True,
			CanvasWidth    = configuration["width"],
			CanvasHeight   = configuration["height"],
			CellSize       = configuration["cell-size"]
		)
		self.append(self.pen)



