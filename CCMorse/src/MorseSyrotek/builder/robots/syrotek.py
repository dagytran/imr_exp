from morse.builder import *
from math import pi
from MorseSyrotek.builder.sensors import Robotproperties

from morse.core.blenderapi import bpy

###################################
# 	SyRoTek Robot                 #
###################################

class Syrotek(GroundRobot):
	"""
		Builder script for SyRoTek robot model with a motion controller,
		a pose sensor, odometry and hokuyo laser scanner.
		Also with options to be controlled by keyboard and using MapWindow.
	"""

	_name = 'SyRoTek robot'
	
	def __init__(self, robot_configuration, map_configuration, debug = False):

		# syrotek.blend is located in the data/robots directory
		GroundRobot.__init__(self, 'MorseSyrotek/robots/syrotek.blend', robot_configuration["name"])
		self.properties(
			classpath = "MorseSyrotek.robots.syrotek.Syrotek") # maybe in the future,
		#	HasSuspension = False,
		#	HasSteering = False,
		#	Influence = 0.1,
		#	Friction = 0.8,
		#	WheelFLName = "Wheel_L",
		#	WheelFRName = "Wheel_R",
		#	WheelRLName = "None",
		#	WheelRRName = "None",
		#	CasterWheelName = "CasterWheel",
		#	FixTurningSpeed = 0.52
		#)

		self.set_rigid_body()
		self.set_collision_bounds()

		# Getting robot properties and configurations from Blender scene.
		robotModel = bpy.data.objects[robot_configuration["name"]]

		# Setting robot geometry information.
		length 		= robotModel.dimensions[0]
		width  		= robotModel.dimensions[1]
		mass   		= robotModel.game.mass
		wheels_dist = (bpy.data.objects["Wheel_L"].location[1] - bpy.data.objects["Wheel_R"].location[1])

		# Setting robot color.
		set_robot_body_color(robotModel, robot_configuration["color"])		

		# If there is no robot radius information, get it from Blender model and use to calculate default driver configuration.
		if robot_configuration["Driver"]["robot_radius"] is None:
			robot_configuration["Driver"]["robot_radius"] = robotModel.game.radius

		if robot_configuration["Driver"]["min_gap_width"] is None:
			robot_configuration["Driver"]["min_gap_width"] = (2 * robot_configuration["Driver"]["robot_radius"])

		if robot_configuration["Driver"]["obstacle_avoid_dist"] is None:
			robot_configuration["Driver"]["obstacle_avoid_dist"] = (4 * robot_configuration["Driver"]["robot_radius"])

		if robot_configuration["Driver"]["goal_pos_tol"] is None:
			robot_configuration["Driver"]["goal_pos_tol"] = (robot_configuration["Driver"]["robot_radius"] / 2)

		# Converting lists to strings
		provides_global_position2d     = '[' + ', '.join(str(x) for x in robot_configuration["Provides"]["position2d"]["global"]) + ']'
		provides_local_position2d      = '[' + ', '.join(str(x) for x in robot_configuration["Provides"]["position2d"]["local"]) + ']'
		provides_laser                 = '[' + ', '.join(str(x) for x in robot_configuration["Provides"]["laser"]) + ']'
		provides_window                = '[' + ', '.join(str(x) for x in robot_configuration["Provides"]["window"]) + ']'
		driver_provides_position2d     = '[' + ', '.join(str(x) for x in robot_configuration["Driver"]["Provides"]["position2d"]) + ']'
		driver_provides_laser          = '[' + ', '.join(str(x) for x in robot_configuration["Driver"]["Provides"]["laser"]) + ']'
		driver_requires_position2d_in  = '[' + ', '.join(str(x) for x in robot_configuration["Driver"]["Requires"]["position2d"]["input"]) + ']'
		driver_requires_position2d_out = '[' + ', '.join(str(x) for x in robot_configuration["Driver"]["Requires"]["position2d"]["output"]) + ']'
		driver_requires_laser_in       = '[' + ', '.join(str(x) for x in robot_configuration["Driver"]["Requires"]["laser"]["input"]) + ']'
		driver_requires_laser_out      = '[' + ', '.join(str(x) for x in robot_configuration["Driver"]["Requires"]["laser"]["output"]) + ']'

		#######################
		# Robot properties
		self.config = Robotproperties()
		self.config.translate(0.0, 0.0, -0.065)
		self.config.frequency(0.001)

		self.config.properties(
			RobotWidth  = width,       # Width of the robot from direction of movement. [m]
			RobotLength = length,      # Length of the robot. [m]
			RobotMass   = mass,        # Mass of the robot. [kg]
			WheelsDist  = wheels_dist, # Distance of the main wheels. [m]

			RobotColor = robot_configuration["color"],           # Color of the robot.
			LaserRssi  = robot_configuration["laser-remission"], # True if Laser Scanner have remission_list.
			LaserFreq  = robot_configuration["laser-frequency"], # Scanning frequency of laser scanner. [Hz]
			
			RobotRadius 	= robot_configuration["Driver"]["robot_radius"],        # Radius of the robot. [m]
			MinGapWidth 	= robot_configuration["Driver"]["min_gap_width"],       # Minimal width of gap that robot can go thru.
			SafetyDistMax 	= robot_configuration["Driver"]["obstacle_avoid_dist"],	# Maximal avoiding distance from obstacle.
			MaxSpeed 		= robot_configuration["Driver"]["max_speed"],           # Maximal speed of the robot. (v_max[m/s])
			MaxTurnRate 	= robot_configuration["Driver"]["max_turn_rate"],       # Maximal turning rate of the robot. (w_max[rad/s])
			Driver 			= robot_configuration["Driver"]["name"],                # Prefered type of driver for CCMorse::Position2dProxy::GoTo() function. You can choose from: {"waypoint", "snd"}
			GoalPositionTol = robot_configuration["Driver"]["goal_pos_tol"],        # Maximum distance allowed from the final goal for the algorithm to stop. [m]
			GoalAngleTol 	= robot_configuration["Driver"]["goal_angl_tol"],       # Maximum angular error from the final goal position for the algorithm to stop. [rad]

			RbtPrvGblPosition2d    = provides_global_position2d,     # Array of global Position2dProxy indexes that this robot provides. Syntax: [0], [0, 1, 2]
			RbtPrvLclPosition2d    = provides_local_position2d,      # Array of local Position2dProxy indexes that this robot provides. Syntax: [0], [0, 1, 2]
			RbtPrvLaser 		   = provides_laser,                 # Array of LaserProxy indexes that this robot provides. Syntax: [0], [0, 1, 2]
			RbtPrvWindow           = provides_window,                # Array of WindowProxy indexes that this robot provides
			DrvPrvPosition2d 	   = driver_provides_position2d,     # Array of Position2dProxy indexes that this driver provides.
			DrvReqInputPosition2d  = driver_requires_position2d_in,  # Array of Position2dProxy indexes that this driver requires as inputs.
			DrvReqOutputPosition2d = driver_requires_position2d_out, # Array of Position2dProxy indexes that this driver requires as outputs.
			DrvPrvLaser            = driver_provides_laser,          # Array of LaserProxy indexes that this driver provides.
			DrvReqInputLaser       = driver_requires_laser_in,       # Array of LaserProxy indexes that this driver requires as inputs.
			DrvReqOutputLaser      = driver_requires_laser_out	     # Array of LaserProxy indexes that this driver requires as outputs.
		)
		self.append(self.config)
	

		###################################
		# Actuators
		###################################

		#######################
		# (v,w) motion controller
		#self.motionvw = MotionVWDiff()
		self.motionvw = MotionVW()
		self.motionvw.frequency(robot_configuration["motionvw-frequency"])
		self.append(self.motionvw)

		#######################
		# Waypoint controller
		if robot_configuration["Driver"]["name"] == "waypoint":
			self.waypoint = Waypoint()
			self.waypoint.frequency(robot_configuration["waypoint-frequency"])
			self.append(self.waypoint)

		#######################
		# Canvas controller
		if (map_configuration["enabled"] == True) or (map_configuration["hide-obstacles-enabled"] == True):
			from MorseSyrotek.builder.actuators import Pen
			
			self.pen = Pen()
			
			if (map_configuration["enabled"] == True):
				self.pen.properties(
					CanvasFolder          = map_configuration["canvas-folder"],
					FullscreenMode        = map_configuration["fullscreen"],
					CanvasWidth           = map_configuration["width"],
					CanvasHeight          = map_configuration["height"],
					CellSize              = map_configuration["cell-size"],
					GroundPositionEnabled = map_configuration["ground-canvas-enabled"],
					HideObstaclesEnabled  = map_configuration["hide-obstacles-enabled"],
					InitPositionOnGround  = map_configuration["init-position-on-ground"]
				)
			else:
				self.pen.properties(HideObstaclesEnabled  = map_configuration["hide-obstacles-enabled"])
			
			self.pen.frequency(robot_configuration["pen-frequency"])
			self.append(self.pen)

		#######################
		# Optionally allow to move the robot with the keyboard
		if debug is True:
			keyboard = Keyboard()
			keyboard.properties(ControlType = 'Position')
			self.append(keyboard)
		

		###################################
		# Sensors
		###################################

		#######################
		# Position sensor
		self.pose = Pose()
		self.pose.translate(0.0, 0.0, -0.065)
		self.pose.frequency(robot_configuration["pose-frequency"])
		self.append(self.pose)

		#######################
		# Odometry
		self.odometry = Odometry()
		self.odometry.translate(0.0, 0.0, 0.0)
		self.odometry.level("differential")
		self.odometry.frequency(robot_configuration["odometry-frequency"])
		self.append(self.odometry)
		
		#######################
		# Hokuyo laser scanner (range_max: 5.0, fov: 270.0, samples: 682)
		hokuyo = Hokuyo()  
		hokuyo.translate(0.035, 0.0, 0.0)
		hokuyo.rotate(0.0, 0.0, 0.0)
    
		# Setting level of sensoring for Hokuyo laser scanner.
		if robot_configuration["laser-remission"] == True:
			hokuyo.level("rssi")
		else:
			hokuyo.level("raw")
    
		# The refresh frequency of the sensor. [Hz] Default: 10Hz
		hokuyo.frequency(robot_configuration["laser-frequency"])
		
		hokuyo.properties(
			Visible_arc = robot_configuration["laser-arc"], # If the laser arc should be displayed during the simulation.
			resolution = 0.39589,   # The angle between each laser in the sensor. Expressed in degrees in decimal format. (i. e.), half a degree is expressed as 0.5. Used when creating the arc object.
			scan_window = 270,      # The full angle covered by the sensor. Expressed in degrees in decimal format. Used when creating the arc object.
			laser_range = 5.0,      # The distance in meters from the center of the sensor to which it is capable of detecting other objects.
		#	layers = 1,             # Number of scanning planes used by the sensor.
		#	layer_separation = 0.8, # The angular distance between the planes, in degrees.
			layer_offset = 0.125    # The horizontal distance between the scan points in consecutive scanning layers. Must be given in degrees.
		)
		self.append(hokuyo)


###
#  Custom methods for Syrotek constructor.
###
def get_children(obj):
	return [obj_child for obj_child in bpy.data.objects if obj_child.parent == obj]

def get_robot_body(robot):
	children = get_children(robot)
	for child in children:
		if child.name.startswith("Body"):
			return child

def set_material(obj, mat):
	if obj.data.materials:
		obj.data.materials[0] = mat
	else:
		obj.data.materials.append(mat)

def make_material(name, rgb):
	mat = bpy.data.materials.new(name)
	mat.diffuse_color = rgb
	mat.diffuse_shader = 'LAMBERT'
	mat.diffuse_intensity = 0.75
	mat.specular_color = (1.0, 1.0, 1.0)
	mat.specular_shader = 'COOKTORR'
	mat.specular_intensity = 0.6
	mat.alpha = 1
	mat.ambient = 1
	return mat

def set_robot_body_color(robot, color):
	if   color in ('blue', 'Blue', 'b', 'B'):
		mat = make_material('Blue', (0.00, 0.15, 0.75))
		
	elif color in ('red', 'Red', 'r', 'R'):
		mat = make_material('Red', (0.75, 0.00 ,0.00))
		
	elif color in ('yellow', 'Yellow', 'y', 'Y'):
		mat = make_material('Yellow', (0.90, 0.65, 0.00))
		
	else:
		mat = make_material('DarkBlue', (0.00, 0.01, 0.10))
		
	set_material(get_robot_body(robot), mat)
