from morse.builder.creator import SensorCreator
from morse.builder.blenderobjects import Cube

class Robotproperties(SensorCreator):
    _classpath = "MorseSyrotek.sensors.robotproperties.Robotproperties"
    _blendname = "Robotproperties"

    def __init__(self, name = None):
        SensorCreator.__init__(self, name)
        mesh = Cube("PropertiesCube")
        mesh.scale = (0.01, 0.01, 0.01)
        mesh.color(0.8, 0.8, 0.8)
        self.append(mesh)
