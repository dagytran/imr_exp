from morse.builder import *
from morse.builder import bpymorse

class Canvas(PassiveObject):
	"""
		Class for setup canvas for MorseSyrotek.
	"""

	def __init__(self, filename, alpha = 1, prefix = None, keep_pose = False):
		PassiveObject.__init__(self, filename, prefix, keep_pose)

		mat = bpymorse.get_material("CanvasMat")
		if alpha >= 1.0:
			mat.use_transparency = False
		else:
			mat.use_transparency = True
			mat.alpha = alpha

