from morse.builder.creator import ActuatorCreator

class Pen(ActuatorCreator):
    _classpath = "MorseSyrotek.actuators.pen.Pen"
    _blendname = "pen"

    def __init__(self, name=None):
        ActuatorCreator.__init__(self, name)

