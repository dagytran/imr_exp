/*
* File name: exploration.cc
* Date:      2009/07/08 08:42
* Author:    Miroslav Kulich, Jan Faigl
*/

#include <queue>
#include <cmath>
#include <limits>
#include <fstream>
#include <vector>


#include "exploration.h"
#include "logging.h"
#include "timerN.h"
#include "imr-h/timerN.h"
#include "planner.h"
#include "robot.h"
#include "plan.h"

using namespace imr::robot;
/// ----------------------------------------------------------------------------
/// Class CExploration


/// - static method ------------------------------------------------------------
imr::CConfig &CExploration::getConfig(imr::CConfig &config) {
	config.add<int>("period", "period of exploration, frontier and path planning, in ms", 1000);
	return config;
}

/// ----------------------------------------------------------------------------
CExploration::CExploration(imr::CConfig &cfg, CMapGrid &gridMap, CRobot *robot) : ThreadBase(),
																				  cfg(cfg),
																				  robot(robot),
																				  gridMap(gridMap),
																				  quit(false) {
}

/// ----------------------------------------------------------------------------
CExploration::~CExploration() {
	stop();
}


/// ----------------------------------------------------------------------------
void CExploration::stop(void) {
	ScopedLock lk(mtx);
	quit = true;
}

void CExploration::setMapHolder(imr::robot::MapHolder &mapHolder) {
	this->mapHolder = &mapHolder;
}

/// - protected method ---------------------------------------------------------
void CExploration::threadBody(void) {
	bool q = quit;
	const long period = cfg.get<int>("period");
//  DEBUG("period " << period);

	imr::CConfig plannerCfg;
	imr::CPlanner::getConfig(plannerCfg);
	imr::CPlanner planner(plannerCfg);
	planner.setMap(gridMap);
	planner.setMapHolder(*mapHolder);

	while (!q) {
		imr::CTimerN::sleep(period);
		// add your code
		// CMapGrid newMap (gridMap);
		DEBUG("EXPL: GET FRONTIER MAP");
		std::vector<std::vector<bool> > frontierMap = getFrontierMap();
		DEBUG("EXPL: GET FRONTIER MAP2");

		// dijkstra k prvnímu frontieru + cesta
		DEBUG("SET PATH");
		std::vector<int> newPath = planner.getPath(robot->getX(), robot->getY(), frontierMap);

		for (int path : newPath) {
			DEBUG("Path index: " + path);
		}
		// TODO INSERT PRINT HERE Print path

		plan.setPath(newPath);
		DEBUG("AFTER SET PATH");
		// Převést path na x a y a to pak převést na reálný x a y
		// vytvořit nějakej getter, kterej bude robot volat
		ScopedLock lk(mtx);
		q = quit;
	}
}

std::vector<std::vector<bool> > CExploration::getFrontierMap() {

	std::vector<std::vector<bool> > frontierMap;
	frontierMap.resize(gridMap.getWidth());
	for (int m = 0; m < gridMap.getWidth(); ++m) {
		frontierMap[m].resize(gridMap.getHeight(), false);
	}

	// najit frontiery
	for (int i = 1; i < gridMap.getWidth() - 1; ++i) {
		for (int j = 1; j < gridMap.getHeight() - 1; ++j) {
			if (mapHolder->getLaserMap()->getCell(i, j) < 0.4) {
				for (int k = -1; k <= 1; ++k) {
					for (int l = -1; l <= 1; ++l) {
						if (mapHolder->getLaserMap()->getCell(i + k, j + l) > 0.4
							&& mapHolder->getLaserMap()->getCell(i + k, j + l) < 0.6) {
							frontierMap[i][j] = true;
						}
					}
				}
			}
			if(frontierMap[i][j]) {
				DEBUG(i << " " << j);
			}
			mapHolder->updateCellFrontierMap(i, j, frontierMap[i][j]);
		}
	}

	mapHolder->flush();

	return frontierMap;
}

void CExploration::setPlan(imr::CPlan &newPlan) {
	plan = newPlan;
}
/* end of exploration.cc */
